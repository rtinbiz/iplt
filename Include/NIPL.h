#pragma once

#include <vector>
#include <xstring>
using namespace std;

#include "NIPLParam.h"
#include "NIPLResult.h"

enum NIPL_ERR {
	NIPL_ERR_NONE = -1,
	NIPL_ERR_SUCCESS = 0,
	NIPL_ERR_FAIL = 1,
	NIPL_ERR_PASS = 2,
	NIPL_ERR_OUT_OF_MEMORY,

	NIPL_ERR_FAIL_TO_OPEN_FILE = 100,
	NIPL_ERR_FAIL_TO_SAVE_FILE,
	NIPL_ERR_FAIL_TO_LOAD_IMAGE,
	NIPL_ERR_FAIL_TO_SAVE_IMAGE,
	NIPL_ERR_FAIL_TO_CONVERT_IMAGE,
	NIPL_ERR_FAIL_INVALID_IMAGE,
	NIPL_ERR_FAIL_INVALID_PARAM_EXPRESSION,
	NIPL_ERR_FAIL_INVALID_DATA,
	NIPL_ERR_FAIL_EMPTY_IMAGE,
	NIPL_ERR_FAIL_NOT_SUPPORT_PROCESS,
	NIPL_ERR_FAIL_NOT_MATCH_IMAGE_SIZE,
	NIPL_ERR_FAIL_NOT_MATCH_MASK_SIZE,

	NIPL_ERR_NO_INPUT = 200,
	NIPL_ERR_NO_OUTPUT,
	NIPL_ERR_NO_PARAM,
	NIPL_ERR_NO_PARAMS,

	NIPL_ERR_INVALID_IMAGE_TYPE = 300,
	NIPL_ERR_INVALID_PARAM_VALUE,
};

#define NIPL_SUCCESS(nErr) (nErr == NIPL_ERR_SUCCESS)
#define NIPL_PASS(nErr) (nErr == NIPL_ERR_PASS)
#define NIPL_FAIL(nErr) (!NIPL_SUCCESS(nErr) && !NIPL_PASS(nErr))
#define VERIFY_PARAMS() if(pInput == 0x00 || pOutput == 0x00 || pInput->m_pParam == 0x00) return NIPL_ERR_NO_PARAMS;
#define CHECK_EMPTY_IMAGE(dImg) (dImg.data == 0x00)
#define CHECK_OPTION_SHOW_IMAGE() (m_pOption != 0x00 && m_pOption->m_bShowImage)
#define CHECK_EXCEPTION(Func) \
	try { Func; } \
	catch (Exception e) { \
		ShowErrorDesc(e.func.c_str(), e.err.c_str()); \
		return NIPL_ERR_INVALID_PARAM_VALUE; \
	}

#define NIPL_METHOD_DECL(FuncName) NIPL_ERR FuncName(NIPLInput *pInput, NIPLOutput *pOutput)
#define NIPL_METHOD_IMPL(FuncName) NIPL_ERR NIPL::FuncName(NIPLInput *pInput, NIPLOutput *pOutput)
#define VERIFY_MASK(dImg, dMask) \
	if (!CHECK_EMPTY_IMAGE(dMask) && dImg.size == dMask.size) { \
		if (dImg.channels() != dMask.channels()) { \
			if (dImg.channels() == 1) { \
				cvtColor(dMask, dMask, CV_BGR2GRAY); \
			} \
			else { \
				cvtColor(dMask, dMask, CV_GRAY2BGR); \
			} \
		} \
	} \
	else { \
		dMask = Mat::zeros(dImg.rows, dImg.cols, dImg.type()); \
	}

inline int wstricmp(wstring str1, wstring str2) { return _wcsicmp(str1.c_str(), str2.c_str()); }
#define CHECK_STRING(str1, str2) (wstricmp(str1, str2) == 0)

struct AFX_EXT_CLASS NIPLInput
{
	NIPLParam *m_pParam;

	Mat m_dImg;
	Mat m_dMask;
	Mat m_dTemplateImg;

	string m_strImgPath;
	string m_strTemplateImgPath;

	NIPLInput() {
		Init();
	}

	~NIPLInput() {
		Clear();
	}

	void Init()
	{
		m_pParam = nullptr;
	}

	void Clear() {
		// don't need to delete m_pParam because it's responsibility of user
		m_dImg.release();
		m_dTemplateImg.release();
		m_dMask.release();
		m_strImgPath.clear();
		m_strTemplateImgPath.clear();

		Init();
	}
};

struct AFX_EXT_CLASS NIPLOutput
{
	NIPLResult *m_pResult;

	Mat m_dImg;
//	Mat m_dMask;

	NIPLOutput() {
		Init();
	}

	~NIPLOutput() {
		Clear();
	}

	void Init()
	{
		m_pResult = nullptr;
	}

	void Clear(bool bClearAll = false) {
		if (bClearAll) {
			ClearResult();
		}

		m_dImg.release();
//		m_dMask.release();

		Init();
	}

	NIPLOutput &operator=(const NIPLOutput &dProcess) {
		Clear();

		m_dImg = dProcess.m_dImg;
		m_pResult = dProcess.m_pResult;

		return *this;
	}

	void ClearResult() {
		if (m_pResult) {
			delete m_pResult;
			m_pResult = nullptr;
		}
	}

	bool IsResultType(wstring strType) {
		if (m_pResult == nullptr) {
			return false;
		}

		return m_pResult->IsType(strType);
	}
};

struct AFX_EXT_CLASS NIPLOption
{
	BOOL m_bUseGPU;
	BOOL m_bShowImage;

	NIPLOption() {
		Init();
	}

	~NIPLOption() {
		Clear();
	}

	void Init() {
		m_bUseGPU = FALSE;
		m_bShowImage = FALSE;
	}

	void Clear() {
		Init();
	}
};

class AFX_EXT_CLASS NIPL {
public :
	static NIPL *pThis;
	static NIPL *GetInstance(BOOL bNew = FALSE);
	static void ReleaseInstance(NIPL *pThat = 0x00);

	NIPL();
	virtual ~NIPL();

	void SetOption(NIPLOption *pOption);

	NIPL_ERR LoadImage(wstring strPath, int nFlags, Mat &dImg);
	NIPL_ERR LoadImage(string strPath, int nFlags, Mat &dImg);
	NIPL_ERR SaveImage(wstring strPath, Mat dImg);
	NIPL_ERR SaveImage(string strPath, Mat dImg);

	void DebugPrintImageProperty(wstring strTitle, Mat dImg);
	void DebugPrintImageProperty(string strTitle, Mat dImg);

	void ShowErrorDesc(wstring strFunc, wstring strMessage);
	void ShowErrorDesc(string strFunc, string strMessage);

	NIPL_METHOD_DECL(CreateImage);
	NIPL_METHOD_DECL(Color2Gray);
	NIPL_METHOD_DECL(Invert);
	NIPL_METHOD_DECL(Operate);
	NIPL_METHOD_DECL(CopySubImageFrom);
	NIPL_METHOD_DECL(CopySubImageTo);
	NIPL_METHOD_DECL(SetCircleROI);
	NIPL_METHOD_DECL(Thresholding);
	NIPL_METHOD_DECL(ColorThresholding);
	NIPL_METHOD_DECL(Smoothing);
	NIPL_METHOD_DECL(EdgeDetecting);

protected :
	NIPLOption *m_pOption;

private :
	void ColorThresholding_SetThreshold(int nMethod, float nMinValue, float nMaxVlue, int &nMin, int &nMax);
};
