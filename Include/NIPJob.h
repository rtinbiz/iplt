#pragma once

class AFX_EXT_CLASS NIPJobParam
{
public :
	enum Type {
		MPT_NONE = 0,
		MPT_INT,
		MPT_FLOAT,
		MPT_BOOL,
		MPT_STRING,
		MPT_COMBO
	};

	wstring m_strName;
	wstring m_strPath;
	wstring m_strValue;
	wstring m_strType;
	wstring m_strMin;
	wstring m_strMax;
	wstring m_strDesc;			
	Type m_nType;
	vector<wstring> m_listComboValue;

	bool m_bGroup;
	vector<NIPJobParam *> m_listParam;

public :
	NIPJobParam(NIPJobParam *pParam, wstring strPathParent = L"") {
		Init();

		m_strName = pParam->m_strName;
		SetPath(strPathParent);

		CopyWithPath(pParam, m_strPath);
	}

	NIPJobParam(wstring strName, wstring strValue, wstring strType, wstring strPathParent = L"");
	NIPJobParam(wstring strGroupName, wstring strPathParent = L"") {
		Init();

		m_strName = strGroupName;
		SetPath(strPathParent);

		m_bGroup = true;
	}

	virtual ~NIPJobParam() {
		Clear();
	};

	void Init() {
		m_strName = L"";
		m_strPath = L"";
		m_strValue = L"";
		m_strType = L"";
		m_strMin = L"";
		m_strMax = L"";
		m_strDesc = L"";
		m_nType = MPT_NONE;
	}

	void Clear() {
		m_listComboValue.clear();

		for (auto pParam : m_listParam) {
			delete pParam;
		}
		m_listParam.clear();

		Init();
	}

	void SetPath(wstring strParent)
	{
		m_strPath = strParent + L"/" + m_strName;
	}

	NIPJobParam &operator=(const NIPJobParam &dParam) {
		Clear();

		m_strName = dParam.m_strName;
		m_strPath = dParam.m_strPath;
		m_strValue = dParam.m_strValue;
		m_strType = dParam.m_strType;
		m_strMin= dParam.m_strMin;
		m_strMax = dParam.m_strMax;
		m_strDesc = dParam.m_strDesc;
		m_nType = dParam.m_nType;

		for (auto strComboValue : dParam.m_listComboValue) {
			AddComboValue(strComboValue);
		}

		m_bGroup = dParam.m_bGroup;
		for (auto pParam : dParam.m_listParam) {
			AddParam(pParam);
		}

		return *this;
	}

	void CopyWithPath(NIPJobParam *pParam, wstring strPath) {
		Clear();

		m_strName = pParam->m_strName;
		m_strPath = strPath;

		m_strValue = pParam->m_strValue;
		m_strType = pParam->m_strType;
		m_strMin = pParam->m_strMin;
		m_strMax = pParam->m_strMax;
		m_strDesc = pParam->m_strDesc;
		m_nType = pParam->m_nType;

		for (auto strComboValue : pParam->m_listComboValue) {
			AddComboValue(strComboValue);
		}

		m_bGroup = pParam->m_bGroup;
		for (auto pParam : pParam->m_listParam) {
			AddParam(pParam);
		}
	}

	NIPJobParam *AddParamGroup(wstring strGroup) {
		NIPJobParam *pGroup = new NIPJobParam(strGroup, m_strPath);
		if (pGroup) {
			m_listParam.push_back(pGroup);
		}

		return pGroup;
	}

	NIPJobParam *AddParam(wstring strName, wstring strValue, wstring strType) {
		NIPJobParam *pParam = new NIPJobParam(strName, strValue, strType, m_strPath);
		if (pParam) {
			m_listParam.push_back(pParam);
		}

		return pParam;
	}

	NIPJobParam *AddParam(NIPJobParam *pParam) {
		NIPJobParam *pNewParam = new NIPJobParam(pParam, m_strPath);
		if (pNewParam) {
			m_listParam.push_back(pNewParam);
		}

		return pNewParam;
	}
	
	void AddComboValue(wstring strValue) {
		m_listComboValue.push_back(strValue);
	}

	wstring GetDesc();

	NIPJobParam *FindParam(wstring strName) {
		if (m_bGroup) {
			NIPJobParam *pFound = nullptr;
			for (auto pParam : m_listParam) {
				pFound = pParam->FindParam(strName);
				if (pFound) {
					return pFound;
				}
			}
		}
		else {
			// if the name to find equals to name or the path contains it
			if (m_strName == strName) {
				return this;
			}

			// if the name contains '/', check path 
			if (strName.find('/') != wstring::npos && m_strPath.find(strName) != wstring::npos) {
				return this;
			}
		}

		return nullptr;
	}
};

class AFX_EXT_CLASS NIPJobProcess
{
public:
	wstring m_strName;
	wstring m_strPath;
	bool m_bEnable;

	bool m_bSubProcess;
	vector<NIPJobProcess *> m_listSubProcess;

	vector<NIPJobParam *> m_listParam;

	NIPJobProcess(NIPJobProcess *pProcess = nullptr, bool bSubProcess = false, wstring strPathParent = L"") {
		Init();

		if (pProcess) {
			m_strName = pProcess->m_strName;
			SetPath(strPathParent);

			CopyWithPath(pProcess, m_strPath);
		}

		m_bSubProcess = bSubProcess;
	}

	NIPJobProcess(wstring strName, bool bSubProcess = false, wstring strPathParent = L"") {
		Init();

		m_strName = strName;
		m_bSubProcess = bSubProcess;
		m_bEnable = true;

		SetPath(strPathParent);
	}

	virtual ~NIPJobProcess() {
		Clear();
	};

	void Init() {
		m_strName = L"";
		m_strPath = L"";
		m_bEnable = false;
		m_bSubProcess = false;
	}

	void Clear(bool bClearOnly = false) {
		m_bSubProcess = false;
		for (auto pSubProcess : m_listSubProcess) {
			delete pSubProcess;
		}
		m_listSubProcess.clear();

		for (auto pParam : m_listParam) {
			delete pParam;
		}
		m_listParam.clear();

		if (!bClearOnly) {
			Init();
		}
	}

	void SetPath(wstring strParent)
	{
		m_strPath = strParent + L"/" + m_strName;
	}

	NIPJobProcess &operator=(const NIPJobProcess &dProcess) {
		Clear();

		m_strName = dProcess.m_strName;
		m_strPath = dProcess.m_strPath;
		m_bEnable = dProcess.m_bEnable;
		m_bSubProcess = dProcess.m_bSubProcess;

		for (auto pParam : dProcess.m_listParam) {
			AddParam(pParam);
		}

		for (auto pSubProcess : dProcess.m_listSubProcess) {
			AddSubProcess(pSubProcess);
		}

		return *this;
	}

	void CopyWithPath(NIPJobProcess *pProcess, wstring strPath) {
		m_strName = pProcess->m_strName;
		m_strPath = strPath;
		m_bEnable = pProcess->m_bEnable;
		m_bSubProcess = pProcess->m_bSubProcess;

		for (auto pParam : pProcess->m_listParam) {
			AddParam(pParam);
		}

		for (auto pSubProcess : pProcess->m_listSubProcess) {
			AddSubProcess(pSubProcess);
		}
	}

	NIPJobParam *AddParamGroup(wstring strGroup) {
		NIPJobParam *pGroup = new NIPJobParam(strGroup, m_strPath);
		if (pGroup) {
			m_listParam.push_back(pGroup);
		}

		return pGroup;
	}

	NIPJobParam *AddParam(wstring strName, wstring strValue, wstring strType) {
		NIPJobParam *pParam = new NIPJobParam(strName, strValue, strType, m_strPath);
		if (pParam) {
			m_listParam.push_back(pParam);
		}

		return pParam;
	}

	NIPJobParam *AddParam(NIPJobParam *pParam) {
		NIPJobParam *pNewParam = new NIPJobParam(pParam, m_strPath);
		if (pNewParam) {
			m_listParam.push_back(pNewParam);
		}

		return pNewParam;
	}

	NIPJobProcess *AddSubProcess(wstring strName) {
		NIPJobProcess *pSubProcess = new NIPJobProcess(strName, true, m_strPath);
		if (pSubProcess) {
			m_listSubProcess.push_back(pSubProcess);
		}

		return pSubProcess;
	}

	NIPJobProcess *AddSubProcess(NIPJobProcess *pSubProcess) {
		NIPJobProcess *pNewSubProcess = new NIPJobProcess(pSubProcess, true, m_strPath);
		if (pNewSubProcess) {
			m_listSubProcess.push_back(pNewSubProcess);
		}

		return pNewSubProcess;
	}

	NIPJobParam *FindParam(wstring strName) {
		NIPJobParam *pFound = nullptr;
		for (auto pParam : m_listParam) {
			pFound = pParam->FindParam(strName);
			if (pFound) {
				return pFound;
			}
		}

		for (auto pSubProcess : m_listSubProcess) {
			pFound = pSubProcess->FindParam(strName);
			if (pFound) {
				return pFound;
			}
		}

		return nullptr;
	}

	NIPJobProcess *FindSubProcess(wstring strName)
	{
		for (auto pSubProcess : m_listSubProcess) {
			if (pSubProcess->m_strName == strName || pSubProcess->m_strPath == strName) {
				return pSubProcess;
			}
		}

		return nullptr;
	}

	void UpdateParamValues(NIPJobProcess *pProcess)
	{
		if (pProcess == nullptr) {
			return;
		}

		for (auto pParam : m_listParam) {
			UpdateParamValues(pParam, pProcess);
		}

		for (auto pSubProcess : m_listSubProcess) {
			pSubProcess->UpdateParamValues(pProcess);
		}
	}

	void UpdateParamValues(NIPJobParam *pParam, NIPJobProcess *pProcess)
	{
		if (pParam == nullptr || pProcess == nullptr) {
			return;
		}

		if (pParam->m_bGroup) {
			for (auto pParamChild : pParam->m_listParam) {
				UpdateParamValues(pParamChild, pProcess);
			}
		}
		else {
			auto pParamFound = pProcess->FindParam(pParam->m_strPath);
			if (pParamFound) {
				pParam->m_strValue = pParamFound->m_strValue;
			}
		}
	}

	wstring GetValue(wstring strKey)
	{
		NIPJobParam *pParam = FindParam(strKey);
		if (pParam) {
			return pParam->m_strValue;
		}

		return L"";
	}

	bool GetValueBool(wstring strKey)
	{
		wstring strValue = GetValue(strKey);
		transform(strValue.begin(), strValue.end(), strValue.begin(), tolower);

		return (strValue == _T("true"))? true : false;
	}

	int GetValueInt(wstring strKey)
	{
		wstring strValue = GetValue(strKey);
		return stoi(strValue);
	}

	float GetValueFloat(wstring strKey)
	{
		wstring strValue = GetValue(strKey);
		if (!IsValueNumber(strValue)) {

		}

		return stof(strValue);
	}

	wstring GetDesc();
	wstring GetParamDesc();

	bool IsEmpty() {
		if (m_strName == L"" && m_listParam.size() == 0) {
			return TRUE;
		}

		return FALSE;
	}

	bool IsValueNumber(wstring strValue)
	{
		return !strValue.empty() && find_if(strValue.begin(), strValue.end(), [](wchar_t c) { return !isdigit(c); }) == strValue.end();
	}
};

class AFX_EXT_CLASS NIPJobCategory
{
public:
	wstring m_strName;
	vector<NIPJobProcess *> m_listProcess;

	NIPJobCategory(NIPJobCategory *pCategory = nullptr) {
		Init();

		if (pCategory) {
			*this = *pCategory;
		}
	}

	NIPJobCategory(wstring strName) {
		m_strName = strName;
	}

	virtual ~NIPJobCategory() {
		Clear();
	};

	void Init() {
		m_strName = L"";
	}

	void Clear() {
		for (auto pProcess : m_listProcess) {
			delete pProcess;
		}
		m_listProcess.clear();

		Init();
	}

	NIPJobCategory &operator=(const NIPJobCategory &dCategory) {
		Clear();

		m_strName = dCategory.m_strName;

		for (auto pProcess : dCategory.m_listProcess) {
			AddProcess(pProcess);
		}

		return *this;
	}

	NIPJobProcess *AddProcess(wstring strName, bool bEnable = true) {
		NIPJobProcess *pProcess = new NIPJobProcess(strName);
		if (pProcess) {
			pProcess->m_bEnable = bEnable;

			m_listProcess.push_back(pProcess);
		}

		return pProcess;
	}

	NIPJobProcess *AddProcess(NIPJobProcess *pProcess) {
		NIPJobProcess *pNewProcess = new NIPJobProcess(pProcess);
		if (pNewProcess) {
			m_listProcess.push_back(pNewProcess);
		}

		return pNewProcess;
	}

	NIPJobProcess *FindProcess(wstring strName)
	{
		for (auto pProcess : m_listProcess) {
			if (pProcess->m_strName == strName || pProcess->m_strPath == strName) {
				return pProcess;
			}
		}

		return nullptr;
	}

	bool IsEmpty() {
		if (m_strName == L"" && m_listProcess.size() == 0) {
			return TRUE;
		}

		return FALSE;
	}
};

class AFX_EXT_CLASS NIPJob
{
public:
	vector<NIPJobCategory *> m_listCategory;
	vector<NIPJobProcess *> m_listProcess;

	NIPJob(NIPJob *pJob = nullptr) {
		Init();

		if (pJob) {
			*this = *pJob;
		}
	}

	virtual ~NIPJob() {
		Clear();
	}

	void Init() {

	}

	void Clear() {
		for (auto pCategory : m_listCategory) {
			delete pCategory;
		}
		m_listCategory.clear();

		for (auto pProcess : m_listProcess) {
			delete pProcess;
		}
		m_listProcess.clear();

		Init();
	}

	NIPJob &operator=(const NIPJob &dJob) {
		Clear();

		for (auto pCategory : dJob.m_listCategory) {
			AddCategory(pCategory);
		}

		for (auto pProcess : dJob.m_listProcess) {
			AddProcess(pProcess);
		}

		return *this;
	}

	NIPJobCategory *AddCategory(wstring strName) {
		NIPJobCategory *pCategory = new NIPJobCategory(strName);
		if (pCategory) {
			m_listCategory.push_back(pCategory);
		}

		return pCategory;
	}

	NIPJobCategory *AddCategory(NIPJobCategory *pCategory) {
		NIPJobCategory *pNewCategory = new NIPJobCategory(pCategory);
		if (pNewCategory) {
			m_listCategory.push_back(pNewCategory);
		}

		return pNewCategory;
	}

	NIPJobProcess *AddProcess(wstring strName, bool bEnable = true) {
		NIPJobProcess *pProcess = new NIPJobProcess(strName);
		if (pProcess) {
			pProcess->m_bEnable = bEnable;

			m_listProcess.push_back(pProcess);
		}

		return pProcess;
	}

	NIPJobProcess *AddProcess(NIPJobProcess *pProcess, NIPJobProcess *pCurProcess = nullptr) {
		if (pProcess == nullptr) {
			return nullptr;
		}

		NIPJobProcess *pNewProcess = new NIPJobProcess(pProcess);
		if (pCurProcess) {
			auto it = find(m_listProcess.begin(), m_listProcess.end(), pCurProcess);
			if (it != m_listProcess.end()) {
				m_listProcess.insert(it, pNewProcess);
				return pNewProcess;
			}
		}

		m_listProcess.push_back(pNewProcess);
		return pNewProcess;
	}

	NIPJobProcess *FindProcess(wstring strName)
	{
		for (auto pProcess : m_listProcess) {
			if (pProcess->m_strName == strName || pProcess->m_strPath== strName) {
				return pProcess;
			}
		}

		for (auto pCategory: m_listCategory) {
			NIPJobProcess *pFound = pCategory->FindProcess(strName);
			if (pFound) {
				return pFound;
			}
		}

		return nullptr;
	}

	bool DeleteProcess(NIPJobProcess *pProcess) {
		if (pProcess == nullptr) {
			return false;
		}

		auto it = find(m_listProcess.begin(), m_listProcess.end(), pProcess);
		if (it != m_listProcess.end()) {
			m_listProcess.erase(it);

			delete pProcess;

			return true;
		}

		return false;
	}

	bool IsEmpty() {
		if (m_listCategory.size() == 0 && m_listProcess.size() == 0) {
			return TRUE;
		}

		return FALSE;
	}
};
