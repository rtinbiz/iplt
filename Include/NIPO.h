#pragma once

#include "NIPL.h"
#include "NIPLCV.h"
#include "NIPLGPU.h"
#include "NIPJob.h"
#include <memory>

#pragma comment (lib, "NIPL.lib")
#pragma comment (lib, "NIPLCV.lib")
#pragma comment (lib, "NIPLGPU.lib")

#define NIPO_VERION L"0.5"

enum NIPO_NOTIFY_LEVEL {
	NNL_NONE = 0,
	NNL_IO,
	NNL_INPUT_UPDATE,
	NNL_OUTPUT_UPDATE,
	NNL_PROCESS,
	NNL_JOB,
	NNL_ALL
};
typedef void(*NIPONotify)(wstring strName, NIPL_ERR nErr, NIPO_NOTIFY_LEVEL nNotifyLevel);

class AFX_EXT_CLASS NIPO {
public :
	static NIPO *pThis;
	static NIPO *GetInstance(BOOL bNew = FALSE);
	static void ReleaseInstance(NIPO *pThat = 0x00);

	NIPO();
	virtual ~NIPO();
	void Clear();

	NIPJob *LoadJob(wstring strPath);
	bool LoadJob(wstring strPath, NIPJob &dJob);
	bool SaveJob(wstring strPath, const NIPJob &dJob);
	void ReleaseJob(NIPJob *pJob);
	void SendNotify(wstring strName, NIPL_ERR nErr = NIPL_ERR_SUCCESS, NIPO_NOTIFY_LEVEL nLevel = NNL_NONE);
	void SendNotifyInputUpdate(wstring strImage);
	void SendNotifyOutputUpdate(wstring strImage);
	void SetNotifyFunc(NIPONotify fnNotify, NIPO_NOTIFY_LEVEL nNotifyLevel = NNL_ALL);

	bool RunJob(NIPJob *pJob);
	bool RunProcess(NIPJobProcess *pProcess);

	bool LoadImage(wstring strPath, Mat &dImg);
	bool SaveImage(wstring strPath, Mat dImg);
	bool LoadInputImage(wstring strImagePath, wstring strMaskPath = L"", wstring strTemplatePath = L"");
	bool LoadOutputImage(wstring strPath, wstring strName);
	bool SaveOutputImage(wstring strPath, wstring strName);
	bool SaveOutputResult(wstring strPath, wstring strName);
	bool ConvertInputImageToGray(NIPJobProcess *pProcess);
	bool BinarizeMask(Mat &dImg);

	void SetInput(const NIPLInput &dInput);
	void GetInput(NIPLInput &dInput);
	bool SetOutput(wstring strName, const NIPLOutput &dOutput);
	bool GetOutput(wstring strName, NIPLOutput &dOutput);
	bool RemoveOutput(wstring strName);

	bool GetInputImage(wstring strName, Mat &dImg);
	bool GetOutputImage(wstring strName, Mat &dImg);

	bool GetParamValue(wstring strValue, bool &bValue);
	bool GetParamValue(wstring strValue, int &nValue);
	bool GetParamValue(wstring strValue, float &nValue);

private:
	NIPLParam *SetNIPLParam(NIPJobProcess *pProcess);
	NIPL_ERR DoNIPLProcess(wstring strProcessName, NIPLInput *pInput, NIPLOutput *pOutput);

private:
	NIPONotify m_fnNotify;
	NIPONotify m_fnProcessNotify;
	NIPO_NOTIFY_LEVEL m_nNotifyLevel;

	NIPLInput m_dInput;
	map<wstring, NIPLOutput> m_mapOutput;

	NIPLOption m_dOption;
};
