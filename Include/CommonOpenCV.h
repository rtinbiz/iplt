#pragma once

#ifdef _DEBUG
#pragma comment (lib, "../Lib/OpenCV300/vc12/lib/opencv_ts300d.lib")
#pragma comment (lib, "../Lib/OpenCV300/vc12/lib/opencv_world300d.lib")
#else
#pragma comment (lib, "../Lib/OpenCV300/vc12/lib/opencv_ts300.lib")
#pragma comment (lib, "../Lib/OpenCV300/vc12/lib/opencv_world300.lib")
#endif

#define CV_NO_CVV_IMAGE
#include "opencv2/opencv.hpp"
using namespace cv;
