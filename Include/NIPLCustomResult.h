#pragma once

#include "NIPLResult.h"

struct AFX_EXT_CLASS NIPLDefect_LDC : public NIPLDefect
{
	enum DEFECT_TYPE {
		DEFECT_TYPE_TERMINAL_ROUND = 0,
		DEFECT_TYPE_TERMINAL_RECT,
		DEFECT_TYPE_TUBE_RED,
		DEFECT_TYPE_TUBE_BLACK,
		DEFECT_TYPE_GUIDE
	};

	NIPLDefect_LDC(int nType, Rect rcBoundingBox) : NIPLDefect(nType, rcBoundingBox, 0)
	{
	}

/*
	wstring GetTypeDesc()
	{
		wstring strType = L"";
		switch (m_nType) {
		case DEFECT_TYPE_TERMINAL_ROUND: strType = L"TERMINAL_ROUND"; break;
		case DEFECT_TYPE_TERMINAL_RECT: strType = L"TERMINAL_RECT"; break;
		case DEFECT_TYPE_TUBE: strType = L"TUBE"; break;
		case DEFECT_TYPE_GUIDE: strType = L"GUARD"; break;
		}

		return strType;
	}
*/
};

class AFX_EXT_CLASS NIPLResult_Defect_LDC : public NIPLResult_Defect
{
public:
	NIPLResult_Defect_LDC() : NIPLResult_Defect() {
	}
	virtual ~NIPLResult_Defect_LDC() {
	}
};
