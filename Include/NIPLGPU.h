#pragma once

#include "NIPL.h"
#include "NIPLGPUParam.h"
#include "NIPLGPUResult.h"

class AFX_EXT_CLASS NIPLGPU : public NIPL {
public :
	static NIPLGPU *pThis;
	static NIPLGPU *GetInstance(BOOL bNew = FALSE);
	static void ReleaseInstance(NIPLGPU *pThat = 0x00);

	NIPL_ERR Smoothing(NIPLInput *pInput, NIPLOutput *pOutput, NIPLParam *pParam);

	NIPLGPU();
	~NIPLGPU();
};