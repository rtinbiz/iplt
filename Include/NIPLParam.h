#pragma once

#include "CommonOpenCV.h"

CV_INLINE RECT NormalizeRect(RECT r);
CV_INLINE RECT NormalizeRect(RECT r)
{
	int t;
	if (r.left > r.right)
	{
		t = r.left;
		r.left = r.right;
		r.right = t;
	}
	if (r.top > r.bottom)
	{
		t = r.top;
		r.top = r.bottom;
		r.bottom = t;
	}

	return r;
}
CV_INLINE CvRect RectToCvRect(RECT sr);
CV_INLINE CvRect RectToCvRect(RECT sr)
{
	sr = NormalizeRect(sr);
	return cvRect(sr.left, sr.top, sr.right - sr.left, sr.bottom - sr.top);
}
CV_INLINE RECT CvRectToRect(CvRect sr);
CV_INLINE RECT CvRectToRect(CvRect sr)
{
	RECT dr;
	dr.left = sr.x;
	dr.top = sr.y;
	dr.right = sr.x + sr.width;
	dr.bottom = sr.y + sr.height;

	return dr;
}
CV_INLINE CvPoint PointToCvPoint(POINT pt);
CV_INLINE CvPoint PointToCvPoint(POINT pt)
{
	return cvPoint(pt.x, pt.y);
}
CV_INLINE POINT CvPointToPoint(CvPoint pt);
CV_INLINE POINT CvPointToPoint(CvPoint pt)
{
	return CPoint(pt.x, pt.y);
}

struct AFX_EXT_CLASS NIPLParam
{
	bool m_bEnable;

	NIPLParam() {
		m_bEnable = false;
	}
	virtual ~NIPLParam() {
	}
};

struct AFX_EXT_CLASS NIPLParam_CreateImage: public NIPLParam
{
	enum {
		TYPE_GRAY_UINT8 = 0x00,
		TYPE_GRAY_FLOAT = 0x01,
		TYPE_COLOR_UINT8 = 0x02
	};

	enum {
		CHANNEL_1 = 0x00,
		TYPE_FLOAT = 0x00,
	};

	int m_nType;
	int m_nWidth;
	int m_nHeight;
	float m_nValue;

	NIPLParam_CreateImage() {
		Init();
	}
	virtual ~NIPLParam_CreateImage() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nType = TYPE_GRAY_UINT8;
		m_nWidth = 0;
		m_nHeight = 0;
		m_nValue = 0.f;
	}

	bool SetType(wstring strType) {
		m_nType = TYPE_GRAY_UINT8;
		if (strType == L"TYPE_GRAY_UINT8") m_nType = TYPE_GRAY_UINT8;
		else if (strType == L"TYPE_GRAY_FLOAT") m_nType = TYPE_GRAY_FLOAT;
		else if (strType == L"TYPE_COLOR_UINT8") m_nType = TYPE_COLOR_UINT8;
		else {
			return false;
		}

		return true;
	}
};

struct AFX_EXT_CLASS NIPLParam_Color2Gray : public NIPLParam
{
	enum {
		GRAYLEVEL_256 = 0x00,
		GRAYLEVEL_FLOAT = 0x01
	};

	int m_nGrayLevel;

	NIPLParam_Color2Gray() {
		Init();
	}
	virtual ~NIPLParam_Color2Gray() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nGrayLevel = GRAYLEVEL_256;
	}

	bool SetGrayLevel(wstring strGrayLevel) {
		m_nGrayLevel = GRAYLEVEL_256;
		if (strGrayLevel == L"GRAYLEVEL_256") m_nGrayLevel = GRAYLEVEL_256;
		else if (strGrayLevel == L"GRAYLEVEL_FLOAT") m_nGrayLevel = GRAYLEVEL_FLOAT;
		else {
			return false;
		}

		return true;
	}
};

struct AFX_EXT_CLASS NIPLParam_Invert : public NIPLParam
{
	NIPLParam_Invert() {
		Init();
	}
	virtual ~NIPLParam_Invert() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
	}
};


struct AFX_EXT_CLASS NIPLParam_Operate : public NIPLParam
{
	enum {
		METHOD_ADD = 0x01,
		METHOD_SUBTRACT = 0x02,
		METHOD_AND = 0x03,
		METHOD_OR = 0x04,
		METHOD_XOR = 0x05,
	};

	int m_nMethod;
	Mat m_dTargetImg;

	NIPLParam_Operate() {
		Init();
	}
	virtual ~NIPLParam_Operate() {
		Clear();
	}
	virtual void Clear() {
		m_dTargetImg.release();

		Init();
	}

	void Init() {
		m_nMethod = METHOD_ADD;
	}

	bool SetMethod(wstring strMethod) {
		m_nMethod = METHOD_ADD;
		if (strMethod == L"METHOD_ADD") m_nMethod = METHOD_ADD;
		else if (strMethod == L"METHOD_SUBTRACT") m_nMethod = METHOD_SUBTRACT;
		else if (strMethod == L"METHOD_AND") m_nMethod = METHOD_AND;
		else if (strMethod == L"METHOD_OR") m_nMethod = METHOD_OR;
		else if (strMethod == L"METHOD_XOR") m_nMethod = METHOD_XOR;
		else {
			return false;
		}

		return true;
	}
};


struct AFX_EXT_CLASS NIPLParam_CopySubImageFrom : public NIPLParam
{
	int m_nStartPosX;
	int m_nStartPosY;
	int m_nEndPosX;
	int m_nEndPosY;

	NIPLParam_CopySubImageFrom() {
		Init();
	}
	virtual ~NIPLParam_CopySubImageFrom() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nStartPosX = 0;
		m_nStartPosY = 0;
		m_nEndPosX = 0;
		m_nEndPosY = 0;
	}
};

struct AFX_EXT_CLASS NIPLParam_CopySubImageTo : public NIPLParam
{
	Mat m_dTargetImg;
	int m_nStartPosX;
	int m_nStartPosY;

	NIPLParam_CopySubImageTo() {
		Init();
	}
	virtual ~NIPLParam_CopySubImageTo() {
		Clear();
	}
	virtual void Clear() {
		m_dTargetImg.release();
		Init();
	}

	void Init() {
		m_nStartPosX = 0;
		m_nStartPosY = 0;
	}
};

struct AFX_EXT_CLASS NIPLParam_Thresholding : public NIPLParam
{
	enum {
		METHOD_NONE = 0x00,
		METHOD_UPPER = 0x01,
		METHOD_LOWER = 0x02,
		METHOD_OTSU = 0x04,	// only used for flag, if this value is set, it'll work same to METHOD_UPPER_OTSU
		METHOD_UPPER_OTSU = METHOD_OTSU | METHOD_UPPER,
		METHOD_LOWER_OTSU = METHOD_OTSU | METHOD_LOWER
	};

	int m_nMethod;
	float m_nThreshold;

	NIPLParam_Thresholding() {
		Init();
	}
	virtual ~NIPLParam_Thresholding() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nMethod = METHOD_NONE;
		m_nThreshold = 0.f;
	}

	bool SetMethod(wstring strMethod) {
		m_nMethod = METHOD_NONE;
		if (strMethod == L"METHOD_UPPER") m_nMethod = METHOD_UPPER;
		else if (strMethod == L"METHOD_LOWER") m_nMethod = METHOD_LOWER;
		else if (strMethod == L"METHOD_OTSU") m_nMethod = METHOD_OTSU;	
		else if (strMethod == L"METHOD_UPPER_OTSU") m_nMethod = METHOD_UPPER_OTSU;
		else if (strMethod == L"METHOD_LOWER_OTSU") m_nMethod = METHOD_LOWER_OTSU;
		else {
			return false;
		}

		return true;
	}
};

struct AFX_EXT_CLASS NIPLParam_ColorThresholding : public NIPLParam
{
	enum {
		METHOD_NONE = 0x00,
		METHOD_UPPER = 0x01,
		METHOD_LOWER = 0x02,
		METHOD_IN_RANGE = 0x03
	};

	int m_nMethod_R;
	int m_nMethod_G;
	int m_nMethod_B;
	float m_nLowerValue_R;
	float m_nLowerValue_G;
	float m_nLowerValue_B;
	float m_nUpperValue_R;
	float m_nUpperValue_G;
	float m_nUpperValue_B;
	float m_nLowerRatio_R_G;
	float m_nLowerRatio_R_B;
	float m_nLowerRatio_G_B;
	float m_nUpperRatio_R_G;
	float m_nUpperRatio_R_B;
	float m_nUpperRatio_G_B;

	NIPLParam_ColorThresholding() {
		Init();
	}
	virtual ~NIPLParam_ColorThresholding() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nMethod_R = METHOD_NONE;
		m_nMethod_G = METHOD_NONE;
		m_nMethod_B = METHOD_NONE;
		m_nLowerValue_R = 0.f;
		m_nLowerValue_G = 0.f;
		m_nLowerValue_B = 0.f;
		m_nUpperValue_R = 0.f;
		m_nUpperValue_G = 0.f;
		m_nUpperValue_B = 0.f;
		m_nLowerRatio_R_G = 0.f;
		m_nLowerRatio_R_B = 0.f;
		m_nLowerRatio_G_B = 0.f;
		m_nUpperRatio_R_G = 0.f;
		m_nUpperRatio_R_B = 0.f;
		m_nUpperRatio_G_B = 0.f;
	}

	bool SetMethod(wstring strMethod, int &nMethod) {
		nMethod = METHOD_NONE;
		if (strMethod == L"METHOD_UPPER") nMethod = METHOD_UPPER;
		else if (strMethod == L"METHOD_LOWER") nMethod = METHOD_LOWER;
		else if (strMethod == L"METHOD_IN_RANGE") nMethod = METHOD_IN_RANGE;
		else {
			return false;
		}

		return true;
	}

	bool SetMethod_R(wstring strMethod) {
		return SetMethod(strMethod, m_nMethod_R);
	}

	bool SetMethod_G(wstring strMethod) {
		return SetMethod(strMethod, m_nMethod_G);
	}

	bool SetMethod_B(wstring strMethod) {
		return SetMethod(strMethod, m_nMethod_B);
	}
};

struct AFX_EXT_CLASS NIPLParam_Smoothing : public NIPLParam
{
	enum {
		METHOD_NONE = 0x00,
		METHOD_FILTER = 0x01
	};

	int m_nMethod;
	int m_nFilterSize;

	NIPLParam_Smoothing() {
		Init();
	}
	virtual ~NIPLParam_Smoothing() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nMethod = METHOD_NONE;
		m_nFilterSize = 0;
	}

	bool SetMethod(wstring strMethod) {
		m_nMethod = METHOD_NONE;
		if (strMethod == L"METHOD_FILTER") m_nMethod = METHOD_FILTER;
		else {
			return false;
		}

		return true;
	}
};

struct AFX_EXT_CLASS NIPLParam_EdgeDetecting : public NIPLParam
{
	enum {
		METHOD_NONE = 0x00,
		METHOD_CANNY = 0x01
	};

	int m_nMethod;
	float m_nCannyLowerThreshold;
	float m_nCannyUpperThreshold;
	int m_nFilterSize;

	NIPLParam_EdgeDetecting() {
		Init();
	}
	virtual ~NIPLParam_EdgeDetecting() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nMethod = METHOD_NONE;
		m_nCannyLowerThreshold = 0.f;
		m_nCannyUpperThreshold = 0.f;
		m_nFilterSize = 0;
	}

	bool SetMethod(wstring strMethod) {
		m_nMethod = METHOD_NONE;
		if (strMethod == L"METHOD_CANNY") m_nMethod = METHOD_CANNY;
		else {
			return false;
		}

		return true;
	}
};


