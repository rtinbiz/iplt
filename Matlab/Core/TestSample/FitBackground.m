function [dResultImage] = FitBackground(dImage, dBlock, nDegree,  nThreshold)
[r c n] = size(dImage);
if n == 3,
    dImage = rgb2gray(dImage);
end;
dImage = double(dImage);

nBlockCountY = dBlock(1);
nBlockCountX = dBlock(2);

if(~exist('dMask'));
    dMask = ones(r, c);
end;
dMask = ones(r, c);
    
% block mean & image coordinates matrix
k = 0;
dBlockMean = zeros(nBlockCountY*nBlockCountX,1);
dIndexX = zeros(nBlockCountY*nBlockCountX,1);
dIndexY = zeros(nBlockCountY*nBlockCountX,1);
for j=1:nBlockCountX;
    for i=1:nBlockCountY;
        nStartY = round((i-1)*r/nBlockCountY) + 1;
        nEndY = round(i*r/nBlockCountY);
        nStartX = round((j-1)*c/nBlockCountX) + 1;
        nEndX = round(j*c/nBlockCountX);

        % add data only if there's no '0' values in the mask
        if(isempty(find(dMask(nStartY:nEndY, nStartX:nEndX) == 0)));
            k = k+1;
            dBlockMean(k) = mean2(dImage(nStartY:nEndY, nStartX:nEndX));
            dIndexX(k) = round((nStartX+nEndX)/2);
            dIndexY(k) = round((nStartY+nEndY)/2);
        end;        
    end;
end;

if(k < nBlockCountX*nBlockCountY);
    dBlockMean(k+1:end) = [];
    dIndexX(k+1:end) = [];
    dIndexY(k+1:end) = [];
end;
A = zeros(k, (nDegree+1)*(nDegree+2)/2);

k=0;
for px=0:nDegree,
    for py=0:nDegree-px,
        k = k+1;
        A(:,k) = (dIndexX.^px) .* (dIndexY.^py);
    end;
end;

% find outliner block
dTempA = A;
dTempBlockMean = dBlockMean;
dMeanError = zeros(length(dBlockMean),1);
for k=1:length(dMeanError);
    if k>1;
        dTempA(k-1,:) = A(k-1,:);
        dTempBlockMean(k-1) = dBlockMean(k-1);
    end;
    dTempA(k,:) = 0;
    dTempBlockMean(k) = 0;
    coef = pinv(dTempA)*dTempBlockMean;
    dMeanError(k) = mean(abs(dTempBlockMean - dTempA*coef));
end;
dOulierBlock = dMeanError<(mean(dMeanError) - nThreshold*std(dMeanError));
dOulierBlock = imdilate(dOulierBlock, ones(3));
dOulierBlock = imerode(dOulierBlock, ones(3));

% remove outlier block
dIndex = find(dOulierBlock == 1);
A(dIndex, :) = 0;
dBlockMean(dIndex) = 0;
coef = pinv(A) * dBlockMean;

% get result fitting surface
[dIndexY, dIndexX] = find(dImage>=-inf);
A = zeros(r*c, (nDegree+1)*(nDegree+2)/2);
k=0;
for px=0:nDegree,
    for py=0:nDegree-px,
        k = k+1;
        A(:,k) = (dIndexX.^px) .* (dIndexY.^py);
    end;
end;
    
dResultImage = zeros(r,c);
dResultImage(:) = A*coef;
