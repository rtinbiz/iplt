function dFlatImage = MakeFlatImage(dImage, dCenterPos, nMinRadius, nMaxRadius, nStepAngle)

nHeight = nMaxRadius - nMinRadius + 1;
nWidth = 360 / nStepAngle;

dFlatImage = zeros(nHeight, nWidth);

dFlatPos.x = 0;
for(nAngle = 0:nStepAngle:360-nStepAngle)
    dFlatPos.x = dFlatPos.x + 1;
    dFlatPos.y = 0;
    for(nRadius = nMinRadius:nMaxRadius)
        dFlatPos.y = dFlatPos.y + 1;
        
        dPos.x = round(dCenterPos.x + nRadius * cos(nAngle*pi/180));
        dPos.y = round(dCenterPos.y + nRadius * sin(nAngle*pi/180));
        
        dFlatImage(dFlatPos.y, dFlatPos.x) = dImage(dPos.y, dPos.x);
    end;
end;
