clear all;
close all;

szPath = '..\..\..\Data\Core\Sample\';

szFileName = 'Pol����.bmp';

szFilePath=sprintf('%s\\%s',szPath, szFileName);
dImage = imread(szFilePath, 'bmp');
dImage = rgb2gray(dImage);
dImage = double(dImage)/255;

% ���� �ϴ�
%dBlock = [190 550 500 860];
% ����
dBlock = [520 270 1200 925];
dImage = dImage(dBlock(2):dBlock(4), dBlock(1):dBlock(3));

level = graythresh(dImage);
dEdgeImage = im2bw(dImage,level);
%dEdgeImage = edge(dEdgeImage,'canny');

[nSizeY nSizeX] = size(dImage);

figure(1000);
subplot(1,2,1);
imshow(uint8(dImage*255));
subplot(1,2,2);
imshow(uint8(dEdgeImage*255));

hold on;
nHalfSize = round(nSizeY/2);
nMinRadius = round(nHalfSize/2);
nStepCircle = round(nHalfSize/4);

% i = nMinRadius;
% while(i <= nHalfSize-nStepCircle); 
%     [dCenter, nRadius, dMetric] = imfindcircles(dEdgeImage, [i i+nStepCircle], 'Sensitivity',0.9, 'Edge', 0.01);
% %     [dCenter, nRadius, dMetric] = imfindcircles(dEdgeImage, [80 155]);
% 
%     if(~isempty(dCenter));
%         break;
%     end;
%     
%     i = i + nStepCircle;
% end;
% 
%  if(isempty(dCenter));
%      disp('Not found Center Pos');
%     return;
% end;

dCenterPos.x = 327;
dCenterPos.y = 332;
nMinRadius = 147;
nMaxRadius = 305;

nStepAngle = 0.1;
dFlatImage = MakeFlatImage(dEdgeImage, dCenterPos, nMinRadius, nMaxRadius, nStepAngle);

%dFlatImage(1:7,:) = 0;
%dFlatImage(30:end,:) = 0;
dFlatImage = ~dFlatImage;

%figure(300);
%imshow(dFlatImage);


dResultImage = RestoreFlatImage(dFlatImage, dCenterPos, nMinRadius, nMaxRadius, nStepAngle, size(dEdgeImage));

%figure(2);
subplot(1,2,2);
imshow(dResultImage);

