function dResultImage = RemoveCircle(dImage, nCenterX, nCenterY, nRadius, nWidth)

dResultImage = dImage;

nStartRadius = nRadius - round(nWidth/2);
nEndRadius = nStartRadius + nWidth - 1;

for r = nStartRadius:nEndRadius;
    for t=0:0.001:2*pi;
        nX = round(nCenterX + r*cos(t));
        nY = round(nCenterY + r*sin(t));

        dResultImage(nY, nX) = 0;
    end;
end;
