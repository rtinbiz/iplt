clear all;

szDataPath = '..\..\..\Data\\LG\SD image_v0.1\';
szCurDir=cd;    
cd(szDataPath);
dFileList=dir;
cd(szCurDir);

nIndex=1;
nCount=length(dFileList);

if(nCount < 3);
    return;
end;

bTest = false;

dChecked = zeros(nCount, 1);
for i=1:nCount;
    szMsg = sprintf('Processing (%d/%d)...', i, nCount);
    disp(szMsg);
    
    if(dChecked(i) ~= 0);
        continue;
    end;
    
    if strfind(lower(dFileList(i).name),'.jpg');
        szDataFile=sprintf('%s\\%s',szDataPath,dFileList(i).name);
 
        for j=i+1:nCount;
            if(dChecked(j) ~= 0);
                continue;
            end;

            szMsg = sprintf(' > Comparing (%d)...', j);
            disp(szMsg);

            if strfind(lower(dFileList(j).name),'.jpg');
                szDataFile2=sprintf('%s\\%s',szDataPath,dFileList(j).name);

                bMatched = CompareImages(szDataFile, szDataFile2);
                if(bMatched);
                    szMsg = sprintf('    -> Mached!!');
                    disp(szMsg);

                    dChecked(i) = i;
                    dChecked(j) = i;


                    szDestDataFolder=sprintf('%s\\%d',szDataPath,i);
                    mkdir(szDestDataFolder);

                    szDestDataFile=sprintf('%s\\%s',szDestDataFolder,dFileList(i).name);                
                    szDestDataFile2=sprintf('%s\\%s',szDestDataFolder,dFileList(j).name);

                    copyfile(szDataFile, szDestDataFile);
                    copyfile(szDataFile2, szDestDataFile2);
                end;

                if(bTest);
                    return;
                end;
            end;
        end;
    end;
end;
