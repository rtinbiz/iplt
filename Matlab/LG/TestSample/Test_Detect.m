clear all;
close all;

szGSPath = '..\..\..\Data\\LG\SD image_v0.1\GoldenSample\';

szGSFileName1 = 'GoldenSample1.JPG';
szGSFileName2 = 'GoldenSample2.JPG';

szGSFilePath=sprintf('%s\\%s',szGSPath, szGSFileName1);
dGSImage1 = imread(szGSFilePath, 'jpg');
dGSImage1 = rgb2gray(dGSImage1);
dGSImage1 = double(dGSImage1)/255;

szGSFilePath=sprintf('%s\\%s',szGSPath, szGSFileName2);
dGSImage2 = imread(szGSFilePath, 'jpg');
dGSImage2 = rgb2gray(dGSImage2);
dGSImage2 = double(dGSImage2)/255;

figure(1000);
imshow(uint8(dGSImage1*255));
figure(1001);
imshow(uint8(dGSImage1*255));

szDataPath = '..\..\..\Data\\LG\SD image_v0.1\TestSample\';
szDataFile = 'Sample2.JPG';
szDataFilePath=sprintf('%s\\%s',szDataPath, szDataFile);

dDataImage = imread(szDataFilePath, 'jpg');
dDataImage = rgb2gray(dDataImage);
dDataImage = double(dDataImage)/255;

[nSizeY, nSizeX] = size(dDataImage);

figure(2);
imshow(uint8(dDataImage*255));
 
nBlockCountX= 4;
nBlockCountY= 4;
nBlockSizeX = nSizeX/nBlockCountX;
nBlockSizeY = nSizeY/nBlockCountY;

dResultImage = zeros(nSizeY, nSizeX);
 for y=1:nBlockCountY
    for x=1:nBlockCountX
        szMsg = sprintf('Processing (%d/%d, %d/%d)..', y, nBlockCountY, x, nBlockCountX);
        disp(szMsg);
        
        nStartX = round((x-1)*nBlockSizeX)+1;
        nEndX = round(x*nBlockSizeX);
        nStartY = round((y-1)*nBlockSizeY)+1;
        nEndY = round(y*nBlockSizeY);
        
        dBlockImage = dDataImage(nStartY:nEndY, nStartX:nEndX);
        dResultBlockImage = CompareWithGoldenSample(dBlockImage, dGSImage1, dGSImage2);
        dResultImage(nStartY:nEndY, nStartX:nEndX) = dResultBlockImage;
       
        figure(3);
        imshow(dResultImage*255);
        drawnow;
    end;
 end;
 
 