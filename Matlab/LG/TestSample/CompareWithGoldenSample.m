function dResultImage = CompareWithGoldenSample(dImage, dGSImage1, dGSImage2)
[nSizeY, nSizeX] = size(dImage);
dResultImage = zeros(nSizeY, nSizeX);

dCRImage1 = normxcorr2(dImage, dGSImage1);
dCRImage2 = normxcorr2(dImage, dGSImage2);


% figure(20); imshow(uint8(dImage*255));
% figure(21); imshow(uint8(dGSImage1*255));
% figure(22); imshow(uint8(dGSImage2*255));

while(true);
    nMax1 = max(dCRImage1(:));
    nMax2 = max(dCRImage2(:));

    if(nMax1 > nMax2);
        dGSImage = dGSImage1;
        dCRImage = dCRImage1;
        nMax = nMax1;
    else;
        dGSImage = dGSImage2;
        dCRImage = dCRImage2;
        nMax = nMax2;
    end;

    nThreshold = 0.9;
    if nMax < nThreshold;
        % not matching
        return;
    end;

    [dIndexY, dIndexX] = find(dCRImage == nMax);

    nStartX = dIndexX(1) - nSizeX + 1;
    nStartY = dIndexY(1) - nSizeY + 1;
    nEndX= dIndexX(1);
    nEndY = dIndexY(1);
    
    if(nStartX < 1 || nStartY < 1);
        if(nMax1 > nMax2);
            dCRImage1(dIndexY(1), dIndexX(1)) = 0;
        else;
            dCRImage2(dIndexY(1), dIndexX(1)) = 0;
        end;
    else;
        break;
    end;
end;

dCompareImage = dGSImage(nStartY:nEndY, nStartX:nEndX);

% dCRBlockImage = normxcorr2(dImage, dCompareImage);
% nMaxBlock = max(dCRBlockImage(:));

figure(5);
subplot(2,1,1); imshow(dImage);
subplot(2,1,2); imshow(dCompareImage);

%
% Get Difference Image and find a defect
%

dDiffImage = abs(dImage - dCompareImage);

figure(6);
imshow(dDiffImage);

% dBlock = [8 8];
% nDegree = 2;
% nThreshold = 2;
% dFitImage = FitBackground(dDiffImage, dBlock, nDegree, nThreshold);
% nMean = mean2(dFitImage);
% dDiffImage = dDiffImage - dFitImage + nMean;

% nMean = mean(dDiffImage(:));
% nStd = std(dDiffImage(:));

% nThreshold = 4;
% dResultImage(find(dDiffImage > (nMean + nThreshold * nStd))) = 1;
% dResultImage(find(dDiffImage < (nMean - nThreshold * nStd))) = 1;

nThreshold = 0.2;
dResultImage(find(dDiffImage > nThreshold)) = 1;

% figure(10); imshow(uint8(dImage*255));
% figure(11); imshow(dDiffImage);
figure(12); imshow(dResultImage*255);
