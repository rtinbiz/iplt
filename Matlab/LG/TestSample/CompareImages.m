function bMatched = CompareImages(szDataFile1, szDataFile2)
dImage1 = imread(szDataFile1, 'jpg');
dImage2 = imread(szDataFile2, 'jpg');

dImage1 = rgb2gray(dImage1);
dImage2 = rgb2gray(dImage2);

[nSizeY, nSizeX] = size(dImage2);
nHalfX = round(nSizeX/2);
nHalfY = round(nSizeY/2);

dRect(1).nCenterX = round(nHalfX - nHalfX/2);
dRect(1).nCenterY = round(nHalfY - nHalfY/2);
dRect(2).nCenterX = round(nHalfX - nHalfX/2);
dRect(2).nCenterY = round(nHalfY + nHalfY/2);
dRect(3).nCenterX = round(nHalfX + nHalfX/2);
dRect(3).nCenterY = round(nHalfY - nHalfY/2);
dRect(4).nCenterX = round(nHalfX + nHalfX/2);
dRect(4).nCenterY = round(nHalfY + nHalfY/2);
dRect(4).nCenterX = nHalfX;
dRect(4).nCenterY = nHalfY;

nTemplateHalfWidth = round(nHalfX/4);
nTemplateHalfHeight = round(nHalfY/4);

nThreshold = 0.9;
for k=1:2;
    for i=1:length(dRect);
        nStartX = dRect(i).nCenterX - nTemplateHalfWidth;
        nEndX = dRect(i).nCenterX + nTemplateHalfWidth;
        nStartY = dRect(i).nCenterY - nTemplateHalfHeight;
        nEndY = dRect(i).nCenterY + nTemplateHalfHeight;

        if(k==1);
            dTemplate = dImage2(nStartY:nEndY, nStartX:nEndX);
            dCRImage = normxcorr2(dTemplate, dImage1);
        else;
            dTemplate = dImage1(nStartY:nEndY, nStartX:nEndX);
            dCRImage = normxcorr2(dTemplate, dImage2);
        end;

        dIndex = find(dCRImage > nThreshold);
        if(~isempty(dIndex));
            bMatched = true;
            return 
        end;
    end;
end;

bMatched = false;
