% This for Green
clear;

dCData = [
4.3 	3.6 	46.9	17.7 	3.4 	3.6 	49.8 	15.3 	4.9 	3.6 	57.9 	18.3 
3.5 	1.2 	31.7 	9.3 	2.3 	1.2 	28.3 	6.9 	3.0 	1.9 	56.2 	17.7 
4.0 	3.2 	49.1 	17.8 	3.8 	3.6 	43.5 	14.2 	4.2 	3.8 	56.6 	18.2 
2.5 	1.7 	33.5 	10.4 	1.0 	1.2 	31.4 	6.2 	3.2 	1.6 	57.4 	18.0 
4.6 	4.6 	48.6 	18.0 	4.8 	3.3 	48.1 	15.3 	4.5 	3.3 	60.2 	18.2 
2.7 	1.8 	34.9 	10.4 	2.7 	1.2 	31.9 	7.6 	3.0 	2.4 	56.4 	17.8 
3.2 	5.5 	56.2 	18.3 	4.2 	4.4 	53.3 	14.1 	4.2 	4.5 	57.2 	18.7 
3.3 	2.9 	31.7 	9.5 	3.1 	1.4 	26.8 	5.9 	3.8 	2.5 	60.4 	18.1 
];
dCData(:, 3:4:end) = []; % remove 'Rib Width' data
dCData(:, 3:3:end) = []; % remove 'Rib Height' data

dCData_Dummy = dCData(1:2:end, :);
dCData_Active = dCData(2:2:end, :);

dData = [
5.1 	1.9 	14.9 	15.3 	4.0 	4.0 	15.1 	16.3 	5.5 	3.6 	31.5 	17.8 
2.2 	1.2 	14.0 	9.2 	2.5 	2.1 	12.7 	7.0 	2.5 	3.9 	25.3 	17.5 
4.8 	3.4 	15.1 	17.1 	4.4 	2.2 	15.1 	17.3 	5.0 	2.7 	31.9 	18.0 
2.9 	0.8 	14.3 	10.8 	2.6 	1.4 	13.5 	7.8 	4.3 	2.7 	29.9 	18.9 
5.9 	3.7 	16.0 	17.9 	5.8 	5.5 	16.8 	17.8 	5.5 	3.2 	30.7 	17.9 
2.5 	2.2 	13.8 	10.3 	4.0 	2.0 	13.2 	8.2 	4.6 	2.9 	29.9 	18.2 
3.3 	4.1 	15.7 	17.9 	3.7 	4.9 	15.4 	17.9 	5.4 	4.0 	31.1 	18.3 
3.7 	3.7 	14.3 	11.1 	5.4 	3.2 	13.2 	8.1 	4.0 	4.2 	29.9 	19.6 
];
dData(:, 3:4:end) = []; % remove 'Rib Width' data
dData(:, 3:3:end) = []; % remove 'Rib Height' data

dData_Dummy = dData(1:2:end, :);
dData_Active = dData(2:2:end, :);

dR = corrcoef(dData(:), dCData(:));
dR_Dummy = corrcoef(dData_Dummy(:), dCData_Dummy(:));
dR_Active = corrcoef(dData_Active(:), dCData_Active(:));

szResult = sprintf('%.3f %.3f %.3f', dR(1,2), dR_Dummy(1,2), dR_Active(1,2));
disp(szResult);

clf;
figure(1);
hold on;
plot(dData(:), dCData(:), 'r.');
dX = [0:max(dData(:))];
plot(dX, dX, 'b');
hold off;

[H, P] = ttest2(dData(:), dCData(:));
[H_Dummy, P_Dummy] = ttest2(dData_Dummy(:), dCData_Dummy(:));
[H_Active, P_Active] = ttest2(dData_Active(:), dCData_Active(:));
szResult = sprintf('%.3f(%d) %.3f(%d) %.3f(%d)', P, H, P_Dummy, H_Dummy, P_Active, H_Active);
disp(szResult);
