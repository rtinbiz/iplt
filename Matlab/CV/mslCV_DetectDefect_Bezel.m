function dResultImage = mslCV_DetectDefect_Bezel(dParam, dOption)
global g_dOption;
g_dOption.bUseGPU = false;
g_dOption.bShowGraph = false;
if(exist('dOption'));
    g_dOption.bUseGPU = dOption.bUseGPU;
    g_dOption.bShowGraph = dOption.bShowGraph;
end;

g_dOption.bUseGPU = true;

dImage = dParam.dImage;
dImage = im2double(dImage);

[nHeight nWidth] = size(dImage);

dExcludeRegion = dParam.dExcludeRegion;
dExcludeMask = zeros(size(dImage));
for(i = 1: length(dExcludeRegion));
    dRegion = dExcludeRegion(i);
    dExcludeMask(dRegion.nTop:dRegion.nBottom, dRegion.nLeft:dRegion.nRight) = 1;
end;
dMask = ~dExcludeMask;

%% Static Pattern Removal
%Exclude big sized region
% dExcludeRegion = dParam.dExcludeRegion;
% nThreshold = dParam.nPatternRemovalThreshold;
% dResultImage = mslCV_BinarizeOutlier(dImage, nThreshold, 'all', dMask);
% nPixelCount = dParam.nPatternRemovalPixelCount;
% dExcludeMask = dExcludeMask | bwareaopen(dResultImage, nPixelCount);

% Exclude region having long length
% Detecting line through image
dResultImage = mslCV_DetectLine(dImage, dMask);

% Expand the region size
nSESize = dParam.nPatternRemovalExpansionSize;
se = strel('disk',nSESize);
dResultImage = imdilate(dResultImage, se);
dExcludeMask = dExcludeMask | dResultImage;
dMask = ~dExcludeMask;

% Adjust Background non-uniformity
dBlock = dParam.nFitBackgroundBlockSize * ones(1, 2);
nDegree = dParam.nFitBackgroundDegree;
nThreshold = dParam.nFitBackgroundThreshold;
dBackgroundImage = mslCV_FitBackground(dImage, dBlock, nDegree, nThreshold, dMask);
nMean = mean2(dBackgroundImage);
dImage = dImage - dBackgroundImage + nMean;

%% Finding Defect
nThreshold = dParam.nBinarizeThreshold;
nDir = dParam.nBinarizeDir;
dResultImage = mslCV_BinarizeOutlier(dImage, nThreshold, nDir, dMask);
        
nPixelCount = dParam.nNoiseRemovalPixelCount;
nPixelMaxCount = dParam.nNoiseRemovalPixelMaxCount;
if(nPixelMaxCount == 0);
    nPixelMaxCount = nHeight * nWidth;
end;
dResultImage = xor(bwareaopen(dResultImage, nPixelCount), bwareaopen(dResultImage, nPixelMaxCount));

if(g_dOption.bShowGraph);
    figure(100);
    subplot(1,3,1);
    imagesc(dParam.dImage);
    subplot(1,3,2);
    imagesc(dImage.*((dMask*0.5)+0.5));
    subplot(1,3,3);
    imagesc(~dMask);
        
    figure(101);
    imshow(dResultImage);
end;





