function dResultImage = mslCV_BinarizeOutlier(dImage, nThreshold, nDir, dMask)
bCheckUpper = true;
bCheckLower = true;
if(exist('nDir'));
    if(nDir < 0);
        bCheckUpper = false;
    elseif(nDir > 0);
        bCheckLower = false;
    end;
end;

if(exist('dMask'));
    nCount = sum(dMask(:));
    dImage = dImage .* dMask;
    nMean = sum(dImage(:)) / nCount;
    
    % Fill the exclude region with mean value;
    dImage(find(dMask == 0)) = nMean;
    
    nStd = sqrt(sum((dImage(:) - nMean).^2) / (nCount -1));
else;
    nMean = mean(dImage(:));
    nStd = std(dImage(:));
end;

dResultImage = zeros(size(dImage));

if(bCheckUpper);
    dResultImage(find(dImage >= (nMean + nThreshold * nStd))) = 1;
end;

if(bCheckLower);
    dResultImage(find(dImage <= (nMean - nThreshold * nStd))) = 1;
end;

