function dResultImage = mslCV_DetectLine(dImage, dMask)
[nSizeY nSizeX] = size(dImage);
nMinLength = min([nSizeY nSizeX]) * 0.8;
[dEdgeImage, th] = edge(dImage, 'canny');
th

if(exist('dMask'));
    dEdgeImage = dEdgeImage & dMask;
end;

[H,T,R] = hough(dEdgeImage);
P  = houghpeaks(H,10,'threshold',ceil(0.3*max(H(:))));
dLines = houghlines(dEdgeImage,T,R,P, 'FillGap', nMinLength, 'MinLength', 3);
dResultImage = zeros(nSizeY, nSizeX);
for i = 1:length(dLines);
    nLength = norm(dLines(i).point1 - dLines(i).point2);
    if ( nLength < nMinLength);
        continue;
    end;
    
    nTheta = dLines(i).theta / 180.0 * pi;
    nRho = dLines(i).rho;
    dX = [];
    if(abs(dLines(i).theta) >= 89); 
        dX = [1:nSizeX];
        dY = round((-cos(nTheta) / sin(nTheta)) * dX + (nRho / sin(nTheta)));
    elseif(abs(dLines(i).theta) <= 1); 
        dY = [1:nSizeY];
        dX = round((-sin(nTheta) / cos(nTheta)) * dY + (nRho / cos(nTheta)));
    end;
    
    for j =1:length(dX);
        nX = dX(j);
        nY = dY(j);
        if(nX >= 1 && nX <= nSizeX && nY >= 1 && nY <= nSizeY);
          dResultImage(dY(j), dX(j)) = 1;
        end;
    end;
end;

nFilterSize=3;
se = strel('disk',nFilterSize);
dResultImage = imdilate(dResultImage, se);

figure(501);
subplot(1,2,1);
imshow(dEdgeImage), hold on
subplot(1,2,2);
imshow(dResultImage);
hold off;


