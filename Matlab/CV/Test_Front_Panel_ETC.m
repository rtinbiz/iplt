clear all;
close all;
format shortg;

%% Data Path
szDataPath = '..\..\Data\SEC\Sample0129\ETC\';

% Data File
%szFileName = '2-4-2-Panel�̹�����.bmp';
%szFileName = '2-4-2-�̹�����.bmp';
szFileName = '2-4-4-��ȣ������.bmp';
%szFileName = '2-4-3-������������.bmp';
%szFileName = '2-4-3-�ĸ���������.bmp';

%% Load Data
szDataFile = strcat(szDataPath, szFileName);
dImage = imread(szDataFile);
dImage = rgb2gray(dImage);

%% Set Options
dOption.bUseGPU = false;
dOption.bShowGraph = true;

%% Set Parameters
dParam.dImage = dImage;
dParam.nBinarizeThreshold = 2;
dParam.nBinarizeDir = -1; % 0:both, 1:upper, -1:lower
dParam.nNoiseRemovalPixelCount = 10;
dParam.nNoiseRemovalPixelMaxCount = min(size(dImage)) * 5;
dParam.nNoiseRemovalOpeningFilterSize = 0;
dParam.nFitBackgroundBlockSize = 32;
dParam.nFitBackgroundDegree = 2;
dParam.nFitBackgroundThreshold = 2;
dParam.nPatternRemovalThreshold = 1.5;
dParam.nPatternRemovalExpansionSize = 15;
dParam.nPatternRemovalPixelCount = 1000;
dParam.nPatternRemovalLengthRate = 0.95;
dParam.dExcludeRegion = [];

% for 2-4-2-Panel�̹�����
% dParam.dExcludeRegion(1).nLeft = 1;
% dParam.dExcludeRegion(1).nRight = 140;
% dParam.dExcludeRegion(1).nTop = 60;
% dParam.dExcludeRegion(1).nBottom = 140;
% tic
% dResultImage = mslCV_DetectDefect(dParam, dOption);
% szDataFile = strcat(szDataFile, '_Result.bmp');
% imwrite(dResultImage,szDataFile,'BMP'); 
% toc

% for 2-4-2-�̹�����, 2-4-4-��ȣ������, 2-4-3-������������
% tic
dResultImage = mslCV_DetectDefect_Bezel(dParam, dOption);
szDataFile = strcat(szDataFile, '_Result.bmp');
imwrite(dResultImage,szDataFile,'BMP'); 
% toc

% for 2-4-3-�ĸ���������
% dParam.dExcludeRegion(1).nLeft = 150;
% dParam.dExcludeRegion(1).nRight = 200;
% dParam.dExcludeRegion(1).nTop = 370;
% dParam.dExcludeRegion(1).nBottom = 420;
% dParam.dExcludeRegion(2).nLeft = 250;
% dParam.dExcludeRegion(2).nRight = 551;
% dParam.dExcludeRegion(2).nTop = 300;
% dParam.dExcludeRegion(2).nBottom = 460;
tic
% dResultImage = mslCV_DetectDefect(dParam, dOption);
% szDataFile = strcat(szDataFile, '_Result.bmp');
% imwrite(dResultImage,szDataFile,'BMP'); 
toc
