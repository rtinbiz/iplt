clear all;
close all;
format shortg;

%% Data Path
szDataPath = '..\..\Data\SEC\Sample0129\ETC\';

% Data File
szFileName = '스크류_체결.bmp';
%szFileName = '스크류_체결2.bmp';
%szFileName = '스크류_미체결.bmp';
%szFileName = '스크류_미체결2.bmp';

%% Load Data
szDataFile = strcat(szDataPath, szFileName);
dImage = imread(szDataFile);
%dImage = rgb2gray(dImage);

figure(2000);  image(dImage);
return;

%% Set Options
dOption.bUseGPU = false;
dOption.bShowGraph = true;

%% Set Parameters
dParam.dImage = dImage;
dParam.nBinarizeThreshold = 1.2;
dParam.nNoiseRemovalPixelCount = 60;
dParam.nNoiseRemovalOpeningFilterSize = 3;
dParam.nSmoothingFilterSize = 30;
dParam.nFitBackgroundDegree = 2;
dParam.nFitBackgroundBlockSize = 32;
dParam.nFitBackgroundThreshold = 2;
dParam.dTargetRegionMargin = 15;

% for 스크래치_25um_100um_하부
dParam.dTargetRegion(1).nLeft = 300;
dParam.dTargetRegion(1).nRight = 400;
dParam.dTargetRegion(1).nTop = 990;
dParam.dTargetRegion(1).nBottom = 1080;

tic
dResultImage = mslCV_DetectScrew(dParam, dOption);
szDataFile = strcat(szDataFile, '_Result.bmp');
imwrite(dResultImage,szDataFile,'BMP'); 
toc
