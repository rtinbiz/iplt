clear all;
close all;
format shortg;

%% Data Path
szDataPath = '..\..\Data\SEC\Sample0129\Front_Bezel\';

% Data File
%szFileName = '스크래치_25um_100um_상부베젤.bmp';
%szFileName = '스크래치_50um_200um_상부베젤.bmp';
%szFileName = '스크래치_100um_400um_상부베젤.bmp';
szFileName = '스크래치_200um_400um_상부베젤.bmp';

%% Load Data
szDataFile = strcat(szDataPath, szFileName);
dImage = imread(szDataFile);
dImage = rgb2gray(dImage);

%figure(1000); imshow(histeq(dImage));

%% Set Options
dOption.bUseGPU = false;
dOption.bShowGraph = true;

%% Set Parameters
dParam.dImage = dImage;
dParam.nBinarizeThreshold = 4;
dParam.nBinarizeDir = 0; % 0:both, 1:upper, -1:lower

dParam.nNoiseRemovalPixelCount = 20;
dParam.nNoiseRemovalPixelMaxCount = min(size(dImage)) * 5;
dParam.nNoiseRemovalOpeningFilterSize = 0;

dParam.nFitBackgroundBlockSize =32;
dParam.nFitBackgroundDegree = 2;
dParam.nFitBackgroundThreshold = 2;

dParam.nPatternRemovalThreshold = 1.5;
dParam.nPatternRemovalExpansionSize = 20;
dParam.nPatternRemovalPixelCount = 1000;
dParam.nPatternRemovalLengthRate = 0.95;

dParam.dExcludeRegion = [];

% for 스크래치_200um_400um_상부베젤
dParam.dExcludeRegion(1).nLeft = 1;
dParam.dExcludeRegion(1).nRight = 1624;
dParam.dExcludeRegion(1).nTop = 1;
dParam.dExcludeRegion(1).nBottom = 580;
dParam.dExcludeRegion(2).nLeft = 1;
dParam.dExcludeRegion(2).nRight = 1624;
dParam.dExcludeRegion(2).nTop = 680;
dParam.dExcludeRegion(2).nBottom = 1234;

tic
dResultImage = mslCV_DetectDefect_Bezel(dParam, dOption);
szDataFile = strcat(szDataFile, '_Result.bmp');
imwrite(dResultImage,szDataFile,'BMP'); 
toc
