clear all;
close all;
format shortg;

%% Data Path
szDataPath = '..\..\Data\SEC\Sample0129\Back\';

% Data File
szFileName = '���_25um_100um_�Ϻ�.bmp';

%% Load Data
szDataFile = strcat(szDataPath, szFileName);
dImage = imread(szDataFile);
dImage = rgb2gray(dImage);

% figure(2000);  image(dImage);
% return;

%% Set Options
dOption.bUseGPU = false;
dOption.bShowGraph = true;

%% Set Parameters
dParam.dImage = dImage;
dParam.nBinarizeDir = 0; % 0:both, 1:upper, -1:lower
dParam.nBinarizeThreshold = 1.2;
dParam.nNoiseRemovalPixelCount = 60;
dParam.nNoiseRemovalOpeningFilterSize = 3;
dParam.nSmoothingFilterSize = 30;
dParam.nFitBackgroundDegree = 2;
dParam.nFitBackgroundBlockSize = 32;
dParam.nFitBackgroundThreshold = 2;
dParam.dExcludeRegion = [];
% dParam.nPatternRemovalThreshold = -0.5;
% dParam.nPatternRemovalExpansionSize = 10;
% dParam.nPatternRemovalPixelCount = 500;
% dParam.nPatternRemovalLengthRate = 0.95;

tic
dResultImage = mslCV_DetectDefect(dParam, dOption);
szDataFile = strcat(szDataFile, '_Result.bmp');
imwrite(dResultImage,szDataFile,'BMP'); 
toc
