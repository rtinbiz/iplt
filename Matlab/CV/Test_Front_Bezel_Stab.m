clear all;
close all;
format shortg;

%% Data Path
szDataPath = '..\..\Data\SEC\Sample0129\Front_Bezel\';

% Data File
%szFileName = '����_25um_100um_��κ���.bmp';
%szFileName = '����_50um_200um_��κ���.bmp';
%szFileName = '����_100um_400um_��κ���.bmp';

%szFileName = '����_200um_400um_��κ���_1.bmp';
%szFileName = '����_200um_400um_��κ���_2.bmp';
szFileName = '����_200um_400um_��κ���_3.bmp';

%% Load Data
szDataFile = strcat(szDataPath, szFileName);
dImage = imread(szDataFile);
dImage = rgb2gray(dImage);

%figure(1000); imshow(histeq(dImage));

%% Set Options
dOption.bUseGPU = false;
dOption.bShowGraph = true;

%% Set Parameters
dParam.dImage = dImage;
dParam.nBinarizeThreshold = 4;
dParam.nBinarizeDir = 0; % 0:both, 1:upper, -1:lower
dParam.nNoiseRemovalPixelCount = 20;
dParam.nNoiseRemovalPixelMaxCount = min(size(dImage)) * 5;
dParam.nNoiseRemovalOpeningFilterSize = 0;
dParam.nFitBackgroundBlockSize = 32;
dParam.nFitBackgroundDegree = 2;
dParam.nFitBackgroundThreshold = 2;
dParam.nPatternRemovalThreshold = 1.5;
dParam.nPatternRemovalExpansionSize = 10;
dParam.nPatternRemovalPixelCount = 1000;
dParam.nPatternRemovalLengthRate = 0.95;
dParam.dExcludeRegion = [];

% for ����_25um_100um_��κ���
% dParam.dExcludeRegion(1).nLeft = 1;
% dParam.dExcludeRegion(1).nRight = 1624;
% dParam.dExcludeRegion(1).nTop = 1;
% dParam.dExcludeRegion(1).nBottom = 400;

% for ����_100um_400um_��κ���
% dParam.dExcludeRegion(1).nLeft = 1;
% dParam.dExcludeRegion(1).nRight = 1624;
% dParam.dExcludeRegion(1).nTop = 1200;
% dParam.dExcludeRegion(1).nBottom = 1234;
% dParam.dExcludeRegion(2).nLeft = 1;
% dParam.dExcludeRegion(2).nRight = 50;
% dParam.dExcludeRegion(2).nTop = 550;
% dParam.dExcludeRegion(2).nBottom = 600;
% dParam.dExcludeRegion(3).nLeft = 1530;
% dParam.dExcludeRegion(3).nRight = 1624;
% dParam.dExcludeRegion(3).nTop = 450;
% dParam.dExcludeRegion(3).nBottom = 630;

% for ����_200um_400um_��κ���_1/2
% dParam.dExcludeRegion(1).nLeft = 1;
% dParam.dExcludeRegion(1).nRight = 1624;
% dParam.dExcludeRegion(1).nTop = 1;
% dParam.dExcludeRegion(1).nBottom = 585;
% dParam.dExcludeRegion(2).nLeft = 1;
% dParam.dExcludeRegion(2).nRight = 1624;
% dParam.dExcludeRegion(2).nTop = 700;
% dParam.dExcludeRegion(2).nBottom = 1234;

% for ����_200um_400um_��κ���_3
dParam.dExcludeRegion(1).nLeft = 1;
dParam.dExcludeRegion(1).nRight = 1624;
dParam.dExcludeRegion(1).nTop = 1;
dParam.dExcludeRegion(1).nBottom = 570;
dParam.dExcludeRegion(2).nLeft = 1;
dParam.dExcludeRegion(2).nRight = 1624;
dParam.dExcludeRegion(2).nTop = 620;
dParam.dExcludeRegion(2).nBottom = 1234;

tic
dResultImage = mslCV_DetectDefect_Bezel(dParam, dOption);
szDataFile = strcat(szDataFile, '_Result.bmp');
imwrite(dResultImage,szDataFile,'BMP'); 
toc
