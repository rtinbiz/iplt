clear all;
close all;
format shortg;

%% Data Path
szDataPath = '..\..\Data\SEC\Sample0129\Back\';

% Data File
%szFileName = '스크래치_25um_100um_하부.bmp';
%szFileName = '스크래치_25um_100um_하부_2.bmp';
%szFileName = '스크래치_50um_200um_하부_1.bmp';
%szFileName = '스크래치_50um_200um_하부_2.bmp';
%szFileName = '스크래치_50um_200um_하부_3.bmp';
%szFileName = '스크래치_100um_400um_하부.bmp';
%szFileName = '스크래치_200um_400um_후면.bmp';
szFileName = '스크래치_200um_400um_후면_2.bmp';


%% Load Data
szDataFile = strcat(szDataPath, szFileName);
dImage = imread(szDataFile);
dImage = rgb2gray(dImage);

% figure(2000);  image(dImage);
% return;

%% Set Options
dOption.bUseGPU = false;
dOption.bShowGraph = true;

%% Set Parameters
dParam.dImage = dImage;
dParam.nSmoothingFilterSize = 3;
dParam.nLengthRate = 0.2;
dParam.nBinarizeThreshold = 1.5;
dParam.nBinarizeDir = 0; % 0:both, 1:upper, -1:lower
dParam.nNoiseRemovalPixelCount = 20;
dParam.nNoiseRemovalPixelMaxCount = min(size(dImage)) * 5;
dParam.nNoiseRemovalOpeningFilterSize = 0;
dParam.nFitBackgroundBlockSize =32;
dParam.nFitBackgroundDegree = 2;
dParam.nFitBackgroundThreshold = 2;
% dParam.nPatternRemovalThreshold = 1.5;
% dParam.nPatternRemovalExpansionSize = 15;
% dParam.nPatternRemovalPixelCount = 1000;
% dParam.nPatternRemovalLengthRate = 0.95;
dParam.dExcludeRegion = [];

% dParam.nPatternRemovalThreshold = -0.5;
% dParam.nPatternRemovalExpansionSize = 10;
% dParam.nPatternRemovalPixelCount = 500;
% dParam.nPatternRemovalLengthRate = 0.95;

% for 스크래치_25um_100um_하부
% dParam.dExcludeRegion(1).nLeft = 150;
% dParam.dExcludeRegion(1).nRight = 530;
% dParam.dExcludeRegion(1).nTop = 520;
% dParam.dExcludeRegion(1).nBottom = 920;

% for 스크래치_25um_100um_하부_2
% dParam.dExcludeRegion(1).nLeft = 190;
% dParam.dExcludeRegion(1).nRight = 590;
% dParam.dExcludeRegion(1).nTop = 670;
% dParam.dExcludeRegion(1).nBottom = 1030;

% for 스크래치_50um_200um_하부_1
% dParam.dExcludeRegion(1).nLeft = 540;
% dParam.dExcludeRegion(1).nRight = 720;
% dParam.dExcludeRegion(1).nTop = 600;
% dParam.dExcludeRegion(1).nBottom = 790;
% dParam.dExcludeRegion(2).nLeft = 330;
% dParam.dExcludeRegion(2).nRight = 940;
% dParam.dExcludeRegion(2).nTop = 1020;
% dParam.dExcludeRegion(2).nBottom = 1234;
% dParam.dExcludeRegion(3).nLeft = 1;
% dParam.dExcludeRegion(3).nRight = 120;
% dParam.dExcludeRegion(3).nTop = 1;
% dParam.dExcludeRegion(3).nBottom = 720;

% for 스크래치_50um_200um_하부_2/3
% dParam.dExcludeRegion(1).nLeft = 520;
% dParam.dExcludeRegion(1).nRight = 730;
% dParam.dExcludeRegion(1).nTop = 580;
% dParam.dExcludeRegion(1).nBottom = 800;
% dParam.dExcludeRegion(2).nLeft = 330;
% dParam.dExcludeRegion(2).nRight = 940;
% dParam.dExcludeRegion(2).nTop = 1000;
% dParam.dExcludeRegion(2).nBottom = 1234;
% dParam.dExcludeRegion(3).nLeft = 1;
% dParam.dExcludeRegion(3).nRight = 120;
% dParam.dExcludeRegion(3).nTop = 1;
% dParam.dExcludeRegion(3).nBottom = 720;

% for 스크래치_100um_400um_하부
% dParam.dExcludeRegion(1).nLeft = 890;
% dParam.dExcludeRegion(1).nRight = 990;
% dParam.dExcludeRegion(1).nTop = 660;
% dParam.dExcludeRegion(1).nBottom = 760;
% dParam.dExcludeRegion(2).nLeft = 760;
% dParam.dExcludeRegion(2).nRight = 1080;
% dParam.dExcludeRegion(2).nTop = 870;
% dParam.dExcludeRegion(2).nBottom = 1234;
% dParam.dExcludeRegion(3).nLeft = 1;
% dParam.dExcludeRegion(3).nRight = 700;
% dParam.dExcludeRegion(3).nTop = 1;
% dParam.dExcludeRegion(3).nBottom = 800;
% dParam.dExcludeRegion(4).nLeft = 670;
% dParam.dExcludeRegion(4).nRight = 750;
% dParam.dExcludeRegion(4).nTop = 1190;
% dParam.dExcludeRegion(4).nBottom = 1234;

% for 스크래치_200um_400um_후면
dParam.dExcludeRegion(1).nLeft = 1;
dParam.dExcludeRegion(1).nRight = 200;
dParam.dExcludeRegion(1).nTop = 1;
dParam.dExcludeRegion(1).nBottom = 1234;
dParam.dExcludeRegion(2).nLeft = 200;
dParam.dExcludeRegion(2).nRight = 570;
dParam.dExcludeRegion(2).nTop = 1;
dParam.dExcludeRegion(2).nBottom = 650;
dParam.dExcludeRegion(3).nLeft = 610;
dParam.dExcludeRegion(3).nRight = 660;
dParam.dExcludeRegion(3).nTop = 70;
dParam.dExcludeRegion(3).nBottom = 100;
dParam.dExcludeRegion(4).nLeft = 1570;
dParam.dExcludeRegion(4).nRight = 1624;
dParam.dExcludeRegion(4).nTop = 70;
dParam.dExcludeRegion(4).nBottom = 110;
dParam.dExcludeRegion(5).nLeft = 670;
dParam.dExcludeRegion(5).nRight = 730;
dParam.dExcludeRegion(5).nTop = 570;
dParam.dExcludeRegion(5).nBottom = 630;
dParam.dExcludeRegion(6).nLeft = 560;
dParam.dExcludeRegion(6).nRight = 800;
dParam.dExcludeRegion(6).nTop = 680;
dParam.dExcludeRegion(6).nBottom = 1020;
dParam.dExcludeRegion(7).nLeft = 1;
dParam.dExcludeRegion(7).nRight = 1624;
dParam.dExcludeRegion(7).nTop = 1;
dParam.dExcludeRegion(7).nBottom = 300;
dParam.dExcludeRegion(8).nLeft = 1624;
dParam.dExcludeRegion(8).nRight = 1624;
dParam.dExcludeRegion(8).nTop = 1;
dParam.dExcludeRegion(8).nBottom = 1234;
dParam.dExcludeRegion(9).nLeft = 1;
dParam.dExcludeRegion(9).nRight = 1624;
dParam.dExcludeRegion(9).nTop = 1230;
dParam.dExcludeRegion(9).nBottom = 1234;

tic
dResultImage = mslCV_DetectDefect_Scratch(dParam, dOption);
szDataFile = strcat(szDataFile, '_Result.bmp');
imwrite(dResultImage,szDataFile,'BMP'); 
toc
