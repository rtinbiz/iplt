function dResultImage = mslCV_DetectDefect_Scratch(dParam, dOption)
global g_dOption;
g_dOption.bUseGPU = false;
g_dOption.bShowGraph = false;
if(exist('dOption'));
    g_dOption.bUseGPU = dOption.bUseGPU;
    g_dOption.bShowGraph = dOption.bShowGraph;
end;

dImage = dParam.dImage;
dImage = im2double(dImage);

%% Smoothing
nFilterSize=dParam.nSmoothingFilterSize;
if(nFilterSize > 1); 
    dFilter=(1/(nFilterSize^2))*ones(nFilterSize, nFilterSize);
    dImage=imfilter(dImage,dFilter,'replicate','conv');
end;

dExcludeRegion = dParam.dExcludeRegion;
dExcludeMask = zeros(size(dImage));
for(i = 1: length(dExcludeRegion));
    dRegion = dExcludeRegion(i);
    dExcludeMask(dRegion.nTop:dRegion.nBottom, dRegion.nLeft:dRegion.nRight) = 1;
end;
dMask = ~dExcludeMask;

% Adjust Background non-uniformity
dBlock = dParam.nFitBackgroundBlockSize * ones(1, 2);
nDegree = dParam.nFitBackgroundDegree;
nThreshold = dParam.nFitBackgroundThreshold;
dBackgroundImage = mslCV_FitBackground(dImage, dBlock, nDegree, nThreshold, dMask);
nMean = mean2(dBackgroundImage);
dImage = dImage - dBackgroundImage + nMean;

nThreshold = dParam.nBinarizeThreshold;
nDir = dParam.nBinarizeDir;
dResultImage = mslCV_BinarizeOutlier(dImage, nThreshold, nDir, dMask);

% Apply median filter to remove noise
% if(g_dOption.bUseGPU);
%     dImage = gpuArray(dImage);
% end;

% Remove small regions
nPixelCount = dParam.nNoiseRemovalPixelCount;
dResultImage = bwareaopen(dResultImage, nPixelCount);

% Leave regions having 'scratch' shape (long length)
nLengthRate = dParam.nLengthRate;
if(nLengthRate > 0);
    dLabel = bwlabel(dResultImage);
    dStat = regionprops(dLabel, 'all');
    dIndex = find([dStat.MinorAxisLength]./[dStat.MajorAxisLength] <= nLengthRate);
    dResultImage = ismember(dLabel, dIndex);
end;

if(g_dOption.bShowGraph);
    figure(101);
    subplot(1,3,1);
    imagesc(dParam.dImage);
    subplot(1,3,2);
    imagesc(dImage.*((dMask*0.5)+0.5));
    subplot(1,3,3);
    imagesc((~dMask)*255 + double(dResultImage)*128);
    
    figure(102);
    imshow(dResultImage);
end;





