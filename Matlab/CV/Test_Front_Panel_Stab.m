clear all;
close all;
format shortg;

%% Data Path
szDataPath = '..\..\Data\SEC\Sample0129\Front_Panel\';

% Data File
%szFileName = '����_50um_200um_��Ƽ��.bmp';
%szFileName = '����_100um_400um_��Ƽ��.bmp';
szFileName = '����_200um_400um_��Ƽ��_����.bmp';


%% Load Data
szDataFile = strcat(szDataPath, szFileName);
dImage = imread(szDataFile);
dImage = rgb2gray(dImage);

%% Set Options
dOption.bUseGPU = false;
dOption.bShowGraph = true;

%% Set Parameters
dParam.dImage = dImage;
dParam.nBinarizeThreshold = 4;
dParam.nBinarizeDir = 0; % 0:both, 1:upper, -1:lower
dParam.nNoiseRemovalPixelCount = 20;
dParam.nNoiseRemovalPixelMaxCount = min(size(dImage)) * 5;
dParam.nNoiseRemovalOpeningFilterSize = 0;
dParam.nFitBackgroundBlockSize =32;
dParam.nFitBackgroundDegree = 2;
dParam.nFitBackgroundThreshold = 2;
% dParam.nPatternRemovalThreshold = 1.5;
% dParam.nPatternRemovalExpansionSize = 15;
% dParam.nPatternRemovalPixelCount = 1000;
% dParam.nPatternRemovalLengthRate = 0.95;
dParam.dExcludeRegion = [];

% for ����_100um_400um_��Ƽ��
% dParam.dExcludeRegion(1).nLeft = 1;
% dParam.dExcludeRegion(1).nRight = 1624;
% dParam.dExcludeRegion(1).nTop = 1;
% dParam.dExcludeRegion(1).nBottom = 50;
% dParam.dExcludeRegion(2).nLeft = 1;
% dParam.dExcludeRegion(2).nRight = 1624;
% dParam.dExcludeRegion(2).nTop = 1170;
% dParam.dExcludeRegion(2).nBottom = 1234;
% dParam.dExcludeRegion(3).nLeft = 1570;
% dParam.dExcludeRegion(3).nRight = 1624;
% dParam.dExcludeRegion(3).nTop = 1;
% dParam.dExcludeRegion(3).nBottom = 1234;
% dParam.dExcludeRegion(4).nLeft = 1;
% dParam.dExcludeRegion(4).nRight = 50;
% dParam.dExcludeRegion(4).nTop = 1;
% dParam.dExcludeRegion(4).nBottom = 1234;

% for ����_200um_400um_��Ƽ��_����
dParam.dExcludeRegion(1).nLeft = 1;
dParam.dExcludeRegion(1).nRight = 1624;
dParam.dExcludeRegion(1).nTop = 1;
dParam.dExcludeRegion(1).nBottom = 270;
dParam.dExcludeRegion(2).nLeft = 1;
dParam.dExcludeRegion(2).nRight = 360;
dParam.dExcludeRegion(2).nTop = 1;
dParam.dExcludeRegion(2).nBottom = 1234;
dParam.dExcludeRegion(3).nLeft = 1;
dParam.dExcludeRegion(3).nRight = 1624;
dParam.dExcludeRegion(3).nTop = 1190;
dParam.dExcludeRegion(3).nBottom = 1234;
dParam.dExcludeRegion(4).nLeft = 1624;
dParam.dExcludeRegion(4).nRight = 1624;
dParam.dExcludeRegion(4).nTop = 1;
dParam.dExcludeRegion(4).nBottom = 1234;

tic
dResultImage = mslCV_DetectDefect(dParam, dOption);
szDataFile = strcat(szDataFile, '_Result.bmp');
imwrite(dResultImage,szDataFile,'BMP'); 
toc
