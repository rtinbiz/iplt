clear all;
close all;
format shortg;

%% Data Path
szDataPath = '..\..\Data\SEC\Sample0129\Back\';

% Data File
% szFileName = '����_25um_100um_�Ϻ�_1.bmp';
%szFileName = '����_25um_100um_�Ϻ�_2.bmp';
%szFileName = '����_25um_100um_�Ϻ�_3.bmp';
%szFileName = '����_50um_200um_�Ϻ�.bmp';
%szFileName = '����_50um_200um_�Ϻ�_2.bmp';
szFileName = '����_100um_400um_�Ϻ�.bmp';

%% Load Data
szDataFile = strcat(szDataPath, szFileName);
dImage = imread(szDataFile);
dImage = rgb2gray(dImage);

% figure(2000);  image(dImage);
% return;

%% Set Options
dOption.bUseGPU = false;
dOption.bShowGraph = true;

%% Set Parameters
dParam.dImage = dImage;
dParam.nBinarizeDir = 0; % 0:both, 1:upper, -1:lower
dParam.nBinarizeThreshold = 3;
dParam.nNoiseRemovalPixelCount = 15;
dParam.nSmoothingFilterSize = 0;
dParam.nFitBackgroundDegree = 2;
dParam.nFitBackgroundBlockSize = 32;
dParam.nFitBackgroundThreshold = 2;
dParam.nNoiseRemovalOpeningFilterSize = 0;

% dParam.nPatternRemovalThreshold = -0.5;
% dParam.nPatternRemovalExpansionSize = 10;
% dParam.nPatternRemovalPixelCount = 500;
% dParam.nPatternRemovalLengthRate = 0.95;

% for ����_25um_100um_�Ϻ�_1
% dParam.dExcludeRegion(1).nLeft = 930;
% dParam.dExcludeRegion(1).nRight = 1624;
% dParam.dExcludeRegion(1).nTop = 1;
% dParam.dExcludeRegion(1).nBottom = 1234;

% for ����_25um_100um_�Ϻ�_2
% dParam.dExcludeRegion(1).nLeft = 800;
% dParam.dExcludeRegion(1).nRight = 1624;
% dParam.dExcludeRegion(1).nTop = 1;
% dParam.dExcludeRegion(1).nBottom = 1234;

% for ����_25um_100um_�Ϻ�_3
% dParam.dExcludeRegion(1).nLeft = 850;
% dParam.dExcludeRegion(1).nRight = 1624;
% dParam.dExcludeRegion(1).nTop = 1;
% dParam.dExcludeRegion(1).nBottom = 1234;

% for ����_50um_200um_�Ϻ�
% dParam.dExcludeRegion(1).nLeft = 920;
% dParam.dExcludeRegion(1).nRight = 1624;
% dParam.dExcludeRegion(1).nTop = 1;
% dParam.dExcludeRegion(1).nBottom = 1010;
% dParam.dExcludeRegion(2).nLeft = 1;
% dParam.dExcludeRegion(2).nRight = 910;
% dParam.dExcludeRegion(2).nTop = 1020;
% dParam.dExcludeRegion(2).nBottom = 1234;
% dParam.dExcludeRegion(3).nLeft = 1420;
% dParam.dExcludeRegion(3).nRight = 1610;
% dParam.dExcludeRegion(3).nTop = 1110;
% dParam.dExcludeRegion(3).nBottom = 1234;

% for ����_50um_200um_�Ϻ�_2
% dParam.dExcludeRegion(1).nLeft = 800;
% dParam.dExcludeRegion(1).nRight = 1624;
% dParam.dExcludeRegion(1).nTop = 1;
% dParam.dExcludeRegion(1).nBottom = 850;
% dParam.dExcludeRegion(2).nLeft = 1;
% dParam.dExcludeRegion(2).nRight = 860;
% dParam.dExcludeRegion(2).nTop = 850;
% dParam.dExcludeRegion(2).nBottom = 1234;
% dParam.dExcludeRegion(3).nLeft = 1300;
% dParam.dExcludeRegion(3).nRight = 1510;
% dParam.dExcludeRegion(3).nTop = 930;
% dParam.dExcludeRegion(3).nBottom = 1150;

% for ����_100um_400um_�Ϻ�
dParam.dExcludeRegion(1).nLeft = 700;
dParam.dExcludeRegion(1).nRight = 1624;
dParam.dExcludeRegion(1).nTop = 1;
dParam.dExcludeRegion(1).nBottom = 780;
dParam.dExcludeRegion(2).nLeft = 1;
dParam.dExcludeRegion(2).nRight = 740;
dParam.dExcludeRegion(2).nTop = 740;
dParam.dExcludeRegion(2).nBottom = 1234;
dParam.dExcludeRegion(3).nLeft = 960;
dParam.dExcludeRegion(3).nRight = 1060;
dParam.dExcludeRegion(3).nTop = 810;
dParam.dExcludeRegion(3).nBottom = 910;
dParam.dExcludeRegion(4).nLeft = 1;
dParam.dExcludeRegion(4).nRight = 60;
dParam.dExcludeRegion(4).nTop = 1;
dParam.dExcludeRegion(4).nBottom = 860;

tic
dResultImage = mslCV_DetectDefect(dParam, dOption);
szDataFile = strcat(szDataFile, '_Result.bmp');
imwrite(dResultImage,szDataFile,'BMP'); 
toc
