function dResultImage = mslCV_DetectDefect(dParam, dOption)
global g_dOption;
g_dOption.bUseGPU = false;
g_dOption.bShowGraph = false;
if(exist('dOption'));
    g_dOption.bUseGPU = dOption.bUseGPU;
    g_dOption.bShowGraph = dOption.bShowGraph;
end;

dImage = dParam.dImage;
dImage = im2double(dImage);

dExcludeRegion = dParam.dExcludeRegion;
dExcludeMask = zeros(size(dImage));
for(i = 1: length(dExcludeRegion));
    dRegion = dExcludeRegion(i);
    dExcludeMask(dRegion.nTop:dRegion.nBottom, dRegion.nLeft:dRegion.nRight) = 1;
end;
dMask = ~dExcludeMask;

% Adjust Background non-uniformity
if(dParam.nFitBackgroundBlockSize > 0);
    dBlock = dParam.nFitBackgroundBlockSize * ones(1, 2);
    nDegree = dParam.nFitBackgroundDegree;
    nThreshold = dParam.nFitBackgroundThreshold;
    dBackgroundImage = mslCV_FitBackground(dImage, dBlock, nDegree, nThreshold, dMask);
    nMean = mean2(dBackgroundImage);
    dImage = dImage - dBackgroundImage + nMean;
end;

nThreshold = dParam.nBinarizeThreshold;
nDir = dParam.nBinarizeDir;
dResultImage = mslCV_BinarizeOutlier(dImage, nThreshold, nDir, dMask);

% Apply median filter to remove noise
% if(g_dOption.bUseGPU);
%     dImage = gpuArray(dImage);
% end;
 
nFilterSize = dParam.nNoiseRemovalOpeningFilterSize;
if(nFilterSize > 1);
    se = strel('disk',nFilterSize);
    dResultImage = imclose(dResultImage, se);
    dResultImage = imopen(dResultImage, se);
end;

nPixelCount = dParam.nNoiseRemovalPixelCount;
dResultImage = bwareaopen(dResultImage, nPixelCount);

if(g_dOption.bShowGraph);
    subplot(1,3,1);
    imagesc(dParam.dImage);
    subplot(1,3,2);
    imagesc(dImage.*((dMask*0.5)+0.5));
    subplot(1,3,3);
    imagesc(~dMask);
    
    figure(101);
    imshow(dResultImage);
end;
