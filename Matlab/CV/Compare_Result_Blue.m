% This for Blue

clear;

dCData = [
4.6 	5.1 	70.2 	24.5 	4.2 	3.9 	64.1 	24.0 	7.6 	6.4 	90.6 	25.0 
4.6 	3.3 	36.7 	13.2 	4.5 	4.4 	39.1 	12.7 	6.9 	6.0 	87.0 	25.0 
5.2 	4.2 	70.4 	25.4 	4.5 	4.0 	68.3 	24.9 	7.5 	7.7 	83.3 	25.1 
4.6 	2.5 	48.2 	15.1 	4.6 	3.5 	41.3 	14.0 	6.2 	5.7 	86.7 	25.0 
5.0 	4.1 	63.9 	26.0 	4.3 	4.1 	66.7 	24.8 	6.5 	6.3 	87.6 	25.2 
3.8 	3.8 	40.2 	12.7 	3.5 	3.8 	41.0 	12.5 	6.5 	6.5 	86.7 	25.1 
4.4 	5.1 	63.4 	25.4 	5.3 	3.8 	64.4 	24.7 	7.7 	6.5 	92.4 	25.2 
3.5 	4.0 	43.0 	14.4 	5.4 	2.6 	38.7 	13.0 	7.1 	5.7 	80.9 	25.4 
];
dCData(:, 3:4:end) = []; % remove 'Rib Width' data
dCData(:, 3:3:end) = []; % remove 'Rib Height' data

dCData_Dummy = dCData(1:2:end, :);
dCData_Active = dCData(2:2:end, :);

dData = [
4.9 	4.1 	26.1 	24.4 	5.2 	5.3 	25.3 	23.9 	7.1 	5.9 	45.4 	24.9 
3.0 	2.5 	24.9 	13.2 	3.3 	3.2 	25.7 	13.1 	5.7 	6.5 	43.5 	25.2 
5.2 	6.6 	23.7 	25.0 	4.5 	5.4 	26.4 	25.0 	6.2 	8.0 	47.9 	25.1 
3.3 	2.3 	25.7 	12.5 	3.4 	3.2 	24.5 	12.9 	5.5 	6.1 	42.9 	24.8 
4.9 	4.0 	26.8 	24.7 	3.7 	4.9 	25.7 	25.0 	7.2 	6.9 	46.8 	24.3 
3.8 	3.5 	25.3 	13.0 	3.0 	4.3 	25.7 	13.9 	6.5 	7.0 	43.7 	25.6 
3.9 	5.5 	26.8 	24.3 	3.0 	4.3 	26.1 	25.1 	6.7 	6.2 	46.8 	25.0 
3.4 	3.3 	26.1 	14.2 	4.8 	3.2 	25.3 	13.0 	5.3 	5.2 	44.6 	25.3
];
dData(:, 3:4:end) = []; % remove 'Rib Width' data
dData(:, 3:3:end) = []; % remove 'Rib Height' data

dData_Dummy = dData(1:2:end, :);
dData_Active = dData(2:2:end, :);

dR = corrcoef(dData(:), dCData(:));
dR_Dummy = corrcoef(dData_Dummy(:), dCData_Dummy(:));
dR_Active = corrcoef(dData_Active(:), dCData_Active(:));

szResult = sprintf('%.3f %.3f %.3f', dR(1,2), dR_Dummy(1,2), dR_Active(1,2));
disp(szResult);

clf;
figure(1);
hold on;
plot(dData(:), dCData(:), 'r.');
dX = [0:max(dData(:))];
plot(dX, dX, 'b');
hold off;

[H, P] = ttest2(dData(:), dCData(:));
[H_Dummy, P_Dummy] = ttest2(dData_Dummy(:), dCData_Dummy(:));
[H_Active, P_Active] = ttest2(dData_Active(:), dCData_Active(:));
szResult = sprintf('%.3f(%d) %.3f(%d) %.3f(%d)', P, H, P_Dummy, H_Dummy, P_Active, H_Active);
disp(szResult);
