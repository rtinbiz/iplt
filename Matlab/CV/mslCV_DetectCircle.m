function dResultImage = mslCV_DetectCircle(dImage, nLength)
[nSizeY nSizeX] = size(dImage);

 [accum, circen, cirrad] = CircularHough_Grd(dImage, [100 600]);
 toc;
 figure(1); imagesc(accum); axis image;
 title('Accumulation Array from Circular Hough Transform');
 figure(2); imagesc(dImage); colormap('gray'); axis image;
 hold on;
 plot(circen(:,1), circen(:,2), 'r+');
 for k = 1 : size(circen, 1),
     DrawCircle(circen(k,1), circen(k,2), cirrad(k), 32, 'b-');
 end
 hold off;
 title(['Raw Image with Circles Detected ', ...
     '(center positions and radii marked)']);
 figure(3); surf(accum, 'EdgeColor', 'none'); axis ij;
 title('3-D View of the Accumulation Array');

% 
% figure(501);
% subplot(2,1,1);
% imshow(dEdgeImage), hold on
% subplot(2,1,2);
% imshow(dResultImage);
% hold off;


