
// NIPLTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NIPLTest.h"
#include "NIPLTestDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CNIPLTestDlg dialog



CNIPLTestDlg::CNIPLTestDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CNIPLTestDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CNIPLTestDlg::~CNIPLTestDlg()
{
	ReleaseNIPLib();
}


void CNIPLTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CNIPLTestDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_TEST, &CNIPLTestDlg::OnBnClickedButtonTest)
END_MESSAGE_MAP()


// CNIPLTestDlg message handlers

BOOL CNIPLTestDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	LoadNIPJob();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CNIPLTestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CNIPLTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CNIPLTestDlg::LoadNIPJob()
{
	wstring strJobFile = L"DetectDefect_MagneticCore.nip";

	NIPO *pNIPO = NIPO::GetInstance();
	if (!pNIPO->LoadJob(strJobFile, m_dJob)) {
		// check fail
	}
}

void CNIPLTestDlg::ReleaseNIPLib()
{
	NIPO::ReleaseInstance();
}

void CNIPLTestDlg::OnBnClickedButtonTest()
{
	// TODO: Add your control notification handler code here
	NIPO *pNIPO = NIPO::GetInstance();

	if (!pNIPO->RunJob(&m_dJob)) {
		return;
	}

	NIPLOutput dOutput;
	if (!pNIPO->GetOutput(L"Output", dOutput)) {
		return;
	}

	if (dOutput.m_pResult == nullptr) {
		return;
	}

	NIPLResult_FindBlob *pResult = (NIPLResult_FindBlob *)dOutput.m_pResult;
	for (auto &dBlob : pResult->m_listBlob) {
		TRACE("\nRect(%d, %d, %d, %d), Size(%d) ",
			dBlob.m_dBoundingBox.x, dBlob.m_dBoundingBox.y, dBlob.m_dBoundingBox.x + dBlob.m_dBoundingBox.width, dBlob.m_dBoundingBox.y + dBlob.m_dBoundingBox.height,
			dBlob.m_nSize);
	}
}
