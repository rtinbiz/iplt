#pragma once

#include "CommonOpenCV.h"

struct AFX_EXT_CLASS NIPLResult
{
	wstring m_strType;

	NIPLResult() {
	}
	virtual ~NIPLResult() {
	}

	virtual bool IsType(wstring strType) {
		return (m_strType == strType);
	}
};
