// NIPL.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include <codecvt>

#include <clocale>
#include <locale>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

inline int clamp(int value, int low, int high)
{
	return value < low ? low : (value > high ? high : value);
}

string wstr2str(wstring ws)
{
	setlocale(LC_ALL, "");
	locale locale("");
	typedef std::codecvt<wchar_t, char, std::mbstate_t> converter_type;
	const converter_type& converter = std::use_facet<converter_type>(locale);
	vector<char> to(ws.length() * converter.max_length());
	mbstate_t state;
	const wchar_t* from_next;
	char* to_next;
	const converter_type::result result = converter.out(state, ws.data(), ws.data() + ws.length(), from_next, &to[0], &to[0] + to.size(), to_next);
	if (result == converter_type::ok || result == converter_type::noconv) {
		string s(&to[0], to_next);
		return s;
	}

	return "";
}

NIPL *NIPL::pThis = 0x00;

NIPL *NIPL::GetInstance(BOOL bNew)
{
	if(bNew) {
		return new NIPL();
	}

	if(pThis == 0x00) {
		pThis = new NIPL();
	}

	return pThis;
}

void NIPL::ReleaseInstance(NIPL *pThat)
{
	if(pThat != 0x00) {
		delete pThat;
	}
	else if(pThis != 0x00) {
		delete pThis;
		pThis = 0x00;
	}
}

NIPL::NIPL()
{
	m_pOption = nullptr;
}

NIPL::~NIPL()
{
}

void NIPL::SetOption(NIPLOption *pOption)
{
	m_pOption = pOption;
}

NIPL_ERR NIPL::LoadImage(wstring strPath, int nFlags, Mat &dImg)
{
	return LoadImage(wstr2str(strPath), nFlags, dImg);
}

NIPL_ERR NIPL::LoadImage(string strPath, int nFlags, Mat &dImg)
{
	dImg = imread(strPath, nFlags);

	if(CHECK_EMPTY_IMAGE(dImg)) {
		return NIPL_ERR_FAIL_TO_LOAD_IMAGE;
	} 

	return NIPL_ERR_SUCCESS;
}

NIPL_ERR NIPL::SaveImage(wstring strPath, Mat dImg)
{
	return SaveImage(wstr2str(strPath), dImg);
}

NIPL_ERR NIPL::SaveImage(string strPath, Mat dImg)
{
	if (!imwrite(strPath, dImg)) {
		return NIPL_ERR_FAIL_TO_SAVE_IMAGE;
	}

	return NIPL_ERR_SUCCESS;
}

void NIPL::DebugPrintImageProperty(wstring strTitle, Mat dImg)
{
	DebugPrintImageProperty(wstr2str(strTitle), dImg);
}

void NIPL::DebugPrintImageProperty(string strTitle, Mat dImg)
{
	int depth = dImg.depth();
	int channels = dImg.channels();
	double nMin, nMax;
	minMaxLoc(dImg, &nMin, &nMax);
	char szText[256];
	sprintf_s(szText, "\n[NIPL] %s - depth : %d, channels : %d, nMin : %.3f, nMax : %.3f ", strTitle.c_str(), depth, channels, nMin, nMax);
	OutputDebugStringA(szText);
}

void NIPL::ShowErrorDesc(wstring strFunc, wstring strMsg)
{
	ShowErrorDesc(wstr2str(strFunc), wstr2str(strMsg));
}

void NIPL::ShowErrorDesc(string strFunc, string strMsg)
{
	string strErrDesc = "\n[NIPL] Exception - " + strFunc + " : " + strMsg;
	OutputDebugStringA(strErrDesc.c_str());
}

NIPL_METHOD_IMPL(CreateImage)
{
	VERIFY_PARAMS();
	NIPLParam_CreateImage *pParam_CreateImage = (NIPLParam_CreateImage *)pInput->m_pParam;

	int nType = pParam_CreateImage->m_nType;
	int nWidth = pParam_CreateImage->m_nWidth;
	int nHeight = pParam_CreateImage->m_nHeight;
	float nValue = pParam_CreateImage->m_nValue;

	if (nWidth <= 0 || nHeight <= 0) {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}

	Mat dOutputImg;
	switch (nType) {
	case NIPLParam_CreateImage::TYPE_GRAY_UINT8:
		dOutputImg = Mat(nHeight, nWidth, CV_8UC1, cvScalar(nValue));
		break;
	case NIPLParam_CreateImage::TYPE_GRAY_FLOAT:
		dOutputImg = Mat(nHeight, nWidth, CV_32FC1, cvScalar(nValue));
		break;
	case NIPLParam_CreateImage::TYPE_COLOR_UINT8:
		dOutputImg = Mat(nHeight, nWidth, CV_8UC3, cvScalar(nValue, nValue, nValue));
		break;
	}

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}


NIPL_METHOD_IMPL(Color2Gray)
{
	VERIFY_PARAMS();
	NIPLParam_Color2Gray *pParam_Color2Gray = (NIPLParam_Color2Gray *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	int nGrayLevel = pParam_Color2Gray->m_nGrayLevel;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	// assume that a color image has CV_8U depth(256 level) and 3 channels
	if (dImg.depth() != CV_8U || dImg.channels() != 3) {
		return NIPL_ERR_PASS;
	}

	Mat dOutputImg;
	if (nGrayLevel == NIPLParam_Color2Gray::GRAYLEVEL_256) {
		cvtColor(dImg, dOutputImg, CV_BGR2GRAY);
		pOutput->m_dImg = dOutputImg;
	}
	else if (nGrayLevel == NIPLParam_Color2Gray::GRAYLEVEL_FLOAT) {
		dImg.copyTo(dOutputImg);
		dOutputImg.convertTo(dOutputImg, CV_32F);
		dOutputImg *= (1.f / 255.f);
		cvtColor(dOutputImg, dOutputImg, CV_BGR2GRAY);

		pOutput->m_dImg = dOutputImg;
	}

	if (CHECK_EMPTY_IMAGE(pOutput->m_dImg)) {
		return NIPL_ERR_PASS;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(Invert)
{
	VERIFY_PARAMS();
	NIPLParam_Invert *pParam_Invert = (NIPLParam_Invert *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	Mat dOutputImg;
	bitwise_not(dImg, dOutputImg);

	pOutput->m_dImg = dOutputImg;

	if (CHECK_EMPTY_IMAGE(pOutput->m_dImg)) {
		return NIPL_ERR_PASS;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(Operate)
{
	VERIFY_PARAMS();
	NIPLParam_Operate *pParam_Operate = (NIPLParam_Operate *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	int nMethod = pParam_Operate->m_nMethod;
	Mat dTargetImg = pParam_Operate->m_dTargetImg;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if (dImg.size != dTargetImg.size) {
		return NIPL_ERR_FAIL_NOT_MATCH_IMAGE_SIZE;
	}

	Mat dImg2 = dTargetImg;
	if (dImg.type() != dImg2.type()) {
		CHECK_EXCEPTION(dImg2.convertTo(dImg2, dImg.type()));
	}

	Mat dOutputImg;
	if (nMethod == NIPLParam_Operate::METHOD_ADD) {
		dOutputImg = dImg + dImg2;
	}
	else if (nMethod == NIPLParam_Operate::METHOD_SUBTRACT) {
		dOutputImg = dImg - dImg2;
	}
	else if (nMethod == NIPLParam_Operate::METHOD_AND) {
		dOutputImg = (dImg & dImg2);
	}
	else if (nMethod == NIPLParam_Operate::METHOD_OR) {
		dOutputImg = (dImg | dImg2);
	}
	else if (nMethod == NIPLParam_Operate::METHOD_XOR) {
		dOutputImg = (dImg != dImg2);
	}

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(CopySubImageFrom)
{
	VERIFY_PARAMS();
	NIPLParam_CopySubImageFrom *pParam_CopySubImageFrom = (NIPLParam_CopySubImageFrom *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	int nStartPosX = pParam_CopySubImageFrom->m_nStartPosX;
	int nStartPosY = pParam_CopySubImageFrom->m_nStartPosY;
	int nEndPosX = pParam_CopySubImageFrom->m_nEndPosX;
	int nEndPosY = pParam_CopySubImageFrom->m_nEndPosY;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if (nStartPosX < 0 || nEndPosX >= nImgSizeX || nStartPosY < 0 || nEndPosY >= nImgSizeY || nStartPosX > nEndPosX || nStartPosY > nEndPosY) {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}

//	DebugPrintImageProperty(L"CopySubImageFrom Input", dImg);

	int nSubImageWidth = nEndPosX - nStartPosX + 1;
	int nSubImageHeight = nEndPosY - nStartPosY + 1;
	Mat dOutputImg(dImg, Rect(nStartPosX, nStartPosY, nSubImageWidth, nSubImageHeight));

//	DebugPrintImageProperty(L"CopySubImageFrom Output", dOutputImg);

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(CopySubImageTo)
{
	VERIFY_PARAMS();
	NIPLParam_CopySubImageTo *pParam_CopySubImageTo = (NIPLParam_CopySubImageTo *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	Mat dTargetImg = pParam_CopySubImageTo->m_dTargetImg;
	int nStartPosX = pParam_CopySubImageTo->m_nStartPosX;
	int nStartPosY = pParam_CopySubImageTo->m_nStartPosY;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	int nEndPosX = nStartPosX + nImgSizeX - 1;
	int nEndPosY = nStartPosY + nImgSizeY - 1;

	int nTargetImgSizeY = dTargetImg.rows;
	int nTargetImgSizeX = dTargetImg.cols;

	if (nStartPosX < 0 || nEndPosX >= nTargetImgSizeX || nStartPosY < 0 || nEndPosY >= nTargetImgSizeY || nStartPosX > nEndPosX || nStartPosY > nEndPosY) {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}

	Mat dOutputImg;
	dTargetImg.copyTo(dOutputImg);

	Mat dSubImg = dImg;
	if (dSubImg.type() != dOutputImg.type()) {
		CHECK_EXCEPTION(dSubImg.convertTo(dSubImg, dOutputImg.type()));
	}

	Mat dROI(dOutputImg, Rect(nStartPosX, nStartPosY, nImgSizeX, nImgSizeY));
	dSubImg.copyTo(dROI);

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(Thresholding)
{
	VERIFY_PARAMS();
	NIPLParam_Thresholding *pParam_Thresholding = (NIPLParam_Thresholding *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	Mat dMask = pInput->m_dMask;
	int nMethod = pParam_Thresholding->m_nMethod;
	float nThreshold = pParam_Thresholding->m_nThreshold;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if ((dImg.depth() != CV_8U && dImg.depth() != CV_32F) || dImg.channels() != 1) {
		return NIPL_ERR_INVALID_IMAGE_TYPE;
	}

	int nType = CV_THRESH_BINARY;	// set Upper as default
	if (nMethod & NIPLParam_Thresholding::METHOD_LOWER) {
		nType = CV_THRESH_BINARY_INV;
	}
	if (nMethod & NIPLParam_Thresholding::METHOD_OTSU) {
		nType |= CV_THRESH_OTSU;
	}

//	DebugPrintImageProperty(L"Thresholding Input", dImg);

	Mat dTempImg;
	dImg.copyTo(dTempImg);

	// change first to 8 bit image.
	if (dTempImg.depth() == CV_32F) {
		dTempImg *= 255.f;
		dTempImg.convertTo(dTempImg, CV_8U);
	}

//	DebugPrintImageProperty(L"Thresholding Converted", dTempImg);

	Mat dOutputImg;
	CHECK_EXCEPTION(threshold(dTempImg, dOutputImg, nThreshold, 255, nType));
	
	BOOL bExistMask = !CHECK_EMPTY_IMAGE(dMask);
	if (bExistMask) {
		dOutputImg = dOutputImg.mul(dMask);
	}

//	DebugPrintImageProperty(L"Thresholding Output", dOutputImg);

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(ColorThresholding)
{
	VERIFY_PARAMS();
	NIPLParam_ColorThresholding *pParam_Thresholding = (NIPLParam_ColorThresholding *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	Mat dMask = pInput->m_dMask;
	int nMethod_R = pParam_Thresholding->m_nMethod_R;
	int nMethod_G = pParam_Thresholding->m_nMethod_G;
	int nMethod_B = pParam_Thresholding->m_nMethod_B;
	float nLowerValue_R = pParam_Thresholding->m_nLowerValue_R;
	float nLowerValue_G = pParam_Thresholding->m_nLowerValue_G;
	float nLowerValue_B = pParam_Thresholding->m_nLowerValue_B;
	float nUpperValue_R = pParam_Thresholding->m_nUpperValue_R;
	float nUpperValue_G = pParam_Thresholding->m_nUpperValue_G;
	float nUpperValue_B = pParam_Thresholding->m_nUpperValue_B;
	float nLowerRatio_R_G = pParam_Thresholding->m_nLowerRatio_R_G;
	float nLowerRatio_R_B = pParam_Thresholding->m_nLowerRatio_R_B;
	float nLowerRatio_G_B = pParam_Thresholding->m_nLowerRatio_G_B;
	float nUpperRatio_R_G = pParam_Thresholding->m_nUpperRatio_R_G;
	float nUpperRatio_R_B = pParam_Thresholding->m_nUpperRatio_R_B;
	float nUpperRatio_G_B = pParam_Thresholding->m_nUpperRatio_G_B;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if ((dImg.depth() != CV_8U && dImg.depth() != CV_32F) || dImg.channels() != 3) {
		return NIPL_ERR_INVALID_IMAGE_TYPE;
	}

	Mat dTempImg;
	dImg.copyTo(dTempImg);

	// change first to 8 bit image.
	if (dTempImg.depth() == CV_32F) {
		nLowerValue_R *= 255.f;
		nLowerValue_G *= 255.f;
		nLowerValue_B *= 255.f;
		nUpperValue_R *= 255.f;
		nUpperValue_G *= 255.f;
		nUpperValue_B *= 255.f;

		dTempImg *= 255.f;
		dTempImg.convertTo(dTempImg, CV_8U);
	}
	
	int nMin_R = 0;
	int nMin_G = 0;
	int nMin_B = 0;
	int nMax_R = 255;
	int nMax_G = 255;
	int nMax_B = 255;

	ColorThresholding_SetThreshold(nMethod_R, nLowerValue_R, nUpperValue_R, nMin_R, nMax_R);
	ColorThresholding_SetThreshold(nMethod_G, nLowerValue_G, nUpperValue_G, nMin_G, nMax_G);
	ColorThresholding_SetThreshold(nMethod_B, nLowerValue_B, nUpperValue_B, nMin_B, nMax_B);

	Mat dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);

	Vec3b dPixel;
	UINT8 nR, nG, nB;
	for (int nY = 0; nY < nImgSizeY; nY++) {
		for (int nX = 0; nX < nImgSizeX; nX++) {
			dPixel = dTempImg.at<Vec3b>(nY, nX);
			nR = dPixel[2];
			nG = dPixel[1];
			nB = dPixel[0];

			if (nR < nMin_R || nR > nMax_R) {
				continue;
			}
			if (nG < nMin_G || nG > nMax_G) {
				continue;
			}
			if (nB < nMin_B || nB > nMax_B) {
				continue;
			}

			if (nLowerRatio_R_G != 0.f && nR < nG * nLowerRatio_R_G) {
				continue;
			}
			if (nUpperRatio_R_G != 0.f && nR > nG * nUpperRatio_R_G) {
				continue;
			}
			if (nLowerRatio_R_B != 0.f && nR < nB * nLowerRatio_R_B) {
				continue;
			}
			if (nUpperRatio_R_B != 0.f && nR > nB * nUpperRatio_R_B) {
				continue;
			}
			if (nLowerRatio_G_B != 0.f && nG < nB * nLowerRatio_G_B) {
				continue;
			}
			if (nUpperRatio_G_B != 0.f && nG > nB * nUpperRatio_G_B) {
				continue;
			}

			dOutputImg.at<UINT8>(nY, nX) = 255;
		}
	}

	BOOL bExistMask = !CHECK_EMPTY_IMAGE(dMask);
	if (bExistMask) {
		dOutputImg = dOutputImg.mul(dMask);
	}

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

void NIPL::ColorThresholding_SetThreshold(int nMethod, float nLowerValue, float nUpperValue, int &nMin, int &nMax)
{
	switch (nMethod) {
	case NIPLParam_ColorThresholding::METHOD_LOWER:
		nMin = 0;
		nMax = (int)nLowerValue;
		break;
	case NIPLParam_ColorThresholding::METHOD_UPPER:
		nMin = (int)nUpperValue;
		nMax = 255;
		break;
	case NIPLParam_ColorThresholding::METHOD_IN_RANGE:
		nMin = (int)nLowerValue;
		nMax = (int)nUpperValue;
		break;
	}
}

NIPL_METHOD_IMPL(Smoothing)
{
	VERIFY_PARAMS();
	NIPLParam_Smoothing *pParam_Smoothing = (NIPLParam_Smoothing *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	int nMethod = pParam_Smoothing->m_nMethod;
	int nFilterSize = pParam_Smoothing->m_nFilterSize;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	Mat dOutputImg;
	if (nMethod & NIPLParam_Smoothing::METHOD_FILTER) {
		if (nFilterSize > 0) {
			blur(dImg, dOutputImg, Size(nFilterSize, nFilterSize));
			pOutput->m_dImg = dOutputImg;
		}
	}

	if (CHECK_EMPTY_IMAGE(pOutput->m_dImg)) {
		return NIPL_ERR_PASS;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_METHOD_IMPL(EdgeDetecting)
{
	VERIFY_PARAMS();
	NIPLParam_EdgeDetecting *pParam_EdgeDetecting = (NIPLParam_EdgeDetecting *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	int nMethod = pParam_EdgeDetecting->m_nMethod;
	float nCannyLowerThreshold = pParam_EdgeDetecting->m_nCannyLowerThreshold;
	float nCannyUpperThreshold = pParam_EdgeDetecting->m_nCannyUpperThreshold;
	int nFilterSize = pParam_EdgeDetecting->m_nFilterSize;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	Mat dOutputImg;
	if (nMethod & NIPLParam_Smoothing::METHOD_FILTER) {
		if (nFilterSize > 0) {
			if (nCannyLowerThreshold == 0.f && nCannyUpperThreshold == 0.f) {
				Mat dTemp;
				nCannyUpperThreshold = (float)threshold(dImg, dTemp, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
				nCannyLowerThreshold = nCannyUpperThreshold * 0.5f;
			}

			Canny(dImg, dOutputImg, nCannyLowerThreshold, nCannyUpperThreshold, nFilterSize);
			pOutput->m_dImg = dOutputImg;
		}
	}

	if (CHECK_EMPTY_IMAGE(pOutput->m_dImg)) {
		return NIPL_ERR_PASS;
	}

	return NIPL_ERR_SUCCESS;
}
