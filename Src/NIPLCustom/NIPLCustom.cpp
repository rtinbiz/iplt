// NIPLCustom.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "NIPLCustom.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

NIPLCustom *NIPLCustom::pThis = 0x00;

NIPLCustom *NIPLCustom::GetInstance(BOOL bNew)
{
	if(bNew) {
		return new NIPLCustom();
	}

	if(pThis == 0x00) {
		pThis = new NIPLCustom();
	}

	return pThis;
}

void NIPLCustom::ReleaseInstance(NIPLCustom *pThat)
{
	if(pThat != 0x00) {
		delete pThat;
	}
	else if(pThis != 0x00) {
		delete pThis;
		pThis = 0x00;
	}
}

NIPLCustom::NIPLCustom() : NIPLCV()
{

}

NIPLCustom::~NIPLCustom()
{
}

NIPL_CUSTOM_METHOD_IMPL(LDC_Terminal)
{
	VERIFY_PARAMS();

	NIPLParam_LDC_Terminal *pParam_LDC_Terminal = (NIPLParam_LDC_Terminal *)pInput->m_pParam;
	NIPL_ERR nErr = NIPL_ERR_SUCCESS;

	NIPLInput dInput;
	NIPLOutput dOutput;
	Mat dImg = pInput->m_dImg;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	NIPLResult_Defect_LDC *pResult = new NIPLResult_Defect_LDC();
	if (pResult == nullptr) return NIPL_ERR_OUT_OF_MEMORY;

	int nRadius = pParam_LDC_Terminal->m_nTerminalRadius;
	Rect rcBoundingBox;
	Point ptCenter;
	bool bDefect;

	Mat dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	for (int i = 0; i < LDC_TERMINAL_MAX_ROUND_SHAPE; i++) {
		ptCenter = pParam_LDC_Terminal->m_ptCenter_RoundTerminal[i];
		rcBoundingBox.x = ptCenter.x - nRadius;
		rcBoundingBox.y = ptCenter.y - nRadius;
		rcBoundingBox.width = 2 * nRadius + 1;
		rcBoundingBox.height = 2 * nRadius + 1;

		nErr = LDC_Terminal_CheckShape(dImg, dOutputImg, pParam_LDC_Terminal, rcBoundingBox, true, bDefect);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		if (bDefect) {
			NIPLDefect_LDC dDefect(NIPLDefect_LDC::DEFECT_TYPE_TERMINAL_ROUND, rcBoundingBox);
			pResult->m_listDefect.push_back(dDefect);
		}
	}
	for (int i = 0; i < LDC_TERMINAL_MAX_RECT_SHAPE; i++) {
		ptCenter = pParam_LDC_Terminal->m_ptCenter_RectTerminal[i];
		rcBoundingBox.x = ptCenter.x - nRadius;
		rcBoundingBox.y = ptCenter.y - nRadius;
		rcBoundingBox.width = 2 * nRadius + 1;
		rcBoundingBox.height = 2 * nRadius + 1;

		nErr = LDC_Terminal_CheckShape(dImg, dOutputImg, pParam_LDC_Terminal, rcBoundingBox, false, bDefect);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		if (bDefect) {
			NIPLDefect_LDC dDefect(NIPLDefect_LDC::DEFECT_TYPE_TERMINAL_RECT, rcBoundingBox);
			pResult->m_listDefect.push_back(dDefect);
		}
	}

	pOutput->m_dImg = dOutputImg;

	if (pResult->m_listDefect.size() > 0) {
		pOutput->m_pResult = pResult;
	}
	else {
		delete pResult;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_ERR NIPLCustom::LDC_Terminal_CheckShape(Mat dImg, Mat &dOutputImg, NIPLParam_LDC_Terminal *pParam, Rect rcBoundingBox, bool bRoundTerminal, bool &bDefect)
{
	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	int nRadius = pParam->m_nTerminalRadius;
	int nSamplingRadius = pParam->m_nTerminalSamplingRadius;
	if (nSamplingRadius == 0) {
		nSamplingRadius = nRadius / 2;
	}
	float nEndSamplingThreshold = pParam->m_nEndSamplingThreshold;
	if (nEndSamplingThreshold == 0.f) {
		nEndSamplingThreshold = 1.f;
	}

	int nStartPosX = rcBoundingBox.x;
	int nStartPosY = rcBoundingBox.y;
	int nEndPosX = nStartPosX + rcBoundingBox.width - 1;
	int nEndPosY = nStartPosY + rcBoundingBox.height - 1;

	if (nStartPosX < 0) nStartPosX = 0;
	if (nStartPosY < 0) nStartPosY = 0;
	if (nEndPosX >= nImgSizeX) nEndPosX = nImgSizeX - 1;
	if (nEndPosY >= nImgSizeY) nEndPosY = nImgSizeY - 1;

	NIPLParam_CopySubImageFrom dParam_CopySubImageFrom;
	dParam_CopySubImageFrom.m_nStartPosX = nStartPosX;
	dParam_CopySubImageFrom.m_nStartPosY = nStartPosY;
	dParam_CopySubImageFrom.m_nEndPosX = nEndPosX;
	dParam_CopySubImageFrom.m_nEndPosY = nEndPosY;

	// Get SubImage
	NIPLInput dInput;
	NIPLOutput dOutput;
	dInput.m_dImg = dImg;
	dInput.m_pParam = &dParam_CopySubImageFrom;
	NIPL_ERR nErr = CopySubImageFrom(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dSubImage = dOutput.m_dImg;

	Scalar dMean;
	Scalar dStd;
	LDC_Terminal_GetSamplingValues(dSubImage, nSamplingRadius, dMean, dStd);

	float nMean_R = (float)dMean[2];
	float nMean_G = (float)dMean[1];
	float nMean_B = (float)dMean[0];
	float nStd_R = (float)dStd[2];
	float nStd_G = (float)dStd[1];
	float nStd_B = (float)dStd[0];

	// Color Thresholding
	NIPLParam_ColorThresholding dParam_ColorThresholding;
	dParam_ColorThresholding.m_nMethod_R = NIPLParam_ColorThresholding::METHOD_IN_RANGE;
	dParam_ColorThresholding.m_nLowerValue_R = nMean_R - nEndSamplingThreshold * nStd_R;
	dParam_ColorThresholding.m_nUpperValue_R = nMean_R + nEndSamplingThreshold * nStd_R;
	dParam_ColorThresholding.m_nMethod_G = NIPLParam_ColorThresholding::METHOD_IN_RANGE;
	dParam_ColorThresholding.m_nLowerValue_G = nMean_G - nEndSamplingThreshold * nStd_G;
	dParam_ColorThresholding.m_nUpperValue_G = nMean_G + nEndSamplingThreshold * nStd_G;
	dParam_ColorThresholding.m_nMethod_B = NIPLParam_ColorThresholding::METHOD_IN_RANGE;
	dParam_ColorThresholding.m_nLowerValue_B = nMean_B - nEndSamplingThreshold * nStd_B;
	dParam_ColorThresholding.m_nUpperValue_B = nMean_B + nEndSamplingThreshold * nStd_B;

	dParam_ColorThresholding.m_nLowerRatio_R_G = 0.9f;
	dParam_ColorThresholding.m_nUpperRatio_R_G = 1.1f;
	dParam_ColorThresholding.m_nLowerRatio_R_B = 0.9f;
	dParam_ColorThresholding.m_nUpperRatio_R_B = 1.1f;
	dParam_ColorThresholding.m_nLowerRatio_G_B = 0.9f;
	dParam_ColorThresholding.m_nUpperRatio_G_B = 1.1f;

	dInput.m_dImg = dSubImage;
	dInput.m_pParam = &dParam_ColorThresholding;
	nErr = ColorThresholding(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}

	Mat dBinImage = dOutput.m_dImg;

	// Eliminate Noise
	NIPLParam_EliminateNoise &dParam_EliminateNoise = pParam->m_dParam_EliminateNoise;

	dInput.m_dImg = dBinImage;
	dInput.m_pParam = &dParam_EliminateNoise;
	nErr = EliminateNoise(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	dBinImage = dOutput.m_dImg;

	// Edge Detecting
	NIPLParam_EdgeDetecting &dParam_EdgeDetecting = pParam->m_dParam_EdgeDetecting;

	dInput.m_dImg = dBinImage;
	dInput.m_pParam = &dParam_EdgeDetecting;
	nErr = EdgeDetecting(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dEdgeImage = dOutput.m_dImg;

	// Find Circle
	NIPLParam_FindCircle &dParam_FindCircle = pParam->m_dParam_FindCircle;
	Point ptCenter = Point(dParam_FindCircle.m_nCenterPosX, dParam_FindCircle.m_nCenterPosY);
	int nMaxRadius = dParam_FindCircle.m_nMaxRadius;
	Mat dCircleROIImage;
	LDC_Terminal_ExcludeOutOfRadiusRange(dEdgeImage, dCircleROIImage, ptCenter, nMaxRadius);

	dInput.m_dImg = dCircleROIImage;
	dInput.m_pParam = &dParam_FindCircle;
	nErr = FindCircle(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dCircleImage = dOutput.m_dImg;
	int nCircleCount = 0;
	if (dOutput.m_pResult) {
		NIPLResult_FindCircle *pResult = (NIPLResult_FindCircle *)dOutput.m_pResult;
		nCircleCount = (int)pResult->m_listCircle.size();
	}

	// Find Line
	NIPLParam_FindLine &dParam_FindLine = pParam->m_dParam_FindLine;

	dInput.m_dImg = dEdgeImage;
	dInput.m_pParam = &dParam_FindLine;
	nErr = FindLine(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dLineImage = dOutput.m_dImg;
	int nLineCount = 0;
	if (dOutput.m_pResult) {
		NIPLResult_FindLine *pResult = (NIPLResult_FindLine *)dOutput.m_pResult;
		nLineCount = (int)pResult->m_listLine.size();
	}

	bDefect = false;
	if (bRoundTerminal) {
		if (nCircleCount == 0) {
			bDefect = true;
		}
		else if (nCircleCount < nLineCount) {
			bDefect = true;
		}
	}
	else {
		if (nLineCount == 0) {
			bDefect = true;
		}
		else if (nLineCount < nCircleCount) {
			bDefect = true;
		}
	}
	
/*
	TRACE("\nbRoundTermina : %d, nCircleCount : %d, nLineCount : %d, bDefect : %d",
		bRoundTerminal, nCircleCount, nLineCount, bDefect);
*/


	if (bDefect) {
//		dBinImage = dEdgeImage + dCircleImage + dLineImage;
//		dBinImage = dEdgeImage;

		// Put SubImage
		NIPLParam_CopySubImageTo dParam_CopySubImageTo;
		dParam_CopySubImageTo.m_nStartPosX = nStartPosX;
		dParam_CopySubImageTo.m_nStartPosY = nStartPosY;
		dParam_CopySubImageTo.m_dTargetImg = dOutputImg;

		dInput.m_dImg = dBinImage;
		dInput.m_pParam = &dParam_CopySubImageTo;
		nErr = CopySubImageTo(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		dOutputImg = dOutput.m_dImg;
	}

	return NIPL_ERR_SUCCESS;
}

void NIPLCustom::LDC_Terminal_ExcludeOutOfRadiusRange(Mat dImg, Mat &dOutputImage, Point ptCenter, int nMaxRadius)
{
	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	dImg.copyTo(dOutputImage);
	float nDistance;
	for (int nY = 0; nY < nImgSizeY; nY++) {
		for (int nX = 0; nX < nImgSizeX; nX++) {
			nDistance = sqrtf(powf((float)(ptCenter.x - nX), 2.f) + powf(float(ptCenter.y - nY), 2.f));
			if (nDistance > nMaxRadius) {
				dOutputImage.at<UINT8>(nY, nX) = 0;
			}
		}
	}
}

void NIPLCustom::LDC_Terminal_GetSamplingValues(Mat dImg, int nRadius, Scalar &dMean, Scalar &dStd)
{
	float nAngleStep = 0.3f;

	int nSamplingSize = (int)(360.f / nAngleStep);

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	int nCenterPosX = nImgSizeX / 2;
	int nCenterPosY = nImgSizeY / 2;

	Mat dSamplingLine = Mat::zeros(1, nSamplingSize, dImg.type());
	for (int nPosX = 0; nPosX < nSamplingSize; nPosX++) {
		float nAngle = nAngleStep * nPosX;
		int nImgPosX = cvRound(nCenterPosX + nRadius * cos(nAngle * CV_PI / 180));
		int nImgPosY = cvRound(nCenterPosY + nRadius * sin(nAngle * CV_PI / 180));

		dSamplingLine.at<Vec3b>(nPosX) = dImg.at<Vec3b>(nImgPosY, nImgPosX);
	}

	meanStdDev(dSamplingLine, dMean, dStd);
}

NIPL_CUSTOM_METHOD_IMPL(LDC_Tube)
{
	VERIFY_PARAMS();

	NIPLParam_LDC_Tube *pParam_LDC_Tube = (NIPLParam_LDC_Tube *)pInput->m_pParam;
	NIPL_ERR nErr = NIPL_ERR_SUCCESS;

	NIPLInput dInput;
	NIPLOutput dOutput;
	Mat dImg = pInput->m_dImg;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	NIPLResult_Defect_LDC *pResult = new NIPLResult_Defect_LDC();
	if (pResult == nullptr) return NIPL_ERR_OUT_OF_MEMORY;

	Rect rcBoundingBox;
	Point ptCenter;
	float nThreshold;

	Mat dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	bool bDefect;
	for (int i = 0; i < LDC_TUBE_MAX_RED; i++) {
		rcBoundingBox = pParam_LDC_Tube->m_rcBoundingBox_RedTube[i];
		nThreshold = pParam_LDC_Tube->m_nThreshold_RedTube[i];

		nErr = LDC_Tube_CheckColor(dImg, dOutputImg, pParam_LDC_Tube, rcBoundingBox, nThreshold, true, bDefect);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		if (bDefect) {
			NIPLDefect_LDC dDefect(NIPLDefect_LDC::DEFECT_TYPE_TUBE_RED, rcBoundingBox);
			pResult->m_listDefect.push_back(dDefect);
		}
	}

	for (int i = 0; i < LDC_TUBE_MAX_BLACK; i++) {
		rcBoundingBox = pParam_LDC_Tube->m_rcBoundingBox_BlackTube[i];
		nThreshold = pParam_LDC_Tube->m_nThreshold_BlackTube[i];

		nErr = LDC_Tube_CheckColor(dImg, dOutputImg, pParam_LDC_Tube, rcBoundingBox, nThreshold, false, bDefect);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		if (bDefect) {
			NIPLDefect_LDC dDefect(NIPLDefect_LDC::DEFECT_TYPE_TUBE_BLACK, rcBoundingBox);
			pResult->m_listDefect.push_back(dDefect);
		}
	}

	pOutput->m_dImg = dOutputImg;
	if (pResult->m_listDefect.size() > 0) {
		pOutput->m_pResult = pResult;
	}
	else {
		delete pResult;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_ERR NIPLCustom::LDC_Tube_CheckColor(Mat dImg, Mat &dOutputImg, NIPLParam_LDC_Tube *pParam, Rect rcBoundingBox, float nThreshold, bool bRedColor, bool &bDefect)
{
	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	int nStartPosX = rcBoundingBox.x;
	int nStartPosY = rcBoundingBox.y;
	int nEndPosX = nStartPosX + rcBoundingBox.width - 1;
	int nEndPosY = nStartPosY + rcBoundingBox.height - 1;

	if (nStartPosX < 0) nStartPosX = 0;
	if (nStartPosY < 0) nStartPosY = 0;
	if (nEndPosX >= nImgSizeX) nEndPosX = nImgSizeX - 1;
	if (nEndPosY >= nImgSizeY) nEndPosY = nImgSizeY - 1;

	NIPLParam_CopySubImageFrom dParam_CopySubImageFrom;
	dParam_CopySubImageFrom.m_nStartPosX = nStartPosX;
	dParam_CopySubImageFrom.m_nStartPosY = nStartPosY;
	dParam_CopySubImageFrom.m_nEndPosX = nEndPosX;
	dParam_CopySubImageFrom.m_nEndPosY = nEndPosY;

	// Get SubImage
	NIPLInput dInput;
	NIPLOutput dOutput;
	dInput.m_dImg = dImg;
	dInput.m_pParam = &dParam_CopySubImageFrom;
	NIPL_ERR nErr = CopySubImageFrom(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dSubImage = dOutput.m_dImg;

	// Color Thresholding
	NIPLParam_ColorThresholding dParam_ColorThresholding;

	if (bRedColor) {
		dParam_ColorThresholding.m_nMethod_R = NIPLParam_ColorThresholding::METHOD_UPPER;
		dParam_ColorThresholding.m_nUpperValue_R = 200;
		dParam_ColorThresholding.m_nMethod_G = NIPLParam_ColorThresholding::METHOD_LOWER;
		dParam_ColorThresholding.m_nLowerValue_G = 200;
		dParam_ColorThresholding.m_nMethod_B = NIPLParam_ColorThresholding::METHOD_LOWER;
		dParam_ColorThresholding.m_nLowerValue_B = 200;
	}
	else {
		dParam_ColorThresholding.m_nMethod_R = NIPLParam_ColorThresholding::METHOD_UPPER;
		dParam_ColorThresholding.m_nUpperValue_R = 200;
		dParam_ColorThresholding.m_nMethod_G = NIPLParam_ColorThresholding::METHOD_UPPER;
		dParam_ColorThresholding.m_nUpperValue_G = 200;
		dParam_ColorThresholding.m_nMethod_B = NIPLParam_ColorThresholding::METHOD_UPPER;
		dParam_ColorThresholding.m_nUpperValue_B = 200;
/*
		dParam_ColorThresholding.m_nLowerRatio_R_G = 0.9f;
		dParam_ColorThresholding.m_nUpperRatio_R_G = 1.1f;
		dParam_ColorThresholding.m_nLowerRatio_R_B = 0.9f;
		dParam_ColorThresholding.m_nUpperRatio_R_B = 1.1f;
		dParam_ColorThresholding.m_nLowerRatio_G_B = 0.9f;
		dParam_ColorThresholding.m_nUpperRatio_G_B = 1.1f;
*/
	}

	dInput.m_dImg = dSubImage;
	dInput.m_pParam = &dParam_ColorThresholding;
	nErr = ColorThresholding(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}

	Mat dBinImage = dOutput.m_dImg;

	// Eliminate Noise
	NIPLParam_EliminateNoise dParam_EliminateNoise;
	dParam_EliminateNoise.m_nMethod = NIPLParam_EliminateNoise::METHOD_MORPHOLOGY_OPS;
	dParam_EliminateNoise.m_nMorphologyFilterSize = 3;

	dInput.m_dImg = dBinImage;
	dInput.m_pParam = &dParam_EliminateNoise;
	nErr = EliminateNoise(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	dBinImage = dOutput.m_dImg;

	int nCount = countNonZero(dBinImage);
	float nRatio = ((float)nCount) / (dBinImage.rows * dBinImage.cols);

	bDefect = false;
	if (bRedColor) {
		if (nRatio < nThreshold) {
			bDefect = true;
		}
	}
	else {
		if (nRatio > nThreshold) {
			bDefect = true;
		}
	}

	TRACE("\nbRedColor : %d, Ratio : %.3f, nCount : %d, nTotal : %d, bDefect : %d\n",
		bRedColor, nRatio, nCount, (dSubImage.rows * dSubImage.cols), bDefect);

	if (bDefect) {
		// Put SubImage
		NIPLParam_CopySubImageTo dParam_CopySubImageTo;
		dParam_CopySubImageTo.m_nStartPosX = nStartPosX;
		dParam_CopySubImageTo.m_nStartPosY = nStartPosY;
		dParam_CopySubImageTo.m_dTargetImg = dOutputImg;

		dInput.m_dImg = dBinImage;
		dInput.m_pParam = &dParam_CopySubImageTo;
		nErr = CopySubImageTo(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		dOutputImg = dOutput.m_dImg;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_CUSTOM_METHOD_IMPL(LDC_Guide)
{
	VERIFY_PARAMS();

	NIPLParam_LDC_Guide *pParam_LDC_Guide = (NIPLParam_LDC_Guide *)pInput->m_pParam;
	NIPL_ERR nErr = NIPL_ERR_SUCCESS;

	NIPLInput dInput;
	NIPLOutput dOutput;
	Mat dImg = pInput->m_dImg;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	NIPLResult_Defect_LDC *pResult = new NIPLResult_Defect_LDC();
	if (pResult == nullptr) return NIPL_ERR_OUT_OF_MEMORY;

	Rect rcBoundingBox;
	Point ptCenter;
	float nThreshold;

	Mat dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	bool bDefect = false;

	for (int i = 0; i < LDC_GUIDE_MAX_COUNT; i++) {
		rcBoundingBox = pParam_LDC_Guide->m_rcBoundingBox[i];
		nThreshold = pParam_LDC_Guide->m_nThreshold[i];

		nErr = LDC_Guide_CheckColor(dImg, dOutputImg, pParam_LDC_Guide, rcBoundingBox, nThreshold, bDefect);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		if (bDefect) {
			NIPLDefect_LDC dDefect(NIPLDefect_LDC::DEFECT_TYPE_GUIDE, rcBoundingBox);
			pResult->m_listDefect.push_back(dDefect);
		}
	}

	pOutput->m_dImg = dOutputImg;
	if (pResult->m_listDefect.size() > 0) {
		pOutput->m_pResult = pResult;
	}
	else {
		delete pResult;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_ERR NIPLCustom::LDC_Guide_CheckColor(Mat dImg, Mat &dOutputImg, NIPLParam_LDC_Guide *pParam, Rect rcBoundingBox, float nThreshold, bool &bDefect)
{
	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	int nStartPosX = rcBoundingBox.x;
	int nStartPosY = rcBoundingBox.y;
	int nEndPosX = nStartPosX + rcBoundingBox.width - 1;
	int nEndPosY = nStartPosY + rcBoundingBox.height - 1;

	if (nStartPosX < 0) nStartPosX = 0;
	if (nStartPosY < 0) nStartPosY = 0;
	if (nEndPosX >= nImgSizeX) nEndPosX = nImgSizeX - 1;
	if (nEndPosY >= nImgSizeY) nEndPosY = nImgSizeY - 1;

	NIPLParam_CopySubImageFrom dParam_CopySubImageFrom;
	dParam_CopySubImageFrom.m_nStartPosX = nStartPosX;
	dParam_CopySubImageFrom.m_nStartPosY = nStartPosY;
	dParam_CopySubImageFrom.m_nEndPosX = nEndPosX;
	dParam_CopySubImageFrom.m_nEndPosY = nEndPosY;

	// Get SubImage
	NIPLInput dInput;
	NIPLOutput dOutput;
	dInput.m_dImg = dImg;
	dInput.m_pParam = &dParam_CopySubImageFrom;
	NIPL_ERR nErr = CopySubImageFrom(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dSubImage = dOutput.m_dImg;

	// Color Thresholding
	NIPLParam_ColorThresholding dParam_ColorThresholding;

	dParam_ColorThresholding.m_nMethod_R = NIPLParam_ColorThresholding::METHOD_UPPER;
	dParam_ColorThresholding.m_nUpperValue_R = 200;
	dParam_ColorThresholding.m_nMethod_G = NIPLParam_ColorThresholding::METHOD_UPPER;
	dParam_ColorThresholding.m_nUpperValue_G = 200;
	dParam_ColorThresholding.m_nMethod_B = NIPLParam_ColorThresholding::METHOD_UPPER;
	dParam_ColorThresholding.m_nUpperValue_B = 200;

	/*
	dParam_ColorThresholding.m_nLowerRatio_R_G = 0.9f;
	dParam_ColorThresholding.m_nUpperRatio_R_G = 1.1f;
	dParam_ColorThresholding.m_nLowerRatio_R_B = 0.9f;
	dParam_ColorThresholding.m_nUpperRatio_R_B = 1.1f;
	dParam_ColorThresholding.m_nLowerRatio_G_B = 0.9f;
	dParam_ColorThresholding.m_nUpperRatio_G_B = 1.1f;
	*/

	dInput.m_dImg = dSubImage;
	dInput.m_pParam = &dParam_ColorThresholding;
	nErr = ColorThresholding(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}

	Mat dBinImage = dOutput.m_dImg;

	// Eliminate Noise
	NIPLParam_EliminateNoise dParam_EliminateNoise;
	dParam_EliminateNoise.m_nMethod = NIPLParam_EliminateNoise::METHOD_MORPHOLOGY_OPS;
	dParam_EliminateNoise.m_nMorphologyFilterSize = 3;

	dInput.m_dImg = dBinImage;
	dInput.m_pParam = &dParam_EliminateNoise;
	nErr = EliminateNoise(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	dBinImage = dOutput.m_dImg;

	int nCount = countNonZero(dBinImage);
	float nRatio = ((float)nCount) / (dBinImage.rows * dBinImage.cols);

	bDefect = false;
	if (nRatio > nThreshold) {
		bDefect = true;
	}

	TRACE("\nRatio : %.3f, nCount : %d, nTotal : %d, bDefect : %d\n",
		nRatio, nCount, (dSubImage.rows * dSubImage.cols), bDefect);

	if (bDefect) {
		// Put SubImage
		NIPLParam_CopySubImageTo dParam_CopySubImageTo;
		dParam_CopySubImageTo.m_nStartPosX = nStartPosX;
		dParam_CopySubImageTo.m_nStartPosY = nStartPosY;
		dParam_CopySubImageTo.m_dTargetImg = dOutputImg;

		dInput.m_dImg = dBinImage;
		dInput.m_pParam = &dParam_CopySubImageTo;
		nErr = CopySubImageTo(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}

		dOutputImg = dOutput.m_dImg;
	}

	return NIPL_ERR_SUCCESS;
}
