#pragma once

#include "NIPLCV.h"
#include "NIPLCustomParam.h"
#include "NIPLCustomResult.h"

#define NIPL_CUSTOM_METHOD_IMPL(FuncName) NIPL_ERR NIPLCustom::FuncName(NIPLInput *pInput, NIPLOutput *pOutput)

class AFX_EXT_CLASS NIPLCustom : public NIPLCV {
public :
	static NIPLCustom *pThis;
	static NIPLCustom *GetInstance(BOOL bNew = FALSE);
	static void ReleaseInstance(NIPLCustom *pThat = 0x00);

	NIPLCustom();
	~NIPLCustom();

	NIPL_METHOD_DECL(LDC_Terminal);
	NIPL_METHOD_DECL(LDC_Tube);
	NIPL_METHOD_DECL(LDC_Guide);

private :
	NIPL_ERR LDC_Terminal_CheckShape(Mat dImg, Mat &dOutputImg, NIPLParam_LDC_Terminal *pParam, Rect rcBoundingBox, bool bRoundTerminal, bool &bDefect);
	void LDC_Terminal_GetSamplingValues(Mat dImg, int nRadius, Scalar &dMean, Scalar &dStd);
	void LDC_Terminal_ExcludeOutOfRadiusRange(Mat dImg, Mat &dOutputImg, Point ptCenter, int nMaxRadius);

	NIPL_ERR LDC_Tube_CheckColor(Mat dImg, Mat &dOutputImg, NIPLParam_LDC_Tube *pParam, Rect rcBoundingBox, float nThreshold, bool bRedColor, bool &bDefect);
	NIPL_ERR LDC_Guide_CheckColor(Mat dImg, Mat &dOutputImg, NIPLParam_LDC_Guide *pParam, Rect rcBoundingBox, float nThreshold, bool &bDefect);
};