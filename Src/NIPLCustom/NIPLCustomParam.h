#pragma once

#include "NIPLParam.h"

#define LDC_TERMINAL_MAX_ROUND_SHAPE 3
#define LDC_TERMINAL_MAX_RECT_SHAPE 3
struct AFX_EXT_CLASS NIPLParam_LDC_Terminal : public NIPLParam
{
	Point m_ptCenter_RoundTerminal[LDC_TERMINAL_MAX_ROUND_SHAPE];
	Point m_ptCenter_RectTerminal[LDC_TERMINAL_MAX_RECT_SHAPE];
	int m_nTerminalRadius;
	int m_nTerminalSamplingRadius;
	float m_nEndSamplingThreshold;

	NIPLParam_EliminateNoise m_dParam_EliminateNoise;
	NIPLParam_EdgeDetecting m_dParam_EdgeDetecting;
	NIPLParam_FindCircle m_dParam_FindCircle;
	NIPLParam_FindLine m_dParam_FindLine;

	NIPLParam_LDC_Terminal() {
		Init();
	}
	virtual ~NIPLParam_LDC_Terminal() {
		Clear();
	}
	virtual void Clear() {
		m_dParam_EliminateNoise.Clear();
		m_dParam_EdgeDetecting.Clear();
		m_dParam_FindCircle.Clear();
		m_dParam_FindLine.Clear();

		Init();
	}

	void Init() {
		for (int i = 0; i < LDC_TERMINAL_MAX_ROUND_SHAPE; i++) {
			m_ptCenter_RoundTerminal[i] = Point(0, 0);
		}
		for (int i = 0; i < LDC_TERMINAL_MAX_RECT_SHAPE; i++) {
			m_ptCenter_RectTerminal[i] = Point(0, 0);
		}
		m_nTerminalRadius = 0;
		m_nTerminalSamplingRadius = 0;
		m_nEndSamplingThreshold = 0.f;

		m_dParam_EliminateNoise.Init();
		m_dParam_EdgeDetecting.Init();
		m_dParam_FindCircle.Init();
		m_dParam_FindLine.Init();
	}
};

#define LDC_TUBE_MAX_RED 2
#define LDC_TUBE_MAX_BLACK 4
struct AFX_EXT_CLASS NIPLParam_LDC_Tube : public NIPLParam
{
	Rect m_rcBoundingBox_RedTube[LDC_TUBE_MAX_RED];
	Rect m_rcBoundingBox_BlackTube[LDC_TUBE_MAX_BLACK];

	float m_nThreshold_RedTube[LDC_TUBE_MAX_RED];
	float m_nThreshold_BlackTube[LDC_TUBE_MAX_BLACK];

	NIPLParam_LDC_Tube() {
		Init();
	}
	virtual ~NIPLParam_LDC_Tube() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		for (int i = 0; i < LDC_TUBE_MAX_RED; i++) {
			m_rcBoundingBox_RedTube[i] = Rect(0, 0, 0, 0);
			m_nThreshold_RedTube[i] = 0.f;
		}
		for (int i = 0; i < LDC_TUBE_MAX_BLACK; i++) {
			m_rcBoundingBox_BlackTube[i] = Rect(0, 0, 0, 0);
			m_nThreshold_BlackTube[i] = 0.f;
		}
	}
};

#define LDC_GUIDE_MAX_COUNT 3
struct AFX_EXT_CLASS NIPLParam_LDC_Guide : public NIPLParam
{
	Rect m_rcBoundingBox[LDC_GUIDE_MAX_COUNT];
	float m_nThreshold[LDC_GUIDE_MAX_COUNT];

	NIPLParam_LDC_Guide() {
		Init();
	}
	virtual ~NIPLParam_LDC_Guide() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		for (int i = 0; i < LDC_GUIDE_MAX_COUNT; i++) {
			m_rcBoundingBox[i] = Rect(0, 0, 0, 0);
			m_nThreshold[i] = 0.f;
		}
	}
};
