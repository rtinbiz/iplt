#pragma once

enum LIST_COLUMN_INDEX_BLOB {
	LCI_BLOB_INDEX = 0,
	LCI_BLOB_BOUNDING_BOX_START_X,
	LCI_BLOB_BOUNDING_BOX_START_Y,
	LCI_BLOB_BOUNDING_BOX_END_X,
	LCI_BLOB_BOUNDING_BOX_END_Y,
	LCI_BLOB_SIZE
};

enum LIST_COLUMN_INDEX_LINE {
	LCI_LINE_INDEX = 0,
	LCI_LINE_START_X,
	LCI_LINE_START_Y,
	LCI_LINE_END_X,
	LCI_LINE_END_Y,
	LCI_LINE_LENGTH,
	LCI_LINE_ANGLE
};

enum LIST_COLUMN_INDEX_CIRCLE {
	LCI_CIRCLE_INDEX = 0,
	LCI_CIRCLE_CENTER_X,
	LCI_CIRCLE_CENTER_Y,
	LCI_CIRCLE_RADIUS
};

enum LIST_COLUMN_INDEX_DEFECT {
	LCI_DEFECT_INDEX = 0,
	LCI_DEFECT_TYPE,
	LCI_DEFECT_BOUNDING_BOX_START_X,
	LCI_DEFECT_BOUNDING_BOX_START_Y,
	LCI_DEFECT_BOUNDING_BOX_END_X,
	LCI_DEFECT_BOUNDING_BOX_END_Y,
	LCI_DEFECT_SIZE
};


/////////////////////////////////////////////////////////////////////////////
// CResultList window
class CResultList : public CListCtrl
{
// Construction
public:
	CResultList();

// Implementation
public:
	virtual ~CResultList();
	void SetResultType(wstring strResultType);

	int FindPtInData(CPoint pt);
	int FinrcBoundingBox(Rect rc);

	void SelectAll();
	void UnselectAll();
	void SelectItem(int nIndex, bool bSelect = true);
	bool IsItemSelected(int nIndex);

	void ClearContents();
	static bool m_bSortDesc;
	static int CALLBACK SortBlobCompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	static int CALLBACK SortLineCompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	static int CALLBACK SortCircleCompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	static int CALLBACK SortDefectCompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);

protected:
//	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
//	afx_msg void OnEditCopy();
//	afx_msg void OnEditClear();
//	afx_msg void OnViewResult();
	DECLARE_MESSAGE_MAP()
public:

private :
	wstring m_strResultType;
public:
	afx_msg void OnLvnColumnclick(NMHDR *pNMHDR, LRESULT *pResult);
};

