
#pragma once

#define JOB_VIEW_LIST_ID 2

enum LIST_COLUMN_INDEX {
	LCI_SEQ = 0,
	LCI_PROCESS,
	LCI_PARAMETER
};

/////////////////////////////////////////////////////////////////////////////
// CNIPJobViewList window

class CNIPJobViewList : public CListCtrl
{
// Construction
public:
	CNIPJobViewList();
	void SelectAll();
	void UnselectAll();
	void SelectItem(int nIndex, bool bSelect = true);
	int GetSelectedItemIndex();
	void SelectNextItem();
	bool FindData(void *pData);

	bool HasItem();
	bool IsItemSelected(int nIndex);
	bool IsAnyItemSelected();
	bool IsOneItemSelected();

// Overrides
protected:
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);

// Implementation
public:
	virtual ~CNIPJobViewList();

protected:
	DECLARE_MESSAGE_MAP()
public:
};
