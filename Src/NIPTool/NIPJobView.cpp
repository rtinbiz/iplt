
// NIPJobView.cpp : implementation of the CNIPJobView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "NIPTool.h"
#endif

#include "NIPJobView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CNIPJobView

IMPLEMENT_DYNCREATE(CNIPJobView, CView)

BEGIN_MESSAGE_MAP(CNIPJobView, CView)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
	ON_NOTIFY(LVN_ITEMCHANGED, JOB_VIEW_LIST_ID, OnLvnItemchanged)
	ON_NOTIFY(NM_CLICK, JOB_VIEW_LIST_ID, OnNMClick)
	ON_NOTIFY(NM_DBLCLK, JOB_VIEW_LIST_ID, OnNMDblClick)
//	ON_WM_SETCURSOR()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_COMMAND(ID_RUN_JOB, &CNIPJobView::OnRunJob)
	ON_UPDATE_COMMAND_UI(ID_RUN_JOB, &CNIPJobView::OnUpdateRunJob)
	ON_COMMAND(ID_RUN_SELECTED_PROCESS, &CNIPJobView::OnRunSelectedProcess)
	ON_UPDATE_COMMAND_UI(ID_RUN_SELECTED_PROCESS, &CNIPJobView::OnUpdateRunSelectedProcess)
	ON_COMMAND(ID_MOVE_UP_JOB_PROCESS, &CNIPJobView::OnMoveUpJobProcess)
	ON_UPDATE_COMMAND_UI(ID_MOVE_UP_JOB_PROCESS, &CNIPJobView::OnUpdateMoveUpJobProcess)
	ON_COMMAND(ID_MOVE_DOWN_JOB_PROCESS, &CNIPJobView::OnMoveDownJobProcess)
	ON_UPDATE_COMMAND_UI(ID_MOVE_DOWN_JOB_PROCESS, &CNIPJobView::OnUpdateMoveDownJobProcess)
	ON_COMMAND(ID_DELETE_JOB_PROCESS, &CNIPJobView::OnDeleteJobProcess)
	ON_UPDATE_COMMAND_UI(ID_DELETE_JOB_PROCESS, &CNIPJobView::OnUpdateDeleteJobProcess)
	ON_UPDATE_COMMAND_UI(ID_OPEN_JOB, &CNIPJobView::OnUpdateOpenJob)
	ON_COMMAND(ID_OPEN_JOB, &CNIPJobView::OnOpenJob)
	ON_COMMAND(ID_NEW_JOB, &CNIPJobView::OnNewJob)
	ON_UPDATE_COMMAND_UI(ID_NEW_JOB, &CNIPJobView::OnUpdateNewJob)
	ON_COMMAND(ID_SAVE_JOB, &CNIPJobView::OnSaveJob)
	ON_UPDATE_COMMAND_UI(ID_SAVE_JOB, &CNIPJobView::OnUpdateSaveJob)
	ON_COMMAND(ID_SAVE_JOB_AS, &CNIPJobView::OnSaveJobAs)
	ON_UPDATE_COMMAND_UI(ID_SAVE_JOB_AS, &CNIPJobView::OnUpdateSaveJobAs)
	ON_COMMAND(ID_RUN_PROCESS_TO, &CNIPJobView::OnRunProcessTo)
	ON_UPDATE_COMMAND_UI(ID_RUN_PROCESS_TO, &CNIPJobView::OnUpdateRunProcessTo)
	ON_COMMAND(ID_COPY_JOB_PROCESS, &CNIPJobView::OnCopyJobProcess)
	ON_UPDATE_COMMAND_UI(ID_COPY_JOB_PROCESS, &CNIPJobView::OnUpdateCopyJobProcess)
	ON_COMMAND(ID_PASTE_JOB_PROCESS, &CNIPJobView::OnPasteJobProcess)
	ON_UPDATE_COMMAND_UI(ID_PASTE_JOB_PROCESS, &CNIPJobView::OnUpdatePasteJobProcess)
	ON_MESSAGE(WM_HOTKEY, OnHotKey)
END_MESSAGE_MAP()

// CNIPJobView construction/destruction

CNIPJobView::CNIPJobView()
{
	// TODO: add construction code here
}

CNIPJobView::~CNIPJobView()
{
}

int CNIPJobView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// Create views:
	const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_SHOWSELALWAYS | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
	if (!m_wndNIPJobViewList.Create(dwViewStyle, rectDummy, this, JOB_VIEW_LIST_ID))
	{
		TRACE0("Failed to create Class View\n");
		return -1;      // fail to create
	}
	m_wndNIPJobViewList.SetFont(&afxGlobalData.fontRegular);

	// TODO:  Add your specialized creation code here
	m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_NIPJOB);
	m_wndToolBar.LoadToolBar(IDR_NIPJOB, 0, 0, TRUE /* Is locked */);
	m_wndToolBar.CleanUpLockedImages();
	m_wndToolBar.LoadBitmap(theApp.m_bHiColorIcons ? IDB_NIPJOB_VIEW_24 : IDR_NIPJOB, 0, 0, TRUE /* Locked */);

	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));
	m_wndToolBar.SetOwner(this);

	// All commands will be routed via this control , not via the parent frame:
	m_wndToolBar.SetRouteCommandsViaFrame(FALSE);

	// Hot Key
//	RegisterHotKey(GetSafeHwnd(), ID_COPY_JOB_PROCESS, MOD_CONTROL, 'C');
//	RegisterHotKey(GetSafeHwnd(), ID_PASTE_JOB_PROCESS, MOD_CONTROL, 'V');

	AdjustLayout();

	InitListCtrl();

	return 0;
}

void CNIPJobView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CNIPJobView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CNIPJobView diagnostics

#ifdef _DEBUG
void CNIPJobView::AssertValid() const
{
	CView::AssertValid();
}

void CNIPJobView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CNIPToolDoc* CNIPJobView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CNIPToolDoc)));
	return (CNIPToolDoc*)m_pDocument;
}
#endif //_DEBUG


// CNIPJobView message handlers
void CNIPJobView::AdjustLayout()
{
	if (GetSafeHwnd() == NULL || (AfxGetMainWnd() != NULL && AfxGetMainWnd()->IsIconic()))
	{
		return;
	}

	CRect rectClient;
	GetClientRect(rectClient);

	int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

	m_wndToolBar.SetWindowPos(NULL, rectClient.left, rectClient.top, rectClient.Width(), cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
	m_wndNIPJobViewList.SetWindowPos(NULL, rectClient.left, rectClient.top + cyTlb, rectClient.Width(), rectClient.Height() - cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
}

void CNIPJobView::InitListCtrl()
{
	m_wndNIPJobViewList.SetExtendedStyle(m_wndNIPJobViewList.GetExtendedStyle() | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_CHECKBOXES);

	m_wndNIPJobViewList.InsertColumn(LCI_SEQ, STR_NIPTOOLVIEW_LIST_COLUMM_SEQ, LVCFMT_LEFT, 60, 0);
	m_wndNIPJobViewList.InsertColumn(LCI_PROCESS, STR_NIPTOOLVIEW_LIST_COLUMM_PROCESS, LVCFMT_LEFT, 200, 0);
	m_wndNIPJobViewList.InsertColumn(LCI_PARAMETER, STR_NIPTOOLVIEW_LIST_COLUMM_PARAMETER, LVCFMT_LEFT, 1000, 0);
}

void CNIPJobView::UpdateJob()
{
	m_wndNIPJobViewList.DeleteAllItems();

	NIPJob *pJob = GetDocument()->GetJob();

	int nIndex = 0;
	int nIndexSelected = -1;
	wstring strSeq;
	wstring strProcess;
	wstring strParam;
	bool bEnable;
	for (auto pProcess : pJob->m_listProcess) {
		strSeq = to_wstring(nIndex + 1);
		strProcess = pProcess->m_strName;
		strParam = pProcess->GetParamDesc();
		bEnable = pProcess->m_bEnable;

		m_wndNIPJobViewList.InsertItem(nIndex, strSeq.c_str());
		m_wndNIPJobViewList.SetItemText(nIndex, LCI_PROCESS, strProcess.c_str());
		m_wndNIPJobViewList.SetItemText(nIndex, LCI_PARAMETER, strParam.c_str());
		m_wndNIPJobViewList.SetItemData(nIndex, (DWORD_PTR)pProcess);
		m_wndNIPJobViewList.SetCheck(nIndex, bEnable);

		nIndex++;
	}
}

void CNIPJobView::AddProcess(NIPJobProcess *pProcess, bool bUnselectPrevItems)
{
	if (pProcess == nullptr) {
		return;
	}

	NIPJobProcess *pCurProcess = nullptr;
	int nIndex = m_wndNIPJobViewList.GetSelectedItemIndex();
	if (nIndex >= 0) {
		pCurProcess = GetProcess(nIndex);
	}
	else {
		// if no selected item, add to end
		nIndex = m_wndNIPJobViewList.GetItemCount();
	}

	NIPJobProcess *pNewProcess = GetDocument()->AddProcess(pProcess, pCurProcess);
	if (pNewProcess == nullptr) {
		return;
	}

	if (bUnselectPrevItems) {
		m_wndNIPJobViewList.UnselectAll();
	}

	wstring strSeq = to_wstring(nIndex + 1);;
	wstring strProcess = pNewProcess->m_strName;
	wstring strParam = pNewProcess->GetParamDesc();
	bool bEnable = pNewProcess->m_bEnable;

	m_wndNIPJobViewList.InsertItem(nIndex, strSeq.c_str());
	m_wndNIPJobViewList.SetItemText(nIndex, LCI_PROCESS, strProcess.c_str());
	m_wndNIPJobViewList.SetItemText(nIndex, LCI_PARAMETER, strParam.c_str());
	m_wndNIPJobViewList.SetItemData(nIndex, (DWORD_PTR)pNewProcess);
	m_wndNIPJobViewList.SetCheck(nIndex, bEnable);
	m_wndNIPJobViewList.SelectItem(nIndex);

	RearrangeSeq();
}

void CNIPJobView::UpdateProcess(int nIndex, bool bSelectItem, bool bUnselectPreItems)
{
	NIPJobProcess *pProcess = GetProcess(nIndex);
	if (pProcess == nullptr) {
		return;
	}

	if (bUnselectPreItems) {
		m_wndNIPJobViewList.UnselectAll();
	}

	wstring strProcess = pProcess->m_strName;
	wstring strParam = pProcess->GetParamDesc();
	bool bEnable = pProcess->m_bEnable;
	m_wndNIPJobViewList.SetItemText(nIndex, LCI_PROCESS, strProcess.c_str());
	m_wndNIPJobViewList.SetItemText(nIndex, LCI_PARAMETER, strParam.c_str());
	m_wndNIPJobViewList.SetCheck(nIndex, bEnable);

	m_wndNIPJobViewList.SelectItem(nIndex, bSelectItem);
}

void CNIPJobView::OnLvnItemchanged(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here

	// Check if the status in the check box is changed
	if (pNMLV->uChanged == LVIF_STATE)
	{
		UINT nOldCheck = pNMLV->uOldState & LVIS_STATEIMAGEMASK;
		UINT nNewCheck = pNMLV->uNewState & LVIS_STATEIMAGEMASK;
		if (nOldCheck && nNewCheck && nOldCheck != nNewCheck)
		{
			int nIndex = pNMLV->iItem;
			NIPJobProcess *pProcess = GetProcess(nIndex);
			if (pProcess) {
				bool bEnable = m_wndNIPJobViewList.GetCheck(nIndex);
				pProcess->m_bEnable = bEnable;
			}
		}
	}

	*pResult = 0;
}

void CNIPJobView::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here

/*
	CPoint pt;
	GetCursorPos(&pt);
	m_wndNIPJobViewList.ScreenToClient(&pt);
	int nIndex = m_wndNIPJobViewList.HitTest(pt);
	if (!m_wndNIPJobViewList.IsSelected(nIndex)) {
		nIndex = GetSelectedItemIndex();
	}

	SelectProcess(nIndex);
*/

	*pResult = 0;
}


void CNIPJobView::OnNMDblClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	CPoint pt;
	GetCursorPos(&pt);
	m_wndNIPJobViewList.ScreenToClient(&pt);
	int nIndex = m_wndNIPJobViewList.HitTest(pt);
	SetModifyingProcess(GetProcess(nIndex));

	*pResult = 0;
}


//BOOL CNIPJobView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
//{
//	// TODO: Add your message handler code here and/or call default
//
//	return CView::OnSetCursor(pWnd, nHitTest, message);
//}

void CNIPJobView::SetModifyingProcess(NIPJobProcess *pProcess)
{
	GetDocument()->SetModifyingProcess(pProcess);

	CMainFrame *pMainFrame = (CMainFrame *)AfxGetMainWnd();
	pMainFrame->UpdateParameterView(pProcess, false);
}

void CNIPJobView::UpdateModifyingProcess()
{
	UpdateProcess(GetModifyingProcessIndex());
}

int CNIPJobView::GetModifyingProcessIndex()
{
	NIPJobProcess *pModifyingProcess = GetDocument()->GetModifyingProcess();
	return FindProcessIndex(pModifyingProcess);
}

int CNIPJobView::FindProcessIndex(NIPJobProcess *pProcess)
{
	if (pProcess == nullptr) {
		return -1;
	}

	int nCount = m_wndNIPJobViewList.GetItemCount();
	for (int i = 0; i < nCount; i++) {
		NIPJobProcess *pProcess2 = GetProcess(i);
		if (pProcess == pProcess2) {
			return i;
		}
	}

	return -1;
}

NIPJobProcess *CNIPJobView::GetProcess(int nIndex)
{
	if (nIndex < 0) {
		return nullptr;
	}

	return (NIPJobProcess *)m_wndNIPJobViewList.GetItemData(nIndex);
}


void CNIPJobView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	AdjustLayout();
}

void CNIPJobView::OnDraw(CDC* pDC)
{
	CRect rectList;
	m_wndNIPJobViewList.GetWindowRect(rectList);
	ScreenToClient(rectList);

	rectList.InflateRect(1, 1);
	pDC->Draw3dRect(rectList, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));
}

void CNIPJobView::OnSetFocus(CWnd* pOldWnd)
{
//	UpdateParameter();
	m_wndNIPJobViewList.SetFocus();

	CView::OnSetFocus(pOldWnd);
}

void CNIPJobView::OnInitialUpdate()
{
	CView::OnInitialUpdate();

	// TODO: You may populate your ListView with items by directly accessing
	//  its list control through a call to GetListCtrl().
	UpdateJob();
}

void CNIPJobView::OnRunJob()
{
	// TODO: Add your command handler code here
	GetDocument()->RunJob();
}

void CNIPJobView::OnUpdateRunJob(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_wndNIPJobViewList.HasItem());
}

void CNIPJobView::OnRunSelectedProcess()
{
	// TODO: Add your command handler code here
	int nIndex = m_wndNIPJobViewList.GetSelectedItemIndex();
	NIPJobProcess *pProcess = GetProcess(nIndex);
	if (GetDocument()->RunProcess(pProcess)) {
		m_wndNIPJobViewList.SelectNextItem();
	}
}

void CNIPJobView::OnUpdateRunProcessTo(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_wndNIPJobViewList.IsOneItemSelected());
}

void CNIPJobView::OnRunProcessTo()
{
	// TODO: Add your command handler code here
	int nLastIndex = m_wndNIPJobViewList.GetSelectedItemIndex();
	for (int i = 0; i <= nLastIndex; i++) {
		NIPJobProcess *pProcess = GetProcess(i);
		if (!GetDocument()->RunProcess(pProcess)) {
			break;
		}
	}
}

void CNIPJobView::OnUpdateRunSelectedProcess(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_wndNIPJobViewList.IsOneItemSelected());
}

void CNIPJobView::OnMoveUpJobProcess()
{
	// TODO: Add your command handler code here
	POSITION dPos = m_wndNIPJobViewList.GetFirstSelectedItemPosition();
	while (dPos) {
		int nIndex = m_wndNIPJobViewList.GetNextSelectedItem(dPos);
		if (nIndex <= 0) {
			return;
		}

		int nIndexSwitch = nIndex - 1;
		NIPJobProcess *pProcess = GetProcess(nIndex);
		NIPJobProcess *pProcessTarget = GetProcess(nIndexSwitch);
		if (GetDocument()->SwitchProcess(pProcess, pProcessTarget)) {
			UpdateProcess(nIndex, false, false);
			UpdateProcess(nIndexSwitch, true, false);
		}
	}
}

void CNIPJobView::OnUpdateMoveUpJobProcess(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	BOOL bEnable = TRUE;
	int nSelectedCount = m_wndNIPJobViewList.GetSelectedCount();

	if (nSelectedCount == 0) {
		bEnable = FALSE;
	}
	else {
		POSITION dPos = m_wndNIPJobViewList.GetFirstSelectedItemPosition();
		while (dPos) {
			int nIndex = m_wndNIPJobViewList.GetNextSelectedItem(dPos);
			if (nIndex <= 0) {
				bEnable = FALSE;
				break;
			}
		}
	}

	pCmdUI->Enable(bEnable);
}

void CNIPJobView::OnMoveDownJobProcess()
{
	vector<int> listIndex;
	int nCount = m_wndNIPJobViewList.GetItemCount();
	POSITION dPos = m_wndNIPJobViewList.GetFirstSelectedItemPosition();
	while (dPos) {
		int nIndex = m_wndNIPJobViewList.GetNextSelectedItem(dPos);
		if (nIndex < 0 || nIndex >= nCount - 1) {
			return;
		}

		listIndex.push_back(nIndex);
	}

	// switch from bottom of list
	int nSelectedCount = (int)listIndex.size();
	for (int i = nSelectedCount - 1; i >= 0; i--) {
		int nIndex = listIndex[i];
		int nIndexSwitch = nIndex + 1;
		NIPJobProcess *pProcess = GetProcess(nIndex);
		NIPJobProcess *pProcessTarget = GetProcess(nIndexSwitch);
		if (GetDocument()->SwitchProcess(pProcess, pProcessTarget)) {
			UpdateProcess(nIndex, false, false);
			UpdateProcess(nIndexSwitch, true, false);
		}
	}
}

void CNIPJobView::OnUpdateMoveDownJobProcess(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	BOOL bEnable = TRUE;
	int nSelectedCount = m_wndNIPJobViewList.GetSelectedCount();

	if (nSelectedCount == 0) {
		bEnable = FALSE;
	}
	else {
		int nCount = m_wndNIPJobViewList.GetItemCount();
		POSITION dPos = m_wndNIPJobViewList.GetFirstSelectedItemPosition();
		while (dPos) {
			int nIndex = m_wndNIPJobViewList.GetNextSelectedItem(dPos);
			if (nIndex < 0 || nIndex >= nCount - 1) {
				bEnable = FALSE;
				break;
			}
		}
	}

	pCmdUI->Enable(bEnable);
}

void CNIPJobView::OnDeleteJobProcess()
{
	// TODO: Add your command handler code here
	int nDeleteIndex = -1;
	int nIndex = m_wndNIPJobViewList.GetSelectedItemIndex();
	while (nIndex >= 0) {
		NIPJobProcess *pProcess = GetProcess(nIndex);
		if (GetDocument()->DeleteProcess(pProcess)) {
			m_wndNIPJobViewList.DeleteItem(nIndex);
			nDeleteIndex = nIndex;
		}

		nIndex = m_wndNIPJobViewList.GetSelectedItemIndex();
	}

	if (nDeleteIndex >= 0) {
		RearrangeSeq();

		int nCount = m_wndNIPJobViewList.GetItemCount();
		if (nDeleteIndex >= nCount) nDeleteIndex = nCount - 1;
		m_wndNIPJobViewList.SelectItem(nDeleteIndex);
	}
}

void CNIPJobView::OnUpdateDeleteJobProcess(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_wndNIPJobViewList.IsAnyItemSelected());
}

void CNIPJobView::RearrangeSeq()
{
	wstring strSeq;
	int nCount = m_wndNIPJobViewList.GetItemCount();
	for (int i = 0; i < nCount; i++) {
		strSeq = to_wstring(i + 1);
		m_wndNIPJobViewList.SetItemText(i, LCI_SEQ, strSeq.c_str());
	}
}

void CNIPJobView::OnOpenJob()
{
	// TODO: Add your command handler code here
	CMainFrame *pMainFrame = (CMainFrame *)AfxGetMainWnd();
	if (pMainFrame) {
		pMainFrame->SendMessage(WM_COMMAND, ID_FILE_OPEN, 0);
	}
}

void CNIPJobView::OnUpdateOpenJob(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(TRUE);
}

void CNIPJobView::OnNewJob()
{
	// TODO: Add your command handler code here
	CMainFrame *pMainFrame = (CMainFrame *)AfxGetMainWnd();
	if (pMainFrame) {
		pMainFrame->SendMessage(WM_COMMAND, ID_FILE_NEW, 0);
	}
}

void CNIPJobView::OnUpdateNewJob(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(TRUE);
}

void CNIPJobView::OnSaveJob()
{
	// TODO: Add your command handler code here
	CMainFrame *pMainFrame = (CMainFrame *)AfxGetMainWnd();
	if (pMainFrame) {
		pMainFrame->SendMessage(WM_COMMAND, ID_FILE_SAVE, 0);
	}
}

void CNIPJobView::OnUpdateSaveJob(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(TRUE);
}

void CNIPJobView::OnSaveJobAs()
{
	// TODO: Add your command handler code here
	CMainFrame *pMainFrame = (CMainFrame *)AfxGetMainWnd();
	if (pMainFrame) {
		pMainFrame->SendMessage(WM_COMMAND, ID_FILE_SAVE_AS, 0);
	}
}

void CNIPJobView::OnUpdateSaveJobAs(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(TRUE);
}


void CNIPJobView::OnCopyJobProcess()
{
	// TODO: Add your command handler code here
	NIPJob &dClipboardJob = CNIPToolDoc::m_dClipboardJob;
	dClipboardJob.Clear();

	POSITION dPos = m_wndNIPJobViewList.GetFirstSelectedItemPosition();
	while (dPos) {
		int nIndex = m_wndNIPJobViewList.GetNextSelectedItem(dPos);
		NIPJobProcess *pProcess = GetProcess(nIndex);
		if (pProcess) {
			dClipboardJob.AddProcess(pProcess);
		}
	}
}

void CNIPJobView::OnUpdateCopyJobProcess(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_wndNIPJobViewList.IsAnyItemSelected());
}


void CNIPJobView::OnPasteJobProcess()
{
	// TODO: Add your command handler code here
	NIPJob &dClipboardJob = CNIPToolDoc::m_dClipboardJob;

	bool bUnselectPrevItems = true;
	int nCount = (int)dClipboardJob.m_listProcess.size();
	for (int i = nCount - 1; i >= 0; i--) {
		NIPJobProcess *pProcess = dClipboardJob.m_listProcess[i];
		AddProcess(pProcess, bUnselectPrevItems);
		bUnselectPrevItems = false;
	}
}

void CNIPJobView::OnUpdatePasteJobProcess(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	bool bNoItemSelected = !m_wndNIPJobViewList.IsAnyItemSelected();
	bool bOneItemSelected = m_wndNIPJobViewList.IsOneItemSelected();
	pCmdUI->Enable(CNIPToolDoc::CanPaste() && (bNoItemSelected || bOneItemSelected));
}

LRESULT CNIPJobView::OnHotKey(WPARAM wParam, LPARAM lParam)
{
	int nID = (int)wParam;
	switch (nID) {
	case ID_COPY_JOB_PROCESS:
		if (m_wndNIPJobViewList.IsAnyItemSelected()) {
			OnCopyJobProcess();
		}
		break;
	case ID_PASTE_JOB_PROCESS:
		{
			bool bNoItemSelected = !m_wndNIPJobViewList.IsAnyItemSelected();
			bool bOneItemSelected = m_wndNIPJobViewList.IsOneItemSelected();
			if(CNIPToolDoc::CanPaste() && (bNoItemSelected || bOneItemSelected)) {
				OnPasteJobProcess();
			}
		}
		break;
	}

	return 0;
}