
#pragma once

#include "ResultList.h"
#include "SplitterBar.h"

#define RESULT_LIST_SIZE_X 200
#define RESULT_LIST_SIZE_Y 100
#define RESULT_LIST_ID 2
#define SPLITTERBAR_ID 3

class CImageViewToolBar : public CMFCToolBar
{
	virtual void OnUpdateCmdUI(CFrameWnd* /*pTarget*/, BOOL bDisableIfNoHndler)
	{
		CMFCToolBar::OnUpdateCmdUI((CFrameWnd*) GetOwner(), bDisableIfNoHndler);
	}

	virtual BOOL AllowShowOnList() const { return FALSE; }
};

class CImageView : public CDockablePane
{
// Construction
public:
	CImageView();
	virtual ~CImageView();

	void AdjustLayout();

// Attributes
protected:
	CImageViewToolBar m_wndToolBar;

	NIPLImageCtl m_wndImg;
	CSplitterBar m_wndSplitterBar;
	CResultList m_wndResultList;
	CStatusBar m_wndStatusBar;

	NIPLInput m_dInput;
	NIPLOutput m_dOutput;

	Mat m_dImg;
	Mat m_dMask;

	wstring m_strName;

	bool m_bInputImage;
	bool m_bShowResult;
	bool m_bShowMask;
	bool m_bShowTemplate;

	bool m_bCreatedDinamically;

// Implementation
public:
	BOOL Create(LPCTSTR lpszCaption, CWnd* pParentWnd, UINT nID, bool bCreatedDinamically = false);
	void Clear();

	bool LoadImage(const wstring &strPath);
	void ShowImage(Mat dImg, bool bKeepPosAndZoom = false);
	void ShowInputImage(bool bKeepPosAndZoom = false);
	void ShowOutputImage();

	Mat GetImage() { return m_dImg; }
	wstring GetName() { return m_strName;  }

	void SetInputImage() { m_bInputImage = true; }
	void SetInput(wstring strName, const NIPLInput &dInput);
	void SetOutput(wstring strName, const NIPLOutput &dOutput);
	void SetResultList();
	void ShowImageInfo();
	void ShowImagePosValue(CPoint ptImagePos);
	void CheckResultByImagePos(CPoint ptImagePos);
	void ShowImageZoom();
	void ShowResultBox(int nIndexCurSel = -1);
	void ShowResultDesc(wstring strDesc);
	void ShowTemplate();

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
public :
	afx_msg void OnLvnItemchanged(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnOpenImage();
	afx_msg void OnUpdateOpenImage(CCmdUI *pCmdUI);
	afx_msg void OnSaveImage();
	afx_msg void OnUpdateSaveImage(CCmdUI *pCmdUI);
	afx_msg void OnSyncImage();
	afx_msg void OnUpdateSyncImage(CCmdUI *pCmdUI);
	afx_msg void OnKeepImagePosZoom();
	afx_msg void OnUpdateKeepImagePosZoom(CCmdUI *pCmdUI);
	afx_msg LRESULT OnImagePosValue(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnImagePosDblClick(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnImageZoom(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMaskUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnImageMarkUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg void OnShowImagePos();
	afx_msg void OnUpdateShowImagePos(CCmdUI *pCmdUI);
	afx_msg void OnShowOutputResult();
	afx_msg void OnUpdateShowOutputResult(CCmdUI *pCmdUI);
	afx_msg void OnRefreshImage();
	afx_msg void OnUpdateRefreshImage(CCmdUI *pCmdUI);
	afx_msg void OnShowMask();
	afx_msg void OnUpdateShowMask(CCmdUI *pCmdUI);
	afx_msg void OnShowTemplate();
	afx_msg void OnUpdateShowTemplate(CCmdUI *pCmdUI);
	afx_msg void OnFillMask();
	afx_msg void OnUpdateFillMask(CCmdUI *pCmdUI);
	afx_msg void OnClearMask();
	afx_msg void OnUpdateClearMask(CCmdUI *pCmdUI);
	afx_msg void OnShowOutputResultAll();
	afx_msg void OnUpdateShowOutputResultAll(CCmdUI *pCmdUI);
	afx_msg void OnClearOutputResult();
	afx_msg void OnUpdateClearOutputResult(CCmdUI *pCmdUI);
	virtual void PostNcDestroy();
};

