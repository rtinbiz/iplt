
// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "NIPTool.h"

#include "MainFrm.h"
#include "NIPToolDoc.h"
#include <codecvt>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWndEx)

const int  iMaxUserToolbars = 10;
const UINT uiFirstUserToolBarId = AFX_IDW_CONTROLBAR_FIRST + 40;
const UINT uiLastUserToolBarId = uiFirstUserToolBarId + iMaxUserToolbars - 1;

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWndEx)
	ON_WM_CREATE()
	ON_COMMAND(ID_WINDOW_MANAGER, &CMainFrame::OnWindowManager)
	ON_COMMAND(ID_VIEW_CUSTOMIZE, &CMainFrame::OnViewCustomize)
	ON_REGISTERED_MESSAGE(AFX_WM_CREATETOOLBAR, &CMainFrame::OnToolbarCreateNew)
//	ON_COMMAND_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_WINDOWS_7, &CMainFrame::OnApplicationLook)
//	ON_UPDATE_COMMAND_UI_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_WINDOWS_7, &CMainFrame::OnUpdateApplicationLook)
	ON_WM_SETTINGCHANGE()
	ON_REGISTERED_MESSAGE(AFX_WM_ON_PRESS_CLOSE_BUTTON, OnClosePane)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	theApp.m_nAppLook = theApp.GetInt(_T("ApplicationLook"), ID_VIEW_APPLOOK_VS_2008);
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{

	if (CMDIFrameWndEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	NIPO *pNIPO = NIPO::GetInstance();
	pNIPO->SetNotifyFunc(NIPONotify);

	BOOL bNameValid;
	CMDITabInfo mdiTabParams;
	mdiTabParams.m_style = CMFCTabCtrl::STYLE_3D_ONENOTE; // other styles available...
	mdiTabParams.m_bActiveTabCloseButton = TRUE;      // set to FALSE to place close button at right of tab area
	mdiTabParams.m_bTabIcons = FALSE;    // set to TRUE to enable document icons on MDI taba
	mdiTabParams.m_bAutoColor = TRUE;    // set to FALSE to disable auto-coloring of MDI tabs
	mdiTabParams.m_bDocumentMenu = TRUE; // enable the document menu at the right edge of the tab area
	EnableMDITabbedGroups(TRUE, mdiTabParams);

	if (!m_wndMenuBar.Create(this))
	{
		TRACE0("Failed to create menubar\n");
		return -1;      // fail to create
	}

	m_wndMenuBar.SetPaneStyle(m_wndMenuBar.GetPaneStyle() | CBRS_SIZE_DYNAMIC | CBRS_TOOLTIPS | CBRS_FLYBY);

	// prevent the menu bar from taking the focus on activation
	CMFCPopupMenu::SetForceMenuFocus(FALSE);

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(theApp.m_bHiColorIcons ? IDR_MAINFRAME_256 : IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	CString strToolBarName;
	bNameValid = strToolBarName.LoadString(IDS_TOOLBAR_STANDARD);
	ASSERT(bNameValid);
	m_wndToolBar.SetWindowText(strToolBarName);

	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);
	m_wndToolBar.EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);

	// Allow user-defined toolbars operations:
	InitUserToolbars(NULL, uiFirstUserToolBarId, uiLastUserToolBarId);

	if (!m_wndStatusBar.Create(this))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}
	m_wndStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT));

	// TODO: Delete these five lines if you don't want the toolbar and menubar to be dockable
	m_wndMenuBar.EnableDocking(CBRS_ALIGN_ANY);
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockPane(&m_wndMenuBar);
	DockPane(&m_wndToolBar);
	
	// enable Visual Studio 2005 style docking window behavior
	CDockingManager::SetDockingMode(DT_SMART);
	// enable Visual Studio 2005 style docking window auto-hide behavior
	EnableAutoHidePanes(CBRS_ALIGN_ANY);

	// Load menu item image (not placed on any standard toolbars):
	CMFCToolBar::AddToolBarForImageCollection(IDR_MENU_IMAGES, theApp.m_bHiColorIcons ? IDB_MENU_IMAGES_24 : 0);

	// create docking windows
	if (!CreateDockingWindows())
	{
		TRACE0("Failed to create docking windows\n");
		return -1;
	}

	m_wndProcessViewTree.EnableDocking(CBRS_ALIGN_ANY);
	DockPane(&m_wndProcessViewTree);
//	CDockablePane* pTabbedBar = NULL;
//	m_wndProcessViewTree.AttachToTabWnd(&m_wndFileView, DM_SHOW, TRUE, &pTabbedBar);

	m_wndParameterView.EnableDocking(CBRS_ALIGN_ANY);
	m_wndParameterView.DockToWindow(&m_wndProcessViewTree, CBRS_ALIGN_BOTTOM);
	//DockPane(&m_wndParameterView);

	m_wndInputImgView.EnableDocking(CBRS_ALIGN_ANY);
	DockPane(&m_wndInputImgView);

	m_wndLogView.EnableDocking(CBRS_ALIGN_ANY);
	DockPane(&m_wndLogView);

	// set the visual manager and style based on persisted value
	OnApplicationLook(theApp.m_nAppLook);

	// Enable enhanced windows management dialog
	EnableWindowsDialog(ID_WINDOW_MANAGER, ID_WINDOW_MANAGER, TRUE);

	// Enable toolbar and docking window menu replacement
	// remove 'customize' button
	EnablePaneMenu(TRUE, 0, L"", ID_VIEW_TOOLBAR);

	// enable quick (Alt+drag) toolbar customization
	CMFCToolBar::EnableQuickCustomization();

	if (CMFCToolBar::GetUserImages() == NULL)
	{
		// load user-defined toolbar images
		if (m_UserImages.Load(_T(".\\UserImages.bmp")))
		{
			CMFCToolBar::SetUserImages(&m_UserImages);
		}
	}

	// enable menu personalization (most-recently used commands)
	// TODO: define your own basic commands, ensuring that each pulldown menu has at least one basic command.
	CList<UINT, UINT> lstBasicCommands;

	lstBasicCommands.AddTail(ID_FILE_NEW);
	lstBasicCommands.AddTail(ID_FILE_OPEN);
	lstBasicCommands.AddTail(ID_FILE_SAVE);
	lstBasicCommands.AddTail(ID_FILE_PRINT);
	lstBasicCommands.AddTail(ID_APP_EXIT);
	lstBasicCommands.AddTail(ID_EDIT_CUT);
	lstBasicCommands.AddTail(ID_EDIT_PASTE);
	lstBasicCommands.AddTail(ID_EDIT_UNDO);
	lstBasicCommands.AddTail(ID_APP_ABOUT);
	lstBasicCommands.AddTail(ID_VIEW_STATUS_BAR);
	lstBasicCommands.AddTail(ID_VIEW_TOOLBAR);
// 	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2003);
// 	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_VS_2005);
// 	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_BLUE);
// 	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_SILVER);
// 	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_BLACK);
// 	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_AQUA);
// 	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_WINDOWS_7);
// 	lstBasicCommands.AddTail(ID_SORTING_SORTALPHABETIC);
// 	lstBasicCommands.AddTail(ID_SORTING_SORTBYTYPE);
// 	lstBasicCommands.AddTail(ID_SORTING_SORTBYACCESS);
// 	lstBasicCommands.AddTail(ID_SORTING_GROUPBYTYPE);

	CMFCToolBar::SetBasicCommands(lstBasicCommands);



	// Switch the order of document name and application name on the window title bar. This
	// improves the usability of the taskbar because the document name is visible with the thumbnail.
//	ModifyStyle(0, FWS_PREFIXTITLE);

	LoadProcessMenu();

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWndEx::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

BOOL CMainFrame::CreateDockingWindows()
{
	BOOL bNameValid;

	// Create Log View FIRST
	CString strLogView;
	bNameValid = strLogView.LoadString(IDS_LOG_VIEW);
	ASSERT(bNameValid);
	if (!m_wndLogView.Create(strLogView, this, CRect(0, 0, 400, 400), TRUE, ID_VIEW_LOG, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_BOTTOM | CBRS_FLOAT_MULTI))
	{
		TRACE0("Failed to create log view\n");
		return FALSE; // failed to create
	}

	// Create class view
	CString strProcessView;
	bNameValid = strProcessView.LoadString(IDS_PROCESS_VIEW);
	ASSERT(bNameValid);
	if (!m_wndProcessViewTree.Create(strProcessView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_METHODVIEW, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT | CBRS_FLOAT_MULTI))
	{
		TRACE0("Failed to create Class View window\n");
		return FALSE; // failed to create
	}

	// Create Parameter View
	CString strParameterView;
	bNameValid = strParameterView.LoadString(IDS_PARAMETER_VIEW);
	ASSERT(bNameValid);
	if (!m_wndParameterView.Create(strParameterView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_PARAMETER, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_RIGHT | CBRS_FLOAT_MULTI))
	{
		TRACE0("Failed to create Parameter window\n");
		return FALSE; // failed to create
	}

	// Create Input Image View
	m_wndInputImgView.SetInputImage();
	CString strInputImageView;
	bNameValid = strInputImageView.LoadString(IDS_INPUT_IMAGE_VIEW);
	ASSERT(bNameValid);
	if (!m_wndInputImgView.Create(STR_NAME_INPUT, this, ID_VIEW_INPUT_IMAGE)) {
		TRACE0("Failed to create input image view\n");
		return FALSE; // failed to create
	}

	SetDockingWindowIcons(theApp.m_bHiColorIcons);
	return TRUE;
}

void CMainFrame::SetDockingWindowIcons(BOOL bHiColorIcons)
{
//	HICON hFileViewIcon = (HICON) ::LoadImg(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_FILE_VIEW_HC : IDI_FILE_VIEW), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
//	m_wndFileView.SetIcon(hFileViewIcon, FALSE);

	HICON hProcessViewIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_CLASS_VIEW_HC : IDI_CLASS_VIEW), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndProcessViewTree.SetIcon(hProcessViewIcon, FALSE);

/*
	HICON hOutputBarIcon = (HICON) ::LoadImg(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_OUTPUT_WND_HC : IDI_OUTPUT_WND), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndLogView.SetIcon(hOutputBarIcon, FALSE);
*/

	HICON hParameterBarIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_PARAMETER_WND_HC : IDI_PARAMETER_WND), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndParameterView.SetIcon(hParameterBarIcon, FALSE);

	UpdateMDITabbedBarsIcons();
}

// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWndEx::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWndEx::Dump(dc);
}
#endif //_DEBUG


// CMainFrame message handlers

void CMainFrame::OnWindowManager()
{
	ShowWindowsDialog();
}

void CMainFrame::OnViewCustomize()
{
	CMFCToolBarsCustomizeDialog* pDlgCust = new CMFCToolBarsCustomizeDialog(this, TRUE /* scan menus */);
	pDlgCust->EnableUserDefinedToolbars();
	pDlgCust->Create();
}

LRESULT CMainFrame::OnToolbarCreateNew(WPARAM wp,LPARAM lp)
{
	LRESULT lres = CMDIFrameWndEx::OnToolbarCreateNew(wp,lp);
	if (lres == 0)
	{
		return 0;
	}

	CMFCToolBar* pUserToolbar = (CMFCToolBar*)lres;
	ASSERT_VALID(pUserToolbar);

	BOOL bNameValid;
	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);

	pUserToolbar->EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);

	return lres;
}


void CMainFrame::OnApplicationLook(UINT id)
{
	CWaitCursor wait;

	theApp.m_nAppLook = id;

/*
	switch (theApp.m_nAppLook)
	{

	case ID_VIEW_APPLOOK_WIN_2000:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManager));
		break;

	case ID_VIEW_APPLOOK_OFF_XP:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOfficeXP));
		break;

	case ID_VIEW_APPLOOK_WIN_XP:
		CMFCVisualManagerWindows::m_b3DTabsXPTheme = TRUE;
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));
		break;

	case ID_VIEW_APPLOOK_OFF_2003:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2003));
		CDockingManager::SetDockingMode(DT_SMART);
		break;

	case ID_VIEW_APPLOOK_VS_2005:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2005));
		CDockingManager::SetDockingMode(DT_SMART);
		break;


	case ID_VIEW_APPLOOK_VS_2008:
*/
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2008));
		CDockingManager::SetDockingMode(DT_SMART);
/*		break;

	case ID_VIEW_APPLOOK_WINDOWS_7:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows7));
		CDockingManager::SetDockingMode(DT_SMART);
		break;

	default:
		switch (theApp.m_nAppLook)
		{
		case ID_VIEW_APPLOOK_OFF_2007_BLUE:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_LunaBlue);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_BLACK:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_ObsidianBlack);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_SILVER:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Silver);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_AQUA:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Aqua);
			break;
		}

		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2007));
		CDockingManager::SetDockingMode(DT_SMART);
	}
*/

	m_wndLogView.UpdateFonts();
	RedrawWindow(NULL, NULL, RDW_ALLCHILDREN | RDW_INVALIDATE | RDW_UPDATENOW | RDW_FRAME | RDW_ERASE);

	theApp.WriteInt(_T("ApplicationLook"), theApp.m_nAppLook);
}

/*
void CMainFrame::OnUpdateApplicationLook(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(theApp.m_nAppLook == pCmdUI->m_nID);
}
*/

BOOL CMainFrame::LoadFrame(UINT nIDResource, DWORD dwDefaultStyle, CWnd* pParentWnd, CCreateContext* pContext) 
{
	// base class does the real work

	if (!CMDIFrameWndEx::LoadFrame(nIDResource, dwDefaultStyle, pParentWnd, pContext))
	{
		return FALSE;
	}


	// enable customization button for all user toolbars

	BOOL bNameValid;
	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);

	for (int i = 0; i < iMaxUserToolbars; i ++)
	{
		CMFCToolBar* pUserToolbar = GetUserToolBarByIndex(i);
		if (pUserToolbar != NULL)
		{
			pUserToolbar->EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);
		}
	}

	return TRUE;
}


void CMainFrame::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
	CMDIFrameWndEx::OnSettingChange(uFlags, lpszSection);
	m_wndLogView.UpdateFonts();
}

CImageView *CMainFrame::AddOutputImage(wstring strName, Mat dImg)
{
	CImageView *pImageView = FindOutputImage(strName);
	if (pImageView) {
		return pImageView;
	}

	pImageView = new CImageView();
	if (pImageView == nullptr) {
		return nullptr;
	}

	int nID = GetNextOutputImageViewID();
	if (!pImageView->Create(L"", this, nID, true)) {
		delete pImageView;
		return nullptr;
	}

	pImageView->EnableDocking(CBRS_ALIGN_ANY);

	BOOL bAttached = false;
	for (auto dItem : m_mapOutputImageView) {
		auto pDockedImageView = dItem.second;
		if (pDockedImageView->IsVisible() && pDockedImageView->IsDocked()) {
			CDockablePane* pTabbedBar = NULL;
			pImageView->AttachToTabWnd(pDockedImageView, DM_SHOW, TRUE, &pTabbedBar);

			bAttached = true;
			break;
		}
	}

	if (!bAttached) {
		if (m_wndInputImgView.IsVisible() && m_wndInputImgView.IsDocked()) {
			pImageView->DockToWindow(&m_wndInputImgView, CBRS_ALIGN_RIGHT);
		}
		else {
			DockPane(pImageView);
		}
	}

	AdjustDockingLayout();

	m_mapOutputImageView[nID] = pImageView;

	return pImageView;
}

int CMainFrame::GetNextOutputImageViewID()
{
	int nMaxID = ID_VIEW_OUTPUT_IMAGE;
	for (auto dItem : m_mapOutputImageView) {
		auto pImageView = dItem.second;
		int nID = pImageView->GetDlgCtrlID();
		if (nID > nMaxID) {
			nMaxID = nID;
		}
	}

	return (nMaxID + 1);
}

bool CMainFrame::RemoveOutputImage(int nID)
{
	auto dPos = m_mapOutputImageView.find(nID);
	if (dPos != m_mapOutputImageView.end()) {
		CImageView *pImageView = dPos->second;
		NIPO *pNIPO = NIPO::GetInstance();
		pNIPO->RemoveOutput(pImageView->GetName());

		m_mapOutputImageView.erase(dPos);

		return true;
	}

	return false;
}

CImageView *CMainFrame::FindOutputImage(wstring strName)
{
	for (auto dItem : m_mapOutputImageView) {
		auto pImageView = dItem.second;
		if (strName == pImageView->GetName()) {
			return pImageView;
		}
	}

	return nullptr;
}

LRESULT CMainFrame::OnClosePane(WPARAM wp, LPARAM lp)
{
	CBasePane* pPane = (CBasePane*)lp;
	if (pPane == nullptr) {
		return FALSE;
	}

	int nID = pPane->GetDlgCtrlID();
	BOOL bRemoved = RemoveOutputImage(nID);
	if (bRemoved) {
		pPane->ShowPane(FALSE, FALSE, FALSE);
		RemovePaneFromDockManager(pPane, TRUE, TRUE, TRUE, NULL);
		AdjustDockingLayout();
		pPane->PostMessage(WM_CLOSE);

		return (LRESULT)TRUE;
	}

	return (LRESULT)FALSE;
}

BOOL CMainFrame::OnCloseMiniFrame(CPaneFrameWnd* pWnd)
{
	if (OnClosePane(0, (LPARAM)pWnd->GetPane())) {
		return TRUE;
	}

	return CMDIFrameWndEx::OnCloseMiniFrame(pWnd);
}

BOOL CMainFrame::LoadProcessMenu()
{
	m_dProcessMenu.Clear();

	wchar_t szModulePath[MAX_PATH];
	GetModuleFileName(NULL, szModulePath, MAX_PATH - 1);
	wchar_t *pCut = wcsrchr(szModulePath, '\\');
	if (pCut) {
		*(pCut+1) = 0x00;
	}
	wstring strProcessMenuFile = szModulePath;
	strProcessMenuFile += STR_NIPTOOL_PROCESS_MENU_FILENAME;

	NIPO *pNIPO = NIPO::GetInstance();
	if (pNIPO->LoadJob(strProcessMenuFile.c_str(), m_dProcessMenu) == FALSE) {
		return FALSE;
	}

	m_wndProcessViewTree.FillProcessView(m_dProcessMenu);

	return TRUE;
}

NIPJob *CMainFrame::GetProcessMenu()
{
	return &m_dProcessMenu;
}

void CMainFrame::UpdateParameterView(NIPJobProcess *pProcess, bool bMenu)
{
	m_wndParameterView.UpdateParameter(pProcess, bMenu);
}

void CMainFrame::UpdateJobView(bool bModifyingProcess)
{
	CNIPJobView *pView = GetView();
	if (pView) {
		if (bModifyingProcess) {
			pView->UpdateModifyingProcess();
		}
		else {
			pView->UpdateJob();
		}
	}
}

void CMainFrame::AddProcessToJobView(NIPJobProcess *pProcess)
{
	CNIPJobView *pView = GetView();
	if (pView) {
		pView->AddProcess(pProcess);
	}
}

//void CMainFrame::PostNcDestroy()
//{
//	CMDIFrameWndEx::PostNcDestroy();
//}

void CMainFrame::UpdateInput(wstring strName)
{
	NIPO *pNIPO = NIPO::GetInstance();

	NIPLInput dInput;
	pNIPO->GetInput(dInput);

	wstring strTitle = STR_NAME_INPUT;
	if (strName != STR_NAME_INPUT) {
		strTitle += _T(" - ") + strName;
	}

	m_wndInputImgView.SetInput(strTitle, dInput);
	m_wndInputImgView.ShowInputImage();
}

void CMainFrame::UpdateOutput(wstring strName)
{
	NIPO *pNIPO = NIPO::GetInstance();
	NIPLOutput dOutput;
	if (!pNIPO->GetOutput(strName, dOutput)) {
		return;
	}

	CImageView *pImageView = AddOutputImage(strName, dOutput.m_dImg);
	if (pImageView == nullptr) {
		return;
	}

	pImageView->SetOutput(strName, dOutput);
	pImageView->ShowOutputImage();
}

void CMainFrame::NIPONotify(wstring strName, NIPL_ERR nErr, NIPO_NOTIFY_LEVEL nNotifyLevel)
{
	CMainFrame *pMainFrame = (CMainFrame *)AfxGetMainWnd();
	if (nNotifyLevel == NNL_INPUT_UPDATE) {
		pMainFrame->UpdateInput(strName);
	}
	else if (nNotifyLevel == NNL_OUTPUT_UPDATE) {
		pMainFrame->UpdateOutput(strName);
	}
	else {
		pMainFrame->m_wndLogView.ProcessNotify(strName, nErr);
	}
}

CNIPJobView *CMainFrame::GetView()
{
	CMDIChildWnd *pChildFrame = MDIGetActive();
	if (pChildFrame == nullptr) {
		return nullptr;
	}

	return (CNIPJobView *)pChildFrame->GetActiveView();
}


CNIPToolDoc *CMainFrame::GetDocument()
{
	CMDIChildWnd *pChildFrame = MDIGetActive();
	if (pChildFrame == nullptr) {
		return nullptr;
	}

	return (CNIPToolDoc *)pChildFrame->GetActiveDocument();
}
