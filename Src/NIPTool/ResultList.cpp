// This MFC Samples source code demonstrates using MFC Microsoft Office Fluent User Interface 
// (the "Fluent UI") and is provided only as referential material to supplement the 
// Microsoft Foundation Classes Reference and related electronic documentation 
// included with the MFC C++ library software.  
// License terms to copy, use or distribute the Fluent UI are available separately.  
// To learn more about our Fluent UI licensing program, please visit 
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// Copyright (C) Microsoft Corporation
// All rights reserved.

#include "stdafx.h"

#include "ResultList.h"
#include "Resource.h"
#include "MainFrm.h"
#include "ImageView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

bool CResultList::m_bSortDesc = false;

/////////////////////////////////////////////////////////////////////////////
// CResultList

CResultList::CResultList()
{
}

CResultList::~CResultList()
{
}

BEGIN_MESSAGE_MAP(CResultList, CListCtrl)
//	ON_WM_CONTEXTMENU()
//	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
//	ON_COMMAND(ID_EDIT_CLEAR, OnEditClear)
//	ON_WM_WINDOWPOSCHANGING()
	ON_NOTIFY_REFLECT(LVN_COLUMNCLICK, &CResultList::OnLvnColumnclick)
END_MESSAGE_MAP()
/////////////////////////////////////////////////////////////////////////////
// CResultList message handlers

/*
void CResultList::OnContextMenu(CWnd* pWnd, CPoint point)
{
	CMenu menu;
	menu.LoadMenu(IDR_RESULT_POPUP);

	CMenu* pSumMenu = menu.GetSubMenu(0);

	if (AfxGetMainWnd()->IsKindOf(RUNTIME_CLASS(CMDIFrameWndEx)))
	{
		CMFCPopupMenu* pPopupMenu = new CMFCPopupMenu;

		if (!pPopupMenu->Create(this, point.x, point.y, (HMENU)pSumMenu->m_hMenu, FALSE, TRUE))
			return;

		((CMDIFrameWndEx*)AfxGetMainWnd())->OnShowPopupMenu(pPopupMenu);
		UpdateDialogControls(this, FALSE);
	}

	SetFocus();
}

void CResultList::OnEditCopy()
{
}

void CResultList::OnEditClear()
{
	ResetContent();
}
*/
/*
void CResultList::OnViewResult()
{
	CDockablePane* pParentBar = DYNAMIC_DOWNCAST(CDockablePane, GetOwner());
	CMDIFrameWndEx* pMainFrame = DYNAMIC_DOWNCAST(CMDIFrameWndEx, GetTopLevelFrame());

	if (pMainFrame != NULL && pParentBar != NULL)
	{
		pMainFrame->SetFocus();
		pMainFrame->ShowPane(pParentBar, FALSE, FALSE, FALSE);
		pMainFrame->RecalcLayout();
	}
}
*/

int CResultList::FindPtInData(CPoint pt)
{
	if (m_strResultType == NIPL_RESULT_FIND_BLOB) {
		int nCount = GetItemCount();
		for (int i = 0; i < nCount; i++) {
			NIPLBlob *pBlob = (NIPLBlob *)GetItemData(i);
			if (pBlob) {
				Rect rc = pBlob->GetBoundingBox();
				if (pt.x >= rc.x && pt.x <= (rc.x + rc.width - 1)
					&& pt.y >= rc.y && pt.y <= (rc.y + rc.height - 1)) {
					return i;
				}
			}
		}
	}
	else if (m_strResultType == NIPL_RESULT_FIND_LINE) {
		int nCount = GetItemCount();
		for (int i = 0; i < nCount; i++) {
			NIPLLine *pLine = (NIPLLine *)GetItemData(i);
			if (pLine) {
				Rect rc = pLine->GetBoundingBox();
				if (pt.x >= rc.x && pt.x <= (rc.x + rc.width - 1)
					&& pt.y >= rc.y && pt.y <=(rc.y + rc.height -1)) {
					return i;
				}
			}
		}
	}
	else if (m_strResultType == NIPL_RESULT_FIND_CIRCLE) {
		int nCount = GetItemCount();
		for (int i = 0; i < nCount; i++) {
			NIPLCircle *pCircle = (NIPLCircle *)GetItemData(i);
			if (pCircle) {
				Point ptCenter = pCircle->m_ptCenter;
				int nDist = cvRound(sqrt(pow(ptCenter.x - pt.x, 2) + pow(ptCenter.y - pt.y, 2)));
				if (nDist < pCircle->m_nRadius) {
					return i;
				}
			}
		}
	}
	else if (m_strResultType == NIPL_RESULT_DEFECT) {
		int nCount = GetItemCount();
		for (int i = 0; i < nCount; i++) {
			NIPLDefect *pDefect = (NIPLDefect *)GetItemData(i);
			if (pDefect) {
				Rect rc = pDefect->GetBoundingBox();
				if (pt.x >= rc.x && pt.x <= (rc.x + rc.width - 1)
					&& pt.y >= rc.y && pt.y <= (rc.y + rc.height - 1)) {
					return i;
				}
			}
		}
	}

	return -1;
}

int CResultList::FinrcBoundingBox(Rect rc)
{
	if (m_strResultType == NIPL_RESULT_FIND_BLOB) {
		int nCount = GetItemCount();
		for (int i = 0; i < nCount; i++) {
			NIPLBlob *pBlob = (NIPLBlob *)GetItemData(i);
			if (pBlob) {
				if (rc == pBlob->m_rcBoundingBox) {
					return i;
				}
			}
		}
	}
	else if (m_strResultType == NIPL_RESULT_FIND_LINE) {
		int nCount = GetItemCount();
		for (int i = 0; i < nCount; i++) {
			NIPLLine *pLine = (NIPLLine *)GetItemData(i);
			if (pLine) {
				if (rc == pLine->GetBoundingBox()) {
					return i;
				}
			}
		}
	}
	else if (m_strResultType == NIPL_RESULT_FIND_CIRCLE) {
		int nCount = GetItemCount();
		for (int i = 0; i < nCount; i++) {
			NIPLCircle *pCircle = (NIPLCircle *)GetItemData(i);
			if (pCircle) {
				if (rc == pCircle->GetBoundingBox()) {
					return i;
				}
			}
		}
	}
	else if (m_strResultType == NIPL_RESULT_DEFECT) {
		int nCount = GetItemCount();
		for (int i = 0; i < nCount; i++) {
			NIPLDefect *pDefect = (NIPLDefect *)GetItemData(i);
			if (pDefect) {
				if (rc == pDefect->m_rcBoundingBox) {
					return i;
				}
			}
		}
	}

	return -1;
}


void CResultList::SelectAll()
{
	int nCount = GetItemCount();
	for (int i = 0; i < nCount; i++) {
		SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
	}
}

void CResultList::UnselectAll()
{
	POSITION dPos = GetFirstSelectedItemPosition();
	while (dPos) {
		int nIndex = GetNextSelectedItem(dPos);
		SetItemState(nIndex, 0, LVIS_SELECTED);
	}
}

void CResultList::SelectItem(int nIndex, bool bSelect)
{
	int nCount = GetItemCount();
	if (nIndex < 0 || nIndex > nCount - 1) {
		return;
	}

	if (bSelect) {
		SetItemState(nIndex, LVIS_SELECTED, LVIS_SELECTED);
		EnsureVisible(nIndex, FALSE);
	}
	else {
		SetItemState(nIndex, 0, LVIS_SELECTED);
	}

	SetFocus();
}

bool CResultList::IsItemSelected(int nIndex)
{
	return (GetItemState(nIndex, LVIS_SELECTED) == LVIS_SELECTED);
}

void CResultList::ClearContents()
{
	DeleteAllItems();
	while (DeleteColumn(0));
}

void CResultList::SetResultType(wstring strResultType)
{
	m_strResultType = strResultType;
}

void CResultList::OnLvnColumnclick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	m_bSortDesc = !m_bSortDesc;

	if (m_strResultType == NIPL_RESULT_FIND_BLOB) {
		SortItems(SortBlobCompareFunc, pNMLV->iSubItem);
	}
	else if (m_strResultType == NIPL_RESULT_FIND_LINE) {
		SortItems(SortLineCompareFunc, pNMLV->iSubItem);
	}
	else if (m_strResultType == NIPL_RESULT_FIND_CIRCLE) {
		SortItems(SortCircleCompareFunc, pNMLV->iSubItem);
	}
	else if (m_strResultType == NIPL_RESULT_DEFECT) {
		SortItems(SortDefectCompareFunc, pNMLV->iSubItem);
	}

	*pResult = 0;
}

int CALLBACK CResultList::SortBlobCompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	int nRetVal = 0;

	NIPLBlob *pBlob1 = (NIPLBlob *)lParam1;
	NIPLBlob *pBlob2 = (NIPLBlob *)lParam2;
	int nColumnIndex = (int)lParamSort;

	switch (nColumnIndex) {
	case LCI_BLOB_INDEX: 
		nRetVal = (int)(pBlob1 - pBlob2); 
		break;
	case LCI_BLOB_BOUNDING_BOX_START_X: 
		nRetVal = pBlob1->m_rcBoundingBox.x - pBlob2->m_rcBoundingBox.x;
		break;
	case LCI_BLOB_BOUNDING_BOX_START_Y: 
		nRetVal = pBlob1->m_rcBoundingBox.y - pBlob2->m_rcBoundingBox.y; 
		break;
	case LCI_BLOB_BOUNDING_BOX_END_X: 
		nRetVal = (pBlob1->m_rcBoundingBox.x + pBlob1->m_rcBoundingBox.width) - (pBlob2->m_rcBoundingBox.x + pBlob2->m_rcBoundingBox.width);
		break;
	case LCI_BLOB_BOUNDING_BOX_END_Y: 
		nRetVal = (pBlob1->m_rcBoundingBox.y + pBlob1->m_rcBoundingBox.height) - (pBlob2->m_rcBoundingBox.y + pBlob2->m_rcBoundingBox.height);
		break;
	case LCI_BLOB_SIZE:
		nRetVal = pBlob1->m_nSize - pBlob2->m_nSize;
		break;
	}

	if (m_bSortDesc) {
		nRetVal = -nRetVal;
	}

	return nRetVal;
}

int CALLBACK CResultList::SortLineCompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	int nRetVal = 0;

	NIPLLine *pLine1 = (NIPLLine *)lParam1;
	NIPLLine *pLine2 = (NIPLLine *)lParam2;
	int nColumnIndex = (int)lParamSort;

	switch (nColumnIndex) {
	case LCI_LINE_INDEX:
		nRetVal = (int)(pLine1 - pLine2);
		break;
	case LCI_LINE_START_X:
		nRetVal = pLine1->m_ptStart.x - pLine2->m_ptStart.x;
		break;
	case LCI_LINE_START_Y:
		nRetVal = pLine1->m_ptStart.y - pLine2->m_ptStart.y;
		break;
	case LCI_LINE_END_X:
		nRetVal = pLine1->m_ptEnd.x - pLine2->m_ptEnd.x;
		break;
	case LCI_LINE_END_Y:
		nRetVal = pLine1->m_ptEnd.y - pLine2->m_ptEnd.y;
		break;
	case LCI_LINE_LENGTH:
		nRetVal = (int)(pLine1->m_nLength- pLine2->m_nLength);
		break;
	case LCI_LINE_ANGLE:
		nRetVal = (int)(pLine1->m_nAngle - pLine2->m_nAngle);
		break;
	}

	if (m_bSortDesc) {
		nRetVal = -nRetVal;
	}

	return nRetVal;
}


int CALLBACK CResultList::SortCircleCompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	int nRetVal = 0;

	NIPLCircle *pCircle1 = (NIPLCircle *)lParam1;
	NIPLCircle *pCircle2 = (NIPLCircle *)lParam2;
	int nColumnIndex = (int)lParamSort;

	switch (nColumnIndex) {
	case LCI_CIRCLE_INDEX:
		nRetVal = (int)(pCircle1 - pCircle2);
		break;
	case LCI_CIRCLE_CENTER_X:
		nRetVal = pCircle1->m_ptCenter.x - pCircle2->m_ptCenter.x;
		break;
	case LCI_CIRCLE_CENTER_Y:
		nRetVal = pCircle1->m_ptCenter.y - pCircle2->m_ptCenter.y;
		break;
	case LCI_CIRCLE_RADIUS:
		nRetVal = pCircle1->m_nRadius - pCircle2->m_nRadius;
		break;
	}

	if (m_bSortDesc) {
		nRetVal = -nRetVal;
	}

	return nRetVal;
}

int CALLBACK CResultList::SortDefectCompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	int nRetVal = 0;

	NIPLDefect *pDefect1 = (NIPLDefect *)lParam1;
	NIPLDefect *pDefect2 = (NIPLDefect *)lParam2;
	int nColumnIndex = (int)lParamSort;

	switch (nColumnIndex) {
	case LCI_DEFECT_INDEX:
		nRetVal = (int)(pDefect1 - pDefect2);
		break;
	case LCI_DEFECT_TYPE:
		nRetVal = (int)(pDefect1->m_nType - pDefect2->m_nType);
		break;
	case LCI_DEFECT_BOUNDING_BOX_START_X:
		nRetVal = pDefect1->m_rcBoundingBox.x - pDefect2->m_rcBoundingBox.x;
		break;
	case LCI_DEFECT_BOUNDING_BOX_START_Y:
		nRetVal = pDefect1->m_rcBoundingBox.y - pDefect2->m_rcBoundingBox.y;
		break;
	case LCI_DEFECT_BOUNDING_BOX_END_X:
		nRetVal = (pDefect1->m_rcBoundingBox.x + pDefect1->m_rcBoundingBox.width) - (pDefect2->m_rcBoundingBox.x + pDefect2->m_rcBoundingBox.width);
		break;
	case LCI_DEFECT_BOUNDING_BOX_END_Y:
		nRetVal = (pDefect1->m_rcBoundingBox.y + pDefect1->m_rcBoundingBox.height) - (pDefect2->m_rcBoundingBox.y + pDefect2->m_rcBoundingBox.height);
		break;
	case LCI_DEFECT_SIZE:
		nRetVal = pDefect1->m_nSize - pDefect2->m_nSize;
		break;
	}

	if (m_bSortDesc) {
		nRetVal = -nRetVal;
	}

	return nRetVal;
}
