
#include "stdafx.h"

#include "ParameterView.h"
#include "Resource.h"
#include "MainFrm.h"
#include "NIPTool.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////
// CResourceViewBar

CParameterView::CParameterView()
{
//	m_nComboHeight = 0;
	m_bMenuProcess = false;
}

CParameterView::~CParameterView()
{
}

BEGIN_MESSAGE_MAP(CParameterView, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND(ID_EXPAND_ALL, OnExpandAllParameter)
	ON_UPDATE_COMMAND_UI(ID_EXPAND_ALL, OnUpdateExpandAllParameter)
	ON_COMMAND(ID_RUN_PROCESS, OnRunProcess)
	ON_UPDATE_COMMAND_UI(ID_RUN_PROCESS, OnUpdateRunProcess)
	ON_COMMAND(ID_ADD_PROCESS, OnAddProcess)
	ON_UPDATE_COMMAND_UI(ID_ADD_PROCESS, OnUpdateAddProcess)
	ON_WM_SETFOCUS()
	ON_WM_SETTINGCHANGE()
	ON_REGISTERED_MESSAGE(AFX_WM_PROPERTY_CHANGED, &CParameterView::OnPropertyChanged)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CResourceViewBar message handlers

void CParameterView::AdjustLayout()
{
	if (GetSafeHwnd () == NULL || (AfxGetMainWnd() != NULL && AfxGetMainWnd()->IsIconic()))
	{
		return;
	}

	CRect rectClient;
	GetClientRect(rectClient);

	int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

	m_wndToolBar.SetWindowPos(NULL, rectClient.left, rectClient.top, rectClient.Width(), cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
	m_wndName.SetWindowPos(NULL, rectClient.left, rectClient.top + cyTlb, rectClient.Width(), cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
	m_wndParamList.SetWindowPos(NULL, rectClient.left, rectClient.top + 2*cyTlb, rectClient.Width(), rectClient.Height() - 2*cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
}

void CParameterView::Clear()
{
	m_dProcess.Clear();
	if (m_wndParamList.GetSafeHwnd()) {
		m_wndParamList.RemoveAll();
	}
}

int CParameterView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rc(0, 0, 0, 0);

	const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | WS_BORDER | WS_EX_CLIENTEDGE | SS_CENTER;
	if (!m_wndName.Create(L"", dwViewStyle, rc, this, 1))
	{
		TRACE0("Failed to create Name window.\n");
		return -1;      // fail to create
	}

//	m_wndName.SetExtendedStyle()

	// Create combo:
/*
	const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST | WS_BORDER | CBS_SORT | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

	if (!m_wndObjectCombo.Create(dwViewStyle, rectDummy, this, 1))
	{
		TRACE0("Failed to create Parameter Combo \n");
		return -1;      // fail to create
	}

	m_wndObjectCombo.AddString(_T("Application"));
	m_wndObjectCombo.AddString(_T("Parameter Window"));
	m_wndObjectCombo.SetCurSel(0);

	CRect rectCombo;
	m_wndObjectCombo.GetClientRect (&rectCombo);

	m_nComboHeight = rectCombo.Height();
*/

	if (!m_wndParamList.Create(WS_VISIBLE | WS_CHILD, rc, this, 2))
	{
		TRACE0("Failed to create Parameter Grid \n");
		return -1;      // fail to create
	}

	InitParamList();

	m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_PARAMETER);
	m_wndToolBar.LoadToolBar(IDR_PARAMETER, 0, 0, TRUE /* Is locked */);
	m_wndToolBar.CleanUpLockedImages();
	m_wndToolBar.LoadBitmap(theApp.m_bHiColorIcons ? IDB_PARAMETER_HC : IDR_PARAMETER, 0, 0, TRUE /* Locked */);

	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));
	m_wndToolBar.SetOwner(this);

	// All commands will be routed via this control , not via the parent frame:
	m_wndToolBar.SetRouteCommandsViaFrame(FALSE);

	AdjustLayout();
	return 0;
}

void CParameterView::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}

void CParameterView::OnExpandAllParameter()
{
	m_wndParamList.ExpandAll();
}

void CParameterView::OnUpdateExpandAllParameter(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!m_dProcess.IsEmpty());
}

/*
void CParameterView::OnSortParameter()
{
	m_wndParamList.SetAlphabeticMode(!m_wndParamList.IsAlphabeticMode());
}

void CParameterView::OnUpdateSortParameter(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_wndParamList.IsAlphabeticMode());
}
*/

void CParameterView::OnRunProcess()
{
	// TODO: Add your command handler code here
	CNIPToolDoc *pDoc = GetDocument();
	if (pDoc) {
		SetPropListToProcess();
		pDoc->RunProcess(&m_dProcess);
	}
}

void CParameterView::OnUpdateRunProcess(CCmdUI* pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(!m_dProcess.IsEmpty());
}

void CParameterView::OnAddProcess()
{
	// TODO: Add your command handler code here
	CNIPToolDoc *pDoc = GetDocument();
	if (pDoc) {
		SetPropListToProcess();
		CMainFrame *pMainFrame = (CMainFrame *)AfxGetMainWnd();
		if (pMainFrame) {
			pMainFrame->AddProcessToJobView(&m_dProcess);
			m_bMenuProcess = false;
			m_wndParamList.MarkModifiedProperties(FALSE);
		}
	}
}

void CParameterView::OnUpdateAddProcess(CCmdUI* pCmdUI)
{
	// TODO: Add your command update UI handler code here
	BOOL bEnable = FALSE;
	CNIPToolDoc *pDoc = GetDocument();
	if (pDoc != nullptr && !m_dProcess.IsEmpty()) {
		bEnable = TRUE;
	}

	pCmdUI->Enable(bEnable);
}

void CParameterView::ChangeProcess()
{
	// TODO: Add your command handler code here
	CNIPToolDoc *pDoc = GetDocument();
	if (pDoc) {
		SetPropListToProcess();
		if (pDoc->ModifyProcess(&m_dProcess)) {
			CMainFrame *pMainFrame = (CMainFrame *)AfxGetMainWnd();
			if (pMainFrame) {
				pMainFrame->UpdateJobView(true);
			}
		}
	}
}

void CParameterView::InitParamList()
{
	SetParamListFont();

	m_wndParamList.EnableHeaderCtrl(FALSE);
	m_wndParamList.EnableDescriptionArea();
	m_wndParamList.SetVSDotNetLook();
	m_wndParamList.MarkModifiedProperties(FALSE);
}

void CParameterView::UpdateParameter(NIPJobProcess *pProcess, bool bMenu)
{
	Clear();

	if (pProcess == nullptr) {
		m_wndName.SetWindowText(L"");
		m_wndParamList.Invalidate(TRUE);
		return;
	}

	m_bMenuProcess = bMenu;

	if (!bMenu) {
		CMainFrame *pMainFrame = (CMainFrame *)AfxGetMainWnd();
		NIPJob *pJob = pMainFrame->GetProcessMenu();
		NIPJobProcess *pMenuProcess = pJob->FindProcess(pProcess->m_strName);
		if (pMenuProcess) {
			m_dProcess.CopyWithPath(pMenuProcess, pProcess->m_strPath);
			m_dProcess.UpdateParamValues(pProcess);
		}
		else {
			m_dProcess = *pProcess;
		}

		m_wndParamList.MarkModifiedProperties(FALSE);
	}
	else {
		m_dProcess = *pProcess;

		// if it came from menu, set the modified property to true in order to apply it to the NIPJob process
		m_wndParamList.MarkModifiedProperties(TRUE);		
	}

	m_wndName.SetWindowText(m_dProcess.m_strName.c_str());

	AddPropGroup(&m_dProcess);
}

void CParameterView::AddPropGroup(NIPJobProcess *pProcess, CMFCPropertyGridProperty *pParentPropGroup)
{
	for (auto pParam : pProcess->m_listParam) {
		if (pParam->m_bGroup) {
			// if it's the "Input / Output" param group of subprocess, skip it..
			if (pProcess->m_bSubProcess && pParam->m_strName == STR_PARAM_GROUP_INPUT_OUTPUT) {
				continue;
			}

			AddPropGroup(pParam, pParentPropGroup);
		}
		else {
			AddProp(pParam, pParentPropGroup);
		}
	}

	for (auto pSubProcess : pProcess->m_listSubProcess) {
		wstring strPropGroupName = L"[";
		strPropGroupName += pSubProcess->m_strName + L"]";
		CMFCPropertyGridProperty* pPropGroup = new CMFCPropertyGridProperty(strPropGroupName.c_str());

		AddPropGroup(pSubProcess, pPropGroup);

		if (pParentPropGroup == nullptr) {
			m_wndParamList.AddProperty(pPropGroup);
		}
		else {
			pParentPropGroup->AddSubItem(pPropGroup);
		}
	}
}

void CParameterView::AddPropGroup(NIPJobParam *pGroup, CMFCPropertyGridProperty *pParentPropGroup)
{
	if (pGroup == nullptr) {
		return;
	}

	wstring strPropGroupName = pGroup->m_strName;
	CMFCPropertyGridProperty* pPropGroup = new CMFCPropertyGridProperty(strPropGroupName.c_str());

	for (auto pParam : pGroup->m_listParam) {
		if (pParam->m_bGroup) {
			AddPropGroup(pParam, pPropGroup);
		}
		else {
			AddProp(pParam, pPropGroup);
		}
	}

	if (pParentPropGroup == nullptr) {
		m_wndParamList.AddProperty(pPropGroup);
	}
	else {
		pParentPropGroup->AddSubItem(pPropGroup);
	}
}

void CParameterView::AddProp(NIPJobParam *pParam, CMFCPropertyGridProperty *pPropGroup)
{
	if (pParam == nullptr) {
		return;
	}

	wstring strName = pParam->m_strName;
	wstring strValue= pParam->m_strValue;
	wstring strMin = pParam->m_strMin;
	wstring strMax = pParam->m_strMax;
	wstring strDesc = pParam->m_strDesc;

	CMFCPropertyGridProperty* pParamGrid = nullptr;
	
	if (pParam->m_nType == NIPJobParam::MPT_INT || pParam->m_nType == NIPJobParam::MPT_FLOAT || pParam->m_nType == NIPJobParam::MPT_STRING) {
		const wchar_t *pValue = strValue.c_str();
		pParamGrid = new CMFCPropertyGridProperty(strName.c_str(), (_variant_t)pValue, strDesc.c_str());
	}
	else if (pParam->m_nType == NIPJobParam::MPT_BOOL) {
		bool bValue = (strValue != L"false");
		pParamGrid = new CMFCPropertyGridProperty(strName.c_str(), (_variant_t)bValue, strDesc.c_str());
	}
	else if (pParam->m_nType == NIPJobParam::MPT_COMBO) {
		const wchar_t *pValue = strValue.c_str();
		pParamGrid = new CMFCPropertyGridProperty(strName.c_str(), (_variant_t)pValue, strDesc.c_str());
		for (auto strComboValue : pParam->m_listComboValue) {
			pParamGrid->AddOption(strComboValue.c_str());
		}
		pParamGrid->AllowEdit(FALSE);
	}
	else {
		return;
	}

	if (pPropGroup == nullptr) {
		m_wndParamList.AddProperty(pParamGrid);
	}
	else {
		pPropGroup->AddSubItem(pParamGrid);
	}
}

void CParameterView::SetPropListToProcess()
{
	m_dProcess.Clear(true);

	// set enable true as default
	m_dProcess.m_bEnable = true;

	NIPJobParam *pParam = nullptr;

	int nCount = m_wndParamList.GetPropertyCount();
	for (int i = 0; i < nCount; i++) {
		CMFCPropertyGridProperty *pProp = m_wndParamList.GetProperty(i);
		if (pProp == nullptr) {
			continue;
		}

		if (pProp->IsGroup()) {
			wstring strName = pProp->GetName();
			if (strName[0] == '[') {		// Sub Processs
				SetPropGroupToSubProcess(pProp, &m_dProcess);
			}
			else {
				SetPropGroupToParamGroup(pProp, &m_dProcess, true);
			}
		}
		else {
			SetPropToParam(pProp, (void *)&m_dProcess, true);
		}
	}
}

void CParameterView::SetPropGroupToSubProcess(CMFCPropertyGridProperty *pPropGroup, NIPJobProcess *pProcess)
{
	if (pPropGroup == nullptr || pProcess == nullptr) {
		return;
	}

	wstring strName = pPropGroup->GetName();
	size_t nLength = strName.size();
	strName = strName.substr(1, nLength - 2);	// remove '[' and ']'

	NIPJobProcess *pSubProcess = pProcess->AddSubProcess(strName);
	if (pSubProcess == nullptr) {
		return;
	}

	int nCount = pPropGroup->GetSubItemsCount();
	for (int i = 0; i < nCount; i++) {
		CMFCPropertyGridProperty *pProp = pPropGroup->GetSubItem(i);
		if (pProp == nullptr) {
			continue;
		}

		if (pProp->IsGroup()) {
			wstring strName = pProp->GetName();
			if (strName[0] == '[') {			// Nested Sub Process
				SetPropGroupToSubProcess(pProp, pSubProcess);
			}
			else {
				SetPropGroupToParamGroup(pProp, pSubProcess, true);
			}
		}
		else {
			SetPropToParam(pProp, (void *)pSubProcess, true);
		}
	}
}


void CParameterView::SetPropGroupToParamGroup(CMFCPropertyGridProperty *pPropGroup, void *pParent, bool bProcess)
{
	if (pPropGroup == nullptr || pParent == nullptr) {
		return;
	}

	NIPJobParam *pGroup;
	if (bProcess) {
		pGroup = ((NIPJobProcess *)pParent)->AddParamGroup(pPropGroup->GetName());
	}
	else {
		pGroup = ((NIPJobParam *)pParent)->AddParamGroup(pPropGroup->GetName());
	}

	int nCount = pPropGroup->GetSubItemsCount();
	for (int i = 0; i < nCount; i++) {
		CMFCPropertyGridProperty *pParamGrid = pPropGroup->GetSubItem(i);
		if (pParamGrid == nullptr) {
			continue;
		}

		if (pParamGrid->IsGroup()) {
			SetPropGroupToParamGroup(pParamGrid, (void *)pGroup, false);
		}
		else {
			SetPropToParam(pParamGrid, (void *)pGroup, false);
		}
	}
}

void CParameterView::SetPropToParam(CMFCPropertyGridProperty *pParamGrid, void *pParent, bool bProcess)
{
	if (pParamGrid == nullptr) {
		return;
	}

	const COleVariant &dValue = pParamGrid->GetValue();
	wstring strValue = L"";
	wstring strType = L"";
	int nOptionCount = pParamGrid->GetOptionCount();
	switch (dValue.vt) {
	case VT_I4 :
	case VT_INT:
		strValue = to_wstring(dValue.intVal);
		strType = L"int";
		break;
	case VT_R4:
		strValue = to_wstring(dValue.fltVal);
		strType = L"float";
		break;
	case VT_BOOL:
		if (dValue.boolVal == 0) strValue = L"false";
		else strValue = L"true";
		strType = L"bool";
		break;
	default:
		strValue = wstring((wchar_t *)dValue.pbstrVal);
		if (nOptionCount > 0) {
			strType = L"combo";
		}
		else {
			strType = L"string";
		}
		break;
	}

	NIPJobParam *pNewParam = nullptr;
	if (bProcess) {
		pNewParam = ((NIPJobProcess *)pParent)->AddParam(pParamGrid->GetName(), strValue, strType);
	}
	else {
		pNewParam = ((NIPJobParam *)pParent)->AddParam(pParamGrid->GetName(), strValue, strType);
	}

	if (pNewParam) {
		CMFCPropertyGridPropertyEx *pParamGridEx = static_cast<CMFCPropertyGridPropertyEx *>(pParamGrid);
		if (pParamGridEx) {
			if (pParamGridEx->GetMaxValue() != 0) {
				wstring strMin = to_wstring(pParamGridEx->GetMinValue());
				pNewParam->m_strMin = strMin;

				wstring strMax = to_wstring(pParamGridEx->GetMaxValue());
				pNewParam->m_strMax = strMax;
			}
		}

		wstring strDesc(LPCTSTR(pParamGrid->GetDescription()));
		pNewParam->m_strDesc = strDesc;

		if (nOptionCount > 0) {
			for (int i = 0; i < nOptionCount; i++) {
				pNewParam->AddComboValue(pParamGrid->GetOption(i));
			}
		}
	}
}

void CParameterView::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);
	m_wndParamList.SetFocus();
}

void CParameterView::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
	CDockablePane::OnSettingChange(uFlags, lpszSection);
	SetParamListFont();
}

void CParameterView::SetParamListFont()
{
	::DeleteObject(m_fntParamList.Detach());

	LOGFONT lf;
	afxGlobalData.fontRegular.GetLogFont(&lf);

	NONCLIENTMETRICS info;
	info.cbSize = sizeof(info);

	afxGlobalData.GetNonClientMetrics(info);

	lf.lfHeight = info.lfMenuFont.lfHeight;
	lf.lfWeight = info.lfMenuFont.lfWeight;
	lf.lfItalic = info.lfMenuFont.lfItalic;

	m_fntParamList.CreateFontIndirect(&lf);

	m_wndParamList.SetFont(&m_fntParamList);
	//_wndObjectCombo.SetFont(&m_fntParamList);
	m_wndName.SetFont(&m_fntParamList);
}

LRESULT CParameterView::OnPropertyChanged(WPARAM wp, LPARAM lp)
{
	if (!m_bMenuProcess) {
		ChangeProcess();
	}
//	m_wndParamList.MarkModifiedProperties(TRUE);

	return 0;
}

CNIPToolDoc *CParameterView::GetDocument()
{
	CMainFrame *pMainFrame = (CMainFrame *)AfxGetMainWnd();
	if (pMainFrame == nullptr) {
		return nullptr;
	}

	return pMainFrame->GetDocument();
}


//void CParameterView::PostNcDestroy()
//{
//	// TODO: Add your specialized code here and/or call the base class
//	m_dProcess.Clear();
//
//	CDockablePane::PostNcDestroy();
//}


void CParameterView::PostNcDestroy()
{
	// TODO: Add your specialized code here and/or call the base class
	Clear();

	CDockablePane::PostNcDestroy();
}
