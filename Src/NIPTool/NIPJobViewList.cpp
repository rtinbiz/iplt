
#include "stdafx.h"
#include "NIPJobViewList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNIPJobViewList

CNIPJobViewList::CNIPJobViewList()
{
}

CNIPJobViewList::~CNIPJobViewList()
{
}

BEGIN_MESSAGE_MAP(CNIPJobViewList, CListCtrl)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNIPJobViewList message handlers

BOOL CNIPJobViewList::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	BOOL bRes = CListCtrl::OnNotify(wParam, lParam, pResult);

	NMHDR* pNMHDR = (NMHDR*)lParam;
	ASSERT(pNMHDR != NULL);

	if (pNMHDR && pNMHDR->code == TTN_SHOW && GetToolTips() != NULL)
	{
		GetToolTips()->SetWindowPos(&wndTop, -1, -1, -1, -1, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOSIZE);
	}

	return bRes;
}

void CNIPJobViewList::SelectAll()
{
	int nCount = GetItemCount();
	for (int i = 0; i < nCount; i++) {
		SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
	}
}

void CNIPJobViewList::UnselectAll()
{
	POSITION dPos = GetFirstSelectedItemPosition();
	while (dPos) {
		int nIndex = GetNextSelectedItem(dPos);
		SetItemState(nIndex, 0, LVIS_SELECTED);
	}
}

void CNIPJobViewList::SelectItem(int nIndex, bool bSelect)
{
	int nCount = GetItemCount();
	if (nIndex < 0 || nIndex > nCount - 1) {
		return;
	}

	if (bSelect) {
		SetItemState(nIndex, LVIS_SELECTED, LVIS_SELECTED);
		EnsureVisible(nIndex, FALSE);
	}
	else {
		SetItemState(nIndex, 0, LVIS_SELECTED);
	}

	SetFocus();
}

int CNIPJobViewList::GetSelectedItemIndex()
{
	int nIndex = -1;
	POSITION dPos = GetFirstSelectedItemPosition();
	if (dPos) {
		nIndex = GetNextSelectedItem(dPos);
	}

	return nIndex;
}

void CNIPJobViewList::SelectNextItem()
{
	int nIndex = GetSelectedItemIndex();
	int nCount = GetItemCount();
	if (nIndex < 0 || nIndex >= nCount - 1) {
		return;
	}

	nIndex++;

	UnselectAll();
	SelectItem(nIndex);
}

bool CNIPJobViewList::HasItem()
{
	return (GetItemCount() > 0);
}

bool CNIPJobViewList::IsItemSelected(int nIndex)
{
	return (GetItemState(nIndex, LVIS_SELECTED) == LVIS_SELECTED);
}

bool CNIPJobViewList::IsAnyItemSelected()
{
	return (GetSelectedItemIndex() >= 0);
}

bool CNIPJobViewList::IsOneItemSelected()
{
	return (GetSelectedCount() == 1);
}
