// This MFC Samples source code demonstrates using MFC Microsoft Office Fluent User Interface 
// (the "Fluent UI") and is provided only as referential material to supplement the 
// Microsoft Foundation Classes Reference and related electronic documentation 
// included with the MFC C++ library software.  
// License terms to copy, use or distribute the Fluent UI are available separately.  
// To learn more about our Fluent UI licensing program, please visit 
// http://go.microsoft.com/fwlink/?LinkId=238214.
//
// Copyright (C) Microsoft Corporation
// All rights reserved.

#include "stdafx.h"

#include "LogView.h"
#include "Resource.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COutputBar

CLogView::CLogView()
{
}

CLogView::~CLogView()
{
}

BEGIN_MESSAGE_MAP(CLogView, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

int CLogView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// Create tabs window:
	if (!m_wndTabs.Create(CMFCTabCtrl::STYLE_3D_ROUNDED, rectDummy, this, 1))
	{
		TRACE0("Failed to create output tab window\n");
		return -1;      // fail to create
	}

	// Create output panes:
	const DWORD dwStyle = LBS_NOINTEGRALHEIGHT | WS_CHILD | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL;

	if (!m_wndLogOutput.Create(dwStyle, rectDummy, &m_wndTabs, 2)) {
		TRACE0("Failed to create output windows\n");
		return -1;      // fail to create
	}

	UpdateFonts();

	CString strTabName;
	BOOL bNameValid;

	// Attach list windows to tab:
	bNameValid = strTabName.LoadString(IDS_OUTPUT_TAB);
	ASSERT(bNameValid);
	m_wndTabs.AddTab(&m_wndLogOutput, strTabName, (UINT)0);

	return 0;
}

void CLogView::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);

	// Tab control should cover the whole client area:
	m_wndTabs.SetWindowPos (NULL, -1, -1, cx, cy, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER);
}

void CLogView::AdjustHorzScroll(CListBox& wndListBox)
{
	CClientDC dc(this);
	CFont* pOldFont = dc.SelectObject(&afxGlobalData.fontRegular);

	int cxExtentMax = 0;

	for (int i = 0; i < wndListBox.GetCount(); i ++)
	{
		CString strItem;
		wndListBox.GetText(i, strItem);

		cxExtentMax = max(cxExtentMax, (int)dc.GetTextExtent(strItem).cx);
	}

	wndListBox.SetHorizontalExtent(cxExtentMax);
	dc.SelectObject(pOldFont);
}

void CLogView::ProcessNotify(wstring strName, NIPL_ERR nErr)
{
	wstring strLog = strName + _T(" ");
	switch (nErr) {
	case NIPL_ERR_SUCCESS: strLog += STR_PROCESS_SUCESS; break;
	case NIPL_ERR_FAIL: strLog += STR_PROCESS_FAIL; break;
	case NIPL_ERR_PASS: strLog += STR_PROCESS_PASS; break;
	case NIPL_ERR_FAIL_TO_OPEN_FILE: strLog += STR_PROCESS_FAIL_TO_OPEN_FILE; break;
	case NIPL_ERR_FAIL_TO_SAVE_FILE: strLog += STR_PROCESS_FAIL_TO_SAVE_FILE; break;
	case NIPL_ERR_FAIL_TO_LOAD_IMAGE: strLog += STR_PROCESS_FAIL_TO_LOAD_IMAGE; break;
	case NIPL_ERR_FAIL_TO_SAVE_IMAGE: strLog += STR_PROCESS_FAIL_TO_SAVE_IMAGE; break;
	case NIPL_ERR_FAIL_TO_CONVERT_IMAGE: strLog += STR_PROCESS_FAIL_TO_CONVERT_IMAGE; break;
	case NIPL_ERR_FAIL_INVALID_IMAGE: strLog += STR_PROCESS_FAIL_INVALID_IMAGE; break;
	case NIPL_ERR_FAIL_INVALID_PARAM_EXPRESSION: strLog += STR_PROCESS_FAIL_INVALID_PARAM_EXPRESSION; break;
	case NIPL_ERR_FAIL_INVALID_DATA: strLog += STR_PROCESS_FAIL_INVALID_DATA; break;
		
	case NIPL_ERR_FAIL_NOT_SUPPORT_PROCESS: strLog += STR_PROCESS_FAIL_NOT_SUPPORT_PROCESS; break;
	case NIPL_ERR_FAIL_NOT_MATCH_MASK_SIZE: strLog += STR_PROCESS_FAIL_NOT_MATCH_MASK_SIZE; break;
	case NIPL_ERR_FAIL_NOT_MATCH_IMAGE_SIZE: strLog += STR_PROCESS_FAIL_NOT_MATCH_IMAGE_SIZE; break;

	default:
		{
			strLog += STR_PROCESS_FAIL;
			strLog += _T(" (");

			switch (nErr) {
			case NIPL_ERR_NO_INPUT: strLog += STR_PROCESS_NO_INPUT; break;
			case NIPL_ERR_NO_OUTPUT: strLog += STR_PROCESS_NO_OUTPUT; break;
			case NIPL_ERR_NO_PARAM: strLog += STR_PROCESS_NO_PARAM; break;
			case NIPL_ERR_NO_PARAMS: strLog += STR_PROCESS_NO_PARAMS; break;
			case NIPL_ERR_INVALID_IMAGE_TYPE: strLog += STR_PROCESS_INVALID_IMAGE_TYPE; break;
			case NIPL_ERR_INVALID_PARAM_VALUE: strLog += STR_PROCESS_INVALID_PARAM_VALUE; break;
			default:
				{
					strLog += STR_PROCESS_FAIL_UNKNOW_REASON;
					strLog += _T(" : ");
					strLog += to_wstring(nErr);
					break;
				}
			}
			strLog += _T(")");
		}
	}

	AddLog(strLog);
}

void CLogView::AddLog(wstring strLog)
{
	SYSTEMTIME dTime;
	GetLocalTime(&dTime);

	wchar_t szTime[256];
	swprintf_s(szTime, L"[%02d:%02d:%02d.%03d] ", dTime.wHour, dTime.wMinute, dTime.wSecond, dTime.wMilliseconds);

	wstring strFullLog(szTime);
	strFullLog += strLog;

	int nIndex = m_wndLogOutput.AddString(strFullLog.c_str());
	m_wndLogOutput.SetCurSel(nIndex);
}

/*
void CLogView::FillDebugWindow()
{
	m_wndLogViewDebug.AddString(_T("Debug output is being displayed here."));
	m_wndLogViewDebug.AddString(_T("The output is being displayed in rows of a list view"));
	m_wndLogViewDebug.AddString(_T("but you can change the way it is displayed as you wish..."));
}

void CLogView::FillFindWindow()
{
	m_wndLogViewFind.AddString(_T("Find output is being displayed here."));
	m_wndLogViewFind.AddString(_T("The output is being displayed in rows of a list view"));
	m_wndLogViewFind.AddString(_T("but you can change the way it is displayed as you wish..."));
}
*/

void CLogView::UpdateFonts()
{
	m_wndLogOutput.SetFont(&afxGlobalData.fontRegular);
//	m_wndLogViewDebug.SetFont(&afxGlobalData.fontRegular);
//	m_wndLogViewFind.SetFont(&afxGlobalData.fontRegular);
}

/////////////////////////////////////////////////////////////////////////////
// CLogList1

CLogList::CLogList()
{
}

CLogList::~CLogList()
{
}

BEGIN_MESSAGE_MAP(CLogList, CListBox)
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_CLEAR, OnEditClear)
	ON_COMMAND(ID_VIEW_LOG, OnViewLog)
	ON_WM_WINDOWPOSCHANGING()
END_MESSAGE_MAP()
/////////////////////////////////////////////////////////////////////////////
// CLogList message handlers

void CLogList::OnContextMenu(CWnd* /*pWnd*/, CPoint point)
{
	CMenu menu;
	menu.LoadMenu(IDR_LOG_POPUP);

	CMenu* pSumMenu = menu.GetSubMenu(0);

	if (AfxGetMainWnd()->IsKindOf(RUNTIME_CLASS(CMDIFrameWndEx)))
	{
		CMFCPopupMenu* pPopupMenu = new CMFCPopupMenu;

		if (!pPopupMenu->Create(this, point.x, point.y, (HMENU)pSumMenu->m_hMenu, FALSE, TRUE))
			return;

		((CMDIFrameWndEx*)AfxGetMainWnd())->OnShowPopupMenu(pPopupMenu);
		UpdateDialogControls(this, FALSE);
	}

	SetFocus();
}

void CLogList::OnEditCopy()
{
}

void CLogList::OnEditClear()
{
	ResetContent();
}

void CLogList::OnViewLog()
{
	CDockablePane* pParentBar = DYNAMIC_DOWNCAST(CDockablePane, GetOwner());
	CMDIFrameWndEx* pMainFrame = DYNAMIC_DOWNCAST(CMDIFrameWndEx, GetTopLevelFrame());

	if (pMainFrame != NULL && pParentBar != NULL)
	{
		pMainFrame->SetFocus();
		pMainFrame->ShowPane(pParentBar, FALSE, FALSE, FALSE);
		pMainFrame->RecalcLayout();
	}
}
