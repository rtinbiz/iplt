#pragma once

#define LANGUAGE_KOREAN
//#define LANGUAGE_ENGLISH

#ifdef LANGUAGE_KOREAN
#define STR_LOAD_DATA_FAIL L"프로그램 구성 파일 읽기에 실패하였습니다."
#define STR_PROCESS_SUCESS L"작업을 성공적으로 완료하였습니다."
#define STR_PROCESS_FAIL L"작업에 실패하였습니다."
#define STR_PROCESS_PASS L"작업이 수행되지 않고 PASS 되었습니다."
#define STR_PROCESS_EXCEPTION L"작업 수행시 Exception 이 발생하였습니다."
#define STR_PROCESS_FAIL_TO_OPEN_FILE L"파일 열기에 실패하였습니다."
#define STR_PROCESS_FAIL_TO_SAVE_FILE L"파일 저장에 실패하였습니다."
#define STR_PROCESS_FAIL_TO_LOAD_IMAGE L"이미지 로드에 실패하였습니다."
#define STR_PROCESS_FAIL_TO_SAVE_IMAGE L"이미지 저장에 실패하였습니다."
#define STR_PROCESS_FAIL_TO_CONVERT_IMAGE L"이미지 변환에 실패하였습니다."
#define STR_PROCESS_FAIL_INVALID_IMAGE L"는 유효한 이미지가 아닙니다."
#define STR_PROCESS_FAIL_INVALID_PARAM_EXPRESSION L"은 유효한 표현식이 아닙니다."
#define STR_PROCESS_FAIL_INVALID_DATA L"가 유효하지 않습니다."
#define STR_PROCESS_FAIL_NOT_SUPPORT_PROCESS L"작업은 아직 지원되지 않습니다."
#define STR_PROCESS_FAIL_NOT_MATCH_MASK_SIZE L"의 크기가 원본 이미지와 일치하지 않습니다."
#define STR_PROCESS_FAIL_NOT_MATCH_IMAGE_SIZE L"작업 대상 이미지의 크기가 원본 이미지와 일치하지 않습니다."
#define STR_PROCESS_FAIL_UNKNOW_REASON L"확인 불가"

#define STR_PROCESS_NO_INPUT L"입력 정보가 설정되지 않았습니다."
#define STR_PROCESS_NO_OUTPUT L"출력 정보가 설정되지 않았습니다."
#define STR_PROCESS_NO_PARAM L"파라미터가 설정되지 않았습니다."
#define STR_PROCESS_NO_PARAMS L"파라미터들이 설정되지 않았습니다."
#define STR_PROCESS_INVALID_IMAGE_TYPE L"이미지 타입이 맞지 않습니다."
#define STR_PROCESS_INVALID_PARAM_VALUE L"파라미터 값이 적절하지 않습니다."

#define STR_NIPTOOLVIEW_LIST_COLUMM_SEQ L"Seq"
#define STR_NIPTOOLVIEW_LIST_COLUMM_PROCESS L"Process"
#define STR_NIPTOOLVIEW_LIST_COLUMM_PARAMETER L"Parameter"

#define STR_RESULT_LIST_COLUMM_INDEX L"Index"
 
#define STR_NIPTOOL_PROCESS_MENU_FILENAME L"NIPTool.npm"

#define STR_PARAM_GROUP_INPUT_OUTPUT L"Input / Output"

#define STR_NAME_INPUT L"Input"

#elif LANGUAGE_ENGLISH
#endif
