// CSplitterBar.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SplitterBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSplitterBar

IMPLEMENT_DYNAMIC(CSplitterBar, CWnd)

BEGIN_MESSAGE_MAP(CSplitterBar, CWnd)
	//{{AFX_MSG_MAP(CSplitterBar)
	ON_WM_PAINT()
	ON_WM_NCHITTEST()
	ON_WM_CREATE()
	ON_WM_SETCURSOR()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CSplitterBar::CSplitterBar()
{
	m_rcSplitter = CRect(0, 0, 0, 0);
	m_bDragging=FALSE;
	m_pwndLeftPane=m_pwndRightPane=NULL;
}

CSplitterBar::~CSplitterBar()
{
}

BOOL CSplitterBar::Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, BOOL bHorizontal)
{
	CWnd* pWnd = this;
	m_bHorizontal=bHorizontal;
	return pWnd->Create(NULL, L"", dwStyle, rect, pParentWnd, nID);
}

/////////////////////////////////////////////////////////////////////////////
// CSplitterBar message handlers

void CSplitterBar::OnPaint() 
{
	CPaintDC dc(this);

	CRect rc;
	GetClientRect(&rc);
	dc.FillSolidRect(&rc, ::GetSysColor(COLOR_3DFACE));
}

LRESULT CSplitterBar::OnNcHitTest(CPoint point) 
{	
	return HTCLIENT;
}

int CSplitterBar::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetWindowPos(&CWnd::wndTop,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);

	return 0;
}

BOOL CSplitterBar::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	CPoint ptCursor=GetMessagePos();		
	if(IsCursorOverSplitter(ptCursor))
	{
		::SetCursor( AfxGetApp()->LoadCursor(m_bHorizontal?AFX_IDC_VSPLITBAR:AFX_IDC_HSPLITBAR));	
		return TRUE;
	}

	return CWnd::OnSetCursor(pWnd, nHitTest, message);
}

BOOL CSplitterBar::IsCursorOverSplitter( const CPoint& ptCursor )
{
	CRect rectSplitter;
	GetWindowRect(rectSplitter);
	return rectSplitter.PtInRect( ptCursor );
}

void CSplitterBar::OnMouseMove(UINT nFlags, CPoint point) 
{
	if(nFlags & MK_LBUTTON && m_bDragging)
	{
		GetCursorPos(&point);
		DrawDraggingBar(point);
		return;
	}

	CWnd::OnMouseMove(nFlags, point);
}

void CSplitterBar::OnLButtonDown(UINT nFlags, CPoint point) 
{	
	ClientToScreen(&point);
/*
	if(IsCursorOverSplitter(point))
	{
*/
	SetCapture();
	m_bDragging = TRUE;

	GetCursorPos(&point);
	DrawDraggingBar(point, DRAG_ENTER);
/*
		return;
	}
*/
	
	CWnd::OnLButtonDown(nFlags, point);
}

void CSplitterBar::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if(m_bDragging)
	{
		GetCursorPos(&point);
		DrawDraggingBar(point, DRAG_EXIT);

		ReleaseCapture();
		m_bDragging = FALSE;

		GetParent()->ScreenToClient(&point);

		if(m_bHorizontal)
		{
		}
		else
		{
			if (point.x < m_cxLeftMost) point.x = m_cxLeftMost;
			else if (point.x > m_cxRightMost) point.x = m_cxRightMost;

			m_rcSplitter.left = point.x;
			m_rcSplitter.right = m_rcSplitter.left + SPLITTERBAR_WIDTH;
		}

		MovePanes();
	}
	
	CWnd::OnLButtonUp(nFlags, point);
}

void CSplitterBar::DrawDraggingBar(CPoint point, DRAGFLAG df)
{
	CWnd *pParentWnd = GetParent();
	if (pParentWnd == NULL) {
		return;
	}

	pParentWnd->ScreenToClient(&point);
	if (point.x < m_cxLeftMost) point.x = m_cxLeftMost;
	else if (point.x > m_cxRightMost) point.x = m_cxRightMost;

	m_rcDrag = m_rcSplitter;
	if(m_bHorizontal)
	{
		m_rcDrag.top = point.y;
		m_rcDrag.bottom = point.y + m_rcSplitter.Height();
	}
	else
	{
		m_rcDrag.left = point.x;
		m_rcDrag.right = point.x + m_rcSplitter.Width();
	}

	CSize size(m_rcDrag.Width(), m_rcDrag.Height());

	pParentWnd->ClientToScreen(&m_rcDrag);

	CDC dc;
	dc.Attach(::GetDC(NULL));
	switch(df)
	{
	case DRAG_ENTER:
		dc.DrawDragRect(m_rcDrag, size, NULL, size);
		break;

	default:
		dc.DrawDragRect(m_rcDrag, size, m_rcDragPrev, size);
		break;
	}

	m_rcDragPrev = m_rcDrag;
}

void CSplitterBar::SetPanes(CWnd *pwndLeftPane,CWnd *pwndRightPane)
{
	ASSERT(pwndLeftPane);
	ASSERT(pwndRightPane);

	m_pwndLeftPane=pwndLeftPane;
	m_pwndRightPane=pwndRightPane;
}

void CSplitterBar::MovePanes()
{
	ASSERT(m_pwndLeftPane);
	ASSERT(m_pwndRightPane);

	if(m_bHorizontal)
	{
		//Get position of the splitter bar
/*
		CRect rectBar;
		GetWindowRect(rectBar);
		GetParent()->ScreenToClient(rectBar);

		//reposition top pane
		CRect rectLeft;
		m_pwndLeftPane->GetWindowRect(rectLeft);
		GetParent()->ScreenToClient(rectLeft);
		rectLeft.bottom=rectBar.top+GetSystemMetrics(SM_CXBORDER);
		m_pwndLeftPane->MoveWindow(rectLeft);

		//reposition bottom pane
		CRect rectRight;
		m_pwndRightPane->GetWindowRect(rectRight);
		GetParent()->ScreenToClient(rectRight);
		rectRight.top=rectBar.bottom-GetSystemMetrics(SM_CXBORDER);;
		m_pwndRightPane->MoveWindow(rectRight);
*/
	}
	else
	{
		CRect rcSplitter = m_rcSplitter;
		SetWindowPos(NULL, m_rcSplitter.left, m_rcSplitter.top, m_rcSplitter.Width(), m_rcSplitter.Height(), SWP_NOZORDER | SWP_NOACTIVATE);

		int cxLeftPane = rcSplitter.left - m_rcClient.left + 1;
		m_pwndLeftPane->SetWindowPos(NULL, m_rcClient.left, m_rcClient.top, cxLeftPane, m_rcClient.Height(), SWP_NOZORDER);

		int cxRightPane = m_rcClient.right - rcSplitter.right - 1;
		m_pwndRightPane->SetWindowPos(NULL, rcSplitter.right, m_rcClient.top, cxRightPane, m_rcClient.Height(), SWP_NOZORDER | SWP_NOACTIVATE);
	}

	//repaint client area
	GetParent()->Invalidate();
}

void CSplitterBar::UpdateClientRect(CRect rc)
{
	m_rcClient = rc;

	if (m_rcSplitter.Width() == 0 || m_rcSplitter.right >= m_rcClient.right) {
		CRect rcParant;
		GetParent()->GetClientRect(&rcParant);
		m_rcSplitter.left = (LONG)(rcParant.Width() * 0.75f);
		m_rcSplitter.top = m_rcClient.top;
	}
	m_rcSplitter.right = m_rcSplitter.left + SPLITTERBAR_WIDTH;
	m_rcSplitter.bottom = m_rcClient.bottom;

	m_cxLeftMost = m_rcClient.left + PANE_MIN_SIZE;
	m_cxRightMost = m_rcClient.right - PANE_MIN_SIZE;

	MovePanes();
}