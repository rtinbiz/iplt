
// MainFrm.h : interface of the CMainFrame class
//

#pragma once
//#include "FileView.h"
#include "ProcessView.h"
#include "ImageView.h"
#include "ParameterView.h"
#include "LogView.h"
#include "NIPJobView.h"
#include <map>

#define NIPTOOL_NOTIFY CMainFrame::NIPONotify

class CMainFrame : public CMDIFrameWndEx
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL LoadFrame(UINT nIDResource, DWORD dwDefaultStyle = WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, CWnd* pParentWnd = NULL, CCreateContext* pContext = NULL);
	virtual BOOL OnCloseMiniFrame(CPaneFrameWnd* pWnd);

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CMFCMenuBar       m_wndMenuBar;
	CMFCToolBar       m_wndToolBar;
	CMFCStatusBar     m_wndStatusBar;
	CMFCToolBarImages m_UserImages;

	CLogView        m_wndLogView;
	CProcessView        m_wndProcessViewTree;
	CParameterView    m_wndParameterView;
	CImageView m_wndInputImgView;
	map<int, CImageView *> m_mapOutputImageView;

	NIPJob m_dProcessMenu;

// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnWindowManager();
	afx_msg void OnViewCustomize();
	afx_msg LRESULT OnToolbarCreateNew(WPARAM wp, LPARAM lp);
	afx_msg void OnApplicationLook(UINT id);
//	afx_msg void OnUpdateApplicationLook(CCmdUI* pCmdUI);
	afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);
	DECLARE_MESSAGE_MAP()

	BOOL CreateDockingWindows();
	void SetDockingWindowIcons(BOOL bHiColorIcons);

public :
	CImageView *AddOutputImage(wstring strName, Mat dImg);
	int GetNextOutputImageViewID();
	bool RemoveOutputImage(int nID);
	CImageView *FindOutputImage(wstring strName);

	BOOL LoadProcessMenu();

	NIPJob *GetProcessMenu();
	void UpdateParameterView(NIPJobProcess *pProcess = nullptr, bool bMenu = true);
	void UpdateJobView(bool bModifyingProcess = false);
	void AddProcessToJobView(NIPJobProcess *pProcess);

	CNIPJobView *GetView();
	CNIPToolDoc *GetDocument();

	LRESULT OnClosePane(WPARAM, LPARAM lp);
//	virtual void PostNcDestroy();
	void UpdateInput(wstring strName);
	void UpdateOutput(wstring strName);

	static void NIPONotify(wstring strName, NIPL_ERR nErr, NIPO_NOTIFY_LEVEL nNotifyLevel = NNL_NONE);
};
