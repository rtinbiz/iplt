
// NIPJobView.h : interface of the CNIPJobView class
//

#pragma once

#include "NIPJobViewList.h"
#include "NIPToolDoc.h"

class CNIPTooViewToolBar : public CMFCToolBar
{
public:
	virtual void OnUpdateCmdUI(CFrameWnd* /*pTarget*/, BOOL bDisableIfNoHndler)
	{
		CMFCToolBar::OnUpdateCmdUI((CFrameWnd*)GetOwner(), bDisableIfNoHndler);
	}

	virtual BOOL AllowShowOnList() const { return FALSE; }
};


class CNIPJobView : public CView
{
protected: // create from serialization only
	CNIPJobView();
	DECLARE_DYNCREATE(CNIPJobView)

// Attributes
private:
	CNIPToolDoc* GetDocument() const;
	CNIPTooViewToolBar m_wndToolBar;
	CNIPJobViewList m_wndNIPJobViewList;

// Operations
public:

// Overrides
public:
	void UpdateJob();
	void AddProcess(NIPJobProcess *pProcess, bool bUnselectPrevItems = true);
	void UpdateModifyingProcess();

protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual void OnDraw(CDC* pDC);

	void SetModifyingProcess(NIPJobProcess *pProcess);
	int GetModifyingProcessIndex();
	int FindProcessIndex(NIPJobProcess *pProcess);
	NIPJobProcess *GetProcess(int nIndex);

	void AdjustLayout();
	void InitListCtrl();

	void UpdateProcess(int nIndex, bool bSelectItem = true, bool bUnselectPreItems = true);
	void RearrangeSeq();

// Implementation
public:
	virtual ~CNIPJobView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
public:
	afx_msg void OnLvnItemchanged(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblClick(NMHDR *pNMHDR, LRESULT *pResult);
//	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnRunJob();
	afx_msg void OnUpdateRunJob(CCmdUI *pCmdUI);
	afx_msg void OnRunSelectedProcess();
	afx_msg void OnUpdateRunSelectedProcess(CCmdUI *pCmdUI);
	afx_msg void OnMoveUpJobProcess();
	afx_msg void OnUpdateMoveUpJobProcess(CCmdUI *pCmdUI);
	afx_msg void OnMoveDownJobProcess();
	afx_msg void OnUpdateMoveDownJobProcess(CCmdUI *pCmdUI);
	afx_msg void OnDeleteJobProcess();
	afx_msg void OnUpdateDeleteJobProcess(CCmdUI *pCmdUI);
	afx_msg void OnUpdateOpenJob(CCmdUI *pCmdUI);
	afx_msg void OnOpenJob();
	afx_msg void OnNewJob();
	afx_msg void OnUpdateNewJob(CCmdUI *pCmdUI);
	afx_msg void OnSaveJob();
	afx_msg void OnUpdateSaveJob(CCmdUI *pCmdUI);
	afx_msg void OnSaveJobAs();
	afx_msg void OnUpdateSaveJobAs(CCmdUI *pCmdUI);
	afx_msg void OnRunProcessTo();
	afx_msg void OnUpdateRunProcessTo(CCmdUI *pCmdUI);
	afx_msg void OnCopyJobProcess();
	afx_msg void OnUpdateCopyJobProcess(CCmdUI *pCmdUI);
	afx_msg void OnPasteJobProcess();
	afx_msg void OnUpdatePasteJobProcess(CCmdUI *pCmdUI);
	afx_msg LRESULT OnHotKey(WPARAM wParam, LPARAM lParam);
};

#ifndef _DEBUG  // debug version in NIPJobView.cpp
inline CNIPToolDoc* CNIPJobView::GetDocument() const
   { return reinterpret_cast<CNIPToolDoc*>(m_pDocument); }
#endif

