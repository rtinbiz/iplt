
#include "stdafx.h"
#include "mainfrm.h"
#include "ImageView.h"
#include "Resource.h"
#include "NIPTool.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define ID_INDICATOR_IMAGE_INFO         0
#define ID_INDICATOR_IMAGE_POS_VALUE    1
#define ID_INDICATOR_RESULT_DESC		2
#define ID_INDICATOR_IMAGE_ZOOM			3

static UINT image_view_indicators[] =
{
	ID_INDICATOR_IMAGE_INFO,
	ID_INDICATOR_IMAGE_POS_VALUE,
	ID_INDICATOR_RESULT_DESC,
	ID_INDICATOR_IMAGE_ZOOM
};

/////////////////////////////////////////////////////////////////////////////
// CImageView

CImageView::CImageView()
{
	m_bInputImage = false;
	m_bShowResult = false;
	m_bShowMask = false;
	m_bShowTemplate = false;
	m_strName = _T("");
}

CImageView::~CImageView()
{
	Clear();
}

BEGIN_MESSAGE_MAP(CImageView, CDockablePane)
	ON_NOTIFY(LVN_ITEMCHANGED, RESULT_LIST_ID, OnLvnItemchanged)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_CONTEXTMENU()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_COMMAND(ID_OPEN_IMAGE, &CImageView::OnOpenImage)
	ON_UPDATE_COMMAND_UI(ID_OPEN_IMAGE, &CImageView::OnUpdateOpenImage)
	ON_COMMAND(ID_SAVE_IMAGE, &CImageView::OnSaveImage)
	ON_UPDATE_COMMAND_UI(ID_SAVE_IMAGE, &CImageView::OnUpdateSaveImage)
	ON_COMMAND(ID_SYNC_IMAGE, &CImageView::OnSyncImage)
	ON_UPDATE_COMMAND_UI(ID_SYNC_IMAGE, &CImageView::OnUpdateSyncImage)
	ON_COMMAND(ID_KEEP_IMAGE_POS_ZOOM, &CImageView::OnKeepImagePosZoom)
	ON_UPDATE_COMMAND_UI(ID_KEEP_IMAGE_POS_ZOOM, &CImageView::OnUpdateKeepImagePosZoom)
	ON_MESSAGE(WM_NIPL_IMAGE_POS_VALUE, &CImageView::OnImagePosValue)
	ON_MESSAGE(WM_NIPL_IMAGE_POS_DBLCLICK, &CImageView::OnImagePosDblClick)
	ON_MESSAGE(WM_NIPL_IMAGE_ZOOM, &CImageView::OnImageZoom)
	ON_MESSAGE(WM_NIPL_IMAGE_MARK_UPDATE, &CImageView::OnImageMarkUpdate)
	ON_MESSAGE(WM_NIPL_MASK_UPDATE, &CImageView::OnMaskUpdate)
	ON_COMMAND(ID_SHOW_IMAGE_POS, &CImageView::OnShowImagePos)
	ON_UPDATE_COMMAND_UI(ID_SHOW_IMAGE_POS, &CImageView::OnUpdateShowImagePos)
	ON_COMMAND(ID_SHOW_OUTPUT_RESULT, &CImageView::OnShowOutputResult)
	ON_UPDATE_COMMAND_UI(ID_SHOW_OUTPUT_RESULT, &CImageView::OnUpdateShowOutputResult)
	ON_COMMAND(ID_REFRESH_IMAGE, &CImageView::OnRefreshImage)
	ON_UPDATE_COMMAND_UI(ID_REFRESH_IMAGE, &CImageView::OnUpdateRefreshImage)
	ON_COMMAND(ID_SHOW_MASK, &CImageView::OnShowMask)
	ON_UPDATE_COMMAND_UI(ID_SHOW_MASK, &CImageView::OnUpdateShowMask)
	ON_COMMAND(ID_SHOW_TEMPLATE, &CImageView::OnShowTemplate)
	ON_UPDATE_COMMAND_UI(ID_SHOW_TEMPLATE, &CImageView::OnUpdateShowTemplate)
	ON_COMMAND(ID_FILL_MASK, &CImageView::OnFillMask)
	ON_UPDATE_COMMAND_UI(ID_FILL_MASK, &CImageView::OnUpdateFillMask)
	ON_COMMAND(ID_CLEAR_MASK, &CImageView::OnClearMask)
	ON_UPDATE_COMMAND_UI(ID_CLEAR_MASK, &CImageView::OnUpdateClearMask)
	ON_COMMAND(ID_SHOW_OUTPUT_RESULT_ALL, &CImageView::OnShowOutputResultAll)
	ON_UPDATE_COMMAND_UI(ID_SHOW_OUTPUT_RESULT_ALL, &CImageView::OnUpdateShowOutputResultAll)
	ON_COMMAND(ID_CLEAR_OUTPUT_RESULT, &CImageView::OnClearOutputResult)
	ON_UPDATE_COMMAND_UI(ID_CLEAR_OUTPUT_RESULT, &CImageView::OnUpdateClearOutputResult)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWorkspaceBar message handlers
void CImageView::OnContextMenu(CWnd* pWnd, CPoint point)
{

}

BOOL CImageView::Create(LPCTSTR lpszCaption, CWnd* pParentWnd, UINT nID, bool bCreatedDinamically)
{
	BOOL bCreated = CDockablePane::Create(lpszCaption, pParentWnd, CRect(0, 0, 800, 800), TRUE, nID, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_TOP | CBRS_FLOAT_MULTI);
	if (bCreated) {
		m_bCreatedDinamically = bCreatedDinamically;
	}

	return bCreated;
}


int CImageView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndImg.CreateCtl(this, 1))
	{
		TRACE0("Failed to create image view\n");
		return -1;      // fail to create
	}

	// Tool bar
	int nIconID24 = m_bInputImage ? IDB_INPUTIMAGE_VIEW_24 : IDB_OUTPUTIMAGE_VIEW_24;
	int nIconID = m_bInputImage ? IDR_INPUTIMAGE : IDR_OUTPUTIMAGE;

	m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, nIconID);
	m_wndToolBar.LoadToolBar(nIconID, 0, 0, TRUE /* Is locked */);
	m_wndToolBar.CleanUpLockedImages();
	m_wndToolBar.LoadBitmap(theApp.m_bHiColorIcons ? nIconID24 : nIconID, 0, 0, TRUE /* Locked */);

	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));
	m_wndToolBar.SetOwner(this);

	// All commands will be routed via this control , not via the parent frame:
	m_wndToolBar.SetRouteCommandsViaFrame(FALSE);

	// Result List
	const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_SHOWSELALWAYS | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
	if (!m_wndResultList.Create(dwViewStyle, CRect(0, 0, 600, 600), this, RESULT_LIST_ID))
	{
		TRACE0("Failed to create result list\n");
		return -1;      // fail to create
	}
	m_wndResultList.SetExtendedStyle(m_wndResultList.GetExtendedStyle() | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_wndResultList.SetFont(&afxGlobalData.fontRegular);

	CRect rect(0, 0, 0, 0);
	m_wndSplitterBar.Create(WS_CHILD | WS_VISIBLE, rect, this, SPLITTERBAR_ID);
	m_wndSplitterBar.SetPanes(&m_wndImg, &m_wndResultList);

	if (!m_wndStatusBar.Create(this))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}
	m_wndStatusBar.SetIndicators(image_view_indicators, sizeof(image_view_indicators) / sizeof(UINT));

	GetClientRect(&rect);
	m_wndStatusBar.SetPaneInfo(ID_INDICATOR_IMAGE_INFO, ID_INDICATOR_IMAGE_INFO, SBPS_NORMAL, 350);
	m_wndStatusBar.SetPaneInfo(ID_INDICATOR_IMAGE_POS_VALUE, ID_INDICATOR_IMAGE_POS_VALUE, SBPS_NORMAL, 180);
	m_wndStatusBar.SetPaneInfo(ID_INDICATOR_RESULT_DESC, ID_INDICATOR_RESULT_DESC, SBPS_STRETCH, 0);
	m_wndStatusBar.SetPaneInfo(ID_INDICATOR_IMAGE_ZOOM, ID_INDICATOR_IMAGE_ZOOM, SBPS_NORMAL, 80);

	AdjustLayout();

	if (m_bInputImage) {
		NIPLInput dInput;
		dInput.m_dImg = Mat::zeros(1, 1, CV_8UC1);		// make empty image

		NIPO *pNIPO = NIPO::GetInstance();
		pNIPO->SetInput(dInput);

		SetInput(STR_NAME_INPUT, dInput);
		ShowInputImage();
	}

	return 0;
}

void CImageView::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}

void CImageView::AdjustLayout()
{
	if (GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect rectClient;
	GetClientRect(rectClient);

	int cyToolBar = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;
	int cyStatusBar = m_wndStatusBar.CalcFixedLayout(FALSE, TRUE).cy;
	int cyImg = rectClient.Height() - 1 - cyToolBar - cyStatusBar;

	m_wndToolBar.SetWindowPos(NULL, rectClient.left, rectClient.top, rectClient.Width(), cyToolBar, SWP_NOZORDER | SWP_NOACTIVATE);
	if (m_bShowResult) {
		m_wndSplitterBar.ShowWindow(SW_SHOW);
		m_wndResultList.ShowWindow(SW_SHOW);

		m_wndSplitterBar.UpdateClientRect(CRect(CPoint(rectClient.left + 1, rectClient.top + 1 + cyToolBar), CSize(rectClient.Width() - 1, cyImg)));
//		m_wndResultList.SetWindowPos(NULL, rectClient.left, rectClient.top + 1 + cyToolBar + cyImg, rectClient.Width(), cyResultList, SWP_NOACTIVATE | SWP_NOZORDER);
	}
	else {
		m_wndImg.SetWindowPos(NULL, rectClient.left + 1, rectClient.top + 1 + cyToolBar, rectClient.Width() - 1, cyImg, SWP_NOZORDER);
		m_wndSplitterBar.ShowWindow(SW_HIDE);
		m_wndResultList.ShowWindow(SW_HIDE);
	}
	m_wndStatusBar.SetWindowPos(NULL, rectClient.left, rectClient.bottom - cyStatusBar, rectClient.Width(), cyStatusBar, SWP_NOZORDER | SWP_NOACTIVATE);
}

void CImageView::OnPaint()
{
	CPaintDC dc(this); // device context for painting

//	m_wndImg.Invalidate();
	CRect rc;
	m_wndImg.GetWindowRect(rc);
	ScreenToClient(rc);
	rc.InflateRect(1, 1);
	dc.Draw3dRect(rc, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));

	m_wndResultList.GetWindowRect(rc);
	ScreenToClient(rc);
	rc.InflateRect(1, 1);
	dc.Draw3dRect(rc, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));

	m_wndImg.Invalidate();
	m_wndResultList.Invalidate();
/*
	m_wndResultList.GetWindowRect(rc);
	ScreenToClient(rc);
	rc.InflateRect(1, 1);
	dc.Draw3dRect(rc, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));

	m_wndStatusBar.GetWindowRect(rc);
	ScreenToClient(rc);
	rc.InflateRect(1, 1);
	dc.Draw3dRect(rc, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));
*/
}

void CImageView::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);

	m_wndImg.SetFocus();
}

void CImageView::Clear()
{
	m_dImg.release();
}

void CImageView::ShowImage(Mat dImg, bool bKeepPosAndZoom)
{
	m_dImg = dImg;
	m_wndImg.Show(dImg, bKeepPosAndZoom);

	ShowImageInfo();

	ShowPane(TRUE, FALSE, TRUE);
	BringWindowToTop();
}

void CImageView::ShowInputImage(bool bKeepPosAndZoom)
{
	if (!m_bInputImage) {
		return;
	}

	if (m_bShowTemplate) {
		// if there's no template, turn off the flag
		if (CHECK_EMPTY_IMAGE(m_dInput.m_dTemplateImg)) {
			m_bShowTemplate = false;
		}
	}

	m_wndImg.SetShowMask(m_bShowMask);
	if (m_bShowTemplate) {
		ShowImage(m_dInput.m_dTemplateImg, bKeepPosAndZoom);
	}
	else {
		ShowImage(m_dInput.m_dImg, bKeepPosAndZoom);
	}
}

void CImageView::ShowOutputImage()
{
	ShowImage(m_dOutput.m_dImg);
}

void CImageView::ShowTemplate()
{
	if (!m_bInputImage) {
		return;
	}

	if (CHECK_EMPTY_IMAGE(m_dInput.m_dTemplateImg)) {
		return;
	}

	m_dImg = m_dInput.m_dTemplateImg;
	m_wndImg.Show(m_dImg);

	ShowImageInfo();
}

void CImageView::OnOpenImage()
{
	// TODO: Add your command handler code here
	wchar_t szFilter[] = L"Image Files (*.bmp;*.png;*.jpg)|*.bmp;*.png;*.jpg|All Files (*.*)|*.*||";

	CFileDialog iFileDlg(TRUE, L"bmp", NULL, OFN_READONLY, szFilter);
	if (iFileDlg.DoModal() != IDOK) {
		return;
	}

	wstring strPath(LPCTSTR(iFileDlg.GetPathName()));

	bool bRet = false;
	NIPO *pNIPO = NIPO::GetInstance();
	if (m_bInputImage) {
		wstring strImagePath = L"";
		wstring strMaskPath = L"";
		wstring strTemplatePath = L"";
		if (m_bShowMask) {
			strMaskPath = strPath;
		}
		else if (m_bShowTemplate) {
			strTemplatePath = strPath;
		}
		else {
			strImagePath = strPath;
		}

		if (pNIPO->LoadInputImage(strImagePath, strMaskPath, strTemplatePath)) {
			pNIPO->GetInput(m_dInput);

			if (!m_bShowTemplate) {
				if (!CHECK_EMPTY_IMAGE(m_dInput.m_dMask)) {
					m_dMask = m_dInput.m_dMask;
					m_wndImg.SetMask(m_dInput.m_dMask);
				}
			}

			ShowInputImage();
		}
	}
	else {
		if (pNIPO->LoadOutputImage(strPath, m_strName)) {
			pNIPO->GetOutputImage(m_strName, m_dImg);
			ShowOutputImage();
		}
	}
}

void CImageView::OnUpdateOpenImage(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(TRUE);
}

void CImageView::ShowImageInfo()
{
	int nSizeX = m_dImg.cols;
	int nSizeY = m_dImg.rows;

	wstring strDepth = L"?";
	int nDepth = m_dImg.depth();
	switch (nDepth) {
	case CV_8U: strDepth = L"8U"; break;
	case CV_8S: strDepth = L"8S"; break;
	case CV_16U: strDepth = L"16U"; break;
	case CV_16S: strDepth = L"16S"; break;
	case CV_32S: strDepth = L"32S"; break;
	case CV_32F: strDepth = L"32F"; break;
	case CV_64F: strDepth = L"64F"; break;
	}

	int nChannels = m_dImg.channels();

	double nMin, nMax;
	if (m_bShowMask && nChannels == 1) {
		NIPL *pNIPL = NIPL::GetInstance();
		pNIPL->DebugPrintImageProperty(L"Image", m_dImg);
		pNIPL->DebugPrintImageProperty(L"Mask", m_dMask);

		minMaxLoc(m_dImg, &nMin, &nMax, 0, 0, m_dMask);
	}
	else {
		minMaxLoc(m_dImg, &nMin, &nMax);
	}

	wchar_t szMinMaxText[256];
	if (nDepth == CV_32F || nDepth == CV_64F) {
		swprintf_s(szMinMaxText, L"Min:%.3f Max:%.3f", nMin, nMax);
	}
	else {
		swprintf_s(szMinMaxText, L"Min:%d Max:%d", (int)nMin, (int)nMax);
	}

	Scalar dMean;
	Scalar dStd;

	if (m_bShowMask && nChannels == 1) {
		meanStdDev(m_dImg, dMean, dStd, m_dMask);
	}
	else {
		meanStdDev(m_dImg, dMean, dStd);
	}
	double nMean = dMean[0];
	double nStd = dStd[0];

	wchar_t szMeanStdText[256];
	swprintf_s(szMeanStdText, L"Mean:%.3f Std:%.3f", nMean, nStd);
	
	wchar_t szText[256];

	swprintf_s(szText, L"X:%d x Y:%d x %s x %dC, %s %s", nSizeX, nSizeY, strDepth.c_str(), nChannels, szMinMaxText, szMeanStdText);
	m_wndStatusBar.SetPaneText(ID_INDICATOR_IMAGE_INFO, szText);
}

void CImageView::ShowImagePosValue(CPoint ptImagePos)
{
	wchar_t szValue[256] = L"Unsupported Type";
	int nDepth = m_dImg.depth();
	int nChannels = m_dImg.channels();
	if (nChannels == 1) {
		switch (nDepth) {
		case CV_8U:
			swprintf_s(szValue, L"%u", m_dImg.at<UINT8>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_8S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT8>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_16U:
			swprintf_s(szValue, L"%u", m_dImg.at<UINT16>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_16S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT16>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_32S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT32>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_32F:
			swprintf_s(szValue, L"%.3f", m_dImg.at<FLOAT>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_64F:
			swprintf_s(szValue, L"%.3f", m_dImg.at<DOUBLE>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		}
	}
	else if (nChannels == 3) {
		if (nDepth == CV_8U) {
			Vec3b dPixel = m_dImg.at<Vec3b>(Point(ptImagePos.x, ptImagePos.y));
			swprintf_s(szValue, L"(%u, %u, %u)", dPixel[2], dPixel[1], dPixel[0]);
		}
	}

	wchar_t szText[256];
	swprintf_s(szText, L"X:%d Y:%d Value:%s", ptImagePos.x, ptImagePos.y, szValue);
	m_wndStatusBar.SetPaneText(ID_INDICATOR_IMAGE_POS_VALUE, szText);
}

void CImageView::ShowResultDesc(wstring strDesc)
{
	if (strDesc.size() > 0) {
		strDesc = L"R:" + strDesc;
	}
	m_wndStatusBar.SetPaneText(ID_INDICATOR_RESULT_DESC, strDesc.c_str());
}

void CImageView::ShowImageZoom()
{
	float nZoom;
	float nMinZoom;
	float nMaxZoom;

	m_wndImg.GetZoom(nZoom, nMinZoom, nMaxZoom);

	wchar_t szText[256];
	swprintf_s(szText, L"Zoom:%d%%", (int)(nZoom * 100));
	m_wndStatusBar.SetPaneText(ID_INDICATOR_IMAGE_ZOOM, szText);
}

void CImageView::OnSaveImage()
{
	// TODO: Add your command handler code here
	wchar_t szFilter[] = L"Image Files (*.bmp;*.png;*.jpg)|*.bmp;*.png;*.jpg|All Files (*.*)|*.*||";

	wstring strName = m_strName;
	if (m_bInputImage) {
		if (strName != STR_NAME_INPUT) {
			wchar_t szDrive[MAX_PATH];
			wchar_t szDir[MAX_PATH];
			wchar_t szFileName[MAX_PATH];
			wchar_t szExt[MAX_PATH];
			_wsplitpath_s<MAX_PATH, MAX_PATH, MAX_PATH, MAX_PATH>(strName.c_str(), szDrive, szDir, szFileName, szExt);

			strName = szFileName;
		}
	}

	CFileDialog iFileDlg(FALSE, L"bmp", m_strName.c_str(), OFN_OVERWRITEPROMPT, szFilter);
	if (iFileDlg.DoModal() != IDOK) {
		return;
	}

	wstring strPath(LPCTSTR(iFileDlg.GetPathName()));

	NIPO *pNIPO = NIPO::GetInstance();

	Mat dImg;
	if (m_bShowMask) {
		dImg = m_dMask;
	}
	else if (m_bShowTemplate) {
		dImg = m_dInput.m_dTemplateImg;
	}
	else {
		dImg = m_dImg;
	}

	pNIPO->SaveImage(strPath, dImg);
}

void CImageView::OnUpdateSaveImage(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(TRUE);
}

void CImageView::OnSyncImage()
{
	// TODO: Add your command handler code here
	bool bSyncImageTarget = m_wndImg.IsSyncImageTarget();
	m_wndImg.SetSyncImageTarget(!bSyncImageTarget);
}

void CImageView::OnUpdateSyncImage(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	bool bSyncImageTarget = m_wndImg.IsSyncImageTarget();
	pCmdUI->SetCheck(bSyncImageTarget);
}

void CImageView::OnKeepImagePosZoom()
{
	// TODO: Add your command handler code here
	bool bKeepPosAndZoom = m_wndImg.IsKeepPosAndZoom();
	m_wndImg.SetKeepPosAndZoom(!bKeepPosAndZoom);
}

void CImageView::OnUpdateKeepImagePosZoom(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	bool bKeepPosAndZoom = m_wndImg.IsKeepPosAndZoom();
	if (m_bInputImage) {
		pCmdUI->SetCheck(bKeepPosAndZoom);
	}
	else {
		bool bSyncImageTarget = m_wndImg.IsSyncImageTarget();
		pCmdUI->Enable(!bSyncImageTarget);
		pCmdUI->SetCheck(!bSyncImageTarget && bKeepPosAndZoom);
 	}
}

void CImageView::OnShowImagePos()
{
	// TODO: Add your command handler code here
	bool bShowImagePos = m_wndImg.IsShowImagePos();
	m_wndImg.SetShowImagePos(!bShowImagePos);
}

void CImageView::OnUpdateShowImagePos(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	bool bShowImagePos = m_wndImg.IsShowImagePos();
	pCmdUI->SetCheck(bShowImagePos);
}

LRESULT CImageView::OnImagePosValue(WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default
	int nPosX = (int)wParam;
	int nPosY = (int)lParam;
	ShowImagePosValue(CPoint(nPosX, nPosY));

	return 0;
}

LRESULT CImageView::OnImagePosDblClick(WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default
	int nPosX = (int)wParam;
	int nPosY = (int)lParam;
	CheckResultByImagePos(CPoint(nPosX, nPosY));

	return 0;
}


LRESULT CImageView::OnImageZoom(WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default
	ShowImageZoom();

	return 0;
}

LRESULT CImageView::OnMaskUpdate(WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default
	m_bShowMask = (bool)wParam;

	Mat dMask = m_wndImg.GetMask();
	dMask.copyTo(m_dMask);
	VERIFY_MASK(m_dImg, m_dMask);

	if (m_bInputImage) {
		m_dInput.m_dMask = m_dMask;

		NIPO *pNIPO = NIPO::GetInstance();
		pNIPO->SetInput(m_dInput);
	}

	ShowImageInfo();

	return 0;
}

LRESULT CImageView::OnImageMarkUpdate(WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default

	if (m_dOutput.m_pResult == nullptr) {
		return 0;
	}

	m_wndResultList.UnselectAll();
	vector<ImageMark> listImageMark = m_wndImg.GetImageMarkList();

	for (auto dMark : listImageMark) {
		if ((m_dOutput.IsResultType(NIPL_RESULT_FIND_CIRCLE) && dMark.m_bCircle) ||
			(!m_dOutput.IsResultType(NIPL_RESULT_FIND_CIRCLE) && !dMark.m_bCircle)) {
			int nIndex = m_wndResultList.FinrcBoundingBox(dMark.m_rcBoundingBox);
			if (nIndex >= 0) m_wndResultList.SelectItem(nIndex);
		}
	}

	ShowImageInfo();

	return 0;
}

void CImageView::SetInput(wstring strName, const NIPLInput &dInput)
{
	m_strName = strName;
	SetWindowText(m_strName.c_str());

	m_dInput = dInput;
	if (!CHECK_EMPTY_IMAGE(m_dInput.m_dMask)) {
		m_dMask = m_dInput.m_dMask;
		m_wndImg.SetMask(m_dInput.m_dMask);
	}
}

void CImageView::SetOutput(wstring strName, const NIPLOutput &dOutput)
{
	m_strName = strName;
	SetWindowText(m_strName.c_str());

	m_dOutput = dOutput;
	SetResultList();
}

void CImageView::SetResultList()
{
	if (m_bInputImage) {
		return;
	}

	m_wndResultList.ClearContents();

	m_wndImg.ClearImageMark();
	ShowResultDesc(L"");

	if (m_dOutput.m_pResult == nullptr) {
		m_bShowResult = false;
		ShowResultBox();
		AdjustLayout();
		return;
	}

	if (m_dOutput.IsResultType(NIPL_RESULT_FIND_BLOB)) {
		m_wndResultList.SetResultType(NIPL_RESULT_FIND_BLOB);

		m_wndResultList.InsertColumn(LCI_BLOB_INDEX, STR_RESULT_LIST_COLUMM_INDEX, LVCFMT_CENTER, 40, 0);
		m_wndResultList.InsertColumn(LCI_BLOB_BOUNDING_BOX_START_X, NIPLBLOB_BOUNDING_BOX_START_X, LVCFMT_CENTER, 50, 0);
		m_wndResultList.InsertColumn(LCI_BLOB_BOUNDING_BOX_START_Y, NIPLBLOB_BOUNDING_BOX_START_Y, LVCFMT_CENTER, 50, 0);
		m_wndResultList.InsertColumn(LCI_BLOB_BOUNDING_BOX_END_X, NIPLBLOB_BOUNDING_BOX_END_X, LVCFMT_CENTER, 50, 0);
		m_wndResultList.InsertColumn(LCI_BLOB_BOUNDING_BOX_END_Y, NIPLBLOB_BOUNDING_BOX_END_Y, LVCFMT_CENTER, 50, 0);
		m_wndResultList.InsertColumn(LCI_BLOB_SIZE, NIPLBLOB_SIZE, LVCFMT_CENTER, 50, 0);

		int nIndex = 0;
		NIPLResult_FindBlob *pResult = (NIPLResult_FindBlob *)m_dOutput.m_pResult;
		for (auto &dBlob : pResult->m_listBlob) {
			wstring strIndex = to_wstring(nIndex);
			wstring strBoundingBoxStartX = to_wstring(dBlob.m_rcBoundingBox.x);
			wstring strBoundingBoxStartY = to_wstring(dBlob.m_rcBoundingBox.y);
			wstring strBoundingBoxEndX = to_wstring(dBlob.m_rcBoundingBox.x + dBlob.m_rcBoundingBox.width);
			wstring strBoundingBoxEndY = to_wstring(dBlob.m_rcBoundingBox.y + dBlob.m_rcBoundingBox.height);
			wstring strSize = to_wstring(dBlob.m_nSize);

			m_wndResultList.InsertItem(nIndex, strIndex.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_BLOB_BOUNDING_BOX_START_X, strBoundingBoxStartX.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_BLOB_BOUNDING_BOX_START_Y, strBoundingBoxStartY.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_BLOB_BOUNDING_BOX_END_X, strBoundingBoxEndX.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_BLOB_BOUNDING_BOX_END_Y, strBoundingBoxEndY.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_BLOB_SIZE, strSize.c_str());
			m_wndResultList.SetItemData(nIndex, (DWORD_PTR)&dBlob);

			nIndex++;
		}
	}
	else if (m_dOutput.IsResultType(NIPL_RESULT_FIND_LINE)) {
		m_wndResultList.SetResultType(NIPL_RESULT_FIND_LINE);

		m_wndResultList.InsertColumn(LCI_LINE_INDEX, STR_RESULT_LIST_COLUMM_INDEX, LVCFMT_CENTER, 40, 0);
		m_wndResultList.InsertColumn(LCI_LINE_START_X, NIPLLINE_START_X, LVCFMT_CENTER, 60, 0);
		m_wndResultList.InsertColumn(LCI_LINE_START_Y, NIPLLINE_START_Y, LVCFMT_CENTER, 60, 0);
		m_wndResultList.InsertColumn(LCI_LINE_END_X, NIPLLINE_END_X, LVCFMT_CENTER, 60, 0);
		m_wndResultList.InsertColumn(LCI_LINE_END_Y, NIPLLINE_END_Y, LVCFMT_CENTER, 60, 0);
		m_wndResultList.InsertColumn(LCI_LINE_LENGTH, NIPLLINE_LENGTH, LVCFMT_CENTER, 60, 0);
		m_wndResultList.InsertColumn(LCI_LINE_ANGLE, NIPLLINE_ANGLE, LVCFMT_CENTER, 60, 0);

		int nIndex = 0;
		wchar_t szText[256];
		NIPLResult_FindLine *pResult = (NIPLResult_FindLine *)m_dOutput.m_pResult;
		for (auto &dLine : pResult->m_listLine) {
			wstring strIndex = to_wstring(nIndex);
			wstring strLineStartX = to_wstring(dLine.m_ptStart.x);
			wstring strLineStartY = to_wstring(dLine.m_ptStart.y);
			wstring strLineEndX = to_wstring(dLine.m_ptEnd.x);
			wstring strLineEndY = to_wstring(dLine.m_ptEnd.y);
			swprintf_s(szText, L"%.1f", dLine.m_nLength);
			wstring strLineLength = szText;
			swprintf_s(szText, L"%.1f", dLine.m_nAngle);
			wstring strLineAngle = szText;

			m_wndResultList.InsertItem(nIndex, strIndex.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_LINE_START_X, strLineStartX.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_LINE_START_Y, strLineStartY.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_LINE_END_X, strLineEndX.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_LINE_END_Y, strLineEndY.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_LINE_LENGTH, strLineLength.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_LINE_ANGLE, strLineAngle.c_str());
			m_wndResultList.SetItemData(nIndex, (DWORD_PTR)&dLine);

			nIndex++;
		}
	}
	else if (m_dOutput.IsResultType(NIPL_RESULT_FIND_CIRCLE)) {
		m_wndResultList.SetResultType(NIPL_RESULT_FIND_CIRCLE);

		m_wndResultList.InsertColumn(LCI_CIRCLE_INDEX, STR_RESULT_LIST_COLUMM_INDEX, LVCFMT_CENTER, 40, 0);
		m_wndResultList.InsertColumn(LCI_CIRCLE_CENTER_X, NIPLCIRCLE_CENTER_X, LVCFMT_CENTER, 60, 0);
		m_wndResultList.InsertColumn(LCI_CIRCLE_CENTER_Y, NIPLCIRCLE_CENTER_Y, LVCFMT_CENTER, 60, 0);
		m_wndResultList.InsertColumn(LCI_CIRCLE_RADIUS, NIPLCIRCLE_RADIUS, LVCFMT_CENTER, 60, 0);

		int nIndex = 0;
		NIPLResult_FindCircle *pResult = (NIPLResult_FindCircle *)m_dOutput.m_pResult;
		for (auto &dCircle : pResult->m_listCircle) {
			wstring strIndex = to_wstring(nIndex);
			wstring strCircleCenterX = to_wstring(dCircle.m_ptCenter.x);
			wstring strCircleCenterY = to_wstring(dCircle.m_ptCenter.y);
			wstring strCircleRadius = to_wstring(dCircle.m_nRadius);

			m_wndResultList.InsertItem(nIndex, strIndex.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_CIRCLE_CENTER_X, strCircleCenterX.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_CIRCLE_CENTER_Y, strCircleCenterY.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_CIRCLE_RADIUS, strCircleRadius.c_str());
			m_wndResultList.SetItemData(nIndex, (DWORD_PTR)&dCircle);

			nIndex++;
		}
	}
	else if (m_dOutput.IsResultType(NIPL_RESULT_DEFECT)) {
		m_wndResultList.SetResultType(NIPL_RESULT_DEFECT);

		m_wndResultList.InsertColumn(LCI_DEFECT_INDEX, STR_RESULT_LIST_COLUMM_INDEX, LVCFMT_CENTER, 40, 0);
		m_wndResultList.InsertColumn(LCI_DEFECT_TYPE, NIPLDEFECT_TYPE, LVCFMT_CENTER, 50, 0);
		m_wndResultList.InsertColumn(LCI_DEFECT_BOUNDING_BOX_START_X, NIPLBLOB_BOUNDING_BOX_START_X, LVCFMT_CENTER, 50, 0);
		m_wndResultList.InsertColumn(LCI_DEFECT_BOUNDING_BOX_START_Y, NIPLBLOB_BOUNDING_BOX_START_Y, LVCFMT_CENTER, 50, 0);
		m_wndResultList.InsertColumn(LCI_DEFECT_BOUNDING_BOX_END_X, NIPLBLOB_BOUNDING_BOX_END_X, LVCFMT_CENTER, 50, 0);
		m_wndResultList.InsertColumn(LCI_DEFECT_BOUNDING_BOX_END_Y, NIPLBLOB_BOUNDING_BOX_END_Y, LVCFMT_CENTER, 50, 0);
		m_wndResultList.InsertColumn(LCI_DEFECT_SIZE, NIPLBLOB_SIZE, LVCFMT_CENTER, 50, 0);

		int nIndex = 0;
		NIPLResult_Defect *pResult = (NIPLResult_Defect *)m_dOutput.m_pResult;
		for (auto &dDefect : pResult->m_listDefect) {
			wstring strIndex = to_wstring(nIndex);
			wstring strType = to_wstring(dDefect.m_nType);
			wstring strBoundingBoxStartX = to_wstring(dDefect.m_rcBoundingBox.x);
			wstring strBoundingBoxStartY = to_wstring(dDefect.m_rcBoundingBox.y);
			wstring strBoundingBoxEndX = to_wstring(dDefect.m_rcBoundingBox.x + dDefect.m_rcBoundingBox.width);
			wstring strBoundingBoxEndY = to_wstring(dDefect.m_rcBoundingBox.y + dDefect.m_rcBoundingBox.height);
			wstring strSize = to_wstring(dDefect.m_nSize);

			m_wndResultList.InsertItem(nIndex, strIndex.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_DEFECT_TYPE, strType.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_DEFECT_BOUNDING_BOX_START_X, strBoundingBoxStartX.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_DEFECT_BOUNDING_BOX_START_Y, strBoundingBoxStartY.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_DEFECT_BOUNDING_BOX_END_X, strBoundingBoxEndX.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_DEFECT_BOUNDING_BOX_END_Y, strBoundingBoxEndY.c_str());
			m_wndResultList.SetItemText(nIndex, LCI_DEFECT_SIZE, strSize.c_str());
			m_wndResultList.SetItemData(nIndex, (DWORD_PTR)&dDefect);

			nIndex++;
		}
	}


	OnShowOutputResultAll();
}


void CImageView::OnShowOutputResult()
{
	// TODO: Add your command handler code here
	if (m_dOutput.m_pResult == nullptr) {
		return;
	}

	m_bShowResult = !m_bShowResult;

	AdjustLayout();
}

void CImageView::OnUpdateShowOutputResult(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_dOutput.m_pResult != nullptr);
	pCmdUI->SetCheck(m_bShowResult);
}

void CImageView::OnShowOutputResultAll()
{
	// TODO: Add your command handler code here
	m_wndResultList.SelectAll();
	ShowResultBox();
}

void CImageView::OnUpdateShowOutputResultAll(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_dOutput.m_pResult != nullptr && m_wndResultList.GetSelectedCount() != m_wndResultList.GetItemCount());
}

void CImageView::OnClearOutputResult()
{
	// TODO: Add your command handler code here
	m_wndResultList.UnselectAll();
	ShowResultBox();
}

void CImageView::OnUpdateClearOutputResult(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_dOutput.m_pResult != nullptr && m_wndResultList.GetSelectedCount() > 0);
}

void CImageView::ShowResultBox(int nIndexCurSel)
{
	m_wndImg.ClearImageMark();

	POSITION dPos = m_wndResultList.GetFirstSelectedItemPosition();
	while (dPos) {
		int nIndex = m_wndResultList.GetNextSelectedItem(dPos);
		void *pData = (void *)m_wndResultList.GetItemData(nIndex);
		if (pData == nullptr) {
			continue;
		}

		if (m_dOutput.IsResultType(NIPL_RESULT_FIND_BLOB)) {
			NIPLBlob *pBlob = (NIPLBlob *)pData;
			if (pBlob) {
				ImageMark dMark;
				dMark.m_rcBoundingBox = pBlob->GetBoundingBox();
				m_wndImg.AddImageMark(dMark);
				if (nIndex == nIndexCurSel) {
					m_wndImg.GotoImagePos(CvPointToPoint(pBlob->GetCenterPos()));
					ShowResultDesc(pBlob->GetDesc());
				}
			}
		}
		else if (m_dOutput.IsResultType(NIPL_RESULT_FIND_LINE)) {
			NIPLLine *pLine = (NIPLLine *)pData;
			if (pLine) {
				ImageMark dMark;
				dMark.m_rcBoundingBox = pLine->GetBoundingBox();
				m_wndImg.AddImageMark(dMark);
				if (nIndex == nIndexCurSel) {
					m_wndImg.GotoImagePos(CvPointToPoint(pLine->GetCenterPos()));
					ShowResultDesc(pLine->GetDesc());
				}
			}
		}
		else if (m_dOutput.IsResultType(NIPL_RESULT_FIND_CIRCLE)) {
			NIPLCircle *pCircle = (NIPLCircle *)pData;
			if (pCircle) {
				ImageMark dMark;
				dMark.m_bCircle = true;
				dMark.m_rcBoundingBox = pCircle->GetBoundingBox();
				m_wndImg.AddImageMark(dMark);
				if (nIndex == nIndexCurSel) {
					m_wndImg.GotoImagePos(CvPointToPoint(pCircle->GetCenterPos()));
					ShowResultDesc(pCircle->GetDesc());
				}
			}
		}
		else if (m_dOutput.IsResultType(NIPL_RESULT_DEFECT)) {
			NIPLDefect *pDefect = (NIPLDefect *)pData;
			if (pDefect) {
				ImageMark dMark;
				dMark.m_rcBoundingBox = pDefect->GetBoundingBox();
				m_wndImg.AddImageMark(dMark);
				if (nIndex == nIndexCurSel) {
					m_wndImg.GotoImagePos(CvPointToPoint(pDefect->GetCenterPos()));
					ShowResultDesc(pDefect->GetDesc());
				}
			}
		}
	}

	m_wndImg.ShowImageMark();
}

void CImageView::OnRefreshImage()
{
	// TODO: Add your command handler code here
	m_wndImg.Refresh();
	ShowResultDesc(L"");
}

void CImageView::OnUpdateRefreshImage(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(TRUE);
}

void CImageView::CheckResultByImagePos(CPoint ptImagePos)
{
	if (m_dOutput.m_pResult == nullptr) {
		return;
	}

	int nIndex = m_wndResultList.FindPtInData(ptImagePos);
	if (nIndex >= 0) {
		BOOL bSet = FALSE;
		if (!m_wndResultList.IsItemSelected(nIndex)) bSet = TRUE;
		m_wndResultList.SelectItem(nIndex, bSet);
	}
}

void CImageView::OnShowMask()
{
	// TODO: Add your command handler code here
	m_bShowMask = !m_bShowMask;
	if (m_bShowMask) m_bShowTemplate = false;

	ShowInputImage(true);
}

void CImageView::OnUpdateShowMask(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	bool bEnable = !CHECK_EMPTY_IMAGE(m_dMask);
	pCmdUI->Enable(bEnable);
	if (bEnable) {
		pCmdUI->SetCheck(m_bShowMask);
	}
	else {
		pCmdUI->SetCheck(false);
	}
}

void CImageView::OnShowTemplate()
{
	// TODO: Add your command handler code here
	m_bShowTemplate = !m_bShowTemplate;
	if (m_bShowTemplate) m_bShowMask = false;
	ShowInputImage();
}

void CImageView::OnUpdateShowTemplate(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	bool bEnable = !CHECK_EMPTY_IMAGE(m_dInput.m_dTemplateImg);
	pCmdUI->Enable(bEnable);
	if (bEnable) {
		pCmdUI->SetCheck(m_bShowTemplate);
	}
	else {
		pCmdUI->SetCheck(false);
	}
}

void CImageView::OnFillMask()
{
	// TODO: Add your command handler code here
	m_dInput.m_dMask = 255;
	m_dMask = m_dInput.m_dMask;
	m_wndImg.SetMask(m_dMask);
	ShowInputImage(true);
}


void CImageView::OnUpdateFillMask(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_bShowMask);
}

void CImageView::OnClearMask()
{
	// TODO: Add your command handler code here
	m_dInput.m_dMask = 0;
	m_dMask = m_dInput.m_dMask;
	m_wndImg.SetMask(m_dMask);
	ShowInputImage(true);
}

void CImageView::OnUpdateClearMask(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_bShowMask);
}


void CImageView::PostNcDestroy()
{
	// TODO: Add your specialized code here and/or call the base class
	CDockablePane::PostNcDestroy();

	if (m_bCreatedDinamically) {
		delete this;
	}
}

void CImageView::OnLvnItemchanged(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here

	int nIndex = pNMLV->iItem;
	ShowResultBox(nIndex);

	*pResult = 0;
}
