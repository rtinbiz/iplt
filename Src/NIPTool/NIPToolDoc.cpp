
// NIPToolDoc.cpp : implementation of the CNIPToolDoc class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "NIPTool.h"
#endif

#include "NIPToolDoc.h"

#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CNIPToolDoc
NIPJob CNIPToolDoc::m_dClipboardJob;

IMPLEMENT_DYNCREATE(CNIPToolDoc, CDocument)

BEGIN_MESSAGE_MAP(CNIPToolDoc, CDocument)
END_MESSAGE_MAP()

// CNIPToolDoc construction/destruction
CNIPToolDoc::CNIPToolDoc()
{
	// TODO: add one-time construction code here
	m_pModifyingProcess = nullptr;
}

CNIPToolDoc::~CNIPToolDoc()
{
}

BOOL CNIPToolDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
	m_dJob.Clear();

	return TRUE;
}

// CNIPToolDoc serialization
BOOL CNIPToolDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	m_dJob.Clear();

	wstring strPath(lpszPathName);

	NIPO *pNIPO = NIPO::GetInstance();
	if (pNIPO->LoadJob(strPath, m_dJob) == FALSE) {
		NIPTOOL_NOTIFY(strPath, NIPL_ERR_FAIL_TO_OPEN_FILE);
		return FALSE;
	}

	SetModifiedFlag(FALSE);     // start off with unmodified
	
	return TRUE;
}

BOOL CNIPToolDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
	wstring strPath(lpszPathName);

	NIPO *pNIPO = NIPO::GetInstance();
	if (pNIPO->SaveJob(strPath, &m_dJob) == FALSE) {
		NIPTOOL_NOTIFY(strPath, NIPL_ERR_FAIL_TO_SAVE_FILE);
		return FALSE;
	}

	SetModifiedFlag(FALSE);     // start off with unmodified

	return TRUE;
}

void CNIPToolDoc::OnCloseDocument()
{
	m_dJob.Clear();

	CDocument::OnCloseDocument();
}

#ifdef SHARED_HANDLERS

// Support for thumbnails
void CNIPToolDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// Modify this code to draw the document's data
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// Support for Search Handlers
void CNIPToolDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// Set search contents from document's data. 
	// The content parts should be separated by ";"

	// For example:  strSearchContent = _T("point;rectangle;circle;ole object;");
	SetSearchContent(strSearchContent);
}

void CNIPToolDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = NULL;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != NULL)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CNIPToolDoc diagnostics

#ifdef _DEBUG
void CNIPToolDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CNIPToolDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CNIPToolDoc commands
bool CNIPToolDoc::RunProcess(NIPJobProcess *pProcess)
{
	if (pProcess == nullptr) {
		return false;
	}

	NIPO *pNIPO = NIPO::GetInstance();
	return pNIPO->RunProcess(pProcess);
}

bool CNIPToolDoc::RunJob()
{
	NIPO *pNIPO = NIPO::GetInstance();
	return pNIPO->RunJob(&m_dJob);
}

void CNIPToolDoc::SetModifyingProcess(NIPJobProcess *pProcess)
{
	m_pModifyingProcess = pProcess;
}

NIPJobProcess *CNIPToolDoc::GetModifyingProcess()
{
	return m_pModifyingProcess;
}

NIPJobProcess *CNIPToolDoc::AddProcess(NIPJobProcess *pProcess, NIPJobProcess *pCurProcess)
{
	if (pProcess == nullptr) {
		return nullptr;
	}

	NIPJobProcess *pNewProcess = m_dJob.AddProcess(pProcess, pCurProcess);
	if (pNewProcess) {
		SetModifyingProcess(pNewProcess);
	}

	return pNewProcess;
}

NIPJobProcess *CNIPToolDoc::ModifyProcess(NIPJobProcess *pProcess)
{
	if (pProcess == nullptr || m_pModifyingProcess == nullptr) {
		return nullptr;
	}

	bool bEnable = m_pModifyingProcess->m_bEnable;
	*m_pModifyingProcess = *pProcess;
	m_pModifyingProcess->m_bEnable = bEnable;	  // Keep 'enable' status as before

	return m_pModifyingProcess;
}

bool CNIPToolDoc::SwitchProcess(NIPJobProcess *pProcess, NIPJobProcess *pTargetProcess)
{
	if (pProcess == nullptr || pTargetProcess == nullptr) {
		return false;
	}

	NIPJobProcess dTemp(pProcess);
	*pProcess = *pTargetProcess;
	*pTargetProcess = dTemp;

	// Select Target Process
	SetModifyingProcess(pTargetProcess);

	return true;
}

bool CNIPToolDoc::DeleteProcess(NIPJobProcess *pProcess)
{
	if (m_dJob.DeleteProcess(pProcess)) {
		SetModifyingProcess(nullptr);
		return true;
	}

	return false;
}
