
#pragma once

#include "ProcessViewTree.h"

/*
class CProcessToolBar : public CMFCToolBar
{
	virtual void OnUpdateCmdUI(CFrameWnd* / *pTarget* /, BOOL bDisableIfNoHndler)
	{
		CMFCToolBar::OnUpdateCmdUI((CFrameWnd*) GetOwner(), bDisableIfNoHndler);
	}

	virtual BOOL AllowShowOnList() const { return FALSE; }
};
*/

class CProcessView : public CDockablePane
{
public:
	CProcessView();
	virtual ~CProcessView();

	void AdjustLayout();
	void OnChangeVisualStyle();

protected:
//	CProcessToolBar m_wndToolBar;
	CProcessViewTree m_wndProcessViewTree;
	CImageList m_ProcessViewImages;
	UINT m_nCurrSort;

	HTREEITEM InsertCategory(wstring strName);
	HTREEITEM InsertProcess(wstring strName, HTREEITEM hParent, DWORD_PTR pMethod);

// Overrides
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	void FillProcessView(NIPJob &dMenu);
	void UpdateParameter(NIPJobProcess *pProcess);

protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
//	afx_msg void OnClassAddMemberFunction();
//	afx_msg void OnClassAddMemberVariable();
//	afx_msg void OnClassDefinition();
//	afx_msg void OnClassParameter();
//	afx_msg void OnNewFolder();
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg LRESULT OnChangeActiveTab(WPARAM, LPARAM);
//	afx_msg void OnSort(UINT id);
//	afx_msg void OnUpdateSort(CCmdUI* pCmdUI);
//	afx_msg void OnSelChanged(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblClick(NMHDR *pNMHDR, LRESULT *pResult);

	DECLARE_MESSAGE_MAP()
public:
//	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
};

