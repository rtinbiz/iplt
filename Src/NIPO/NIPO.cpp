// NIPO.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "NIPO.h"
#include "StringTable.h"
#include <codecvt>
#include "xml3.h"
using namespace XML3;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define CHECK_PROCESS_NAME(str) CHECK_STRING(pProcess->m_strName, str)
#define SET_PARAM_VALUE(var, name) \
if (!GetParamValue(pProcess->GetValue(name), pParam->var)) { \
	delete pParam; \
	return nullptr; \
}

#define SET_PARAM_COMBO_VALUE(method, name) \
if (!pParam->method(pProcess->GetValue(name))) { \
	delete pParam; \
	return nullptr; \
}

#define SET_PARAM_RECT_VALUE(rc, nameStartX, nameStartY, nameEndX, nameEndY) \
	int nStartX, nStartY, nEndX, nEndY; \
	if (!GetParamValue(pProcess->GetValue(nameStartX), nStartX) || !GetParamValue(pProcess->GetValue(nameStartY), nStartY) \
	|| !GetParamValue(pProcess->GetValue(nameEndX), nEndX) || !GetParamValue(pProcess->GetValue(nameEndY), nEndY)) { \
		delete pParam; \
		return nullptr; \
	} \
	pParam->rc.x = nStartX; \
	pParam->rc.y = nStartY; \
	pParam->rc.width = nEndX - nStartX + 1; \
	pParam->rc.height = nEndY - nStartY + 1;

#define SET_SUBPROCESS_PARAM_VALUE(subprocess, name) \
	NIPLParam_##subprocess *pParam_##subprocess = (NIPLParam_##subprocess *)SetNIPLParam(pProcess->FindSubProcess(name)); \
	if (pParam_##subprocess == nullptr) { \
		delete pParam; \
		return nullptr; \
	} \
	pParam->m_dParam_##subprocess = *pParam_##subprocess; \
	delete pParam_##subprocess;


inline wstring str2wstr(string str) {
	wstring_convert<codecvt_utf8_utf16<wchar_t>> converter;
	return converter.from_bytes(str);
}

inline string wstr2str(wstring str) {
	wstring_convert<codecvt_utf8_utf16<wchar_t>> converter;
	return converter.to_bytes(str);
}

inline bool is_number(wchar_t c)
{
	return (isdigit(c) || c == '.' || c == '-');
}

inline bool is_number(wstring str)
{
	return !str.empty() && find_if(str.begin(), str.end(), [] (wchar_t c) { return !is_number(c); }) == str.end();
}

inline wstring &strtrim(wstring &str)
{
	// left trim
	str.erase(str.begin(), find_if(str.begin(), str.end(), [] (wchar_t c) {
		return (c != ' ');
	}));

	return str;
}

void _LoadNIPJobParam(XMLElement *pXMLParam, void *pParent, bool bProcess)
{
	wstring strParamName = str2wstr(pXMLParam->v(STR_XML_KEY_NAME));
	wstring strElementName = str2wstr(pXMLParam->GetElementName());
	if (CHECK_STRING(strElementName, _T(STR_XML_ELEMENT_PARAM_GROUP))) {
		NIPJobParam *pGroup = nullptr;
		if (bProcess) {
			pGroup = ((NIPJobProcess *)pParent)->AddParamGroup(strParamName);
		}
		else {
			pGroup = ((NIPJobParam *)pParent)->AddParamGroup(strParamName);
		}

		if (pGroup) {
			auto listParamInGroup = pXMLParam->GetChildren();
			for (auto pXMLParamInGroup : listParamInGroup) {
				_LoadNIPJobParam(pXMLParamInGroup.get(), (void *)pGroup, false);
			}
		}
	}
	else {
		wstring strType = str2wstr(pXMLParam->v(STR_XML_KEY_TYPE));
		wstring strValue = str2wstr(pXMLParam->v(STR_XML_KEY_VALUE));

		NIPJobParam *pParam;
		if (bProcess) {
			pParam = ((NIPJobProcess *)pParent)->AddParam(strParamName, strValue, strType);
		}
		else {
			pParam = ((NIPJobParam *)pParent)->AddParam(strParamName, strValue, strType);
		}

		// Set additional data
		if (pParam) {
			pParam->m_strMin = str2wstr(pXMLParam->v(STR_XML_KEY_MIN));
			pParam->m_strMax = str2wstr(pXMLParam->v(STR_XML_KEY_MAX));

			pParam->m_strDesc = str2wstr(pXMLParam->v(STR_XML_KEY_DESC));

			// combo value
			if (CHECK_STRING(strType, _T(STR_XML_KEY_COMBO))) {
				wstring strCombo = str2wstr(pXMLParam->v(STR_XML_KEY_COMBO));
				if (!strCombo.empty()) {
					wstring strComboValue;
					size_t nStartOffset = 0;
					size_t nEndOffset = 0;
					bool bLastValue = false;
					do {
						nEndOffset = strCombo.find(',', nStartOffset);
						if (nEndOffset == wstring::npos) {
							strComboValue = strCombo.substr(nStartOffset);
							bLastValue = true;
						}
						else {
							strComboValue = strCombo.substr(nStartOffset, nEndOffset - nStartOffset);
							nStartOffset = nEndOffset + 1;
						}

						pParam->AddComboValue(strComboValue);
					} while (!bLastValue);
				}
			}
		}
	}
}

void _LoadNIPJobProcess(XMLElement *pXMLProcess, void *pParent, wstring strParentElementName)
{
	wstring strProcessName = str2wstr(pXMLProcess->v(STR_XML_KEY_NAME));
	wstring strEnable = str2wstr(pXMLProcess->v(STR_XML_KEY_ENABLE));
	wstring strElementName = str2wstr(pXMLProcess->GetElementName());

	NIPJobProcess *pProcess = nullptr;
	if (CHECK_STRING(strElementName, _T(STR_XML_ELEMENT_PROCESS))) {
		if (CHECK_STRING(strParentElementName, _T(STR_XML_ELEMENT_JOB))) {
			pProcess = ((NIPJob *)pParent)->AddProcess(strProcessName, strEnable == L"true");
		}
		else if (CHECK_STRING(strParentElementName, _T(STR_XML_ELEMENT_CATEGORY))) {
			pProcess = ((NIPJobCategory *)pParent)->AddProcess(strProcessName);
		}
	}
	else if (CHECK_STRING(strElementName, _T(STR_XML_ELEMENT_SUBPROCESS))) {
		pProcess = ((NIPJobProcess *)pParent)->AddSubProcess(strProcessName);
	}

	if (pProcess == nullptr) {
		return;
	}

	auto listXMLChild = pXMLProcess->GetChildren();
	if (pProcess->m_bSubProcess && listXMLChild.size() == 0) {
		// If there's no children of SubProcess, that means it's a subprocess without the expression of parameters
		// do nothing here and load it later in pass 2.
	}
	else {
		for (auto pXMLChild : listXMLChild) {
			wstring strChildElementName = str2wstr(pXMLChild->GetElementName());
			if (CHECK_STRING(strChildElementName, _T(STR_XML_ELEMENT_SUBPROCESS))) {
				_LoadNIPJobProcess(pXMLChild.get(), (void *)pProcess, strChildElementName);
			}
			else {
				_LoadNIPJobParam(pXMLChild.get(), (void *)pProcess, true);
			}
		}
	}
}

void _LoadNIPJobSubProcess(NIPJobProcess *pProcess, NIPJob *pJob)
{
	if (pProcess == nullptr) {
		return;
	}

	if (pProcess->m_bSubProcess) {
		// if it's a subprocess, find its main process
		NIPJobProcess *pMainProcess = pJob->FindProcess(pProcess->m_strName);
		if (pMainProcess == nullptr) {
			return;
		}

		// load all subprocesses of the main process first
		_LoadNIPJobSubProcess(pMainProcess, pJob);

		// copy the main process to current path
		pProcess->CopyWithPath(pMainProcess, pProcess->m_strPath);
	}
	else {
		// if it's a main process, load its sub processes first
		for (auto pSubProcess : pProcess->m_listSubProcess) {
			if (pSubProcess->m_listParam.size() == 0) {		// if it's parameter was set yet..
				_LoadNIPJobSubProcess(pSubProcess, pJob);
			}
		}
	}
}

void _SaveNIPJobParam(NIPJobParam *pParam, XMLElement *pXMLParent)
{
	if (pParam->m_bGroup) {
		XMLElement dXMLParamGroup(STR_XML_ELEMENT_PARAM_GROUP);
		string strName = wstr2str(pParam->m_strName);
		dXMLParamGroup.AddVariable(STR_XML_KEY_NAME, strName.c_str());

		for (auto pParamInGroup : pParam->m_listParam) {
			_SaveNIPJobParam(pParamInGroup, &dXMLParamGroup);
		}

		pXMLParent->AddElement(dXMLParamGroup);
	}
	else {
		string strName = wstr2str(pParam->m_strName);
		string strValue = wstr2str(pParam->m_strValue);
		string strType = wstr2str(pParam->m_strType);
		string strMin = wstr2str(pParam->m_strMin);
		string strMax = wstr2str(pParam->m_strMax);
		string strDesc = wstr2str(pParam->m_strDesc);

		XMLElement dXMLParam(STR_XML_ELEMENT_PARAM);
		dXMLParam.AddVariable(STR_XML_KEY_NAME, strName.c_str());
		dXMLParam.AddVariable(STR_XML_KEY_TYPE, strType.c_str());
		dXMLParam.AddVariable(STR_XML_KEY_VALUE, strValue.c_str());
		if (strMin != "") {
			dXMLParam.AddVariable(STR_XML_KEY_MIN, strMin.c_str());
		}
		if (strMax != "") {
			dXMLParam.AddVariable(STR_XML_KEY_MAX, strMax.c_str());
		}

		size_t nCount = pParam->m_listComboValue.size();
		if (nCount > 0) {
			string strCombo;
			for (size_t i = 0; i < nCount; i++) {
				string strComboValue = wstr2str(pParam->m_listComboValue[i]);
				strCombo += strComboValue;
				if (i != nCount - 1) {
					strCombo += ",";
				}
			}
			dXMLParam.AddVariable(STR_XML_KEY_COMBO, strCombo.c_str());
		}

		if (strDesc != "") {
			dXMLParam.AddVariable(STR_XML_KEY_DESC, strDesc.c_str());
		}

		pXMLParent->AddElement(dXMLParam);
	}
}

void _SaveNIPJobProcess(NIPJobProcess *pProcess, XMLElement *pXMLParent)
{
	string strProcessName = wstr2str(pProcess->m_strName);
	string strEnable = pProcess->m_bEnable ? "true" : "false";

	pXMLParent->AddVariable(STR_XML_KEY_NAME, strProcessName.c_str());
	pXMLParent->AddVariable(STR_XML_KEY_ENABLE, strEnable.c_str());

	for (auto pParam : pProcess->m_listParam) {
		_SaveNIPJobParam(pParam, pXMLParent);
	}

	for (auto pSubProcess : pProcess->m_listSubProcess) {
		XMLElement dXMLSubProcess(STR_XML_ELEMENT_SUBPROCESS);
		_SaveNIPJobProcess(pSubProcess, &dXMLSubProcess);

		pXMLParent->AddElement(dXMLSubProcess);
	}
}

NIPO *NIPO::pThis = 0x00;

NIPO *NIPO::GetInstance(BOOL bNew)
{
	if(bNew) {
		return new NIPO();
	}

	if(pThis == 0x00) {
		pThis = new NIPO();
	}

	return pThis;
}

void NIPO::ReleaseInstance(NIPO *pThat)
{
	if(pThat != 0x00) {
		delete pThat;
	}
	else if(pThis != 0x00) {
		delete pThis;
		pThis = 0x00;

		// Release NIPL instances
		NIPL::ReleaseInstance();
		NIPLCV::ReleaseInstance();
		NIPLGPU::ReleaseInstance();
	}
}

NIPO::NIPO()
{
	m_fnNotify = nullptr;
	m_nNotifyLevel = NNL_NONE;
}

NIPO::~NIPO()
{
	Clear();
}

void NIPO::Clear()
{
	m_dInput.Clear();

	for (auto dPos : m_mapOutput) {
		NIPLOutput &dOutput = dPos.second;
		dOutput.Clear(true);
	}
	m_mapOutput.clear();
}

void NIPO::SetNotifyFunc(NIPONotify fnNotify, NIPO_NOTIFY_LEVEL nNotifyLevel) {
	m_fnNotify = fnNotify;
	m_nNotifyLevel = nNotifyLevel;
}

void NIPO::SendNotify(wstring strName, NIPL_ERR nErr, NIPO_NOTIFY_LEVEL nNotifyLevel) {
	if (m_fnNotify && m_nNotifyLevel >= nNotifyLevel) {
		m_fnNotify(strName, nErr, nNotifyLevel);
	}
}

void NIPO::SendNotifyInputUpdate(wstring strImage)
{
	SendNotify(strImage, (NIPL_ERR)0, NNL_INPUT_UPDATE);
}

void NIPO::SendNotifyOutputUpdate(wstring strImage)
{
	SendNotify(strImage, (NIPL_ERR)0, NNL_OUTPUT_UPDATE);
}

NIPJob *NIPO::LoadJob(wstring strPath)
{
	NIPJob *pJob = new NIPJob();
	if (pJob == nullptr) {
		return nullptr;
	}

	if (!LoadJob(strPath, *pJob)) {
		return nullptr;
	}

	return pJob;
}

bool NIPO::LoadJob(wstring strPath, NIPJob &dJob)
{
	XML dXML;
	auto nResult = dXML.Load(strPath.c_str());
	if (nResult != XML_PARSE::OK) {
		return false;
	}

	auto dRoot = dXML.GetRootElement();
	wstring strVersion = str2wstr(dRoot.v(STR_XML_KEY_VERSION));
	wstring strElementName = str2wstr(dRoot.GetElementName());

	if (CHECK_STRING(strElementName, _T(STR_XML_ELEMENT_MENU))) {
		auto listXMLCategory = dRoot.GetChildren();
		for (auto pXMLCategory : listXMLCategory) {
			wstring strCategoryName = str2wstr(pXMLCategory->v(STR_XML_KEY_NAME));
			auto pCategory = dJob.AddCategory(strCategoryName);
			if (pCategory == nullptr) {
				continue;
			}

			auto listXMLProcess = pXMLCategory->GetChildren();
			for (auto pXMLProcess : listXMLProcess) {
				_LoadNIPJobProcess(pXMLProcess.get(), pCategory, _T(STR_XML_ELEMENT_CATEGORY));
			}
		}

		// Pass 2 : Set SubProcess
		for (auto pCategory : dJob.m_listCategory) {
			for (auto pProcess : pCategory->m_listProcess) {
				_LoadNIPJobSubProcess(pProcess, &dJob);
			}
		}
	}
	else if (CHECK_STRING(strElementName, _T(STR_XML_ELEMENT_JOB))) {
		auto listXMLProcess = dRoot.GetChildren();
		for (auto pXMLProcess : listXMLProcess) {
			_LoadNIPJobProcess(pXMLProcess.get(), &dJob, _T(STR_XML_ELEMENT_JOB));
		}
	}

	return true;
}

bool NIPO::SaveJob(wstring strPath, const NIPJob &dJob)
{
	XML dXML;

	// Set Header
	XMLHeader dHeader;
	XMLVariable &version = dHeader.GetVersion();
	XMLVariable &encoding = dHeader.GetEncoding();
	XMLVariable &standalone = dHeader.GetStandalone();
	version.SetValue("1.0");
	encoding.SetValue("utf-8");
	standalone.SetValue("");
	dXML.SetHeader(dHeader);

	// Set Body
	XMLElement dRoot(STR_XML_ELEMENT_JOB);
	string strVersion = wstr2str(NIPO_VERION);
	dRoot.AddVariable(STR_XML_KEY_VERSION, strVersion.c_str());

	for (auto pProcess : dJob.m_listProcess) {
		XMLElement dXMLProcess(STR_XML_ELEMENT_PROCESS);
		_SaveNIPJobProcess(pProcess, &dXMLProcess);

		dRoot.AddElement(dXMLProcess);
	}

	dXML.SetRootElement(dRoot);

	auto nResult = dXML.Save(strPath.c_str());
	if (nResult != XML3::XML_ERROR::OK) {
		return false;
	}

	return true;
}

void NIPO::ReleaseJob(NIPJob *pJob)
{
	if (pJob) {
		delete pJob;
	}
}

bool NIPO::RunJob(NIPJob *pJob)
{
	if (pJob == nullptr) {
		return false;
	}

	for (auto pProcess : pJob->m_listProcess) {
		if (!RunProcess(pProcess)) {
			return false;
		}
	}

	return true;
}

bool NIPO::RunProcess(NIPJobProcess *pProcess)
{
	if (pProcess == nullptr) {
		return false;
	}

	if (!pProcess->m_bEnable) {
		SendNotify(pProcess->m_strName, NIPL_ERR_PASS, NNL_PROCESS);
		return true;
	}

	if (CHECK_PROCESS_NAME(_T(STR_PROCESS_LOAD_INPUT_IMAGE))) {
		m_dInput.Clear();

		wstring strImageFileName = pProcess->GetValue(_T(STR_PARAM_IMAGE_FILE_NAME));
		wstring strMaskFileName = pProcess->GetValue(_T(STR_PARAM_MASK_FILE_NAME));
		wstring strTemplateFileName = pProcess->GetValue(_T(STR_PARAM_TEMPLATE_FILE_NAME));

		if (!LoadInputImage(strImageFileName, strMaskFileName, strTemplateFileName)) {
			SendNotify(pProcess->m_strName, NIPL_ERR_FAIL, NNL_PROCESS);
			return false;
		}

		bool bCovertToGrayImage = pProcess->GetValueBool(_T(STR_PARAM_CONVERT_TO_GRAY_IMAGE));
		if (bCovertToGrayImage) {
			if (!ConvertInputImageToGray(pProcess)) {
				SendNotify(pProcess->m_strName, NIPL_ERR_FAIL, NNL_PROCESS);
				return false;
			}
		}

		SendNotifyInputUpdate(strImageFileName);
		SendNotify(pProcess->m_strName, NIPL_ERR_SUCCESS, NNL_PROCESS);
		return true;
	}
	else if(CHECK_PROCESS_NAME(_T(STR_PROCESS_SAVE_OUTPUT_IMAGE))) {
		wstring strFileName = pProcess->GetValue(_T(STR_PARAM_IMAGE_FILE_NAME));
		wstring strOutputName = pProcess->GetValue(_T(STR_PARAM_OUTPUT));

		if (!SaveOutputImage(strFileName, strOutputName)) {
			SendNotify(pProcess->m_strName, NIPL_ERR_FAIL, NNL_PROCESS);
			return false;
		}

		strFileName = pProcess->GetValue(_T(STR_PARAM_RESULT_FILE_NAME));
		if (!SaveOutputResult(strFileName, strOutputName)) {
			SendNotify(pProcess->m_strName, NIPL_ERR_FAIL, NNL_PROCESS);
			return false;
		}


		SendNotify(pProcess->m_strName, NIPL_ERR_SUCCESS, NNL_PROCESS);
		return true;
	}
	else if(CHECK_PROCESS_NAME(_T(STR_PROCESS_CREATE_IMAGE))) {
		NIPLParam *pParam = SetNIPLParam(pProcess);
		if (pParam == nullptr) {
			SendNotify(pProcess->m_strName, NIPL_ERR_NO_PARAM, NNL_PROCESS);
			return false;
		}

		NIPLInput dInput;
		NIPLOutput dOutput;

		dInput.m_pParam = pParam;

		NIPL_ERR nErr = DoNIPLProcess(pProcess->m_strName, &dInput, &dOutput);

		// delete pParam first
		delete pParam;

		if (NIPL_FAIL(nErr)) {
			SendNotify(pProcess->m_strName, nErr, NNL_PROCESS);
			return false;
		}

		wstring strOutputName = pProcess->GetValue(_T(STR_PARAM_OUTPUT));
		if (!SetOutput(strOutputName, dOutput)) {
			dOutput.ClearResult();

			SendNotify(pProcess->m_strName, NIPL_ERR_FAIL, NNL_PROCESS);
			return false;
		}
		else {
			bool bShowOutputImage = pProcess->GetValueBool(_T(STR_PARAM_SHOW_OUTPUT_IMAGE));
			if (bShowOutputImage) {
				SendNotifyOutputUpdate(strOutputName);
			}
		}

		SendNotify(pProcess->m_strName, nErr, NNL_PROCESS);
		return true;
	}

	Mat dInputImage;
	wstring strInputName = pProcess->GetValue(_T(STR_PARAM_INPUT));
	if (!GetInputImage(strInputName, dInputImage)) {
		SendNotify(pProcess->m_strName, NIPL_ERR_FAIL, NNL_PROCESS);
		return false;
	}

	NIPLParam *pParam = SetNIPLParam(pProcess);
	if (pParam == nullptr) {
		SendNotify(pProcess->m_strName, NIPL_ERR_NO_PARAM, NNL_PROCESS);
		return false;
	}

	NIPLInput dInput;
	NIPLOutput dOutput;

	dInput.m_dImg = dInputImage;
	dInput.m_pParam = pParam;

	NIPL_ERR nErr = DoNIPLProcess(pProcess->m_strName, &dInput, &dOutput);

	// delete pParam first
	delete pParam;

	if (NIPL_FAIL(nErr)) {
		SendNotify(pProcess->m_strName, nErr, NNL_PROCESS);
		return false;
	}

	wstring strOutputName = pProcess->GetValue(_T(STR_PARAM_OUTPUT));
	if (!SetOutput(strOutputName, dOutput)) {
		dOutput.ClearResult();

		SendNotify(pProcess->m_strName, NIPL_ERR_FAIL, NNL_PROCESS);
		return false;
	}
	else {
		bool bShowOutputImage = pProcess->GetValueBool(_T(STR_PARAM_SHOW_OUTPUT_IMAGE));
		if (bShowOutputImage) {
			SendNotifyOutputUpdate(strOutputName);
		}
	}

	SendNotify(pProcess->m_strName, nErr, NNL_PROCESS);
	return true;
}

NIPLParam *NIPO::SetNIPLParam(NIPJobProcess *pProcess)
{
	if (pProcess == nullptr) {
		return nullptr;
	}

	//
	// NIPL
	//
	if (CHECK_PROCESS_NAME(_T(STR_PROCESS_CREATE_IMAGE))) {
		NIPLParam_CreateImage *pParam = new NIPLParam_CreateImage();

		SET_PARAM_COMBO_VALUE(SetType, _T(STR_PARAM_TYPE));
		SET_PARAM_VALUE(m_nWidth, _T(STR_PARAM_WIDTH));
		SET_PARAM_VALUE(m_nHeight, _T(STR_PARAM_HEIGHT));
		SET_PARAM_VALUE(m_nValue, _T(STR_PARAM_VALUE));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_COLOR_TO_GRAY))) {
		NIPLParam_Color2Gray *pParam = new NIPLParam_Color2Gray();

		SET_PARAM_COMBO_VALUE(SetGrayLevel, _T(STR_PARAM_GRAY_LEVEL));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_INVERT))) {
		NIPLParam_Invert *pParam = new NIPLParam_Invert();

		// Nothing to set

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_OPERATE))) {
		Mat dTargetImg;
		wstring strName = pProcess->GetValue(_T(STR_PARAM_TARGET_NAME));
		if (!GetOutputImage(strName, dTargetImg)) {
			SendNotify(strName, NIPL_ERR_FAIL_INVALID_IMAGE, NNL_PROCESS);
			return nullptr;
		}

		NIPLParam_Operate *pParam = new NIPLParam_Operate();

		SET_PARAM_COMBO_VALUE(SetMethod, _T(STR_PARAM_METHOD));
		pParam->m_dTargetImg = dTargetImg;

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_COPY_SUBIMAGE_FROM))) {
		NIPLParam_CopySubImageFrom *pParam = new NIPLParam_CopySubImageFrom();

		SET_PARAM_VALUE(m_nStartPosX, _T(STR_PARAM_START_POSITION_X));
		SET_PARAM_VALUE(m_nStartPosY, _T(STR_PARAM_START_POSITION_Y));
		SET_PARAM_VALUE(m_nEndPosX, _T(STR_PARAM_END_POSITION_X));
		SET_PARAM_VALUE(m_nEndPosY, _T(STR_PARAM_END_POSITION_Y));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_COPY_SUBIMAGE_TO))) {
		Mat dTargetImg;
		wstring strName = pProcess->GetValue(_T(STR_PARAM_TARGET_NAME));
		if (!GetOutputImage(strName, dTargetImg)) {
			SendNotify(strName, NIPL_ERR_FAIL_INVALID_IMAGE, NNL_PROCESS);
			return nullptr;
		}

		NIPLParam_CopySubImageTo *pParam = new NIPLParam_CopySubImageTo();

		pParam->m_dTargetImg = dTargetImg;
		SET_PARAM_VALUE(m_nStartPosX, _T(STR_PARAM_START_POSITION_X));
		SET_PARAM_VALUE(m_nStartPosY, _T(STR_PARAM_START_POSITION_Y));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_THRESHOLDING))) {
		NIPLParam_Thresholding *pParam = new NIPLParam_Thresholding();

		SET_PARAM_COMBO_VALUE(SetMethod, _T(STR_PARAM_METHOD));
		SET_PARAM_VALUE(m_nThreshold, _T(STR_PARAM_THRESHOLD));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_COLOR_THRESHOLDING))) {
		NIPLParam_ColorThresholding *pParam = new NIPLParam_ColorThresholding();

		SET_PARAM_COMBO_VALUE(SetMethod_R, _T(STR_PARAM_METHOD_R));
		SET_PARAM_COMBO_VALUE(SetMethod_G, _T(STR_PARAM_METHOD_G));
		SET_PARAM_COMBO_VALUE(SetMethod_B, _T(STR_PARAM_METHOD_B));
		SET_PARAM_VALUE(m_nLowerValue_R, _T(STR_PARAM_LOWER_VALUE_R));
		SET_PARAM_VALUE(m_nLowerValue_G, _T(STR_PARAM_LOWER_VALUE_G));
		SET_PARAM_VALUE(m_nLowerValue_B, _T(STR_PARAM_LOWER_VALUE_B));
		SET_PARAM_VALUE(m_nUpperValue_R, _T(STR_PARAM_UPPER_VALUE_R));
		SET_PARAM_VALUE(m_nUpperValue_G, _T(STR_PARAM_UPPER_VALUE_G));
		SET_PARAM_VALUE(m_nUpperValue_B, _T(STR_PARAM_UPPER_VALUE_B));

		SET_PARAM_VALUE(m_nLowerRatio_R_G, _T(STR_PARAM_LOWER_RATIO_R_G));
		SET_PARAM_VALUE(m_nLowerRatio_R_B, _T(STR_PARAM_LOWER_RATIO_R_B));
		SET_PARAM_VALUE(m_nLowerRatio_G_B, _T(STR_PARAM_LOWER_RATIO_G_B));
		SET_PARAM_VALUE(m_nUpperRatio_R_G, _T(STR_PARAM_UPPER_RATIO_R_G));
		SET_PARAM_VALUE(m_nUpperRatio_R_B, _T(STR_PARAM_UPPER_RATIO_R_B));
		SET_PARAM_VALUE(m_nUpperRatio_G_B, _T(STR_PARAM_UPPER_RATIO_G_B));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_SMOOTHIG))) {
		NIPLParam_Smoothing *pParam = new NIPLParam_Smoothing;

		SET_PARAM_COMBO_VALUE(SetMethod, _T(STR_PARAM_METHOD));
		SET_PARAM_VALUE(m_nFilterSize, _T(STR_PARAM_FILTER_SIZE));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_EDGE_DETECTING))) {
		NIPLParam_EdgeDetecting *pParam = new NIPLParam_EdgeDetecting;

		SET_PARAM_COMBO_VALUE(SetMethod, _T(STR_PARAM_METHOD));
		SET_PARAM_VALUE(m_nCannyLowerThreshold, _T(STR_PARAM_CANNY_LOWER_THRESHOLD));
		SET_PARAM_VALUE(m_nCannyUpperThreshold, _T(STR_PARAM_CANNY_UPPER_THRESHOLD));
		SET_PARAM_VALUE(m_nFilterSize, _T(STR_PARAM_FILTER_SIZE));

		return pParam;
	}

	//
	// NIPL CV
	//
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_BINARIZE))) {
		NIPLParam_Binarize *pParam = new NIPLParam_Binarize();

		SET_PARAM_COMBO_VALUE(SetMethod, _T(STR_PARAM_METHOD));
		SET_PARAM_VALUE(m_nThreshold, _T(STR_PARAM_THRESHOLD));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_FIT_BACKGROUND))) {
		NIPLParam_FitBackground *pParam = new NIPLParam_FitBackground();

		SET_PARAM_VALUE(m_nBlockCount, _T(STR_PARAM_BLOCK_COUNT));
		SET_PARAM_VALUE(m_nDegree, _T(STR_PARAM_DEGREE));
		SET_PARAM_VALUE(m_nThreshold, _T(STR_PARAM_THRESHOLD));
		SET_PARAM_VALUE(m_bSubtrackFromImage, _T(STR_PARAM_SUBTRACT_FROM_IMAGE));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_ELIMINATE_NOISE))) {
		NIPLParam_EliminateNoise *pParam = new NIPLParam_EliminateNoise;

		SET_PARAM_COMBO_VALUE(SetMethod, _T(STR_PARAM_METHOD));
		SET_PARAM_VALUE(m_nMorphologyFilterSize, _T(STR_PARAM_MORPHOLOGY_FILTER_SIZE));
		SET_PARAM_VALUE(m_nMedianFilterSize, _T(STR_PARAM_MEDIAN_FILTER_SIZE));
		SET_PARAM_VALUE(m_nMinSize, _T(STR_PARAM_MIN_SIZE));
		SET_PARAM_VALUE(m_nMaxSize, _T(STR_PARAM_MAX_SIZE));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_ANALYIZE_SHAPE))) {
		NIPLParam_AnalyzeShape *pParam = new NIPLParam_AnalyzeShape();

		SET_PARAM_COMBO_VALUE(SetMethod, _T(STR_PARAM_METHOD));
		SET_PARAM_VALUE(m_nScratchLengthRatio, _T(STR_PARAM_SCRATCH_LENGTH_RATIO));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_FIND_BLOB))) {
		NIPLParam_FindBlob *pParam = new NIPLParam_FindBlob();

		SET_PARAM_VALUE(m_nMinSize, _T(STR_PARAM_MIN_SIZE));
		SET_PARAM_VALUE(m_nMaxSize, _T(STR_PARAM_MAX_SIZE));
		SET_PARAM_VALUE(m_bFindInRange, _T(STR_PARAM_FIND_IN_RANGE));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_FIND_LINE))) {
		NIPLParam_FindLine *pParam = new NIPLParam_FindLine();

		SET_PARAM_VALUE(m_nHoughThreshold, _T(STR_PARAM_HOUGH_THRESHOLD));
		SET_PARAM_VALUE(m_nLineMinLength, _T(STR_PARAM_LINE_MIN_LENGTH));
		SET_PARAM_VALUE(m_nLineMaxLength, _T(STR_PARAM_LINE_MAX_LENGTH));
		SET_PARAM_VALUE(m_nLineMinGap, _T(STR_PARAM_LINE_MIN_GAP));
		SET_PARAM_VALUE(m_nLineThickness, _T(STR_PARAM_LINE_THICKNESS));
		SET_PARAM_VALUE(m_nCannyLowerThreshold, _T(STR_PARAM_CANNY_LOWER_THRESHOLD));
		SET_PARAM_VALUE(m_nCannyUpperThreshold, _T(STR_PARAM_CANNY_UPPER_THRESHOLD));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_FIND_CIRCLE))) {
		NIPLParam_FindCircle *pParam = new NIPLParam_FindCircle();

		SET_PARAM_VALUE(m_nMinCircleDistance, _T(STR_PARAM_MIN_CIRCLE_DISTANCE));
		SET_PARAM_VALUE(m_nCannyUpperThreshold, _T(STR_PARAM_CANNY_UPPER_THRESHOLD));
		SET_PARAM_VALUE(m_nCenterDetectionThreshold, _T(STR_PARAM_CENTER_DETECTION_THRESHOLD));
		SET_PARAM_VALUE(m_nMinRadius, _T(STR_PARAM_MIN_RADIUS));
		SET_PARAM_VALUE(m_nMaxRadius, _T(STR_PARAM_MAX_RADIUS));
		SET_PARAM_VALUE(m_nLineThickness, _T(STR_PARAM_LINE_THICKNESS));

		SET_PARAM_VALUE(m_bCheckCenterPos, _T(STR_PARAM_CHECK_CENTER_POSITION));
		SET_PARAM_VALUE(m_nCenterPosX, _T(STR_PARAM_CENTER_POSITION_X));
		SET_PARAM_VALUE(m_nCenterPosY, _T(STR_PARAM_CENTER_POSITION_Y));
		SET_PARAM_VALUE(m_nCenterPosRange, _T(STR_PARAM_CENTER_POSITION_RANGE));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_SET_CIRCLE_ROI))) {
		NIPLParam_Circle2Rect *pParam = new NIPLParam_Circle2Rect();

		SET_PARAM_VALUE(m_nCenterPosX, _T(STR_PARAM_CENTER_POSITION_X));
		SET_PARAM_VALUE(m_nCenterPosY, _T(STR_PARAM_CENTER_POSITION_Y));
		SET_PARAM_VALUE(m_nMinRadius, _T(STR_PARAM_MIN_RADIUS));
		SET_PARAM_VALUE(m_nMaxRadius, _T(STR_PARAM_MAX_RADIUS));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_CIRCLE_TO_RECT))) {
		NIPLParam_Circle2Rect *pParam = new NIPLParam_Circle2Rect();

		SET_PARAM_VALUE(m_nCenterPosX, _T(STR_PARAM_CENTER_POSITION_X));
		SET_PARAM_VALUE(m_nCenterPosY, _T(STR_PARAM_CENTER_POSITION_Y));
		SET_PARAM_VALUE(m_nMinRadius, _T(STR_PARAM_MIN_RADIUS));
		SET_PARAM_VALUE(m_nMaxRadius, _T(STR_PARAM_MAX_RADIUS));
		SET_PARAM_VALUE(m_nAngleStep, _T(STR_PARAM_ANGLE_STEP));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_RECT_TO_CIRCLE))) {
		NIPLParam_Rect2Circle *pParam = new NIPLParam_Rect2Circle();

		SET_PARAM_VALUE(m_nOutputImgSizeX, _T(STR_PARAM_OUTPUT_IMAGE_WIDTH));
		SET_PARAM_VALUE(m_nOutputImgSizeY, _T(STR_PARAM_OUTPUT_IMAGE_HEIGHT));
		SET_PARAM_VALUE(m_nCenterPosX, _T(STR_PARAM_CENTER_POSITION_X));
		SET_PARAM_VALUE(m_nCenterPosY, _T(STR_PARAM_CENTER_POSITION_Y));
		SET_PARAM_VALUE(m_nMinRadius, _T(STR_PARAM_MIN_RADIUS));
		SET_PARAM_VALUE(m_nAngleStep, _T(STR_PARAM_ANGLE_STEP));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_FIT_TO_CORRECT_CIRCLE))) {
		NIPLParam_FitToCorrectCircle *pParam = new NIPLParam_FitToCorrectCircle();

		SET_SUBPROCESS_PARAM_VALUE(FindCircle, _T(STR_PROCESS_FIND_CIRCLE));

		return pParam;
	}

	//
	// Custom
	//
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_LDC_TERMINAL))) {
		NIPLParam_LDC_Terminal *pParam = new NIPLParam_LDC_Terminal();

		for (int i = 0; i < LDC_TERMINAL_MAX_ROUND_SHAPE; i++) {
			wstring strParamX = wstring(_T(STR_PARAM_GROUP_ROUND_TERMINAL)) + L"/" + _T(STR_PARAM_CENTER_POSITION_X) + L" " + to_wstring(i + 1);
			wstring strParamY = wstring(_T(STR_PARAM_GROUP_ROUND_TERMINAL)) + L"/" + _T(STR_PARAM_CENTER_POSITION_Y) + L" " + to_wstring(i + 1);
			SET_PARAM_VALUE(m_ptCenter_RoundTerminal[i].x, strParamX);
			SET_PARAM_VALUE(m_ptCenter_RoundTerminal[i].y, strParamY);
		}
		for (int i = 0; i < LDC_TERMINAL_MAX_RECT_SHAPE; i++) {
			wstring strParamX = wstring(_T(STR_PARAM_GROUP_RECT_TERMINAL)) + L"/" + _T(STR_PARAM_CENTER_POSITION_X) + L" " + to_wstring(i + 1);
			wstring strParamY = wstring(_T(STR_PARAM_GROUP_RECT_TERMINAL)) + L"/" + _T(STR_PARAM_CENTER_POSITION_Y) + L" " + to_wstring(i + 1);
			SET_PARAM_VALUE(m_ptCenter_RectTerminal[i].x, strParamX);
			SET_PARAM_VALUE(m_ptCenter_RectTerminal[i].y, strParamY);
		}

		SET_PARAM_VALUE(m_nTerminalRadius, _T(STR_PARAM_TERMINAL_RADIUS));
		SET_PARAM_VALUE(m_nTerminalSamplingRadius, _T(STR_PARAM_TERMINAL_SAMPLING_RADIUS));
		SET_PARAM_VALUE(m_nEndSamplingThreshold, _T(STR_PARAM_TERMINAL_SAMPLING_THRESHOLD));

		SET_SUBPROCESS_PARAM_VALUE(EliminateNoise, _T(STR_PROCESS_ELIMINATE_NOISE));
		SET_SUBPROCESS_PARAM_VALUE(EdgeDetecting, _T(STR_PROCESS_EDGE_DETECTING));
		SET_SUBPROCESS_PARAM_VALUE(FindCircle, _T(STR_PROCESS_FIND_CIRCLE));
		SET_SUBPROCESS_PARAM_VALUE(FindLine, _T(STR_PROCESS_FIND_LINE));

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_LDC_TUBE))) {
		NIPLParam_LDC_Tube *pParam = new NIPLParam_LDC_Tube();

		for (int i = 0; i < LDC_TUBE_MAX_RED; i++) {
			wstring strParamStartX = wstring(_T(STR_PARAM_GROUP_RED_TUBE)) + L" " + to_wstring(i + 1) + L"/" + _T(STR_PARAM_START_POSITION_X);
			wstring strParamStartY = wstring(_T(STR_PARAM_GROUP_RED_TUBE)) + L" " + to_wstring(i + 1) + L"/" + _T(STR_PARAM_START_POSITION_Y);
			wstring strParamEndX = wstring(_T(STR_PARAM_GROUP_RED_TUBE)) + L" " + to_wstring(i + 1) + L"/" + _T(STR_PARAM_END_POSITION_X);
			wstring strParamEndY = wstring(_T(STR_PARAM_GROUP_RED_TUBE)) + L" " + to_wstring(i + 1) + L"/" + _T(STR_PARAM_END_POSITION_Y);
			wstring strParamThreshold = wstring(_T(STR_PARAM_GROUP_RED_TUBE)) + L" " + to_wstring(i + 1) + L"/" + _T(STR_PARAM_THRESHOLD);

			SET_PARAM_RECT_VALUE(m_rcBoundingBox_RedTube[i], strParamStartX, strParamStartY, strParamEndX, strParamEndY);
			SET_PARAM_VALUE(m_nThreshold_RedTube[i], strParamThreshold);
		}
		for (int i = 0; i < LDC_TUBE_MAX_BLACK; i++) {
			wstring strParamStartX = wstring(_T(STR_PARAM_GROUP_BLACK_TUBE)) + L" " + to_wstring(i + 1) + L"/" + _T(STR_PARAM_START_POSITION_X);
			wstring strParamStartY = wstring(_T(STR_PARAM_GROUP_BLACK_TUBE)) + L" " + to_wstring(i + 1) + L"/" + _T(STR_PARAM_START_POSITION_Y);
			wstring strParamEndX = wstring(_T(STR_PARAM_GROUP_BLACK_TUBE)) + L" " + to_wstring(i + 1) + L"/" + _T(STR_PARAM_END_POSITION_X);
			wstring strParamEndY = wstring(_T(STR_PARAM_GROUP_BLACK_TUBE)) + L" " + to_wstring(i + 1) + L"/" + _T(STR_PARAM_END_POSITION_Y);
			wstring strParamThreshold = wstring(_T(STR_PARAM_GROUP_BLACK_TUBE)) + L" " + to_wstring(i + 1) + L"/" + _T(STR_PARAM_THRESHOLD);

			SET_PARAM_RECT_VALUE(m_rcBoundingBox_BlackTube[i], strParamStartX, strParamStartY, strParamEndX, strParamEndY);
			SET_PARAM_VALUE(m_nThreshold_BlackTube[i], strParamThreshold);
		}

		return pParam;
	}
	else if (CHECK_PROCESS_NAME(_T(STR_PROCESS_LDC_GUIDE))) {
		NIPLParam_LDC_Guide *pParam = new NIPLParam_LDC_Guide();

		for (int i = 0; i < LDC_GUIDE_MAX_COUNT; i++) {
			wstring strParamStartX = wstring(_T(STR_PARAM_GROUP_GUIDE)) + L" " + to_wstring(i + 1) + L"/" + _T(STR_PARAM_START_POSITION_X);
			wstring strParamStartY = wstring(_T(STR_PARAM_GROUP_GUIDE)) + L" " + to_wstring(i + 1) + L"/" + _T(STR_PARAM_START_POSITION_Y);
			wstring strParamEndX = wstring(_T(STR_PARAM_GROUP_GUIDE)) + L" " + to_wstring(i + 1) + L"/" + _T(STR_PARAM_END_POSITION_X);
			wstring strParamEndY = wstring(_T(STR_PARAM_GROUP_GUIDE)) + L" " + to_wstring(i + 1) + L"/" + _T(STR_PARAM_END_POSITION_Y);
			wstring strParamThreshold = wstring(_T(STR_PARAM_GROUP_GUIDE)) + L" " + to_wstring(i + 1) + L"/" + _T(STR_PARAM_THRESHOLD);

			SET_PARAM_RECT_VALUE(m_rcBoundingBox[i], strParamStartX, strParamStartY, strParamEndX, strParamEndY);
			SET_PARAM_VALUE(m_nThreshold[i], strParamThreshold);
		}

		return pParam;
	}

	return nullptr;
}

NIPL_ERR NIPO::DoNIPLProcess(wstring strProcessName, NIPLInput *pInput, NIPLOutput *pOutput)
{
	NIPL *pNIPL = NIPL::GetInstance();
	NIPLCV *pNIPLCV = NIPLCV::GetInstance();
	NIPLCustom *pNIPLCustom = NIPLCustom::GetInstance();

	//
	// NIPL CV
	//
	if (CHECK_STRING(strProcessName, _T(STR_PROCESS_CREATE_IMAGE))) {
		return pNIPL->CreateImage(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_COLOR_TO_GRAY))) {
		return pNIPL->Color2Gray(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_INVERT))) {
		return pNIPL->Invert(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_OPERATE))) {
		return pNIPL->Operate(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_COPY_SUBIMAGE_FROM))) {
		return pNIPL->CopySubImageFrom(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_COPY_SUBIMAGE_TO))) {
		return pNIPL->CopySubImageTo(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_THRESHOLDING))) {
		return pNIPL->Thresholding(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_COLOR_THRESHOLDING))) {
		return pNIPL->ColorThresholding(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_SMOOTHIG))) {
		return pNIPL->Smoothing(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_EDGE_DETECTING))) {
		return pNIPL->EdgeDetecting(pInput, pOutput);
	}

	//
	// NIPL CV
	//
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_BINARIZE))) {
		return pNIPLCV->Binarize(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_FIT_BACKGROUND))) {
		return pNIPLCV->FitBackground(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_ELIMINATE_NOISE))) {
		return pNIPLCV->EliminateNoise(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_ANALYIZE_SHAPE))) {
		return pNIPLCV->AnalyzeShape(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_FIND_BLOB))) {
		return pNIPLCV->FindBlob(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_FIND_LINE))) {
		return pNIPLCV->FindLine(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_FIND_CIRCLE))) {
		return pNIPLCV->FindCircle(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_SET_CIRCLE_ROI))) {
		return pNIPLCV->SetCircleROI(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_CIRCLE_TO_RECT))) {
		return pNIPLCV->Circle2Rect(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_RECT_TO_CIRCLE))) {
		return pNIPLCV->Rect2Circle(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_FIT_TO_CORRECT_CIRCLE))) {
		return pNIPLCV->FitToCorrectCircle(pInput, pOutput);
	}

	// Custom
	if (CHECK_STRING(strProcessName, _T(STR_PROCESS_LDC_TERMINAL))) {
		return pNIPLCustom->LDC_Terminal(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_LDC_TUBE))) {
		return pNIPLCustom->LDC_Tube(pInput, pOutput);
	}
	else if (CHECK_STRING(strProcessName, _T(STR_PROCESS_LDC_GUIDE))) {
		return pNIPLCustom->LDC_Guide(pInput, pOutput);
	}

	return NIPL_ERR_FAIL_NOT_SUPPORT_PROCESS;
}

void NIPO::SetInput(const NIPLInput &dInput)
{
	m_dInput = dInput;
}

void NIPO::GetInput(NIPLInput &dInput)
{
	dInput = m_dInput;
}

bool NIPO::GetInputImage(wstring strName, Mat &dImg)
{
	// first check input image name
	if (CHECK_STRING(strName, _T(STR_NAME_INPUT))) {
		if (CHECK_EMPTY_IMAGE(m_dInput.m_dImg)) {
			SendNotify(strName, NIPL_ERR_FAIL_INVALID_IMAGE, NNL_PROCESS);
			return false;
		}

		dImg = m_dInput.m_dImg;
		return true;
	}

	// check also output image list
	Mat dOutputImage;
	if (GetOutputImage(strName, dOutputImage)) {
		dImg = dOutputImage;
		return true;
	}
	
	SendNotify(strName, NIPL_ERR_FAIL_INVALID_IMAGE, NNL_PROCESS);
	return false;
}

bool NIPO::LoadImage(wstring strPath, Mat &dImg)
{
	NIPL *pNIPL = NIPL::GetInstance();
	NIPL_ERR nErr = pNIPL->LoadImage(strPath, CV_LOAD_IMAGE_ANYCOLOR, dImg);
	if (!NIPL_SUCCESS(nErr)) {
		SendNotify(strPath, nErr, NNL_IO);
		return false;
	}

	return true;
}

bool NIPO::SaveImage(wstring strPath, Mat dImg)
{
	NIPL *pNIPL = NIPL::GetInstance();
	NIPL_ERR nErr = pNIPL->SaveImage(strPath, dImg);
	if (!NIPL_SUCCESS(nErr)) {
		SendNotify(strPath, nErr, NNL_IO);
		return false;
	}

	return true;
}

bool NIPO::LoadInputImage(wstring strImagePath, wstring strMaskPath, wstring strTemplatePath)
{
	bool bLoad = false;
	if (strImagePath.size() > 0) {
		bLoad = LoadImage(strImagePath, m_dInput.m_dImg);
		if (!bLoad) {
			return false;
		}
	}

	if (strMaskPath.size() > 0) {
		bLoad = LoadImage(strMaskPath, m_dInput.m_dMask);
		if (!bLoad) {
			return false;
		}

		bLoad = BinarizeMask(m_dInput.m_dMask);
		if (!bLoad) {
			return false;
		}
	}

	if (strTemplatePath.size() > 0) {
		bLoad = LoadImage(strTemplatePath, m_dInput.m_dTemplateImg);
		if (!bLoad) {
			return false;
		}
	}

	if (m_dInput.m_dImg.size != m_dInput.m_dMask.size) {
		// Send warning
		SendNotify(L"Mask", NIPL_ERR_FAIL_NOT_MATCH_MASK_SIZE, NNL_PROCESS);
	}
	VERIFY_MASK(m_dInput.m_dImg, m_dInput.m_dMask);
	
	return bLoad;
}

bool NIPO::ConvertInputImageToGray(NIPJobProcess *pProcess)
{
	if (pProcess == nullptr) {
		return false;
	}

	NIPLParam_Color2Gray *pParam = (NIPLParam_Color2Gray *)SetNIPLParam(pProcess->FindSubProcess(_T(STR_PROCESS_COLOR_TO_GRAY)));
	if (pParam == nullptr) {
		return nullptr;
	}

	NIPL *pNIPL = NIPL::GetInstance();

	NIPLInput dInput;
	NIPLOutput dOutput;

	dInput.m_pParam = pParam;
	if (!CHECK_EMPTY_IMAGE(m_dInput.m_dImg)) {
		dInput.m_dImg = m_dInput.m_dImg;

		NIPL_ERR nErr = pNIPL->Color2Gray(&dInput, &dOutput);
		if (NIPL_FAIL(nErr)) {
			SendNotify(L"Input", NIPL_ERR_FAIL_TO_CONVERT_IMAGE, NNL_PROCESS);

			delete pParam;
			return false;
		}

		m_dInput.m_dImg = dOutput.m_dImg;
	}

	if (!CHECK_EMPTY_IMAGE(m_dInput.m_dMask)) {
		dInput.m_dImg = m_dInput.m_dMask;

		NIPL_ERR nErr = pNIPL->Color2Gray(&dInput, &dOutput);
		if (NIPL_FAIL(nErr)) {
			SendNotify(L"Mask", NIPL_ERR_FAIL_TO_CONVERT_IMAGE, NNL_PROCESS);

			delete pParam;
			return false;
		}

		m_dInput.m_dMask = dOutput.m_dImg;
	}

	if (!CHECK_EMPTY_IMAGE(m_dInput.m_dTemplateImg)) {
		dInput.m_dImg = m_dInput.m_dTemplateImg;

		NIPL_ERR nErr = pNIPL->Color2Gray(&dInput, &dOutput);
		if (NIPL_FAIL(nErr)) {
			SendNotify(L"Template", NIPL_ERR_FAIL_TO_CONVERT_IMAGE, NNL_PROCESS);

			delete pParam;
			return false;
		}

		m_dInput.m_dTemplateImg = dOutput.m_dImg;
	}

	delete pParam;
	return true;
}

bool NIPO::BinarizeMask(Mat &dMask)
{
	NIPL *pNIPL = NIPL::GetInstance();

	// convert to gray scale
	NIPLInput dInput;
	NIPLOutput dOutput;

	NIPLParam_Color2Gray dParam_Color2Gray;
	dParam_Color2Gray.m_nGrayLevel = NIPLParam_Color2Gray::GRAYLEVEL_256;

	dInput.m_dImg = dMask;
	dInput.m_pParam = &dParam_Color2Gray;
	NIPL_ERR nErr = pNIPL->Color2Gray(&dInput, &dOutput);
	if (NIPL_FAIL(nErr)) {
		SendNotify(L"Mask", NIPL_ERR_FAIL_TO_CONVERT_IMAGE, NNL_PROCESS);
		return false;
	}

	NIPLParam_Thresholding dParam_Thresholding;
	dParam_Thresholding.m_nMethod = NIPLParam_Thresholding::METHOD_UPPER_OTSU;

	dInput.m_dImg = dOutput.m_dImg;
	dInput.m_pParam = &dParam_Thresholding;
	nErr = pNIPL->Thresholding(&dInput, &dOutput);
	if (NIPL_FAIL(nErr)) {
		SendNotify(L"Mask", NIPL_ERR_FAIL_TO_CONVERT_IMAGE, NNL_PROCESS);
		return false;
	}

	dMask = dOutput.m_dImg;

	return true;
}

bool NIPO::LoadOutputImage(wstring strPath, wstring strName)
{
	Mat dImg;
	if (!LoadImage(strPath, dImg)) {
		SendNotify(strPath, NIPL_ERR_FAIL_TO_LOAD_IMAGE, NNL_IO);
		return false;
	}

	NIPLOutput dOutput;
	dOutput.m_dImg = dImg;
	return SetOutput(strName, dOutput);
}

bool NIPO::SaveOutputImage(wstring strPath, wstring strName)
{
	NIPLOutput dOutput;
	if (!GetOutput(strName, dOutput)) {
		SendNotify(strName, NIPL_ERR_FAIL_INVALID_IMAGE, NNL_PROCESS);
		return false;
	}

	return SaveImage(strPath, dOutput.m_dImg);
}

bool NIPO::SaveOutputResult(wstring strPath, wstring strName)
{
	NIPLOutput dOutput;
	if (!GetOutput(strName, dOutput)) {
		SendNotify(strName, NIPL_ERR_FAIL_INVALID_IMAGE, NNL_PROCESS);
		return false;
	}

	if (dOutput.m_pResult == nullptr) {
		return true;
	}

	FILE *pFile = _wfsopen(strPath.c_str(), L"w", _SH_DENYNO);
	if (pFile == nullptr) {
		SendNotify(strPath, NIPL_ERR_FAIL_TO_SAVE_FILE, NNL_PROCESS);
		return false;
	}

	if (dOutput.IsResultType(NIPL_RESULT_FIND_BLOB)) {
		NIPLResult_FindBlob *pResult = (NIPLResult_FindBlob *)dOutput.m_pResult;

		int nIndex = 0;
		for (auto dBlob : pResult->m_listBlob) {
			if (nIndex == 0) {
				wstring strTitle = dBlob.GetTitleForSaving();
				fwprintf_s(pFile, L"Index\t%s\n", strTitle.c_str());
			}

			wstring strDesc = dBlob.GetDescForSaving();
			fwprintf_s(pFile, L"%d\t%s\n", nIndex, strDesc.c_str());
			nIndex++;
		}
	}
	else if (dOutput.IsResultType(NIPL_RESULT_FIND_LINE)) {
		NIPLResult_FindLine *pResult = (NIPLResult_FindLine *)dOutput.m_pResult;

		int nIndex = 0;
		for (auto dLine : pResult->m_listLine) {
			if (nIndex == 0) {
				wstring strTitle = dLine.GetTitleForSaving();
				fwprintf_s(pFile, L"Index\t%s\n", strTitle.c_str());
			}

			wstring strDesc = dLine.GetDesc();
			fwprintf_s(pFile, L"%d\t%s\n", nIndex, strDesc.c_str());
			nIndex++;
		}
	}
	else if (dOutput.IsResultType(NIPL_RESULT_FIND_CIRCLE)) {
		NIPLResult_FindCircle *pResult = (NIPLResult_FindCircle *)dOutput.m_pResult;

		int nIndex = 0;
		for (auto dCircle : pResult->m_listCircle) {
			if (nIndex == 0) {
				wstring strTitle = dCircle.GetTitleForSaving();
				fwprintf_s(pFile, L"Index\t%s\n", strTitle.c_str());
			}

			wstring strDesc = dCircle.GetDesc();
			fwprintf_s(pFile, L"%d\t%s\n", nIndex, strDesc.c_str());
			nIndex++;
		}
	}
	else if (dOutput.IsResultType(NIPL_RESULT_DEFECT)) {
		NIPLResult_Defect *pResult = (NIPLResult_Defect *)dOutput.m_pResult;

		int nIndex = 0;
		for (auto dDefect : pResult->m_listDefect) {
			if (nIndex == 0) {
				wstring strTitle = dDefect.GetTitleForSaving();
				fwprintf_s(pFile, L"Index\t%s\n", strTitle.c_str());
			}

			wstring strDesc = dDefect.GetDescForSaving();
			fwprintf_s(pFile, L"%d\t%s\n", nIndex, strDesc.c_str());
			nIndex++;
		}
	}

	fclose(pFile);

	return true;
}


bool NIPO::SetOutput(wstring strName, const NIPLOutput &dOutput)
{
	if (CHECK_STRING(strName, _T(""))) {
		SendNotify(strName, NIPL_ERR_FAIL_INVALID_IMAGE, NNL_PROCESS);
		return false;
	}

	NIPLOutput dOutputFound;
	if (GetOutput(strName, dOutputFound)) {
		dOutputFound.Clear(true);
	}

	m_mapOutput[strName] = dOutput;

	return true;
}

bool NIPO::GetOutput(wstring strName, NIPLOutput &dOutput)
{
	auto dPos = m_mapOutput.find(strName);
	if (dPos != m_mapOutput.end()) {
		dOutput = dPos->second;
		return true;
	}

	return false;
}

bool NIPO::RemoveOutput(wstring strName)
{
	auto dPos = m_mapOutput.find(strName);
	if (dPos != m_mapOutput.end()) {
		NIPLOutput dOutput = dPos->second;
		dOutput.Clear(true);

		m_mapOutput.erase(dPos);
		return true;
	}

	return false;
}

bool NIPO::GetOutputImage(wstring strName, Mat &dImg)
{
	NIPLOutput dOutput;
	if (GetOutput(strName, dOutput)) {
		dImg = dOutput.m_dImg;
		return true;
	}

	return false;
}

bool NIPO::GetParamValue(wstring strValue, bool &bValue)
{
	bValue = false;
	if (CHECK_STRING(strValue, _T("true"))) {
		bValue = true;
	}

	return true;
}

bool NIPO::GetParamValue(wstring strValue, int &nValue)
{
	float nValueFloat = 0.f;;
	if (!GetParamValue(strValue, nValueFloat)) {
		return false;
	}

	nValue = (int)nValueFloat;

	return true;
}

bool NIPO::GetParamValue(wstring strValue, float &nValue)
{
	nValue = 0.f;

	// trim spaces
	strtrim(strValue);

	if (strValue.empty()) {
		return true;
	}

	if (is_number(strValue)) {
		nValue = stof(strValue);
		return true;
	}

	size_t nStartOffset = 0;
	size_t nEndOffset = nStartOffset;

	// Output Name
	nEndOffset = strValue.find('.', nStartOffset);
	if (nEndOffset == wstring::npos) {
		SendNotify(strValue, NIPL_ERR_FAIL_INVALID_PARAM_EXPRESSION, NNL_PROCESS);
		return false;
	}
	wstring strOutputName = strValue.substr(nStartOffset, nEndOffset - nStartOffset);
	nStartOffset = nEndOffset + 1;

	NIPLOutput dOutput;
	if (!GetOutput(strOutputName, dOutput)) {
		SendNotify(strOutputName, NIPL_ERR_FAIL_INVALID_IMAGE, NNL_PROCESS);
		return false;
	}

	// Data Name (e.g. Result)
	nEndOffset = strValue.find('.', nStartOffset);
	if (nEndOffset == wstring::npos) {
		SendNotify(strValue, NIPL_ERR_FAIL_INVALID_PARAM_EXPRESSION, NNL_PROCESS);
		return false;
	}
	wstring strDataName = strValue.substr(nStartOffset, nEndOffset - nStartOffset);
	nStartOffset = nEndOffset + 1;

	if (!CHECK_STRING(strDataName, _T(STR_DATA_RESULT))) {
		SendNotify(strDataName, NIPL_ERR_FAIL_INVALID_DATA, NNL_PROCESS);
		return false;
	}

	// Index
	nEndOffset = strValue.find('.', nStartOffset);
	if (nEndOffset == wstring::npos) {
		SendNotify(strValue, NIPL_ERR_FAIL_INVALID_PARAM_EXPRESSION, NNL_PROCESS);
		return false;
	}
	wstring strIndex = strValue.substr(nStartOffset, nEndOffset - nStartOffset);
	nStartOffset = nEndOffset + 1;

	if (!is_number(strIndex)) {
		SendNotify(strIndex, NIPL_ERR_FAIL_INVALID_DATA, NNL_PROCESS);
		return false;
	}
	int nIndex = stoi(strIndex);
	if (nIndex < 0) {
		SendNotify(strIndex, NIPL_ERR_FAIL_INVALID_DATA, NNL_PROCESS);
		return false;
	}

	// Field Name
	nEndOffset = strValue.find('.', nStartOffset);
	if (nEndOffset == wstring::npos) {
		SendNotify(strValue, NIPL_ERR_FAIL_INVALID_PARAM_EXPRESSION, NNL_PROCESS);
		return false;
	}
	wstring strFieldName = strValue.substr(nStartOffset, nEndOffset - nStartOffset);
	nStartOffset = nEndOffset + 1;

	if (dOutput.IsResultType(NIPL_RESULT_FIND_BLOB)) {
		NIPLResult_FindBlob *pResult = (NIPLResult_FindBlob *)dOutput.m_pResult;
		size_t nCount = pResult->m_listBlob.size();
		if (nIndex >= nCount) {
			SendNotify(strIndex, NIPL_ERR_FAIL_INVALID_DATA, NNL_PROCESS);
			return false;
		}
		
		auto &dBlob = pResult->m_listBlob[nIndex];
		if (!dBlob.GetValue(strFieldName, nValue)) {
			SendNotify(strFieldName, NIPL_ERR_FAIL_INVALID_DATA, NNL_PROCESS);
			return false;
		}
	}
	else if (dOutput.IsResultType(NIPL_RESULT_FIND_LINE)) {
		NIPLResult_FindLine *pResult = (NIPLResult_FindLine *)dOutput.m_pResult;
		size_t nCount = pResult->m_listLine.size();
		if (nIndex >= nCount) {
			SendNotify(strIndex, NIPL_ERR_FAIL_INVALID_DATA, NNL_PROCESS);
			return false;
		}

		auto dLine = pResult->m_listLine[nIndex];
		if (!dLine.GetValue(strFieldName, nValue)) {
			SendNotify(strFieldName, NIPL_ERR_FAIL_INVALID_DATA, NNL_PROCESS);
			return false;
		}
	}
	else if (dOutput.IsResultType(NIPL_RESULT_FIND_CIRCLE)) {
		NIPLResult_FindCircle *pResult = (NIPLResult_FindCircle *)dOutput.m_pResult;
		size_t nCount = pResult->m_listCircle.size();
		if (nIndex >= nCount) {
			SendNotify(strIndex, NIPL_ERR_FAIL_INVALID_DATA, NNL_PROCESS);
			return false;
		}

		auto dCircle = pResult->m_listCircle[nIndex];
		if (!dCircle.GetValue(strFieldName, nValue)) {
			SendNotify(strFieldName, NIPL_ERR_FAIL_INVALID_DATA, NNL_PROCESS);
			return false;
		}
	}
	else if (dOutput.IsResultType(NIPL_RESULT_DEFECT)) {
		NIPLResult_Defect *pResult = (NIPLResult_Defect *)dOutput.m_pResult;
		size_t nCount = pResult->m_listDefect.size();
		if (nIndex >= nCount) {
			SendNotify(strIndex, NIPL_ERR_FAIL_INVALID_DATA, NNL_PROCESS);
			return false;
		}

		auto &dDefect = pResult->m_listDefect[nIndex];
		if (!dDefect.GetValue(strFieldName, nValue)) {
			SendNotify(strFieldName, NIPL_ERR_FAIL_INVALID_DATA, NNL_PROCESS);
			return false;
		}
	}

	// Calculate Rest Part
	size_t nNumberStartOffset = wstring::npos;
	size_t nNumberLength = 0;
	wchar_t cOperator = 0;
	wchar_t cNextOperator = 0;

	wstring strNumber = L"";
	bool bCalculate = false;

	size_t nOffset = nStartOffset;
	nEndOffset = strValue.size();
	while (nOffset < nEndOffset) {
		wchar_t c = strValue[nOffset];
		if (c == ' ' || nOffset == (nEndOffset - 1)) {
			if (nNumberStartOffset != wstring::npos) {
				if (cOperator == 0) {
					SendNotify(strFieldName, NIPL_ERR_FAIL_INVALID_DATA, NNL_PROCESS);
					return 0.f;
				}

				if (c == ' ') nNumberLength = nOffset - nNumberStartOffset;
				else nNumberLength = nOffset - nNumberStartOffset + 1;

				bCalculate = true;
			}
		}
		else if (c == '+' || c == '-' || c == '*' || c == '/') {
			if (cOperator != 0) {
				if (nNumberStartOffset == wstring::npos) {
					SendNotify(strFieldName, NIPL_ERR_FAIL_INVALID_DATA, NNL_PROCESS);
					return 0.f;
				}

				nNumberLength = nEndOffset - nNumberStartOffset + 1;
				cNextOperator = c;
				bCalculate = true;
			}
			else {
				cOperator = c;
			}
		}
		else if (!is_number(c)) {
			SendNotify(strFieldName, NIPL_ERR_FAIL_INVALID_DATA, NNL_PROCESS);
			return 0.f;
		}
		else if (nNumberStartOffset == wstring::npos) {
			nNumberStartOffset = nOffset;
		}

		if (bCalculate) {
			strNumber = strValue.substr(nNumberStartOffset, nNumberLength);
			float nNumber = stof(strNumber);

			if (cOperator == '+') nValue += nNumber;
			else if (cOperator == '-') nValue -= nNumber;
			else if (cOperator == '*') nValue *= nNumber;
			else if (cOperator == '/') nValue /= nNumber;

			nNumberStartOffset = wstring::npos;
			nNumberLength = 0;
			if (cNextOperator != 0) {
				cOperator = cNextOperator;
				cNextOperator = 0;
			}
			else {
				cOperator = 0;
			}

			bCalculate = false;
		}

		nOffset++;
	}

	return true;
}
