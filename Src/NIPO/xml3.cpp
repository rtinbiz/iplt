﻿#include "stdafx.h"
#include "xml3.h"


namespace XML3
	{
	// Debugging class for profiling
	class TICKCOUNT
		{
		public:
			unsigned long long var;
			unsigned long long a;
			unsigned long long freq = 0;
			string fn;
			TICKCOUNT(const char* ffn)
				{
				var = 0;
				a = 0;
				fn = ffn;
				QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
				QueryPerformanceCounter((LARGE_INTEGER*)&a);
				}
			~TICKCOUNT()
				{
				r();
				}

			void r()
				{
				unsigned long long b = 0;
				QueryPerformanceCounter((LARGE_INTEGER*)&b);
				var = (b - a);
				var *= 1000;
				var /= freq;
				char f[1000];
				sprintf_s(f,1000,"%03llu %s\r\n",var,fn.c_str());
				OutputDebugStringA(f);
				}
		};



	string Char2Base64(const char *Z,size_t s)
		{
		size_t L = s;
		char *oo;
		Base64Encoder<const char*,char*>e;
		size_t output_size = ((s * 4) / 3) + (s / 96) + 6 + 1000;
		unique_ptr<char> out(new char[output_size]);
		oo = e.Filter(out.get(),Z,Z + L);
		oo = e.Finish(oo);
		*oo = 0;                     // Put a zero to the end
		return out.get();
		}



	BXML::BXML(size_t s)
		{
		d = 0;
		if (s)
			{
			d = new char[s];
			memset(d,0,s);
			ss = s;
			}
		}

	BXML::BXML()
		{
		d = 0;
		ss = 0;
		}

	BXML::~BXML()
		{
		if (d)
			delete[] d;
		d = 0;
		}

	BXML::BXML(const BXML& d2)
		{
		d = 0;
		ss = 0;
		operator =(d2);
		}

	BXML::BXML(BXML&& d2)
		{
		d = 0;
		ss = 0;
		operator =(std::forward<BXML>(d2));
		}

	void BXML::operator =(const BXML& d2)
		{
		if (d)
			delete[] d;
		d = 0;
		ss = 0;
		size_t ns = d2.size();
		if (ns)
			{
			d = new char[ns];
			memcpy(d,d2.operator char*(),ns);
			ss = ns;
			}
		}
		
		void BXML::operator =(BXML&& d2)
		{
			if (d)
				delete[] d;
			d = 0;
			d = d2.d;
			ss = d2.ss;
			d2.d = 0;
			d2.ss = 0;
		}
		
	bool BXML::operator ==(const BXML& b2)
		{
		if (size() != b2.size())
			return false;
		if (memcmp(p(),b2.p(),size()) != 0)
			return false;
		return true;
		}

	void BXML::ToB(string& s)
		{
		s = Char2Base64(d,ss);
		}

	void BXML::FromB(const char* ba)
		{
		Base64ToChar(ba,strlen(ba),*this);
		}

	BXML::operator char*() const
		{
		return d;
		}


	char* BXML::p() const
		{
		return d;
		}

	size_t BXML::size() const
		{
		return ss;
		}

	void BXML::clear()
		{
		memset(d,0,ss);
		}

	void BXML::reset()
		{
		if (d)
			delete[] d;
		d = 0;
		ss = 0;
		}

	void BXML::Ensure(size_t news)
		{
		if (news < ss)
			return; // capacity is ok
		Resize(news);
		}
	void BXML::Resize(size_t news)
		{
		if (news == ss)
			return; // same size

		// Create buffer to store existing BXML
		char* newd = new char[news];
		size_t newbs = news;
		memset((void*)newd,0,newbs);

		if (ss < news)
			memcpy((void*)newd,d,ss);
		else
			// we created a smaller BXML structure
			memcpy((void*)newd,d,news);
		delete[] d;
		d = newd;
		ss = news;
		}

	void BXML::AddResize(size_t More)
		{
		Resize(ss + More);
		}



	void Base64ToChar(const char *Z,size_t s,BXML& out)
		{
		if (s == -1)
			s = (size_t)strlen(Z);
		size_t L = s;
		char *oo;
		Base64Decoder<const char*,char*>e;
		oo = e.Filter(out.p(),Z,Z + L);
		oo = e.Finish(oo);
		*oo = 0;
		out.Resize(oo - out);
		}

	string Decode(const char* src)
		{
		string trg;
		if (!src)
			return trg;
		if (strchr(src,'&') == 0)
			return src;
		//vector<string> t = {"&amp;","&","&apos;","'","&quot;","\"","&gt;",">","&lt;","<"};
		vector<string> t = {"&amp;","&","&quot;","\"","&gt;",">","&lt;","<"};
		std::string s = src;
		for (size_t i = 0; i < t.size() / 2; i++)
			{
			std::regex re(t[i * 2].c_str());
			s = std::regex_replace(s,re,t[i * 2 + 1]);
			}
		return s;
		}

	string Format(const char* f,...)
		{
		va_list args;
		va_start(args,f);

		int len = _vscprintf(f,args) // _vscprintf doesn't count
			+ 100; // terminating '\0'
		if (len < 8192)
			len = 8192;

		std::unique_ptr<char> b(new char[len]);
		vsprintf_s(b.get(),len,f,args);
		string a = b.get();
		va_end(args);
		return a;
		}

	string Trim(const char* src,int m)
		{
		string s = src;
		if (m == 0 || m == 2)
			s.erase(s.begin(),std::find_if(s.begin(),s.end(),std::not1(std::ptr_fun<int,int>(std::isspace))));
		if (m == 1 || m == 2)
			s.erase(std::find_if(s.rbegin(),s.rend(),std::not1(std::ptr_fun<int,int>(std::isspace))).base(),s.end());
		return s;
		}

	string Encode(const char* src)
		{
		string trg;
		if (!src)
			return trg;
		size_t Y = strlen(src);
		for (size_t i = 0; i < Y; i++)
			{
			if (src[i] == '&' && src[i + 1] != '#')
				{
				trg += "&amp;";
				continue;
				}
			if (src[i] == '>')
				{
				trg += "&gt;";
				continue;
				}
			if (src[i] == '<')
				{
				trg += "&lt;";
				continue;
				}
			if (src[i] == '\"')
				{
				trg += "&quot;";
				continue;
				}
			/*		if (src[i] == '\'')
						{
						trg += "&apos;";
						continue;
						}
						*/		trg += src[i];
			}
		return trg;
		}


	void strreplace(std::string& str,const std::string& from,const std::string& to)
		{
		if (from.empty())
			return;
		size_t start_pos = 0;
		while ((start_pos = str.find(from,start_pos)) != std::string::npos)
			{
			str.replace(start_pos,from.length(),to);
			start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
			}
		}

	void JsonParser(XMLElement* root,const char* txt)
		{
		XMLElement* sroot = root;
		// replace \" with &quot;
		std::string f = txt;
		strreplace(f,"\\\"","___quot___");
		txt = f.c_str();
		size_t ts = strlen(txt);

		string LastData;

		//	XMLElement* FirstRoot = root;

		int KeyTypeParse = 0;
		// 0 Begin
		// 1 End
		// 2 : found, begin new
		// 3 Variable Name
		// 4 Variable Value

		XMLVariable cv;
		bool NextBraceArray = false;

		for (size_t j = 0; j < ts; j++)
			{
			char c = txt[j];
			if (c == ' ' || c == '\r' || c == '\n' || c == '\t')
				continue;

			if (c == '{')
				{
				// Begin XMLElement
				/*
					XMLElement* nr = (XMLElement*)XMLJsonCB(root,0,LastData,0);
					if (nr)
					{
					root->AddElement(nr);
					root = nr;
					}
					*/

				if (LastData.empty())
					LastData = "json";


				root = &root->AddElement(LastData.c_str());


				continue;
				}
			if (c == '}')
				{
				// End XMLElement
				XMLElement* nr = root->GetParent(sroot);
				if (!nr) // End JSon parsing
					break;
				unsigned long long lp = root->GetElementParam();
				if (lp != 1)
					{
					root = nr;
					}
				else
					{
					// Same, we must add another one with same name
					LastData = root->GetElementName();
					root = nr;
					}
				continue;
				}
			if (c == ']')
				{
				// End array
				XMLElement* nr = root->GetParent(sroot);
				if (!nr) // End JSon parsing
					break;
				root = nr;
				continue;
				}
			if (c == '\"')
				{
				if (KeyTypeParse == 0)
					{
					// Start key
					LastData.clear();
					KeyTypeParse = 1;
					continue;
					}
				if (KeyTypeParse == 1)
					{
					// End Key
					KeyTypeParse = 2;
					continue;
					}
				}
			if (c == ':' && KeyTypeParse == 2)
				{
				// It's something new

				//			int BreakType = 0;
				bool InQ = false;
				bool FinishedQ = false;
				for (size_t jj = j + 1; jj < ts; jj++)
					{
					char cc = txt[jj];
					if (cc == ' ' || cc == '\r' || cc == '\n' || cc == '\t')
						continue;

					if (cc == '\"')
						{
						InQ = !InQ;
						if (InQ == false)
							FinishedQ = true;
						continue;
						}
					if (InQ)
						continue;

					if (cc == '[')
						{
						// Array
						NextBraceArray = true;
						continue;
						}

					if (cc == '{')
						{
						// Element
						XMLElement& bb = root->AddElement(LastData.c_str());
						if (NextBraceArray)
							bb.SetElementParam(1);
						NextBraceArray = false;
						root = &bb;
						KeyTypeParse = 0;
						j = jj;
						break;
						}

					if (cc == ',' || FinishedQ)
						{
						// Variable
						cv.SetName(LastData.c_str());


						// From j + 1 to jj, value
						string vval;
						for (size_t k = j + 1; k < jj; k++)
							{
							char ccc = txt[k];
							if (ccc == ' ' || ccc == '\r' || ccc == '\n' || ccc == '\t')
								continue;
							if (ccc == '\"')
								continue;
							vval.append(txt + k,1);

							}
						cv.SetValue(vval);

						KeyTypeParse = 0;
						j = jj;
						if (cc != ',')
							j = jj - 1;
						FinishedQ = false;

						root->AddVariable(cv);
						break;
						}
					}
				continue;
				}
			if (KeyTypeParse == 1)
				{
				// In key
				LastData.append(txt + j,1);

				continue;
				}
			}
		}



	XMLU::XMLU(const char* x)
		{
		tu = x;
		size_t si = strlen(x) * 4 + 1000;
		unique_ptr<wchar_t> ws(new wchar_t[si]);
#ifdef _WIN32
		MultiByteToWideChar(CP_UTF8,0,tu.c_str(),-1,ws.get(),(int)si);
#endif
		wu = ws.get();
		}

	XMLU::XMLU(const wchar_t* x)
		{
		wu = x;
		size_t si = wcslen(x) * 4 + 1000;
		unique_ptr<char> ws(new char[si]);
#ifdef _WIN32
		WideCharToMultiByte(CP_UTF8,0,wu.c_str(),-1,ws.get(),(int)si,0,0);
#endif
		tu = ws.get();
		}

	const wchar_t* XMLU::wc()
		{
		return wu.c_str();
		}
	const char* XMLU::bc()
		{
		return tu.c_str();
		}
	XMLU::operator const wchar_t*()
		{
		return wu.c_str();
		}
	XMLU::operator const char*()
		{
		return tu.c_str();
		}



	XMLId::XMLId()
		{
		ts = __rdtsc();
		}
	XMLId::XMLId(unsigned long long a)
		{
		operator =(a);
		}
	XMLId::XMLId(const XMLId& a)
		{
		operator =(a);
		}
	void XMLId::operator =(unsigned long long a)
		{
		ts = a;
		}

	bool XMLId::operator ==(const XMLId& a)
		{
		if (ts == a.ts)
			return true;
		return false;
		}
	void XMLId::operator =(const XMLId& a)
		{
		ts = a.g();
		}
	unsigned long long XMLId::g() const { return ts; }





	XMLContent::XMLContent()
		{
		ep = (size_t)-1;
		v.clear();
		}
	XMLContent::XMLContent(size_t ElementPosition,const char* ht)
		{
		ep = ElementPosition;
		v.clear();
		if (ht)
			v = ht;
		}
	XMLContent::XMLContent(size_t ElementPosition,const wchar_t* ht)
		{
		ep = ElementPosition;
		v.clear();
		if (ht)
			v = XMLU(ht);
		}

	size_t XMLContent::MemoryUsage()
		{
		size_t m = 0;
		// Our size
		m += sizeof(*this);
		m += v.length();
		m += sizeof(v);
		return m;
		}

	// Operators
	bool XMLContent::operator==(const XMLContent& t) const
		{
		if (v != t.v)
			return false;
		return true;
		}

	XMLContent::operator const string& () const { return v; }

	// Sets
	const string& XMLContent::SetFormattedValue(const char* fmt,...)
		{
		va_list args;
		va_start(args,fmt);
		unique_ptr<char> data(new char[10000]);
		vsprintf_s(data.get(),10000,fmt,args);
		SetValue(data.get());
		va_end(args);
		return v;
		}


	XMLContent&  XMLContent::operator =(const char* s)
		{
		if (s)
			v = s;
		else
			v.clear();
		return *this;
		}

	XMLContent&  XMLContent::operator =(const string& s)
		{
		v = s;
		return *this;
		}

	const string& XMLContent::SetBinaryValue(const char* data,size_t len)
		{
		v = Char2Base64(data,len);
		return v;
		}

	void XMLContent::SetValue(const char* VV)
		{
		if (!VV)
			{
			v.clear();
			return;
			}


		v = VV;
		}

	void XMLContent::SetValue(const string& VV)
		{
		v = VV;
		}
	void XMLContent::Clear()
		{
		v.clear();
		}

	void XMLContent::SetEP(size_t epp)
		{
		ep = epp;
		}


	// Gets
	BXML XMLContent::GetBinaryValue() const
		{
		size_t s = v.length();
		size_t output_size = ((s * 4) / 3) + (s / 96) + 6 + 1000;
		BXML b;
		b.Resize(output_size);
		Base64ToChar(v.c_str(),s,b);
		return b;
		}


	const string& XMLContent::GetValue() const
		{
		return v;
		}

	size_t XMLContent::GetEP() const
		{
		return ep;
		}

	// Serialization
	string XMLContent::Serialize(bool NoEnc) const
		{
		if (NoEnc)
			return Format("%s",v.c_str());
		return Format("%s",Encode(v.c_str()).c_str());
		}






	XMLVariable::XMLVariable() : XMLContent((size_t)-1,"")
		{
		n = "v";
		}

	XMLVariable::XMLVariable(const char* nn,const char* vv,bool Temp) : XMLContent((size_t)-1,vv)
		{
		if (nn)	n = nn;
		tmp = Temp;
		}
		
/*
	XMLVariable::XMLVariable(const XMLVariable& h) : XMLContent((size_t)-1,(const char*)0)
		{
		operator =(h);
		}
	XMLVariable& XMLVariable :: operator =(const XMLVariable& h)
		{
		Clear();
		tmp = h.tmp;
		v = h.v;
		n = h.n;
		return *this;
		}
*/
/*	
	XMLVariable& XMLVariable :: operator =(const std::initializer_list<string>& s)
		{
		if (s.size() == 1)
			v = s.begin()->c_str();
		if (s.size() == 2)
			{
			n = s.begin()->c_str();
			v = (s.begin() + 1)->c_str();
			}
		return *this;
		}
		
*/
	const string& XMLVariable::SetName(const char* VN)
		{
		if (!VN)
			return n;
		n = VN;
		n.erase(n.find_last_not_of(" \n\r\t") + 1);
		return n;
		}

	const string& XMLVariable::SetName(const string& VN)
		{
		n = VN;
		n.erase(n.find_last_not_of(" \n\r\t") + 1);
		return n;
		}

	void XMLVariable::Clear()
		{
		n.clear();
		v.clear();
		}

	XMLVariable&  XMLVariable::operator =(const char* s)
		{
		if (s)
			v = s;
		else
			v.clear();
		return *this;
		}

	XMLVariable&  XMLVariable::operator =(const string& s)
		{
		v = s;
		return *this;
		}



	// Compare 
	bool XMLVariable::operator <(const XMLVariable& x) const
		{
		if (n > x.n)
			return false;
		return true;
		}

	bool XMLVariable::operator ==(const XMLVariable& x) const
		{
		if (strcmp(n.c_str(),x.n.c_str()) != 0)
			return false;
		if (strcmp(v.c_str(),x.v.c_str()) != 0)
			return false;
		return true;
		}

	bool XMLVariable::operator == (const char* x) const
		{
		if (strcmp(v.c_str(),x) != 0)
			return false;
		return true;
		}

	// Gets
	const string& XMLVariable::GetName() const
		{
		return n;
		}


	// Memory usage
	size_t XMLVariable::MemoryUsage() const
		{
		size_t m = 0;

		// Our size
		m += sizeof(*this);

		// Variable size
		m += n.length() + v.length();

		return m;
		}


	// Serialization
	string XMLVariable::Serialize(bool NoEnc) const
		{
		if (NoEnc)
			return Format("%s=\"%s\"",n.c_str(),v.c_str());
		return Format("%s=\"%s\"",Encode(n.c_str()).c_str(),Encode(v.c_str()).c_str());
		}






	XMLCData::XMLCData(size_t ElementPosition,const char* ht) : XMLContent(ElementPosition,ht)
		{
		}

	XMLCData::XMLCData(size_t ElementPosition,const wchar_t* ht) : XMLContent(ElementPosition,ht)
		{
		}

	// Serialization
	string XMLCData::Serialize(bool) const
		{
		return Format("<![CDATA[%s]]>",v.c_str());
		}


	XMLDocType::XMLDocType(const char* ht) : XMLContent(0,ht)
		{
		}

	XMLDocType::XMLDocType(const wchar_t* ht) : XMLContent(0,ht)
		{
		}

	// Serialization
	string XMLDocType::Serialize(bool) const
		{
		return Format("<!DOCTYPE %s>",v.c_str());
		}



	XMLComment::XMLComment(size_t ElementPosition,const char* ht) : XMLContent(ElementPosition,ht)
		{
		}

	XMLComment::XMLComment(size_t ElementPosition,const wchar_t* ht) : XMLContent(ElementPosition,ht)
		{
		}

	// Serialization
	string XMLComment::Serialize(bool) const
		{
		return Format("<!--%s-->",v.c_str());
		}





	// Serialization
	string XMLHeader::Serialize() const
		{
		if (standalone.GetValue() != "") {
			return Format("<?xml version=\"%s\" encoding=\"%s\" standalone=\"%s\" ?>", version.GetValue().c_str(), encoding.GetValue().c_str(), standalone.GetValue().c_str());
		}
		else{
			return Format("<?xml version=\"%s\" encoding=\"%s\" ?>", version.GetValue().c_str(), encoding.GetValue().c_str());
		}
		}

	void XMLHeader::Default()
		{
		version.SetName("version");
		encoding.SetName("encoding");
		standalone.SetName("standalone");

		version = "1.0";
		encoding = "UTF-8";
		standalone = "yes";

		}

	XMLHeader::XMLHeader()
		{
		Default();
		}


	XMLElement::XMLElement()
		{
		children.reserve(100);
		variables.reserve(20);
		}



	// Operators
	bool XMLElement::operator==(const XMLElement& t) const
		{
		if (param != t.param)
			return false;
		if (el != t.el)
			return false;

		if (children.size() != t.children.size())
			return false;
		if (variables.size() != t.variables.size())
			return false;
		if (contents.size() != t.contents.size())
			return false;
		if (cdatas.size() != t.cdatas.size())
			return false;
		if (comments.size() != t.comments.size())
			return false;

		for (size_t i = 0; i < children.size(); i++)
			{
			auto a = children[i];
			auto b = t.children[i];
			if (!(a == b))
				return false;
			}

		for (size_t i = 0; i < variables.size(); i++)
			{
			auto a = variables[i];
			auto b = t.variables[i];
			if (!(a == b))
				return false;
			}

		for (size_t i = 0; i < contents.size(); i++)
			{
			auto a = contents[i];
			auto b = t.contents[i];
			if (!(a == b))
				return false;
			}

		for (size_t i = 0; i < comments.size(); i++)
			{
			auto a = comments[i];
			auto b = t.comments[i];
			if (!(a == b))
				return false;
			}

		for (size_t i = 0; i < cdatas.size(); i++)
			{
			auto a = cdatas[i];
			auto b = t.cdatas[i];
			if (!(a == b))
				return false;
			}


		return true;
		}



	bool XMLElement::operator <(const XMLElement& x) const
		{
		// Compare names
		if (el > x.el)
			return false;
		return true;
		}



	XMLElement& XMLElement::operator =(const char* xx)
		{
		if (!xx)
			{
			clear();
			return *this;
			}
		if (xx[0] != '<')
			{
			SetElementName(xx);
			return *this;
			}

		XML x(xx,strlen(xx));
		CloneMirror(*this,x.GetRootElement());
		return*this;
		}

	XMLElement XMLElement::Mirror() const
		{
		XMLElement t;
		CloneMirror(t,*this);
		return t;
		}

	XMLElement::XMLElement(const char*t)
		{
		operator=(t);
		}

	void XMLElement::CloneMirror(XMLElement& to,const XMLElement& from)
		{
		to.children = from.children;
		to.variables = from.variables;
		to.comments = from.comments;
		to.contents = from.contents;
		to.cdatas = from.cdatas;
		to.el = from.el;
		to.param = from.param;
		to.parent = from.parent;
		to.id = from.id;
		}

	XMLElement::XMLElement(const XMLElement&from)
		{
		el = from.el;
		param = from.param;

		//cdatas
		cdatas.reserve(from.cdatas.size());
		for (auto a : from.cdatas)
			{
			shared_ptr<XMLCData> c = make_shared<XMLCData>(XMLCData(a->ep,a->v.c_str()));
			cdatas.push_back(c);
			}

		//comments
		comments.reserve(from.comments.size());
		for (auto a : from.comments)
			{
			shared_ptr<XMLComment> c = make_shared<XMLComment>(XMLComment(a->ep,a->v.c_str()));
			comments.push_back(c);
			}

		//contents
		contents.reserve(from.contents.size());
		for (auto a : from.contents)
			{
			shared_ptr<XMLContent> c = make_shared<XMLContent>(XMLContent(a->ep,a->v.c_str()));
			contents.push_back(c);
			}

		//vars
		variables.reserve(from.variables.size());
		for (auto a : from.variables)
			{
			shared_ptr<XMLVariable> c = make_shared<XMLVariable>(XMLVariable(a->n.c_str(),a->v.c_str()));
			variables.push_back(c);
			}

		// children
		children.reserve(from.children.size());
		for (auto a : from.children)
			{
			shared_ptr<XMLElement> c = make_shared<XMLElement>(XMLElement());
			c->operator=((XMLElement&)*a.get());
			children.push_back(c);
			}

//			operator=(from.Serialize().c_str());
		}

	XMLElement::XMLElement(XMLElement&&from)
		{
		el = from.el;
		param = from.param;

		cdatas = from.cdatas;
		comments = from.comments;
		contents = from.contents;
		variables = from.variables;
		children = from.children;
		}

	// Gets
	const XMLElement& XMLElement::operator [](size_t idx) const
		{
		if (idx >= children.size())
			throw XML_ERROR::INVALIDIDX;
		return *children[idx];
		}
		
	XMLElement& XMLElement::operator [](size_t idx) 
		{
		if (idx >= children.size())
			throw XML_ERROR::INVALIDIDX;
		return *children[idx];
		}

	XMLElement& XMLElement::operator [](const char* elm)
		{
		if (elm == 0)
			{
			if (children.size() ==  0)
				throw XML_ERROR::INVALIDIDX;
			return *children[0];
			}
		shared_ptr<XMLElement> e = FindElementZ(elm,true);
		return *e;
		}

	string XMLElement :: vd(const char*nn,const char*def) 
		{
		string k;
		if (!nn)
			{
			if (variables.size() == 0)
				return (def ? def : "");
			k = variables[0]->GetValue();
			}
		else
			{
			shared_ptr<XMLVariable> v = FindVariableZ(nn,true);
			k = v->GetValue();
			}
		if (k.empty() && def)
			k = def;
		return k;
		}

	string XMLElement::vd(const char*nn,const char*def) const
		{
		string k;
		if (!nn)
			{
			if (variables.size() == 0)
				return (def ? def : "");
			k = variables[0]->GetValue();
			}
		else
			{
			shared_ptr<XMLVariable> v = FindVariable(nn);
			if (v)
				k = v->GetValue();
			}
		if (k.empty() && def)
			k = def;
		return k;
		}

	const string& XMLElement::v(size_t idx) const
		{
		if (idx >= variables.size())
			throw XML_ERROR::INVALIDIDX;
		return variables[idx]->GetValue();
		}

	const string& XMLElement::v(const char* nn)
		{
		if (!nn)
			{
			if (variables.size() == 0)
				throw XML_ERROR::INVALIDIDX;
			return variables[0]->GetValue();
			}
		shared_ptr<XMLVariable> v = FindVariableZ(nn,true);
		return v->GetValue();
		}

	string XMLElement::Content() const
		{
		if (contents.empty())
			return "";
		return Trim(contents[0]->GetValue().c_str());
		}

	string XMLElement::GetContent() const
		{
		return Content();
		}

	XMLVariable& XMLElement::vv(const char* nn)
		{
		shared_ptr<XMLVariable> v = FindVariableZ(nn,true);
		return *v;
		}

	unsigned long long XMLElement::GetElementParam() const
		{
		return param;
		}

	const string& XMLElement::GetElementName() const
		{
		return el;
		}

	void XMLElement::GetAllChildren(vector<shared_ptr<XMLElement>>& ch) const
		{
		for (auto& a : children)
			{
			ch.push_back(a);
			a->GetAllChildren(ch);
			}
		}

	shared_ptr<XMLElement> XMLElement::GetParent(shared_ptr<XMLElement> r) const
		{
		vector<shared_ptr<XMLElement>> ch;
		r->GetAllChildren(ch);
		for (auto a : ch)
			{
			if (a->id == parent)
				return a;
			}
		if (r->id == parent)
			return r;
		return 0;
		}

	XMLElement* XMLElement::GetParent(XMLElement* r) const
		{
		vector<shared_ptr<XMLElement>> ch;
		r->GetAllChildren(ch);
		for (auto a : ch)
			{
			if (a->id == parent)
				return a.get();
			}
		if (r->id == parent)
			return r;
		return 0;
		}

	size_t XMLElement::GetElementIndex(const XMLElement& e) const
		{
		for (size_t i = 0; i < children.size(); i++)
			{
			auto& el = children[i];
			if (el.get() == &e)
				return i;
			}
		return (size_t)-1;
		}

	// Sets
	void XMLElement::SetElementName(const char* x)
		{
		el.clear();
		if (x)
			el = x;
		}

	void XMLElement::SetElementName(const wchar_t* x)
		{
		el.clear();
		if (!x)
			return;
		XMLU wh(x);
		el = wh;
		}

	XMLContent& XMLElement::SetContent(const char* vp)
		{
		if (contents.size() == 0)
			return AddContent(vp,0);
		else
			{
			GetContents()[0]->SetValue(vp);
			return *GetContents()[0];
			}

		}

	void XMLElement::SetElementParam(unsigned long long p)
		{
		param = p;
		}

	XMLVariable& XMLElement::SetValue(const char* vn,const char* vp)
		{
		auto& a = vv(vn);
		a = vp;
		return a;
		}


	void XMLElement::SortElements(std::function<bool(const shared_ptr<XMLElement>&e1,const shared_ptr<XMLElement>&e2)> f)
		{
		std::sort(children.begin(),children.end(),f);
		}

	void XMLElement::SortVariables(std::function<bool(const shared_ptr<XMLVariable>&e1,const shared_ptr<XMLVariable>&e2)> f)
		{
		std::sort(variables.begin(),variables.end(),f);
		}

	XML_ERROR XMLElement::MoveElement(size_t i,size_t y)
		{
		if (i >= children.size() || y >= children.size())
			return XML_ERROR::INVALIDARG;
		shared_ptr<XMLElement> x = children[i];
		children.erase(children.begin() + i);
		children.insert(children.begin() + y,x);
		return XML_ERROR::OK;
		}

	// Find
	shared_ptr<XMLElement> XMLElement::FindElementZ(const char* n,bool ForceCreate)
		{
		if (!n)
			return 0;
		for (size_t i = 0; i < children.size(); i++)
			{
			shared_ptr<XMLElement>& cc = children[i];
			const string& cn = cc->GetElementName();
			if (strcmp(cn.c_str(),n) == 0)
				return cc;
			}
		if (ForceCreate == 0)
			return 0;
		XMLElement& vv = AddElement();
		vv.SetElementName(n);
		return FindElementZ(vv.GetElementName().c_str());
		}

	shared_ptr<XMLVariable> XMLElement::FindVariable(const char* n) const
		{
		if (!n)
			return 0;
		for (size_t i = 0; i < variables.size(); i++)
			{
			const shared_ptr<XMLVariable>& cc = variables[i];
			const string& cn = cc->GetName();
			if (strcmp(cn.c_str(),n) == 0)
				return cc;
			}
		return 0;
		}


	shared_ptr<XMLVariable> XMLElement::FindVariableZ(const char* n,bool ForceCreate,const char* defv)
		{
		if (!n)
			return 0;
		for (size_t i = 0; i < variables.size(); i++)
			{
			shared_ptr<XMLVariable>& cc = variables[i];
			const string& cn = cc->GetName();
			if (strcmp(cn.c_str(),n) == 0)
				return cc;
			}
		if (ForceCreate == 0)
			return 0;
		XMLVariable& vv = AddVariable(n,defv);
		return FindVariableZ(vv.GetName().c_str());
		}

	// Inserts
	shared_ptr<XMLElement> XMLElement::InsertElement(size_t y,const XMLElement& x)
		{
		if (y >= children.size())
			y = children.size();
		shared_ptr<XMLElement> xx = make_shared<XMLElement>(XMLElement(x));
		children.insert(children.begin() + y,xx);
		children[y]->parent = id;
		return children[y];
		}
	shared_ptr<XMLElement> XMLElement::InsertElement(size_t y,XMLElement&& x)
		{
		if (y >= children.size())
			y = children.size();
		shared_ptr<XMLElement> xx = make_shared<XMLElement>(XMLElement(std::forward<XMLElement>(x)));
		children.insert(children.begin() + y,xx);
		children[y]->parent = id;
		return children[y];
		}
	XMLElement& XMLElement::AddElement(const XMLElement& c)
		{
		return *InsertElement((size_t)-1,c);
		}
	XMLElement& XMLElement::AddElement(XMLElement&& c)
		{
		return *InsertElement((size_t)-1,std::forward<XMLElement>(c));
		}
	XMLElement& XMLElement::AddElement(const char* n)
		{
		XMLElement c = n;
		return *InsertElement((size_t)-1,std::forward<XMLElement>(c));
		}
	void XMLElement::AddElements(const std::initializer_list<string>& s)
		{
		for (auto& a : s)
			{
			XMLElement c = a.c_str();
			InsertElement((size_t)-1,c);
			}
		}

	void XMLElement::SetVariables(const std::initializer_list<string>& s)
		{
		if (s.size() % 2)
			return;
		for (size_t i = 0; i < s.size(); i += 2)
			{
			auto a = s.begin() + i;
			auto b = s.begin() + i + 1;
			vv(a->c_str()) = b->c_str();
			}
		}

	XMLVariable& XMLElement::AddVariable(const XMLVariable& vv,size_t p)
		{
		if (p == (size_t)-1)
			p = (size_t)variables.size();
		shared_ptr<XMLVariable> v = make_shared<XMLVariable>(XMLVariable(vv));
		variables.insert(variables.begin() + p,v);
		return *variables[p];
		}

	XMLVariable& XMLElement::AddVariable(const char* vn,const char* vv,size_t p)
		{
		if (p == (size_t)-1)
			p = (size_t)variables.size();
		shared_ptr<XMLVariable> v = make_shared<XMLVariable>(XMLVariable(vn,vv));
		variables.insert(variables.begin() + p,v);
		return *variables[p];
		}


	XMLContent& XMLElement::AddContent(const char* pv,size_t ep,size_t p)
		{
		if (p == (size_t)-1)
			p = (size_t)contents.size();
		shared_ptr<XMLContent> v = make_shared<XMLContent>(XMLContent(ep,pv));
		contents.insert(contents.begin() + p,v);
		return *contents[p];
		}

	XMLComment& XMLElement::AddComment(const char* pv,size_t ep,size_t p)
		{
		if (p == (size_t)-1)
			p = (size_t)comments.size();
		shared_ptr<XMLComment> v = make_shared<XMLComment>(XMLComment(ep,pv));
		comments.insert(comments.begin() + p,v);
		return *comments[p];
		}

	XMLCData& XMLElement::AddCData(const char* pv,size_t ep,size_t p)
		{
		if (p == (size_t)-1)
			p = (size_t)cdatas.size();
		shared_ptr<XMLCData> v = make_shared<XMLCData>(XMLCData(ep,pv));
		cdatas.insert(cdatas.begin() + p,v);
		return *cdatas[p];
		}


	// Removals
	size_t XMLElement::RemoveAllElements()
		{
		children.clear();
		return 0;
		}

	size_t XMLElement::RemoveElement(size_t i)
		{
		if (i >= children.size())
			return (size_t)-1;
		children.erase(children.begin() + i);
		return children.size();
		}

	shared_ptr<XMLElement> XMLElement::RemoveElementAndKeep(size_t i) throw(XML_ERROR)
		{
		if (i >= children.size())
			throw XML_ERROR::INVALIDARG;
		auto X = children[i];
		RemoveElement(i);
		return X;
		}

	void XMLElement::clear()
		{
		variables.clear();
		children.clear();
		cdatas.clear();
		comments.clear();
		contents.clear();
		param = 0;
		el = "e";
		}

	// Variables 
	size_t XMLElement::RemoveAllVariables()
		{
		variables.clear();
		return 0;
		}

	size_t XMLElement::RemoveVariable(size_t i)
		{
		if (i >= variables.size())
			return variables.size();
		variables.erase(variables.begin() + i);
		return variables.size();
		}

	shared_ptr<XMLVariable> XMLElement::RemoveVariableAndKeep(size_t i) throw (XML_ERROR)
		{
		if (i >= variables.size())
			throw XML_ERROR::INVALIDARG;
		auto v = variables[i];
		variables.erase(variables.begin() + i);
		return v;
		}

	string XMLElement::EorE(const string& s,bool N) const
		{
		if (N)
			return s;
		return Encode(s.c_str());
		}

	string XMLElement::Serialize(bool NoEnc,size_t deep) const
		{
		string v;
		Serialize(v,NoEnc,deep);
		return v;
		}

	void XMLElement::Serialize(string& v,bool NoEnc,size_t deep) const
		{
		string padd;
		for (size_t i = 0; i < deep; i++)
			{
			padd += "\t";
			}

		// <n
		if (variables.empty() && children.empty() && comments.empty() && contents.empty() && cdatas.empty())
			{
			v += Format("%s<%s />",padd.c_str(),EorE(el,NoEnc).c_str());
			return;
			}

		v += Format("%s<%s",padd.c_str(),EorE(el,NoEnc).c_str());
		if (!variables.empty())
			{
			for (auto a : variables)
				{
				v += " ";
				v += a->Serialize(NoEnc);
				}
			}
		if (children.empty() && comments.empty() && contents.empty() && cdatas.empty())
			{
			v += "/>\r\n";
			return;
			}
		v += ">\r\n";


		auto ac = [&] (vector<shared_ptr<XMLContent>>& cx,size_t B,size_t B2)
			{
			for (auto& a : contents)
				{
				if (a->GetEP() == B || a->GetEP() == B2)
					cx.push_back(a);
				}
			for (auto& a : comments)
				{
				if (a->GetEP() == B || a->GetEP() == B2)
					cx.push_back(a);
				}
			for (auto& a : cdatas)
				{
				if (a->GetEP() == B || a->GetEP() == B2)
					cx.push_back(a);
				}
			};

		// Stuff
		for (size_t i = 0; i < children.size(); i++)
			{
			auto& a = children[i];

			// Before items
			vector<shared_ptr<XMLContent>> cx;
			ac(cx,i,i);
			for (auto& n : cx)
				{
				string e = n->Serialize();
				v += Format("%s%s\r\n",padd.c_str(),e.c_str());
				}

			a->Serialize(v,NoEnc,deep + 1);
			}

		// After items
		vector<shared_ptr<XMLContent>> cx;
		ac(cx,(size_t)-1,children.size());
		for (auto& n : cx)
			{
			string e = n->Serialize();
			v += Format("%s%s\r\n",padd.c_str(),e.c_str());
			}


		v += Format("%s</%s>\r\n",padd.c_str(),EorE(el,NoEnc).c_str());
		return;
		}





	// Constructors
	XML::XML()
		{
		}

	XML::XML(const char* file)
		{
		UnicodeFile = false;
		fname = file;
		Load(file);
		}

	XML::XML(const wchar_t* file)
		{
		UnicodeFile = true;
		fname = XMLU(file);
		Load(file);
		}

	XML::XML(const char* mem,size_t l)
		{
		UnicodeFile = false;
		Parse(mem,l);
		}


	void XML::operator =(const char* d)
		{
		UnicodeFile = false;
		Parse(d,strlen(d));
		}

	// Savers
	size_t XML::SaveFP(FILE* fp) const
		{
		string s = Serialize();
		size_t sz = fwrite(s.c_str(),1,s.length(),fp);
		fclose(fp);
		return sz;
		}


	XML_ERROR XML::Save() const
		{
		if (UnicodeFile)
			return Save(XMLU(fname.c_str()).operator const wchar_t *());
		return Save(fname.c_str());
		}


	XML_ERROR XML::Save(const char* f) const
		{
		if (!f)
			f = fname.c_str();
		FILE* fp = fopen(f,"wb");
		if (!fp)
			return XML_ERROR::SAVEFAILED;
		string s = Serialize();
		if (hdr.GetEncoding() == "UTF-16")
			{
			fwrite("\xFE\xFF",1,3,fp);
			wstring s2 = XMLU(s.c_str());
			size_t sz = fwrite(s2.data(),1,s.length() * 2,fp);
			fclose(fp);
			if (sz != s.length() * 2)
				return XML_ERROR::SAVEFAILED;
			}
		else
			{
			fwrite("\xEF\xBB\xBF",1,3,fp);
			size_t sz = fwrite(s.c_str(),1,s.length(),fp);
			fclose(fp);
			if (sz != s.length())
				return XML_ERROR::SAVEFAILED;
			}

		return XML_ERROR::OK;
		}

	XML_ERROR XML::Save(const wchar_t* f) const
		{
		XMLU wf(fname.c_str());
		FILE* fp = _wfopen(f ? f : wf.operator const wchar_t *(),L"wb");
		if (!fp)
			return XML_ERROR::SAVEFAILED;
		string s = Serialize();
		if (hdr.GetEncoding() == "UTF-16")
			{
			fwrite("\xFE\xFF",1,3,fp);
			wstring s2 = XMLU(s.c_str());
			size_t sz = fwrite(s2.data(),1,s.length() * 2,fp);
			fclose(fp);
			if (sz != s.length() * 2)
				return XML_ERROR::SAVEFAILED;
			}
		else
			{
			fwrite("\xEF\xBB\xBF",1,3,fp);
			size_t sz = fwrite(s.c_str(),1,s.length(),fp);
			fclose(fp);
			if (sz != s.length())
				return XML_ERROR::SAVEFAILED;
			}

		return XML_ERROR::OK;
		}


	// Loaders
	XML_PARSE XML::ParseFile(FILE* fp)
		{
		fseek(fp,0,SEEK_END);
		size_t S = ftell(fp);
		fseek(fp,0,SEEK_SET);

		BXML b;
		b.Resize(S + 1);
		fread(b.p(),1,S,fp);
		fclose(fp);

		// Check if this XML we loaded is a unicode XML
#ifdef _WIN32
		unsigned char a1 = b.p()[0];
		unsigned char a2 = b.p()[1];
		unsigned char a3 = b.p()[2];
		if (a1 == 0xFF && a2 == 0xFE)
			{
			BXML b2;
			b2.Resize(S * 4 + 32);
			WideCharToMultiByte(CP_UTF8,0,(wchar_t*)b.p(),(int)-1,b2.p(),(int)(S * 4 + 32),0,0);
			b = b2;
			}
		// UTF-8 BOM? 
		//fwrite("\xEF\xBB\xBF",1,3,fp);
		// 
		if (a1 == 0xEF && a2 == 0xBB && a3 == 0xBF)
			{
			BXML b2;
			b2.Resize(S);
			memcpy(b2.p(),b.p() + 3,S - 3);

			b = b2;
			}
#endif

		// b is the text
		const char* bb = (const char*)b.p();
		return Parse(bb,strlen(bb));
		}

	XML_PARSE XML::Load(const char* f)
		{
		Clear();
		FILE* fp = fopen(f,"rb");
		if (!fp)
			return XML_PARSE::OPENFAILED;
		return ParseFile(fp);
		}

	XML_PARSE XML::Load(const wchar_t* f)
		{
		Clear();
		FILE* fp = 	_wfopen(f,L"rb");
		if (!fp)
			return XML_PARSE::OPENFAILED;
		return ParseFile(fp);
		}

	XML_PARSE XML::Parse(const char* m,size_t len)
		{
//		TICKCOUNT c("Parse");
		hdrcomments.reserve(3);

		enum class PARSEELEMENT
			{
			STARTHDR,
			ENDHDR,
			STARTDOCTYPE,
			ENDDOCTYPE,
			STARTVAR,
			ENDVAR,
			STARTCOMMENT,
			ENDCOMMENT,
			STARTCD,
			ENDCD,
			STARTEL,
			ENDEL,
			ENDDOCUMENT
			};

		int InCData = 0;
		int InComment = 0;
		int InHeader = 0;
		string t1;
		t1.reserve(100000);
		XMLElement* l = 0;
		int OutsideElementLine = 1;
		int InVar = 0; // 1 got =, 2 got ", 
		int InDocType = 0;
		XMLVariable cv;
		XMLCData cd;
		XMLComment comm;
		bool PendingElName = false;
		bool PendingElNameClosing = false;

		auto ParseFunc = [&] (PARSEELEMENT pe) -> XML_PARSE
			{
			//TICKCOUNT c("ParseFunc");
			XML_PARSE e = XML_PARSE::OK;

#ifdef _DEBUG
			//string adsb = Serialize();
#endif

			if (pe == PARSEELEMENT::STARTHDR)
				{
				if (InHeader > 0)
					return XML_PARSE::HDRERROR;
				InHeader = 1;
				}
			if (pe == PARSEELEMENT::STARTEL)
				{
				if (!l)
					l = &root;
				else
					{
//					XMLElement ne;
					XMLElement& ne2 = l->AddElement("el");
					l = &ne2;
					}
				PendingElName = true;
				}
			if (pe == PARSEELEMENT::ENDEL)
				{
				if (l)
					{
					l = l->GetParent(&root);
					}
				}


			if (pe == PARSEELEMENT::ENDHDR)
				{
				if (InHeader != 1)
					return XML_PARSE::HDRERROR;
				InHeader = 2;
				}
			if (pe == PARSEELEMENT::STARTDOCTYPE)
				{
				if (InDocType > 0)
					return XML_PARSE::HDRERROR;
				InDocType = 1;
				}
			if (pe == PARSEELEMENT::ENDDOCTYPE)
				{
				if (InDocType != 1)
					return XML_PARSE::HDRERROR;
				InDocType = 2;
				doctype.SetValue(Decode(t1.c_str()).c_str());
				t1.clear();
				}
			if (pe == PARSEELEMENT::STARTCOMMENT)
				{
				if (InComment != 0)
					return XML_PARSE::COMMENTERROR;
				InComment = 1;
				}
			if (pe == PARSEELEMENT::STARTCD)
				{
				if (InCData != 0 || !l)
					return XML_PARSE::COMMENTERROR;
				InCData = 1;
				}



			if (pe == PARSEELEMENT::ENDCOMMENT)
				{
				if (InComment != 1)
					return XML_PARSE::COMMENTERROR;
				InComment = 0;
				if (l)
					comm.SetEP(l->GetChildren().size());
				comm.SetValue(Decode(t1.c_str()).c_str());
				t1.clear();
				if (l)
					l->AddComment(comm.GetValue().c_str(),comm.GetEP());
				else
					hdrcomments.push_back(make_shared<XMLComment>(XMLComment(comm)));
				}

			if (pe == PARSEELEMENT::ENDCD)
				{
				if (InCData != 1 || !l)
					return XML_PARSE::COMMENTERROR;
				InCData = 0;
				cd.SetEP(l->GetChildren().size());
				cd.SetValue(Decode(t1.c_str()).c_str());
				t1.clear();
				l->AddCData(cd.GetValue().c_str(),cd.GetEP());
				}
			// Header var
			if (pe == PARSEELEMENT::STARTVAR && InHeader == 1)
				{
				if (InVar != 0)
					return XML_PARSE::VARERROR;
				cv.Clear();
				InVar = 1;
				cv.SetName(Decode(t1.c_str()).c_str());
				t1.clear();
				}
			if (pe == PARSEELEMENT::ENDVAR && InHeader == 1)
				{
				if (InVar != 2)
					return XML_PARSE::VARERROR;
				InVar = 0;
				cv.SetValue(Decode(t1.c_str()).c_str());
				t1.clear();
				if (cv.GetName() == "version")
					hdr.GetVersion() = cv;
				else
					if (cv.GetName() == "encoding")
						hdr.GetEncoding() = cv;
					else
						if (cv.GetName() == "standalone")
							hdr.GetStandalone() = cv;
						else
							return XML_PARSE::HDRERROR;
				cv.Clear();
				}



			// Element var
			if (pe == PARSEELEMENT::STARTVAR && l)
				{
				if (InVar != 0)
					return XML_PARSE::VARERROR;
				cv.Clear();
				InVar = 1;
				cv.SetName(Decode(t1.c_str()).c_str());
				t1.clear();
				}
			if (pe == PARSEELEMENT::ENDVAR && l)
				{
				if (InVar != 2)
					return XML_PARSE::VARERROR;
				InVar = 0;
				cv.SetValue(Decode(t1.c_str()).c_str());
				t1.clear();
				l->AddVariable(cv.GetName().c_str(),cv.GetValue().c_str());
				cv.Clear();
				}


			return e;
			};


		XML_PARSE e = XML_PARSE::OK;
		for (size_t i = 0; i < len;)
			{
			const char* mm = m + i;
			bool IsSpace = false;
			if (m[i] == ' ' || m[i] == '\t' || m[i] == '\r' || m[i] == '\n')
				IsSpace = true;

			// Opti CData
			if (InCData)
				{
				const char* s1 = strstr(mm,"]]>");
				if (!s1)
					{
					e = XML_PARSE::COMMENTERROR;
					break;
					}
				t1.assign(mm,s1 - mm);
				i += t1.length();
				e = ParseFunc(PARSEELEMENT::ENDCD);
				if (e != XML_PARSE::OK)
					break;
				i += 3;
				continue;
				}
			// Opti Comment
			if (InComment)
				{
				const char* s1 = strstr(mm,"-->");
				if (!s1)
					{
					e = XML_PARSE::COMMENTERROR;
					break;
					}
				t1.assign(mm,s1 - mm);
				i += t1.length();
				e = ParseFunc(PARSEELEMENT::ENDCOMMENT);
				if (e != XML_PARSE::OK)
					break;
				i += 3;
				continue;
				}

			// El name?
			if (l && PendingElName && IsSpace && !t1.empty())
				{
				l->SetElementName(Trim(t1.c_str()).c_str());
				PendingElName = false;
				t1.clear();
				i++;
				continue;
				}

			// Header test
			bool CanHeader = false;
			if (!InComment && !InCData && l == 0)
				CanHeader = true;
			if (CanHeader)
				{
				if (_strnicmp(mm,"<?xml ",6) == 0)
					{
					e = ParseFunc(PARSEELEMENT::STARTHDR);
					if (e != XML_PARSE::OK)
						break;
					i += 6;
					continue;
					}
				if (_strnicmp(mm,"?>",2) == 0)
					{
					e = ParseFunc(PARSEELEMENT::ENDHDR);
					if (e != XML_PARSE::OK)
						break;
					i += 2;
					continue;
					}
				if (_strnicmp(mm,"<!DOCTYPE ",10) == 0)
					{
					e = ParseFunc(PARSEELEMENT::STARTDOCTYPE);
					if (e != XML_PARSE::OK)
						break;
					i += 10;
					continue;
					}
				if (_strnicmp(mm,">",1) == 0 && InDocType == 1)
					{
					e = ParseFunc(PARSEELEMENT::ENDDOCTYPE);
					if (e != XML_PARSE::OK)
						break;
					i += 1;
					continue;
					}
				}

			// Comment Tests
			bool CanComment = true;
			if (l && OutsideElementLine == 0)
				CanComment = false;
			if (InCData)
				CanComment = false;
			if (CanComment)
				{
				if (_strnicmp(mm,"<!--",4) == 0 && !InComment)
					{
					e = ParseFunc(PARSEELEMENT::STARTCOMMENT);
					if (e != XML_PARSE::OK)
						break;
					i += 4;
					continue;
					}
				if (_strnicmp(mm,"-->",3) == 0 && InComment)
					{
					e = ParseFunc(PARSEELEMENT::ENDCOMMENT);
					if (e != XML_PARSE::OK)
						break;
					i += 3;
					continue;
					}
				}

			// CData Tests
			bool CanCData = true;
			if (l && OutsideElementLine == 0)
				CanCData = false;
			if (CanCData)
				{
				if (_strnicmp(mm,"<![CDATA[",9) == 0 && !InCData)
					{
					e = ParseFunc(PARSEELEMENT::STARTCD);
					if (e != XML_PARSE::OK)
						break;
					i += 9;
					continue;
					}
				if (_strnicmp(mm,"]]>",3) == 0 && InCData)
					{
					e = ParseFunc(PARSEELEMENT::ENDCD);
					if (e != XML_PARSE::OK)
						break;
					i += 3;
					continue;
					}
				}


			// Variable Tests
			bool CanVariable = false;
			if (!InComment && !InCData)
				{
				if (InHeader == 1)
					CanVariable = true;
				else
					if (l && OutsideElementLine == 0)
						CanVariable = true;
				}
			if (_strnicmp(mm,"=",1) == 0 && CanVariable && InVar != 2)
				{
				e = ParseFunc(PARSEELEMENT::STARTVAR);
				if (e != XML_PARSE::OK)
					break;
				i += 1;
				continue;
				}
			if (_strnicmp(mm,"\"",1) == 0 && CanVariable && InVar == 1)
				{
				t1.clear();
				InVar = 2;
				i += 1;
				continue;
				}
			if (_strnicmp(mm,"\"",1) == 0 && CanVariable && InVar == 2)
				{
				e = ParseFunc(PARSEELEMENT::ENDVAR);
				if (e != XML_PARSE::OK)
					break;
				i += 1;
				continue;
				}

			bool CanElement = false;
			if (!InComment && !InCData && !InVar && InDocType != 1)
				CanElement = true;

			if (CanElement)
				{
				if (_strnicmp(mm,"</",2) == 0 && l && OutsideElementLine == 1)
					{
					// Content...
					if (l && !t1.empty())
						{
						l->AddContent(t1.c_str(),(size_t)-1);
						t1.clear();
						}

					e = ParseFunc(PARSEELEMENT::ENDEL);
					if (e != XML_PARSE::OK)
						break;
					t1.clear();
					i += 2;
					PendingElNameClosing = true;
					continue;
					}


				if (_strnicmp(mm,"<",1) == 0 && OutsideElementLine == 1)
					{
					// Content...
					if (l && !t1.empty())
						{
						l->AddContent(t1.c_str(),l->GetChildren().size());
						t1.clear();
						}

					e = ParseFunc(PARSEELEMENT::STARTEL);
					if (e != XML_PARSE::OK)
						break;
					OutsideElementLine = 0;
					i += 1;
					continue;
					}

				if (_strnicmp(mm,"/>",2) == 0)
					{

					// El name?
					if (l && PendingElName && !t1.empty())
						{
						l->SetElementName(Trim(t1.c_str()).c_str());
						PendingElName = false;
						}

					e = ParseFunc(PARSEELEMENT::ENDEL);

					if (e != XML_PARSE::OK)
						break;
					OutsideElementLine = 1;
					i += 2;
					continue;
					}


				if (_strnicmp(mm,">",1) == 0 && (OutsideElementLine == 0 || PendingElNameClosing) && l)
					{
					// El name?
					if (PendingElName)
						{
						l->SetElementName(Trim(t1.c_str()).c_str());
						PendingElName = false;
						t1.clear();
						i++;
						OutsideElementLine = 1;
						continue;
						}
					// El name?
					if (PendingElNameClosing)
						{
						//*
						PendingElNameClosing = false;
						t1.clear();
						i++;
						OutsideElementLine = 1;
						continue;
						}
					i++;
					OutsideElementLine = 1;
					continue;
					}

				}


			if (IsSpace && t1.empty() && !InComment && !InCData)
				{

				}
			else
				t1 += m[i];

			i++;
			continue;
			}

		return e;
		}

	// Gets
	XMLElement& XML::GetRootElement()
		{
		return root;
		}

	XMLHeader& XML::GetHeader()
		{
		return hdr;
		}

	size_t XML::MemoryUsage()
		{
		return 0;
		//			return root.MemoryUsage() + GetHeader().MemoryUsage();
		}

	// Sets
	void XML::SetRootElement(XMLElement& newroot)
		{
		root = newroot;
		}
	void XML::SetHeader(const XMLHeader& h)
		{
		hdr = h;
		}


	void XML::Clear()
		{
		root.clear();
		hdrcomments.clear();
		doctype.Clear();
		hdr.Default();
		}



	void XML::Version(XML_VERSION_INFO* x)
		{
		x->VersionLow = (XML3_VERSION & 0xFF);
		x->VersionHigh = (XML3_VERSION >> 8);
		strcpy_s(x->RDate,sizeof(x->RDate),XML3_VERSION_REVISION_DATE);
		}



	// ser
	string XML::Serialize(bool NoEnc) const
		{
		string v;
		v.reserve(100000);

		v += hdr.Serialize();
		v += "\r\n";
		if (doctype.GetValue().length())
			{
			v += doctype.Serialize();
			v += "\r\n";
			}
		for (auto a : hdrcomments)
			{
			v += a->Serialize();
			v += "\r\n";
			}
		root.Serialize(v,NoEnc);
		return v;
		}




	};




