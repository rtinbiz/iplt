﻿#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:4290)
#endif
#include <stdio.h>
#include <string>
#include <functional>
#include <regex>
#include <algorithm>
#include <cctype>
#include <memory>
#include <vector>
#include <stdarg.h>
#ifdef WIN32
#include <windows.h>
#endif


#pragma once

namespace XML3
{

using namespace std;

#define XML3_VERSION 0x1
#define XML3_VERSION_REVISION_DATE "01-06-2015"

typedef struct
	{
	int VersionHigh;
	int VersionLow;
	char RDate[20];
	} XML_VERSION_INFO;

enum class XML_ERROR
	{
	OK = 0L,
	INVALIDARG = -1,
	OPENFAILED = -2,
	SAVEFAILED = -3,
	INVALIDIDX = -4
	};

enum class XML_PARSE
	{
	OK = 0L,
	OPENFAILED = -1,
	HDRERROR = -2,
	VARERROR = -3,
	COMMENTERROR = -4,
	};

// MIME Code
// Code from Yonat
// http://ootips.org/yonat/4dev/
#ifndef MIME_CODES_H
#define MIME_CODES_H
/******************************************************************************
* MimeCoder -  Abstract base class for MIME filters.
******************************************************************************/
template <class InIter,class OutIter>
class MimeCoder
	{
	public:
		virtual OutIter Filter(OutIter out,InIter inBeg,InIter inEnd) = 0;
		virtual OutIter Finish(OutIter out) = 0;
	};

/******************************************************************************
* Base64
******************************************************************************/
template <class InIter,class OutIter>
class Base64Encoder : public MimeCoder<InIter,OutIter>
	{
	public:
		Base64Encoder() : its3Len(0),itsLinePos(0) {}
		virtual OutIter Filter(OutIter out,InIter inBeg,InIter inEnd);
		virtual OutIter Finish(OutIter out);
	private:
		int             itsLinePos;
		unsigned char   itsCurr3[3];
		int             its3Len;
		void EncodeCurr3(OutIter& out);
	};

template <class InIter,class OutIter>
class Base64Decoder : public MimeCoder<InIter,OutIter>
	{
	public:
		Base64Decoder() : its4Len(0),itsEnded(0) {}
		virtual OutIter Filter(OutIter out,InIter inBeg,InIter inEnd);
		virtual OutIter Finish(OutIter out);
	private:
		int             itsEnded;
		unsigned char   itsCurr4[4];
		int             its4Len;
		int             itsErrNum;
		void DecodeCurr4(OutIter& out);
	};

/******************************************************************************
* Quoted-Printable
******************************************************************************/
template <class InIter,class OutIter>
class QpEncoder : public MimeCoder<InIter,OutIter>
	{
	public:
		QpEncoder() : itsLinePos(0),itsPrevCh('x') {}
		virtual OutIter Filter(OutIter out,InIter inBeg,InIter inEnd);
		virtual OutIter Finish(OutIter out);
	private:
		int             itsLinePos;
		unsigned char   itsPrevCh;
	};

template <class InIter,class OutIter>
class QpDecoder : public MimeCoder<InIter,OutIter>
	{
	public:
		QpDecoder() : itsHexLen(0) {}
		virtual OutIter Filter(OutIter out,InIter inBeg,InIter inEnd);
		virtual OutIter Finish(OutIter out);
	private:
		int             itsHexLen;
		unsigned char   itsHex[2];
	};
#endif // MIME_CODES_H
#define TEST_MIME_CODES
#define ___
static const int cLineLen = 72;

/******************************************************************************
* Base64Encoder
******************************************************************************/
static const char cBase64Codes[] =
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
static const char CR = 13;
static const char LF = 10;

template <class InIter,class OutIter>
OutIter Base64Encoder<InIter,OutIter>::Filter(
	OutIter out,
	InIter inBeg,
	InIter inEnd)
	{
	for (;;)
		{
		for (; itsLinePos < cLineLen; itsLinePos += 4)
			{
			for (; its3Len < 3; its3Len++)
				{
				if (inBeg == inEnd)
					___ ___ ___ ___ ___ return out;
				itsCurr3[its3Len] = *inBeg;
				++inBeg;
				}
			EncodeCurr3(out);
			its3Len = 0;
			} // for loop until end of line
		*out++ = CR;
		*out++ = LF;
		itsLinePos = 0;
		} // for (;;)
	//    return out;
	}

template <class InIter,class OutIter>
OutIter Base64Encoder<InIter,OutIter>::Finish(OutIter out)
	{
	if (its3Len)
		EncodeCurr3(out);
	its3Len = 0;
	itsLinePos = 0;

	return out;
	}

template <class InIter,class OutIter>
void Base64Encoder<InIter,OutIter>::EncodeCurr3(OutIter& out)
	{
	if (its3Len < 3) itsCurr3[its3Len] = 0;

	*out++ = cBase64Codes[itsCurr3[0] >> 2];
	*out++ = cBase64Codes[((itsCurr3[0] & 0x3) << 4) |
		((itsCurr3[1] & 0xF0) >> 4)];
	if (its3Len == 1) *out++ = '=';
	else
		*out++ = cBase64Codes[((itsCurr3[1] & 0xF) << 2) |
		((itsCurr3[2] & 0xC0) >> 6)];
	if (its3Len < 3) *out++ = '=';
	else
		*out++ = cBase64Codes[itsCurr3[2] & 0x3F];
	}

/******************************************************************************
* Base64Decoder
******************************************************************************/

#define XX 127

static const unsigned char cIndex64[256] = {
	XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,
	XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,
	XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,62,XX,XX,XX,63,
	52,53,54,55,56,57,58,59,60,61,XX,XX,XX,XX,XX,XX,
	XX,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,
	15,16,17,18,19,20,21,22,23,24,25,XX,XX,XX,XX,XX,
	XX,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,
	41,42,43,44,45,46,47,48,49,50,51,XX,XX,XX,XX,XX,
	XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,
	XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,
	XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,
	XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,
	XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,
	XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,
	XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,
	XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,XX,
	};


template <class InIter,class OutIter>
OutIter Base64Decoder<InIter,OutIter>::Filter(
	OutIter out,
	InIter inBeg,
	InIter inEnd)
	{
	unsigned char c;
	itsErrNum = 0;

	for (;;)
		{
		while (its4Len < 4)
			{
			if (inBeg == inEnd)
				___ ___ ___ ___ return out;
			c = *inBeg;
			if ((cIndex64[c] != XX) || (c == '='))
				itsCurr4[its4Len++] = c;
			else if ((c != CR) && (c != LF)) ++itsErrNum; // error
			++inBeg;
			} // while (its4Len < 4)
		DecodeCurr4(out);
		its4Len = 0;
		} // for (;;)

	//	 return out;
	}

template <class InIter,class OutIter>
OutIter Base64Decoder<InIter,OutIter>::Finish(OutIter out)
	{
	its4Len = 0;
	if (itsEnded) return out;
	else
		{ // error
		itsEnded = 0;
		return out;
		}
	}

template <class InIter,class OutIter>
void Base64Decoder<InIter,OutIter>::DecodeCurr4(OutIter& out)
	{
	if (itsEnded)
		{
		++itsErrNum;
		itsEnded = 0;
		}

	for (int i = 0; i < 2; i++)
		if (itsCurr4[i] == '=')
			{
			++itsErrNum; // error
			___ ___ ___ return;
			}
		else itsCurr4[i] = cIndex64[itsCurr4[i]];

		*out++ = (itsCurr4[0] << 2) | ((itsCurr4[1] & 0x30) >> 4);
		if (itsCurr4[2] == '=')
			{
			if (itsCurr4[3] == '=') itsEnded = 1;
			else ++itsErrNum;
			}
		else
			{
			itsCurr4[2] = cIndex64[itsCurr4[2]];
			*out++ = ((itsCurr4[1] & 0x0F) << 4) | ((itsCurr4[2] & 0x3C) >> 2);
			if (itsCurr4[3] == '=') itsEnded = 1;
			else *out++ = ((itsCurr4[2] & 0x03) << 6) | cIndex64[itsCurr4[3]];
			}
	}

#undef XX


// BXML
class BXML
	{
	public:
		char* d;
		size_t ss;

		BXML(size_t s);
		BXML();
		~BXML();
		BXML(const BXML& d2);
		BXML(BXML&& d2);

		void operator =(const BXML& d2);
		void operator =(BXML&& d2);
		bool operator ==(const BXML& b2);

		void ToB(string& s);
		void FromB(const char* ba);
		operator char*() const;
		char* p() const;
		size_t size() const;


		void clear();
		void reset();

		void Ensure(size_t news);
		void Resize(size_t news);
		void AddResize(size_t More);
	};

string Char2Base64(const char *Z,size_t s);
void Base64ToChar(const char *Z,size_t s,BXML& out);
string Decode(const char* src);
string Format(const char* f,...);
string Trim(const char* src,int m = 2);
string Encode(const char* src);
void JsonParser(class XMLElement* root,const char* txt);
	


// XMLU Class, converts utf input to wide char and vice versa
class XMLU
	{
	public:
		string tu;
		wstring wu;
		XMLU(const char* x);
		XMLU(const wchar_t* x);

		const wchar_t* wc();
		const char* bc();
		operator const wchar_t*();
		operator const char*();
	};

class XML;

class XMLId
	{
	private:
		unsigned long long ts = 0;
	public:
		XMLId();
		XMLId(unsigned long long a);
		XMLId(const XMLId& a);
		void operator =(unsigned long long a);

		bool operator ==(const XMLId& a);
		void operator =(const XMLId& a);
		unsigned long long g() const;
	};


class XMLContent
	{
	protected:
		string v;
		size_t ep = 0;

	public:

		friend class XMLElement;
		XMLContent();
		XMLContent(size_t ElementPosition,const char* ht);
		XMLContent(size_t ElementPosition,const wchar_t* ht);

		size_t MemoryUsage();

		// Operators
		bool operator==(const XMLContent& t) const;
		operator const string& () const;

		// Sets
		const string& SetFormattedValue(const char* fmt,...);
		template <typename T> const string& SetFormattedValue(const char* fmt,T ty)
			{
			unique_ptr<char> t(new char[10000]);
			sprintf_s(t.get(),10000,fmt,ty);
			SetValue(t.get());
			return v;
			}

		const string& SetValueInt(int v) { return SetFormattedValue<int>("%i",v); }
		const string& SetValueUInt(unsigned int v) { return SetFormattedValue<unsigned int>("%u",v); }
		const string& SetValueLongLong(long long v) { return SetFormattedValue<long long >("%lli",v); }
		const string& SetValueULongLong(unsigned long long v) { return SetFormattedValue<long long >("%llu",v); }

		XMLContent&  operator =(const char* s);
		XMLContent&  operator =(const string& s);
		const string& SetBinaryValue(const char* data,size_t len);

		void SetValue(const char* VV);
		void SetValue(const string& VV);
		void Clear();
		void SetEP(size_t epp);

		// Gets
		BXML GetBinaryValue() const;
		template <typename T> T GetFormattedValue(const char* fmt,const T def) const
			{
			T x = def;
			sscanf_s(v.c_str(),fmt,&x);
			return x;
			}

		int GetValueInt(int def = 0)
			{
			return GetFormattedValue<int>("%i",def);
			}
		unsigned int GetValueUInt(unsigned int def = 0)
			{
			return GetFormattedValue<unsigned int>("%u",def);
			}
		long long GetValueLongLong(long long def = 0)
			{
			return GetFormattedValue<long long>("%lli",def);
			}
		unsigned long long GetValueULongLong(unsigned long long def = 0)
			{
			return GetFormattedValue<unsigned long long>("%llu",def);
			}

		const string& GetValue() const;
		size_t GetEP() const;

		// Serialization
		virtual string Serialize(bool NoEnc = false) const;

	};

class XMLVariable : public XMLContent
	{
	private:
		string n;
		bool tmp = false;
	public:

		friend class XMLElement;
		explicit XMLVariable();
		explicit XMLVariable(const char* nn,const char* vv,bool Temp = false);
	//	explicit XMLVariable::XMLVariable(const XMLVariable& h);
	//	XMLVariable& operator =(const XMLVariable& h);
	//	XMLVariable& operator =(const std::initializer_list<string>& s); 

		const string& SetName(const char* VN);
		const string& SetName(const string& VN);
		void Clear();
		XMLVariable&  operator =(const char* s);
		XMLVariable&  operator =(const string& s);

		// Compare 
		bool operator <(const XMLVariable& x) const;
		bool operator ==(const XMLVariable& x) const;
		bool operator ==(const char* x) const;
		// Gets
		const string& XMLVariable::GetName() const;

		// Memory usage
		size_t MemoryUsage() const;

		
		// Serialization
		virtual string Serialize(bool NoEnc = false) const;

	};

typedef XMLVariable XMLAttribute;


class XMLCData : public XMLContent
	{
	public:
	
		XMLCData(size_t ElementPosition = 0,const char* ht = "");

		XMLCData(size_t ElementPosition,const wchar_t* ht);

		// Serialization
		virtual string Serialize(bool NoEnc = false) const;
	};

class XMLDocType : public XMLContent
	{
	public:

		XMLDocType(const char* ht = "");

		XMLDocType(const wchar_t* ht);

		// Serialization
		virtual string Serialize(bool NoEnc = false) const;

	};

class XMLComment : public XMLContent
	{
	public:

		XMLComment(size_t ElementPosition = 0,const char* ht = "");

		XMLComment(size_t ElementPosition,const wchar_t* ht);

		// Serialization
		virtual string Serialize(bool NoEnc = false) const;

	};


class XMLHeader
	{
	private:

		XMLVariable version;
		XMLVariable encoding;
		XMLVariable standalone;

	public:

		XMLHeader();

		XMLVariable& GetVersion() { return version; }
		XMLVariable& GetEncoding() { return encoding; }
		XMLVariable& GetStandalone() { return standalone; }
		
		const XMLVariable& GetVersion() const {return version;}
		const XMLVariable& GetEncoding() const  {return encoding;}
		const XMLVariable& GetStandalone() const  {return standalone;}

		// Serialization
		string Serialize() const;

		void Default();

	};


class XMLElement
	{
	private:


		string el = "e";
		vector<shared_ptr<XMLElement>> children;
		vector<shared_ptr<XMLVariable>> variables;
		vector<shared_ptr<XMLContent>> contents;
		vector<shared_ptr<XMLComment>> comments;
		vector<shared_ptr<XMLCData>> cdatas;
		unsigned long long param = 0;
		XMLId parent = 0;
		XMLId id;
		
		static void CloneMirror(XMLElement& to,const XMLElement& from);

	public:

		XMLElement();
		XMLElement(const char*);
		XMLElement(const XMLElement&);
		XMLElement(XMLElement&&);

		XMLElement Mirror() const;

		const vector<shared_ptr<XMLComment>>& XMLElement::GetComments() const { return comments; }
		const vector<shared_ptr<XMLElement>>& XMLElement::GetChildren()  const { return children; }
		const vector<shared_ptr<XMLVariable>>& XMLElement::GetVariables() const { return variables; }
		const vector<shared_ptr<XMLCData>>& XMLElement::GetCDatas()  const { return cdatas; }
		const vector<shared_ptr<XMLContent>>& XMLElement::GetContents()  const { return contents; }

		 vector<shared_ptr<XMLComment>>& XMLElement::GetComments() { return comments; }
		 vector<shared_ptr<XMLElement>>& XMLElement::GetChildren()  { return children; }
		 vector<shared_ptr<XMLVariable>>& XMLElement::GetVariables() { return variables; }
		 vector<shared_ptr<XMLCData>>& XMLElement::GetCDatas()  { return cdatas; }
		 vector<shared_ptr<XMLContent>>& XMLElement::GetContents()  { return contents; }

		 size_t GetChildrenNum() const { return children.size(); }

		// Operators
		bool operator==(const XMLElement& t) const;
		bool operator <(const XMLElement& x) const;
		XMLElement& operator =(const char*);
		
		// Gets
		const XMLElement& operator [](size_t idx) const;
		XMLElement& operator [](size_t idx);
		XMLElement& operator [](const char* elm);

		const string& v(size_t idx) const;
		const string& v(const char* nn);
		string vd(const char*nn,const char*def = 0);
		string vd(const char*nn,const char*def = 0) const;

		string Content() const;
		string GetContent() const;
		XMLVariable& vv(const char* nn);
		unsigned long long GetElementParam() const;
		const string& GetElementName() const;
		void GetAllChildren(vector<shared_ptr<XMLElement>>& ch) const;
		shared_ptr<XMLElement> GetParent(shared_ptr<XMLElement> r) const;
		XMLElement* GetParent(XMLElement* r) const;
		size_t GetElementIndex(const XMLElement& e) const;

		// Sets
		void SetElementName(const char* x);
		void SetElementName(const wchar_t* x);
		void SetElementParam(unsigned long long p);
		void SortElements(std::function<bool(const shared_ptr<XMLElement>&e1,const shared_ptr<XMLElement>&e2)>);
		void SortVariables(std::function<bool(const shared_ptr<XMLVariable>&e1,const shared_ptr<XMLVariable>&e2)>);
		XML_ERROR XMLElement::MoveElement(size_t i,size_t y);
		XMLVariable& SetValue(const char* vn,const char* vp);
		XMLContent& SetContent(const char* vp);

		// Find
		shared_ptr<XMLElement> FindElementZ(const char* n,bool ForceCreate = false);
		shared_ptr<XMLVariable> FindVariableZ(const char* n,bool ForceCreate = false,const char* defv = "");
		shared_ptr<XMLVariable> FindVariable(const char* n) const;

		// Inserts
		shared_ptr<XMLElement> InsertElement(size_t y,const XMLElement& x);
		shared_ptr<XMLElement> InsertElement(size_t y,XMLElement&& x);
		XMLElement& AddElement(const XMLElement& c);
		XMLElement& AddElement(XMLElement&& c);
		XMLElement& AddElement(const char* n = "");
		void AddElements(const std::initializer_list<string>& s);
		void SetVariables(const std::initializer_list<string>& s);
		XMLVariable& AddVariable(const char* vn = "n",const char* vv = "v",size_t p = -1);
		XMLVariable& AddVariable(const XMLVariable& v,size_t p = -1);
		XMLContent& AddContent(const char* pv,size_t ep,size_t p = -1);
		XMLComment& AddComment(const char* pv,size_t ep,size_t p = -1);
		XMLCData& AddCData(const char* pv,size_t ep,size_t p = -1);

		// Removals
		size_t RemoveAllElements();
		size_t RemoveElement(size_t i);

		shared_ptr<XMLElement> RemoveElementAndKeep(size_t i) throw(XML_ERROR);

		void clear();

		// Variables 
		size_t RemoveAllVariables();

		size_t RemoveVariable(size_t i);

		shared_ptr<XMLVariable> RemoveVariableAndKeep(size_t i) throw (XML_ERROR);

		string EorE(const string& s,bool N) const;
		string Serialize(bool NoEnc = false,size_t deep = 0) const;

		void Serialize(string& v,bool NoEnc = false,size_t deep = 0) const;

	};

class XML
	{
	private:

		bool UnicodeFile  = false;

		string fname;
		XMLHeader hdr;
		XMLDocType doctype;
		vector<shared_ptr<XMLComment>> hdrcomments;
		XMLElement root;


	public:

		// Constructors
		XML();

		XML(const char* file);

		XML(const wchar_t* file);

		XML(const char* mem,size_t l);


		void operator =(const char* d);

		// Savers
		size_t SaveFP(FILE* fp) const;


		XML_ERROR Save() const;
	
		
		XML_ERROR Save(const char* f) const;

		XML_ERROR Save(const wchar_t* f) const;


		// Loaders
		XML_PARSE ParseFile(FILE* fp);

		XML_PARSE Load(const char* f);

		XML_PARSE Load(const wchar_t* f);
		XML_PARSE Parse(const char* m,size_t len);

		// Gets
		XMLElement& XML::GetRootElement();
		XMLHeader& XML::GetHeader();
		size_t XML::MemoryUsage();

		// Sets
		void SetRootElement(XMLElement& newroot);
		void SetHeader(const XMLHeader& h);


		void Clear();



		void Version(XML_VERSION_INFO* x);




		// ser
		string Serialize(bool NoEnc = false) const;

	};


};





