#include "stdafx.h"
#include "NIPJob.h"
#include "StringTable.h"

//
// NIPJobParam
//
NIPJobParam::NIPJobParam(wstring strName, wstring strData, wstring strType, wstring strPathParent)
{
	Init();

	m_strName = strName;
	m_strValue = strData;
	m_strType = strType;

	if (strType == L"int") m_nType = MPT_INT;
	else if (strType == L"float") m_nType = MPT_FLOAT;
	else if (strType == L"bool") m_nType = MPT_BOOL;
	else if (strType == L"string") m_nType = MPT_STRING;
	else if (strType == L"combo") m_nType = MPT_COMBO;
	else m_nType = MPT_NONE;

	m_bGroup = false;

	if (m_nType == MPT_BOOL) {
		AddComboValue(L"true");
		AddComboValue(L"false");
	}

	SetPath(strPathParent);
}

wstring NIPJobParam::GetDesc()
{
	wstring strDesc;
	if (m_bGroup) {
		strDesc += m_strName + L" { ";
		size_t nCount = m_listParam.size();
		for (auto pParam : m_listParam) {
			strDesc += pParam->GetDesc();
			if (--nCount > 0) {
				strDesc += L" , ";
			}
		}
		strDesc += L" } ";
	}
	else {
		strDesc += m_strName + L" : " + m_strValue;
	}

	return strDesc;
}

//
// NIPJobProcess
// 
wstring NIPJobProcess::GetDesc()
{
	wstring strDesc;
	strDesc += L" [" + m_strName + L"] { " + GetParamDesc() + L" }";

	return strDesc;
}

wstring NIPJobProcess::GetParamDesc()
{
	//	char *szDesc[256];
	//	sprintf_s(szDesc, "[%s] ", m_strName.c_str());
	wstring strDesc;
	size_t nCount = m_listParam.size();
	for (auto pParam : m_listParam) {
		strDesc += pParam->GetDesc();
		if (--nCount > 0) {
			strDesc += L" , ";
		}
	}

	nCount = m_listSubProcess.size();
	for (auto pSubProcess: m_listSubProcess) {
		strDesc += pSubProcess->GetDesc();
		if (--nCount > 0) {
			strDesc += L" , ";
		}
	}
	
	return strDesc;
}
