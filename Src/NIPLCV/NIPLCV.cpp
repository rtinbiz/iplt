// NIPLCV.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "NIPLCV.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

NIPLCV *NIPLCV::pThis = 0x00;

NIPLCV *NIPLCV::GetInstance(BOOL bNew)
{
	if(bNew) {
		return new NIPLCV();
	}

	if(pThis == 0x00) {
		pThis = new NIPLCV();
	}

	return pThis;
}

void NIPLCV::ReleaseInstance(NIPLCV *pThat)
{
	if(pThat != 0x00) {
		delete pThat;
	}
	else if(pThis != 0x00) {
		delete pThis;
		pThis = 0x00;
	}
}

NIPLCV::NIPLCV() : NIPL()
{

}

NIPLCV::~NIPLCV()
{
}

/*
NIPL_CV_METHOD_IMPL(DetectDefect)
{
	VERIFY_PARAMS();

	NIPLParam_DetectDefect *pParam_DetectDefect = (NIPLParam_DetectDefect *)pInput->m_pParam;

	NIPL_ERR nErr = NIPL_ERR_SUCCESS;
	if(CHECK_EMPTY_IMAGE(pInput->m_dImg)) {
		nErr = LoadImage(pInput->m_strImgPath, CV_LOAD_IMAGE_GRAYSCALE, pInput->m_dImg);
		if(NIPL_FAIL(nErr)) {
			return nErr;
		}
	}

	int nImgSizeY = pInput->m_dImg.rows;
	int nImgSizeX =pInput->m_dImg.cols;

	// Convert type to double
	Mat dInputImg, dOutputImg;
	pInput->m_dImg.convertTo(dOutputImg, CV_32FC1);

	// Scaling to 0 ~ 1.
	double nMax;
	minMaxLoc(dOutputImg, 0, &nMax);
	dOutputImg = dOutputImg / nMax;

	//
	// Preprocessing
	//
	NIPLInput dInput;
	NIPLOutput dOutput;
	NIPLParam &dParam_Smoothing = pParam_DetectDefect->m_dParam_Smoothing;
	dInput.m_dImg = dOutputImg;
	dInput.m_pParam = &dParam_Smoothing;
	nErr = Smoothing(&dInput, &dOutput);
	if(NIPL_FAIL(nErr)) {
		return nErr;
	}
	if(!NIPL_PASS(nErr)) {
		dOutputImg = dOutput.m_dImg;
	}

	// Set Mask
	Mat dMask;
	if(CHECK_EMPTY_IMAGE(pInput->m_dMask)) {
		dMask = Mat::ones(nImgSizeY, nImgSizeX, CV_8UC1);
	}
	else {
		pInput->m_dMask.copyTo(dMask);
	}

	//
	// Apply Exclude Region to Mask
	//
	NIPLParam &dParam_ExcludeRegion = pParam_DetectDefect->m_dParam_ExcludeRegion;
	dInput.m_dMask = dMask;
	dInput.m_pParam = &dParam_ExcludeRegion;
	nErr = ApplyExcludingRegionToMask(&dInput, &dOutput);
	if(NIPL_FAIL(nErr)) {
		return nErr;
	}
	dMask = dOutput.m_dMask;

	//
	// Detect Fixture
	//
	NIPLParam &dParam_DetectFixture = pParam_DetectDefect->m_dParam_DetectFixture;
	dInput.m_dImg = dOutputImg;
	dInput.m_pParam = &dParam_DetectFixture;
	dInput.m_dMask = dMask;
	nErr = DetectFixture(&dInput, &dOutput);
	if(NIPL_FAIL(nErr)) {
		return nErr;
	}
	if(!NIPL_PASS(nErr)) {
		Mat dFixtureImg = dOutput.m_dImg;
		dMask = ~(~dMask | dFixtureImg);			// Fixture 를 검사대상에서 제외하도록 Mask 에 반영함.
	}

	//
	// Elimination Background slope
	//
	NIPLParam &dParam_FitBackground = pParam_DetectDefect->m_dParam_FitBackground;
	dInput.m_dImg = dOutputImg;
	dInput.m_pParam = &dParam_FitBackground;
	dInput.m_dMask = dMask;
	nErr = FitBackground(&dInput, &dOutput);
	if(NIPL_FAIL(nErr)) {
		return nErr;
	}
	if(!NIPL_PASS(nErr)) {
		Mat dBackgroundImg = dOutput.m_dImg;
		float nMean = (float)mean(dBackgroundImg)[0];
		dOutputImg = dOutputImg - dBackgroundImg + nMean;
	}

	//
	// Binarize
	//
	NIPLParam &dParam_Binarize = pParam_DetectDefect->m_dParam_Binarize;
	dInput.m_dImg = dOutputImg;
	dInput.m_pParam = &dParam_Binarize;
	dInput.m_dMask = dMask;
	nErr = Binarize(&dInput, &dOutput);
	if(NIPL_FAIL(nErr)) {
		return nErr;
	}
	dOutputImg = dOutput.m_dImg;

	//
	// Elimination Noise
	//
	NIPLParam &dParam_EliminateNoise = pParam_DetectDefect->m_dParam_EliminateNoise;
	dInput.m_dImg = dOutputImg;
	dInput.m_pParam = &dParam_EliminateNoise;
	nErr = EliminateNoise(&dInput, &dOutput);
	if(NIPL_FAIL(nErr)) {
		return nErr;
	}
	if(!NIPL_PASS(nErr)) {
		dOutputImg = dOutput.m_dImg;
	}

	//
	// Analyze Shape
	//
	NIPLParam &dParam_AnalyzeShape = pParam_DetectDefect->m_dParam_AnalyzeShape;
	dInput.m_dImg = dOutputImg;
	dInput.m_pParam = &dParam_AnalyzeShape;
	nErr = AnalyzeShape(&dInput, &dOutput);
	if(NIPL_FAIL(nErr)) {
		return nErr;
	}
	if(!NIPL_PASS(nErr)) {
		dOutputImg = dOutput.m_dImg;
	}

	pOutput->m_dImg = dOutputImg;
	return NIPL_ERR_SUCCESS;
}
*/
/*
NIPL_CV_METHOD_IMPL(ApplyExcludingRegionToMask)
{
	VERIFY_PARAMS();
	NIPLParam_ExcludeRegion *pParam_ExcludeRegion = (NIPLParam_ExcludeRegion *)pInput->m_pParam;

	Mat dMask = pInput->m_dMask;
	vector<Rect> &listExcludeRegion = pParam_ExcludeRegion->m_listExcludeRegion;

	int nMaskSizeY = dMask.rows;
	int nMaskSizeX = dMask.cols;

	Rect dRect;
	for(int i = 0; i < (int)listExcludeRegion.size(); i++) {
		dRect = listExcludeRegion[i];
		if(dRect.x < 0) dRect.x = 0;
		if(dRect.y < 0) dRect.y = 0;
		if(dRect.x + dRect.width > nMaskSizeX) dRect.width = nMaskSizeX - dRect.x;
		if(dRect.y + dRect.height > nMaskSizeY) dRect.height = nMaskSizeY - dRect.y;

		Mat dRegion(dMask, dRect);
		dRegion = 0;
	}
	pOutput->m_dMask = dMask;

	return NIPL_ERR_SUCCESS;
}
*/

NIPL_CV_METHOD_IMPL(FitBackground)
{
	VERIFY_PARAMS();
	NIPLParam_FitBackground *pParam_FitBackground = (NIPLParam_FitBackground *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	Mat dMask = pInput->m_dMask;

	int nBlockCount = pParam_FitBackground->m_nBlockCount;
	int nDegree = pParam_FitBackground->m_nDegree;
	float nThreshold = pParam_FitBackground->m_nThreshold;
	bool bSubtrackFromImage = pParam_FitBackground->m_bSubtrackFromImage;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if (CHECK_EMPTY_IMAGE(dMask)) {
		dMask = Mat::ones(nImgSizeY, nImgSizeX, CV_8UC1);
	}

	Mat dBlockMean = Mat::zeros(nBlockCount*nBlockCount, 1, CV_32FC1);
	Mat dIndexX = Mat::zeros(nBlockCount*nBlockCount, 1, CV_32FC1);
	Mat dIndexY = Mat::zeros(nBlockCount*nBlockCount, 1, CV_32FC1);

	int nStartX;
	int nEndX;
	int nStartY;
	int nEndY;
	int nBlockSizeX;
	int nBlockSizeY;
	int k = 0;
	for(int i=1; i<=nBlockCount; i++) {
		for(int j=1; j<=nBlockCount; j++) {
			nStartX = cvRound((j-1)*nImgSizeX/nBlockCount);
			nEndX = cvRound(j*nImgSizeX/nBlockCount)-1;
			nStartY = cvRound((i-1)*nImgSizeY/nBlockCount);
			nEndY = cvRound(i*nImgSizeY/nBlockCount)-1;
			nBlockSizeX = nEndX - nStartX + 1;
			nBlockSizeY = nEndY - nStartY + 1;

			Mat dMaskBlock(dMask, Rect(nStartX, nStartY, nBlockSizeX, nBlockSizeY));
			if(countNonZero(dMaskBlock) == nBlockSizeX*nBlockSizeY) {	// 0 이 없을 경우, 즉 블럭 내의 Mask가 모두 1인 경우  
				Mat dImgBlock(dImg, Rect(nStartX, nStartY, nEndX-nStartX+1, nEndY-nStartY+1));
				dBlockMean.at<FLOAT>(k) = (float)mean(dImgBlock)[0];
				dIndexX.at<FLOAT>(k) = (float)cvRound((nStartX+nEndX)/2);
				dIndexY.at<FLOAT>(k) = (float)cvRound((nStartY+nEndY)/2);
				k = k+1;
			}
		}
	}

	if(k < 3) {
		return NIPL_ERR_PASS;
	}

	if(k < nBlockCount*nBlockCount) {
		dBlockMean = dBlockMean(Range(0, k), Range::all());
		dIndexX = dIndexX(Range(0, k), Range::all());
		dIndexY = dIndexY(Range(0, k), Range::all());
	}
	nBlockCount = k;

	Mat dA = Mat::zeros(nBlockCount, (nDegree+1)*(nDegree+2)/2, CV_32FC1);

	Mat dTemp1;
	Mat dTemp2;
	k=0;
	for(int px=0; px<=nDegree; px++) {
		for(int py=0; py<=nDegree-px; py++) {
			pow(dIndexX, px, dTemp1);
			pow(dIndexY, py, dTemp2);

			dA.col(k) = dTemp1.mul(dTemp2);
			k = k+1;
		}
	}

	// find outliner block
	Mat dTempA;
	dA.copyTo(dTempA);
	Mat dTempBlockMean;
	dBlockMean.copyTo(dTempBlockMean);

	Mat dMeanError = Mat::zeros(nBlockCount, 1, CV_32FC1);
	for(k=0; k<nBlockCount; k++) {
		if(k>0) {
			dA.row(k-1).copyTo(dTempA.row(k-1));                            
			dBlockMean.row(k-1).copyTo(dTempBlockMean.row(k-1));
		}
		dTempA.row(k) = 0;
		dTempBlockMean.at<FLOAT>(k) = 0;
		Mat dCoef = dTempA.inv(DECOMP_SVD) * dTempBlockMean;
		dMeanError.at<FLOAT>(k) = (float)mean(abs(dTempBlockMean - dTempA * dCoef))[0];
	}

	Scalar dMean;
	Scalar dStd;
	meanStdDev(dMeanError, dMean, dStd);

	Mat dOulierBlock = dMeanError<(dMean[0] - nThreshold*dStd[0]);
	morphologyEx(dOulierBlock, dOulierBlock, MORPH_CLOSE, Mat::ones(3, 3, CV_8UC1));

	// remove outlier block
	for(k=0; k<nBlockCount; k++) {
		if(dOulierBlock.at<UINT8>(k) == 1) {
			dA.row(k) = 0;
			dBlockMean.at<FLOAT>(k) = 0;
		}
	}
	Mat dCoef = dA.inv(DECOMP_SVD) * dBlockMean;

	// get result fitting surface
	dA = Mat::zeros(nImgSizeX*nImgSizeY, (nDegree+1)*(nDegree+2)/2, CV_32FC1);
	dIndexX = Mat::zeros(nImgSizeY, nImgSizeX, CV_32FC1);
	dIndexY = Mat::zeros(nImgSizeY, nImgSizeX, CV_32FC1);
	Mat dRow = Mat::zeros(1, nImgSizeX, CV_32FC1);
	Mat dCol = Mat::zeros(nImgSizeY, 1, CV_32FC1);
	for(int i=0; i<max(nImgSizeX, nImgSizeY); i++) {
		if(i<nImgSizeX) dRow.at<FLOAT>(i) = (float)i;
		if(i<nImgSizeY) dCol.at<FLOAT>(i) = (float)i;
	}

	for(int i=0; i<max(nImgSizeX, nImgSizeY); i++) {
		if(i<nImgSizeX) dCol.copyTo(dIndexY.col(i));
		if(i<nImgSizeY) dRow.copyTo(dIndexX.row(i));
	}

	dIndexX = dIndexX.reshape(0, nImgSizeX*nImgSizeY);
	dIndexY = dIndexY.reshape(0, nImgSizeX*nImgSizeY);

	int nIndex = 0;
	k=0;
	for(int px=0; px<=nDegree; px++) {
		for(int py=0; py<=nDegree-px; py++) {
			pow(dIndexX, px, dTemp1);
			pow(dIndexY, py, dTemp2);
			dA.col(k) = dTemp1.mul(dTemp2);

			k = k+1;
		}
	}

	Mat dBackground = Mat(dA*dCoef).reshape(0, nImgSizeY);
	if(bSubtrackFromImage) {
		meanStdDev(dImg, dMean, dStd);

		Mat dTemp;
		dImg.convertTo(dTemp, dBackground.type());
		dTemp =	dTemp - dBackground + dMean[0];
		dTemp.convertTo(pOutput->m_dImg, dImg.type());
	}
	else {
		pOutput->m_dImg = dBackground;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_CV_METHOD_IMPL(Binarize)
{
	VERIFY_PARAMS();
	NIPLParam_Binarize *pParam_Binarize = (NIPLParam_Binarize *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	Mat dMask = pInput->m_dMask;
	int nMethod = pParam_Binarize->m_nMethod;
	float nThreshold = pParam_Binarize->m_nThreshold;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if ((dImg.depth() != CV_8U && dImg.depth() != CV_32F) || dImg.channels() != 1) {
		return NIPL_ERR_INVALID_IMAGE_TYPE;
	}

	BOOL bCheckUpper = FALSE;
	BOOL bCheckLower = FALSE;
	if(nMethod & NIPLParam_Binarize::METHOD_UPPER) {
		bCheckUpper = TRUE;
	}
	if(nMethod & NIPLParam_Binarize::METHOD_LOWER) {
		bCheckLower = TRUE;
	}
	BOOL bExistMask = !CHECK_EMPTY_IMAGE(dMask);

//	DebugPrintImageProperty(L"Binarize Input", dImg);

	Mat dTempImg;
	dImg.copyTo(dTempImg);

	// change first to 8 bit image.
	if (dTempImg.depth() == CV_32F) {
		dTempImg *= 255.f;
		dTempImg.convertTo(dTempImg, CV_8U);
	}

	Scalar dMean;
	Scalar dStd;
	if (bExistMask) {
		meanStdDev(dTempImg, dMean, dStd, dMask);
	}
	else {
		meanStdDev(dTempImg, dMean, dStd);
	}

	float nMean = (float)dMean[0];
	float nStd = (float)dStd[0];

//	DebugPrintImageProperty(L"Binarize Converted", dTempImg);

	Mat dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	if (bCheckUpper) {
		Mat dBinImg;
		threshold(dTempImg, dBinImg, nMean + nThreshold * nStd, 1, CV_THRESH_BINARY);
		dOutputImg |= dBinImg;
	}
	if (bCheckLower) {
		Mat dBinImg;
		threshold(dTempImg, dBinImg, nMean - nThreshold * nStd, 1, CV_THRESH_BINARY_INV);
		dOutputImg |= dBinImg;
	}

	if (bExistMask) {
		dOutputImg = dOutputImg.mul(dMask);
	}

//	DebugPrintImageProperty(L"Binarize Output", dOutputImg);

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

NIPL_CV_METHOD_IMPL(EliminateNoise)
{
	VERIFY_PARAMS();
	NIPLParam_EliminateNoise *pParam_EliminateNoise = (NIPLParam_EliminateNoise *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	int nMethod = pParam_EliminateNoise->m_nMethod;
	int nMorphologyFilterSize = pParam_EliminateNoise->m_nMorphologyFilterSize;
	int nMedianFilterSize = pParam_EliminateNoise->m_nMedianFilterSize;
	int nMinSize = pParam_EliminateNoise->m_nMinSize;
	int nMaxSize = pParam_EliminateNoise->m_nMaxSize;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if(nMethod == NIPLParam_EliminateNoise::METHOD_NONE) {
		return NIPL_ERR_PASS;
	}

	Mat dOutputImg;
	dImg.copyTo(dOutputImg);

	if(nMethod & NIPLParam_EliminateNoise::METHOD_MORPHOLOGY_OPS) {
		if(nMorphologyFilterSize > 0) {
			Mat dKernel = getStructuringElement(MORPH_ELLIPSE, Size(nMorphologyFilterSize, nMorphologyFilterSize));
			morphologyEx(dImg, dOutputImg, MORPH_CLOSE, dKernel);
			morphologyEx(dOutputImg, dOutputImg, MORPH_OPEN, dKernel);
			pOutput->m_dImg = dOutputImg;
		}
	}

	if(nMethod & NIPLParam_EliminateNoise::METHOD_MEDIAN_FILTER) {
		if(nMedianFilterSize> 0) {
			nMedianFilterSize = (nMedianFilterSize/2)*2 + 1;		// filter size should be odd!
			medianBlur(dOutputImg, dOutputImg, nMedianFilterSize);
			pOutput->m_dImg = dOutputImg;
		}
	}

	if(nMethod & NIPLParam_EliminateNoise::METHOD_SIZE) {
		if(nMinSize > 0) {
			NIPLInput dInput;
			NIPLOutput dOutput;
			NIPLParam_FindBlob dParam_FindBlob;

			dInput.m_dImg = dOutputImg;
			dParam_FindBlob.m_nMinSize = (float)nMinSize;
			dParam_FindBlob.m_nMaxSize = (float)nMaxSize;
			dParam_FindBlob.m_bFindInRange = TRUE;
			dInput.m_pParam = &dParam_FindBlob;
			NIPL_ERR nErr = FindBlob(&dInput, &dOutput);
			if(NIPL_FAIL(nErr)) {
				return nErr;
			}		

			dOutputImg = dOutput.m_dImg;
			pOutput->m_dImg = dOutputImg;
		}
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_CV_METHOD_IMPL(FindBlob)
{
	VERIFY_PARAMS();
	NIPLParam_FindBlob *pParam_FindBlob = (NIPLParam_FindBlob *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	float nMinSize = pParam_FindBlob->m_nMinSize;
	float nMaxSize = pParam_FindBlob->m_nMaxSize;
	BOOL bFindInRange = pParam_FindBlob->m_bFindInRange;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if (nMaxSize == 0) {
		nMaxSize = (float)nImgSizeX * nImgSizeY;
	}

	// Check the type of Image
	if(dImg.depth() != CV_8U || dImg.channels() != 1) {
		return NIPL_ERR_INVALID_IMAGE_TYPE;
	}

	// Find all contours
	Mat dTempImg;
	dImg.copyTo(dTempImg);
	vector<vector<Point>> dContours;
	CHECK_EXCEPTION(findContours(dTempImg, dContours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE));

	NIPLResult_FindBlob *pResult_FindBlob = new NIPLResult_FindBlob();
	if (pResult_FindBlob == nullptr) return NIPL_ERR_OUT_OF_MEMORY;

	Rect dBoundRect;
	Mat dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	bool bFound;
	for(int i = 0; i < (int)dContours.size(); i++) {
		dBoundRect = boundingRect(Mat(dContours[i]));
		Mat dContourImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
		drawContours(dContourImg, dContours, i, CV_RGB(255, 255, 255), CV_FILLED);
		Mat dBlob(dContourImg, dBoundRect);
		int nAreaSize = countNonZero(dBlob);

		bFound = false;
		// Only draw the blobs smaller or bigger than the given sizes
		if (bFindInRange) {
			if (nAreaSize >= (double)nMinSize && nAreaSize <= (double)nMaxSize) {
				bFound = true;
			}
		}
		else {
			if ((nAreaSize > 0 && nAreaSize < (double)nMinSize) || nAreaSize >(double)nMaxSize) {
				bFound = true;
			}
		}

		if (bFound) {
			dOutputImg |= dContourImg;

			NIPLBlob dBlob(dBoundRect, nAreaSize);
			pResult_FindBlob->m_listBlob.push_back(dBlob);
		}
	}

	pOutput->m_dImg = dOutputImg;

	if (pResult_FindBlob->m_listBlob.size() > 0) {
		pOutput->m_pResult = pResult_FindBlob;
	}
	else {
		delete pResult_FindBlob;
	}
	
	return NIPL_ERR_SUCCESS;
}

/*
NIPL_CV_METHOD_IMPL(FindLine)
{
	VERIFY_PARAMS();
	NIPLParam_FindLine *pParam_FindLine = (NIPLParam_FindLine *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	Mat dMask = pInput->m_dMask;
	float nCannyThresholdLow = pParam_FindLine->m_nCannyLowerThreshold;
	float nCannyThresholdHigh = pParam_FindLine->m_nCannyUpperThreshold;
	int nLineThickness = pParam_FindLine->m_nLineThickness;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	Mat dOutputImg;
	((Mat)(dImg * 255)).convertTo(dOutputImg, CV_8UC1);
	if (nCannyThresholdLow == 0.f && nCannyThresholdHigh == 0.f) {
		Mat dTemp;
		nCannyThresholdHigh = (float)threshold(dOutputImg, dTemp, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
		nCannyThresholdLow = nCannyThresholdHigh / 2.5f;
	}

	Canny(dOutputImg, dOutputImg, nCannyThresholdLow, nCannyThresholdHigh);

	if (!CHECK_EMPTY_IMAGE(dMask)) {
		dOutputImg = dOutputImg.mul(dMask);
	}

	Mat dContours;
	vector<Vec4i> listLines;
	double nHoughRho = 1;				// delta_rho resolution
	double nHoughTheta = CV_PI / 180;	// delta_theta resolution
	int nHoughThreshold = (int)(min(nImgSizeX, nImgSizeY) * 0.2);
	double nMinLineLength = 3;
	double nMaxLineGap = min(nImgSizeX, nImgSizeY) * 0.5;

	CHECK_EXCEPTION(HoughLinesP(dOutputImg, listLines, nHoughRho, nHoughTheta, nHoughThreshold, nMinLineLength, nMaxLineGap))

		double nMinLineHeight = nImgSizeY * 0.8;
	double nMinLineWidth = nImgSizeX * 0.8;
	double nLineLength = 0;
	dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	for (int i = 0; i < (int)listLines.size(); i++) {
		Vec4i dLine = listLines[i];
		Point dPoint1(dLine[0], dLine[1]);
		Point dPoint2(dLine[2], dLine[3]);

		nLineLength = sqrt(pow(dPoint1.x - dPoint2.x, 2.0) + pow(dPoint1.y - dPoint2.y, 2.0));

		Point dEndPoint1;
		Point dEndPoint2;
		if (dPoint1.x == dPoint2.x) {
			if (nLineLength < nMinLineHeight) {
				continue;
			}

			dEndPoint1.y = 0;
			dEndPoint2.y = nImgSizeY - 1;
			dEndPoint1.x = dPoint1.x;
			dEndPoint2.x = dPoint2.x;
		}
		else {
			double nA = ((double)(dPoint2.y - dPoint1.y)) / (dPoint2.x - dPoint1.x);
			double nB = dPoint1.y - nA * dPoint1.x;

			double nAngle = atan(nA);
			nAngle = abs(nAngle / CV_PI * 180);

			if (nAngle <= 2) {
				if (nLineLength < nMinLineWidth) {
					continue;
				}

				dEndPoint1.x = 0;
				dEndPoint2.x = nImgSizeX - 1;
				dEndPoint1.y = (int)(nA * dEndPoint1.x + nB);
				dEndPoint2.y = (int)(nA * dEndPoint2.x + nB);
			}
			else if (nAngle >= 88) {
				if (nLineLength < nMinLineHeight) {
					continue;
				}

				dEndPoint1.y = 0;
				dEndPoint2.y = nImgSizeY - 1;
				dEndPoint1.x = (int)((dEndPoint1.y - nB) / nA);
				dEndPoint2.x = (int)((dEndPoint2.y - nB) / nA);
			}
		}

		line(dOutputImg, dEndPoint1, dEndPoint2, CV_RGB(255, 255, 255), nLineThickness);
	}

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}
*/

NIPL_CV_METHOD_IMPL(FindLine)
{
	VERIFY_PARAMS();
	NIPLParam_FindLine *pParam_FindLine = (NIPLParam_FindLine *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	Mat dMask = pInput->m_dMask;
	int nHoughThreshold = pParam_FindLine->m_nHoughThreshold;
	int nLineMinLength = pParam_FindLine->m_nLineMinLength;
	int nLineMaxLength = pParam_FindLine->m_nLineMaxLength;
	int nLineMinGap = pParam_FindLine->m_nLineMinGap;
	int nLineThickness = pParam_FindLine->m_nLineThickness;

	float nCannyThresholdLow = pParam_FindLine->m_nCannyLowerThreshold;
	float nCannyThresholdHigh = pParam_FindLine->m_nCannyUpperThreshold;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	Mat dOutputImg;
	((Mat)(dImg * 255)).convertTo(dOutputImg, CV_8UC1);
	if (nCannyThresholdLow == 0.f && nCannyThresholdHigh == 0.f) {
		Mat dTemp;
		nCannyThresholdHigh = (float)threshold(dOutputImg, dTemp, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
		nCannyThresholdLow = nCannyThresholdHigh / 2.5f;
	}

	Canny(dOutputImg, dOutputImg, nCannyThresholdLow, nCannyThresholdHigh);

	if (!CHECK_EMPTY_IMAGE(dMask)) {
		dOutputImg = dOutputImg.mul(dMask);
	}

	Mat dContours;
	vector<Vec4i> listLines;
	double nHoughRho = 1;				// delta_rho resolution
	double nHoughTheta = CV_PI / 180;	// delta_theta resolution

	if (nHoughThreshold == 0) {
		nHoughThreshold = (int)(min(nImgSizeX, nImgSizeY) * 0.2f);
	}
	if (nLineMinLength == 0) {
		nLineMinLength = 3;
	}
	if (nLineMaxLength == 0) {
		nLineMaxLength = (int)max(nImgSizeX, nImgSizeY);
	}
	if (nLineMinGap == 0) {
		nLineMinGap = (int)(min(nImgSizeX, nImgSizeY) * 0.5f);
	}

	CHECK_EXCEPTION(HoughLinesP(dOutputImg, listLines, nHoughRho, nHoughTheta, nHoughThreshold, (double)nLineMinLength, (double)nLineMinGap))

	NIPLResult_FindLine *pResult_FindLine = new NIPLResult_FindLine();
	if (pResult_FindLine == nullptr) return NIPL_ERR_OUT_OF_MEMORY;

//	double nMinLineHeight = nImgSizeY * 0.8;
//	double nMinLineWidth = nImgSizeX * 0.8;
	float nLineLength = 0.f;
	float nLineAngle = 0.f;
	dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	for (int i = 0; i < (int)listLines.size(); i++) {
		Vec4i vecLine = listLines[i];
		Point dPoint1(vecLine[0], vecLine[1]);
		Point dPoint2(vecLine[2], vecLine[3]);

		nLineLength = sqrtf(powf((float)(dPoint1.x - dPoint2.x), 2.f) + powf(float(dPoint1.y - dPoint2.y), 2.f));
		if (nLineLength > (float)nLineMaxLength) {
			continue;
		}

		if (dPoint1.x == dPoint2.x) {
			nLineAngle = 90.f;
		}
		else {
			double nA = ((double)(-(dPoint2.y - dPoint1.y))) / (dPoint2.x - dPoint1.x);
			double nB = dPoint1.y - nA * dPoint1.x;
			nLineAngle = float(atan(nA) * 180.f / CV_PI);
//			if (nLineAngle < 0.f) nLineAngle += 180.f;
		}
/*

		Point dEndPoint1;
		Point dEndPoint2;
		if (dPoint1.x == dPoint2.x) {
			if (nLineLength < nMinLineHeight) {
				continue;
			}

			dEndPoint1.y = 0;
			dEndPoint2.y = nImgSizeY - 1;
			dEndPoint1.x = dPoint1.x;
			dEndPoint2.x = dPoint2.x;
		}
		else {
			double nA = ((double)(dPoint2.y - dPoint1.y)) / (dPoint2.x - dPoint1.x);
			double nB = dPoint1.y - nA * dPoint1.x;

			double nAngle = atan(nA);
			nAngle = abs(nAngle / CV_PI * 180);

			if (nAngle <= 2) {
				if (nLineLength < nMinLineWidth) {
					continue;
				}

				dEndPoint1.x = 0;
				dEndPoint2.x = nImgSizeX - 1;
				dEndPoint1.y = (int)(nA * dEndPoint1.x + nB);
				dEndPoint2.y = (int)(nA * dEndPoint2.x + nB);
			}
			else if (nAngle >= 88) {
				if (nLineLength < nMinLineHeight) {
					continue;
				}

				dEndPoint1.y = 0;
				dEndPoint2.y = nImgSizeY - 1;
				dEndPoint1.x = (int)((dEndPoint1.y - nB) / nA);
				dEndPoint2.x = (int)((dEndPoint2.y - nB) / nA);
			}
		}

		line(dOutputImg, dEndPoint1, dEndPoint2, CV_RGB(255, 255, 255), nLineThickness);
*/
		line(dOutputImg, dPoint1, dPoint2, CV_RGB(255, 255, 255), nLineThickness);

		NIPLLine dLine(dPoint1, dPoint2, nLineLength, nLineAngle);
		pResult_FindLine->m_listLine.push_back(dLine);
	}

	pOutput->m_dImg = dOutputImg;
	if (pResult_FindLine->m_listLine.size() > 0) {
		pOutput->m_pResult = pResult_FindLine;
	}
	else {
		delete pResult_FindLine;
	}

	return NIPL_ERR_SUCCESS;
}


NIPL_CV_METHOD_IMPL(FindCircle)
{
	VERIFY_PARAMS();
	NIPLParam_FindCircle *pParam_FindCircle = (NIPLParam_FindCircle *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	Mat dMask = pInput->m_dMask;

	float nMinCircleDistance = pParam_FindCircle->m_nMinCircleDistance;
	float nCannyUpperThreshold = pParam_FindCircle->m_nCannyUpperThreshold;
	float nCenterDetectionThreshold = pParam_FindCircle->m_nCenterDetectionThreshold;
	int nMinRadius = pParam_FindCircle->m_nMinRadius;
	int nMaxRadius = pParam_FindCircle->m_nMaxRadius;
	int nLineThickness = pParam_FindCircle->m_nLineThickness;

	bool bCheckCenterPos = pParam_FindCircle->m_bCheckCenterPos;
	int nCenterPosX = pParam_FindCircle->m_nCenterPosX;
	int nCenterPosY = pParam_FindCircle->m_nCenterPosY;
	int nCenterPosRange = pParam_FindCircle->m_nCenterPosRange;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;
	
	if (dImg.type() != CV_8UC1) {
		Mat dTemp;
		dImg.convertTo(dTemp, CV_8UC1);
		dImg = dTemp;
	}

	NIPLResult_FindBlob *pResult_FindBlob = new NIPLResult_FindBlob();

	if (nCannyUpperThreshold == 0.f) {
		Mat dTemp;
		nCannyUpperThreshold = (float)threshold(dImg, dTemp, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
		if (nCannyUpperThreshold == 0.f) {	// if it's still 0, set it to defaul value 100.
			nCannyUpperThreshold = 100.f;
		}
	}
	if (nCenterDetectionThreshold == 0.f)
	{
		nCenterDetectionThreshold = 100.f;
	}

	vector<Vec3f> dCircles;
	CHECK_EXCEPTION(HoughCircles(dImg, dCircles, CV_HOUGH_GRADIENT, 2, nMinCircleDistance, nCannyUpperThreshold, nCenterDetectionThreshold, nMinRadius, nMaxRadius));

	NIPLResult_FindCircle *pResult_FindCircle = new NIPLResult_FindCircle();
	if (pResult_FindCircle == nullptr) return NIPL_ERR_OUT_OF_MEMORY;

	Mat dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	for (int i = 0; i < (int)dCircles.size(); i++) {
		Point ptCenter(cvRound(dCircles[i][0]), cvRound(dCircles[i][1]));
		int nRadius = cvRound(dCircles[i][2]);

		if (bCheckCenterPos) {
			int nDist = cvRound(sqrt(pow(ptCenter.x - nCenterPosX, 2) + pow(ptCenter.y - nCenterPosY, 2)));
			if (nDist > nCenterPosRange) {
				continue;
			}
		}

		// circle outline
		circle(dOutputImg, ptCenter, nRadius, Scalar(255, 255, 255), nLineThickness);

		NIPLCircle dCircle(ptCenter, nRadius);
		pResult_FindCircle->m_listCircle.push_back(dCircle);
	}

	pOutput->m_dImg = dOutputImg;
	if (pResult_FindCircle->m_listCircle.size() > 0) {
		pOutput->m_pResult = pResult_FindCircle;
	}
	else {
		delete pResult_FindCircle;
	}

	return NIPL_ERR_SUCCESS;
}

NIPL_CV_METHOD_IMPL(SetCircleROI)
{
	VERIFY_PARAMS();
	NIPLParam_SetCircleROI *pParam_SetCircleROI = (NIPLParam_SetCircleROI *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;

	int nCenterPosX = pParam_SetCircleROI->m_nCenterPosX;
	int nCenterPosY = pParam_SetCircleROI->m_nCenterPosY;
	int nMinRadius = pParam_SetCircleROI->m_nMinRadius;
	int nMaxRadius = pParam_SetCircleROI->m_nMaxRadius;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if (nMinRadius > nMaxRadius) {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}
	if (nCenterPosX - nMaxRadius < 0 || nCenterPosX + nMaxRadius >= nImgSizeX) {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}
	if (nCenterPosY - nMaxRadius < 0 || nCenterPosY + nMaxRadius >= nImgSizeY) {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}

	Mat dOutputImg;
	dImg.copyTo(dOutputImg);
	float nDistance;
	for (int nY = 0; nY < nImgSizeY; nY++) {
		for (int nX = 0; nX < nImgSizeX; nX++) {
			nDistance = sqrtf(powf((float)(nCenterPosX - nX), 2.f) + powf(float(nCenterPosY - nY), 2.f));
			if (nDistance < nMinRadius || nDistance > nMaxRadius) {
				dOutputImg.at<UINT8>(nY, nX) = 0;
			}
		}
	}

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

NIPL_CV_METHOD_IMPL(Circle2Rect)
{
	VERIFY_PARAMS();
	NIPLParam_Circle2Rect *pParam_Circle2Rect = (NIPLParam_Circle2Rect *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;

	int nCenterPosX = pParam_Circle2Rect->m_nCenterPosX;
	int nCenterPosY = pParam_Circle2Rect->m_nCenterPosY;
	int nMinRadius = pParam_Circle2Rect->m_nMinRadius;
	int nMaxRadius = pParam_Circle2Rect->m_nMaxRadius;
	float nAngleStep = pParam_Circle2Rect->m_nAngleStep;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	if (nMinRadius > nMaxRadius) {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}
	if (nCenterPosX - nMaxRadius < 0 || nCenterPosX + nMaxRadius >= nImgSizeX) {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}
	if (nCenterPosY - nMaxRadius < 0 || nCenterPosY + nMaxRadius >= nImgSizeY) {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}
	if (nAngleStep <= 0.f) {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}

	int nRectImgSizeX = (int)(360.f / nAngleStep);
	int nRectImgSizeY = nMaxRadius - nMinRadius + 1;

	Mat dOutputImg = Mat::zeros(nRectImgSizeY, nRectImgSizeX, dImg.type());

	int nDepth = dOutputImg.depth();
	int nChannels = dOutputImg.channels();

	for (int nRectImgPosX = 0; nRectImgPosX < nRectImgSizeX; nRectImgPosX++) {
		float nAngle = nAngleStep * nRectImgPosX;
		for (int nRectImgPosY = 0; nRectImgPosY < nRectImgSizeY; nRectImgPosY++) {
			int nRadius = nMinRadius + nRectImgPosY;

			int nImgPosX = cvRound(nCenterPosX + nRadius * cos(nAngle * CV_PI / 180));
			int nImgPosY = cvRound(nCenterPosY + nRadius * sin(nAngle * CV_PI / 180));

			if (nChannels == 1) {
				switch (nDepth) {
				case CV_8U:
					dOutputImg.at<UINT8>(nRectImgPosY, nRectImgPosX) = dImg.at<UINT8>(nImgPosY, nImgPosX);
					break;
				case CV_8S:
					dOutputImg.at<INT8>(nRectImgPosY, nRectImgPosX) = dImg.at<INT8>(nImgPosY, nImgPosX);
					break;
				case CV_16U:
					dOutputImg.at<UINT16>(nRectImgPosY, nRectImgPosX) = dImg.at<UINT16>(nImgPosY, nImgPosX);
					break;
				case CV_16S:
					dOutputImg.at<INT16>(nRectImgPosY, nRectImgPosX) = dImg.at<INT16>(nImgPosY, nImgPosX);
					break;
				case CV_32S:
					dOutputImg.at<INT32>(nRectImgPosY, nRectImgPosX) = dImg.at<INT32>(nImgPosY, nImgPosX);
					break;
				case CV_32F:
					dOutputImg.at<FLOAT>(nRectImgPosY, nRectImgPosX) = dImg.at<FLOAT>(nImgPosY, nImgPosX);
					break;
				case CV_64F:
					dOutputImg.at<DOUBLE>(nRectImgPosY, nRectImgPosX) = dImg.at<DOUBLE>(nImgPosY, nImgPosX);
					break;
				}
			}
			else if (nChannels == 3 && nDepth == CV_8U) {
				dOutputImg.at<Vec3b>(nRectImgPosY, nRectImgPosX) = dImg.at<Vec3b>(nImgPosY, nImgPosX);
			}
		}
	}

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}

NIPL_CV_METHOD_IMPL(Rect2Circle)
{
	VERIFY_PARAMS();
	NIPLParam_Rect2Circle *pParam_Rect2Circle = (NIPLParam_Rect2Circle *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;

	int nOutputImgSizeX = pParam_Rect2Circle->m_nOutputImgSizeX;
	int nOutputImgSizeY = pParam_Rect2Circle->m_nOutputImgSizeY;
	int nCenterPosX = pParam_Rect2Circle->m_nCenterPosX;
	int nCenterPosY = pParam_Rect2Circle->m_nCenterPosY;
	int nMinRadius = pParam_Rect2Circle->m_nMinRadius;
	float nAngleStep = pParam_Rect2Circle->m_nAngleStep;

	int nRectImgSizeY = dImg.rows;
	int nRectImgSizeX = dImg.cols;
	int nMaxRadius = nMinRadius + nRectImgSizeY;

	if (nCenterPosX - nMaxRadius < 0 || nCenterPosX + nMaxRadius >= nOutputImgSizeX) {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}
	if (nCenterPosY - nMaxRadius < 0 || nCenterPosY + nMaxRadius >= nOutputImgSizeY) {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}

	Mat dOutputImg = Mat::zeros(nOutputImgSizeY, nOutputImgSizeX, dImg.type());

	int nDepth = dOutputImg.depth();
	int nChannels = dOutputImg.channels();

	for (int nRectImgPosX = 0; nRectImgPosX < nRectImgSizeX; nRectImgPosX++) {
		float nAngle = nAngleStep * nRectImgPosX;
		for (int nRectImgPosY = 0; nRectImgPosY < nRectImgSizeY; nRectImgPosY++) {
			int nRadius = nMinRadius + nRectImgPosY;

			int nImgPosX = cvRound(nCenterPosX + nRadius * cos(nAngle * CV_PI / 180));
			int nImgPosY = cvRound(nCenterPosY + nRadius * sin(nAngle * CV_PI / 180));

			if (nChannels == 1) {
				switch (nDepth) {
				case CV_8U:
					dOutputImg.at<UINT8>(nImgPosY, nImgPosX) = dImg.at<UINT8>(nRectImgPosY, nRectImgPosX);
					break;
				case CV_8S:
					dOutputImg.at<INT8>(nImgPosY, nImgPosX) = dImg.at<INT8>(nRectImgPosY, nRectImgPosX);
					break;
				case CV_16U:
					dOutputImg.at<UINT16>(nImgPosY, nImgPosX) = dImg.at<UINT16>(nRectImgPosY, nRectImgPosX);
					break;
				case CV_16S:
					dOutputImg.at<INT16>(nImgPosY, nImgPosX) = dImg.at<INT16>(nRectImgPosY, nRectImgPosX);
					break;
				case CV_32S:
					dOutputImg.at<INT32>(nImgPosY, nImgPosX) = dImg.at<INT32>(nRectImgPosY, nRectImgPosX);
					break;
				case CV_32F:
					dOutputImg.at<FLOAT>(nImgPosY, nImgPosX) = dImg.at<FLOAT>(nRectImgPosY, nRectImgPosX);
					break;
				case CV_64F:
					dOutputImg.at<DOUBLE>(nImgPosY, nImgPosX) = dImg.at<DOUBLE>(nRectImgPosY, nRectImgPosX);
					break;
				}
			}
			else if (nChannels == 3 && nDepth == CV_8U) {
				dOutputImg.at<Vec3b>(nImgPosY, nImgPosX) = dImg.at<Vec3b>(nRectImgPosY, nRectImgPosX);
			}
		}
	}

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}


NIPL_CV_METHOD_IMPL(AnalyzeShape)
{
	VERIFY_PARAMS();
	NIPLParam_AnalyzeShape *pParam_AnalyzeShape = (NIPLParam_AnalyzeShape *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	int nMethod = pParam_AnalyzeShape->m_nMethod;
	float nScratchLengthRatio = pParam_AnalyzeShape->m_nScratchLengthRatio;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	// Check the type of Image
	if(dImg.depth() != CV_8U || dImg.channels() != 1) {
		return NIPL_ERR_INVALID_IMAGE_TYPE;
	}

	if(nMethod == NIPLParam_AnalyzeShape::METHOD_NONE) {
		return NIPL_ERR_PASS;
	}

	Mat dOutputImg;
	if(nMethod & NIPLParam_AnalyzeShape::METHOD_SCRATCH) {
		if(nScratchLengthRatio > 0.f) {
			// Find all contours
			vector<vector<Point>> dContours;
			findContours(dImg, dContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

			Mat dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);

			RotatedRect dRotateRect;
			float nMinorAxisLength, nMajorAxisLength;
			for(int i = 0; i < (int)dContours.size(); i++) {
				dRotateRect = minAreaRect(dContours[i]);
				nMinorAxisLength = min(dRotateRect.size.width, dRotateRect.size.height);
				nMajorAxisLength = max(dRotateRect.size.width, dRotateRect.size.height);

				// Only draw the region less than the given ratio
				if(nMinorAxisLength/nMajorAxisLength <= nScratchLengthRatio) {
					drawContours(dOutputImg, dContours, i, CV_RGB(255,255,255), CV_FILLED);
				}
			}

			pOutput->m_dImg = dOutputImg;
		}
	}

	return NIPL_ERR_SUCCESS;
}

/*
NIPL_CV_METHOD_IMPL(CheckScrew)
{
	VERIFY_PARAMS();

	NIPLParam_DetectDefect *pParam_DetectDefect = (NIPLParam_DetectDefect *)pInput->m_pParam;

	NIPL_ERR nErr = NIPL_ERR_SUCCESS;
	if(CHECK_EMPTY_IMAGE(pInput->m_dImg)) {
		nErr = LoadImage(pInput->m_strImgPath, CV_LOAD_IMAGE_GRAYSCALE, pInput->m_dImg);
		if(NIPL_FAIL(nErr)) {
			return nErr;
		}
	}

	int nImgSizeY = pInput->m_dImg.rows;
	int nImgSizeX =pInput->m_dImg.cols;
*/

/*
	// temporary codes for finding circle of the screw
	Mat dInputImg, dOutputImg;
	pInput->m_dImg.convertTo(dInputImg, CV_8UC1);
	dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);

	//
	// Check if screws are tighten up or not.
	//
	NIPLParam_CheckScrew &dParam_CheckScrew = pParam_DetectDefect->m_dParam_CheckScrew;
	vector<Rect> &listTargetRegion = dParam_CheckScrew.m_listTargetRegion;
	for(int i = 0; i < (int)listTargetRegion.size(); i++) {
		Mat dRegion(dInputImg, listTargetRegion[i]);
		double nMinDist = min(dRegion.rows, dRegion.cols) * 0.2;

		vector<Vec3f> dCircles;
		HoughCircles(dRegion, dCircles, CV_HOUGH_GRADIENT, 1, nMinDist);

		Mat dOutputRegion(dOutputImg, listTargetRegion[i]);
		for(int i = 0; i<(int)dCircles.size(); i++) 	{
			Point ptCenter(cvRound(dCircles[i][0]), cvRound(dCircles[i][1]));
			int nRadius = cvRound(dCircles[i][2]);
			circle( dOutputRegion, ptCenter, nRadius, CV_RGB(255,255,255), CV_FILLED);
		}
	}
*/

/*
	// Convert type to double
	Mat dInputImg, dOutputImg;
	pInput->m_dImg.convertTo(dInputImg, CV_32FC1);
	dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);

	// Scaling to 0 ~ 1.
	double nMax;
	minMaxLoc(dInputImg, 0, &nMax);
	dInputImg = dInputImg / nMax;

	NIPLParam_CheckScrew &dParam_CheckScrew = pParam_DetectDefect->m_dParam_CheckScrew;
	vector<Rect> &listTargetRegion = dParam_CheckScrew.m_listTargetRegion;
	float nVarianceThreshold = dParam_CheckScrew.m_nVarianceThreahold;

	for(int i = 0; i < (int)listTargetRegion.size(); i++) {
		Mat dRegion(dInputImg, listTargetRegion[i]);
		Point ptCenter(cvRound(dRegion.rows/2), cvRound(dRegion.cols/2));
		int nRadius = cvRound(min(dRegion.rows, dRegion.cols) * 0.1);

		Mat dMask = Mat::zeros(dRegion.rows, dRegion.cols, CV_8UC1);
		circle(dMask, ptCenter, nRadius, CV_RGB(255,255,255), CV_FILLED);

		// 중심 영역의 점들의 표준 편차를 구한다.
		Scalar dMean;
		Scalar dStd;
		meanStdDev(dRegion, dMean, dStd, dMask);
		double nStd = dStd[0];

		if(nStd < nVarianceThreshold) {
			Mat dOutputRegion(dOutputImg, listTargetRegion[i]);
			nRadius = cvRound(min(dRegion.rows, dRegion.cols) * 0.3);
			circle(dOutputRegion, ptCenter, nRadius, CV_RGB(255,255,255), 3);

			int nOffset = cvRound(nRadius / sqrtf(2));
			Point dPoint1, dPoint2;
			dPoint1 = Point(ptCenter.x + nOffset, ptCenter.y + nOffset);
			dPoint2 = Point(ptCenter.x - nOffset, ptCenter.y - nOffset);
			line(dOutputRegion, dPoint1, dPoint2, CV_RGB(255,255,255), 4);

			dPoint1 = Point(ptCenter.x - nOffset, ptCenter.y + nOffset);
			dPoint2 = Point(ptCenter.x + nOffset, ptCenter.y - nOffset);
			line(dOutputRegion, dPoint1, dPoint2, CV_RGB(255,255,255), 4);
		}
	}

	pOutput->m_dImg = dOutputImg;
	return NIPL_ERR_SUCCESS;
}
*/

/*
NIPL_CV_METHOD_IMPL(CheckLogo)
{
	VERIFY_PARAMS();

	NIPLParam_DetectDefect *pParam_DetectDefect = (NIPLParam_DetectDefect *)pInput->m_pParam;

	NIPL_ERR nErr = NIPL_ERR_SUCCESS;
	if(CHECK_EMPTY_IMAGE(pInput->m_dImg)) {
		nErr = LoadImage(pInput->m_strImgPath, CV_LOAD_IMAGE_GRAYSCALE, pInput->m_dImg);
		if(NIPL_FAIL(nErr)) {
			return nErr;
		}
	}

	int nImgSizeY = pInput->m_dImg.rows;
	int nImgSizeX =pInput->m_dImg.cols;

	Mat dInputImg, dOutputImg, dTemplateImg;
	pInput->m_dImg.convertTo(dInputImg, CV_8UC1);
	pInput->m_dTemplateImg.convertTo(dTemplateImg, CV_8UC1);
	dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);

	NIPLParam_CheckLogo &dParam_CheckLogo = pParam_DetectDefect->m_dParam_CheckLogo;
	vector<Rect> &listTargetRegion = dParam_CheckLogo.m_listTargetRegion;
	float nSimilarityThreshold = dParam_CheckLogo.m_nSimilarityThreshold;
	BOOL bFlip = dParam_CheckLogo.m_bFlip;

	float nSimilarity = 0.f;
	Mat dTemplate;
	for(int i = 0; i < (int)listTargetRegion.size(); i++) {
		Mat dRegion(dInputImg, listTargetRegion[i]);
		Mat dOutputRegion(dOutputImg, listTargetRegion[i]);

		dTemplateImg.copyTo(dTemplate);
		if(bFlip) {
			flip(dTemplate, dTemplate, -1);
		}

		if(dRegion.rows > dRegion.cols) {
			transpose(dTemplate, dTemplate);
			flip(dTemplate, dTemplate, 0);
		}

		_CheckLogoSimilarity(dRegion, dTemplate, dOutputRegion, dParam_CheckLogo);
	}				

	pOutput->m_dImg = dOutputImg;

	return NIPL_ERR_SUCCESS;
}
*/

void NIPLCV::_CheckLogoSimilarity(Mat dInputImg, Mat dTemplate, Mat dOutputImg, NIPLParam_CheckLogo &dParam)
{
	float nMaxAngle = dParam.m_nMaxAngle;
	float nSimilarityThreshold = dParam.m_nSimilarityThreshold;

	Mat dImg;
	dInputImg.copyTo(dImg);

	Mat dBinTemplate, dTempBinTemplate;
	threshold(dTemplate, dBinTemplate, 0, 1, CV_THRESH_BINARY | CV_THRESH_OTSU);
	dBinTemplate.copyTo(dTempBinTemplate);

	Mat dBinImg, dTempBinImg;
	threshold(dImg, dBinImg, 0, 1, CV_THRESH_BINARY | CV_THRESH_OTSU);
	dBinImg.copyTo(dTempBinImg);

	// Find all contours
	vector<vector<Point>> dTemplateContours;
	findContours(dTempBinTemplate, dTemplateContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	vector<vector<Point>> dContours;
	findContours(dTempBinImg, dContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	//
	// Analyze Template Letters' Size and LengthRatio
	//
	int nTemplateCount = (int)dTemplateContours.size();
	Mat dTemplateLengthRatio(1, nTemplateCount, CV_32FC1);
	Mat dTemplateSize(1, nTemplateCount, CV_32FC1);
	Rect dRect;
	for(int i = 0; i < nTemplateCount; i++) {
		dRect = boundingRect(dTemplateContours[i]);
		dTemplateLengthRatio.at<FLOAT>(i) = (float)dRect.height/ dRect.width;
		dTemplateSize.at<FLOAT>(i) = (float)contourArea(dTemplateContours[i]);
	}

	double nTemplateMinSize;
	minMaxLoc(dTemplateSize, &nTemplateMinSize, 0);

	Scalar dMean;
	Scalar dStd;
	meanStdDev(dTemplateLengthRatio, dMean, dStd);
	double nTemplateMeanLenghRatio = dMean[0];
	double nTemplateStdLenghRatio = dStd[0];

	//
	// Remove non-letter blobs
	//
	RotatedRect dRotateRect;
	int nCount = 0;
	double nSize;
	float nLengthRatio;
	float nLengthRatioThreashold = 5.f;
	Mat dBinLettersImg = Mat::zeros(dBinImg.size(), dBinImg.type());
	for(int i = 0; i < (int)dContours.size(); i++) {
		dRotateRect = minAreaRect(dContours[i]);
		nSize = contourArea(dContours[i]);
		nLengthRatio = dRotateRect.size.height / dRotateRect.size.width;

		BOOL bLetter = TRUE;
		if(nSize < nTemplateMinSize * 0.9) {
			bLetter = FALSE;
		}
		else 	if((abs(nLengthRatio - nTemplateMeanLenghRatio) / nTemplateStdLenghRatio) > nLengthRatioThreashold) {
			bLetter = FALSE;
		}

		if(bLetter) {
			drawContours(dBinLettersImg, dContours, i, CV_RGB(255,255,255), CV_FILLED);
			nCount++;
		}
	}

	if(nCount < nTemplateCount) {
		dBinLettersImg.copyTo(dOutputImg);
		return;
	}

	//
	// Find if it's rotated or not.
	//
	Mat dBinMassImg;
	int nFilterSize = (int)(min(dTemplate.size().width, dTemplate.size().height) / 2.5);
	morphologyEx(dBinLettersImg, dBinMassImg, MORPH_CLOSE, Mat::ones(nFilterSize, nFilterSize, CV_8UC1));

	Mat dTempBinMassImg;
	dBinMassImg.copyTo(dTempBinMassImg);
	vector<vector<Point>> dMassContours;
	findContours(dTempBinMassImg, dMassContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	int nIndex = 0;
	double nMaxSize = 0;
	for(int i = 0; i < (int)dMassContours.size(); i++) {
		nSize = contourArea(dMassContours[i]);
		if(nSize >= nMaxSize) {
			nMaxSize = nSize;
			nIndex = i;
		}
	}

	Mat dBinRotatedLettersImg;
	dBinLettersImg.copyTo(dBinRotatedLettersImg);

	dRotateRect = minAreaRect(dMassContours[nIndex]);
	float nAngle = abs(dRotateRect.angle);
	if(nAngle > 45) {
		nAngle += 90;
	}

	if(nAngle > nMaxAngle && nAngle < (90-nMaxAngle)) {
		dBinLettersImg.copyTo(dOutputImg);
		return;
	}
	else if(nAngle > 0.01 && nAngle <  89.99) {
		Mat dTrans = getRotationMatrix2D(dRotateRect.center, dRotateRect.angle, 1);
		warpAffine(dBinRotatedLettersImg, dBinRotatedLettersImg, dTrans, dBinImg.size());
		warpAffine(dImg, dImg, dTrans, dImg.size());
	}

	//
	// Check similarity of each letters
	//
	nIndex = 0;
	double nSimilarity = 1;
	double nLetterSimilarity = 0;

	vector<vector<Point>> dContours2;
	findContours(dBinLettersImg, dContours2, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	findContours(dBinRotatedLettersImg, dContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	dOutputImg = Mat::zeros(dBinImg.size(), dBinImg.type());
	for(int i = 0; i < (int)dContours.size(); i++) {
		dRect = boundingRect(dContours[i]);
		if(dRect.width > dTemplate.size().width) {
			dRect.width = dTemplate.size().width;
		}
		if(dRect.height > dTemplate.size().height) {
			dRect.height = dTemplate.size().height;
		}
		Mat dLetter(dImg, dRect);

		Mat dResult;
		matchTemplate(dTemplate, dLetter, dResult, CV_TM_CCOEFF_NORMED);

		minMaxLoc(dResult, 0, &nLetterSimilarity);
//		TRACE("\nSimilarity (%d) : %.4f", i, nLetterSimilarity);

		if(nLetterSimilarity < nSimilarity) {
			nSimilarity = nLetterSimilarity;
		}

		if(nLetterSimilarity < nSimilarityThreshold) {
			drawContours(dOutputImg, dContours2, i, CV_RGB(255,255,255), CV_FILLED);
		}
	}
}

NIPL_CV_METHOD_IMPL(FitToCorrectCircle)
{
	VERIFY_PARAMS();
	NIPLParam_FitToCorrectCircle *pParam_FitToCorrectCircle = (NIPLParam_FitToCorrectCircle *)pInput->m_pParam;

	Mat dImg = pInput->m_dImg;
	NIPLParam_FindCircle *pParam_FindCircle = &(pParam_FitToCorrectCircle->m_dParam_FindCircle);

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	// Check the depth of Image
	if (dImg.depth() != CV_8U) {
		return NIPL_ERR_INVALID_IMAGE_TYPE;
	}

	NIPLInput dInput;
	NIPLOutput dOutput;
	Mat dOutputImg;
	// Thresholding
	NIPLParam_Thresholding dParam_Thresholding;

	dInput.m_dImg = dImg;
	dParam_Thresholding.m_nMethod = NIPLParam_Thresholding::METHOD_UPPER_OTSU;
	dInput.m_pParam = &dParam_Thresholding;
	NIPL_ERR nErr = Thresholding(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}

	dOutputImg = dOutput.m_dImg;

	// Edge Detection
	NIPLParam_EdgeDetecting dParam_EdgeDetecting;

	dInput.m_dImg = dOutputImg;
	dParam_EdgeDetecting.m_nMethod = NIPLParam_EdgeDetecting::METHOD_CANNY;
	dParam_EdgeDetecting.m_nFilterSize = 3;
	dInput.m_pParam = &dParam_EdgeDetecting;
	nErr = EdgeDetecting(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	dOutputImg = dOutput.m_dImg;

	// Set Circle ROI
	NIPLParam_SetCircleROI dParam_SetCircleROI;

	dInput.m_dImg = dOutputImg;
	dParam_SetCircleROI.m_nCenterPosX = pParam_FindCircle->m_nCenterPosX;
	dParam_SetCircleROI.m_nCenterPosY = pParam_FindCircle->m_nCenterPosY;
	dParam_SetCircleROI.m_nMinRadius = pParam_FindCircle->m_nMinRadius;
	dParam_SetCircleROI.m_nMaxRadius = pParam_FindCircle->m_nMaxRadius;
	dInput.m_pParam = &dParam_SetCircleROI;
	nErr = SetCircleROI(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	Mat dCircleROIImg = dOutput.m_dImg;

	// Find Circle
	dInput.m_dImg = dCircleROIImg;
	dInput.m_pParam = pParam_FindCircle;
	nErr = FindCircle(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return nErr;
	}
	
	dOutputImg = dOutput.m_dImg;
	if (dOutput.m_pResult == nullptr) {
		return NIPL_ERR_INVALID_PARAM_VALUE;
	}

	NIPLResult_FindCircle *pResult = (NIPLResult_FindCircle *)dOutput.m_pResult;
	int nFoundCircleCenterPosX = pResult->m_listCircle[0].m_ptCenter.x;
	int nFoundCircleCenterPosY = pResult->m_listCircle[0].m_ptCenter.y;
	int nFoundCircleRadius = pResult->m_listCircle[0].m_nRadius;

	int nMinPosX = 0;
	int nMaxPosX = nImgSizeX - 1;
	int nMinPosY = 0;
	int nMaxPosY = nImgSizeY - 1;
	for (int nX = nFoundCircleCenterPosX; nX >= 0 ; nX--) {
		if (dCircleROIImg.at<UINT8>(nFoundCircleCenterPosY, nX) != 0) {
			nMinPosX = nX;
			break;
		}
	}
	for (int nX = nFoundCircleCenterPosX; nX < nImgSizeX; nX++) {
		if (dCircleROIImg.at<UINT8>(nFoundCircleCenterPosY, nX) != 0) {
			nMaxPosX = nX;
			break;
		}
	}
	for (int nY = nFoundCircleCenterPosY; nY >= 0; nY--) {
		if (dCircleROIImg.at<UINT8>(nY, nFoundCircleCenterPosX) != 0) {
			nMinPosY = nY;
			break;
		}
	}
	for (int nY = nFoundCircleCenterPosY; nY < nImgSizeY; nY++) {
		if (dCircleROIImg.at<UINT8>(nY, nFoundCircleCenterPosX) != 0) {
			nMaxPosY = nY;
			break;
		}
	}

	float nMinPosXRatio = (float)(nFoundCircleCenterPosX - nMinPosX) / nFoundCircleRadius;
	float nMaxPosXRatio = (float)(nMaxPosX - nFoundCircleCenterPosX) / nFoundCircleRadius;
	float nMinPosYRatio = (float)(nFoundCircleCenterPosY - nMinPosY) / nFoundCircleRadius;
	float nMaxPosYRatio = (float)(nMaxPosY - nFoundCircleCenterPosY) / nFoundCircleRadius;

	dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, dImg.type());

	int nChannels = dOutputImg.channels();

	int nImgPosX;
	int nImgPosY;
	for (int nPosY = 0; nPosY < nImgSizeY; nPosY++) {
		for (int nPosX = 0; nPosX < nImgSizeX; nPosX++) {
			if (nPosX <= nFoundCircleCenterPosX) {
				nImgPosX = (int)(nFoundCircleCenterPosX - (nFoundCircleCenterPosX - nPosX) * nMinPosXRatio);
				if (nImgPosX < 0) nImgPosX = 0;
			}
			else {
				nImgPosX = (int)(nFoundCircleCenterPosX + (nPosX - nFoundCircleCenterPosX) * nMaxPosXRatio);
				if (nImgPosX >= nImgSizeX) nImgPosX = nImgSizeX - 1;
			}
			if (nPosY <= nFoundCircleCenterPosY) {
				nImgPosY = (int)(nFoundCircleCenterPosY - (nFoundCircleCenterPosY - nPosY) * nMinPosYRatio);
				if (nImgPosY < 0) nImgPosY = 0;
			}
			else {
				nImgPosY = (int)(nFoundCircleCenterPosY + (nPosY - nFoundCircleCenterPosY) * nMaxPosYRatio);
				if (nImgPosY >= nImgSizeY) nImgPosY = nImgSizeY - 1;
			}

			if (nChannels == 1) {
				dOutputImg.at<UINT8>(nPosY, nPosX) = dImg.at<UINT8>(nImgPosY, nImgPosX);
			}
			else if (nChannels == 3) {
				dOutputImg.at<Vec3b>(nPosY, nPosX) = dImg.at<Vec3b>(nImgPosY, nImgPosX);
			}
		}
	}

	pOutput->m_dImg = dOutput.m_dImg;
	pOutput->m_pResult = dOutput.m_pResult;

	return NIPL_ERR_SUCCESS;
}
