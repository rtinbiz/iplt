#pragma once

#include "NIPLParam.h"

struct AFX_EXT_CLASS NIPLParam_Binarize : public NIPLParam
{
	enum {
		METHOD_NONE = 0x00,
		METHOD_UPPER = 0x01,
		METHOD_LOWER = 0x02,
		METHOD_BOTH = METHOD_UPPER | METHOD_LOWER 
	};

	int m_nMethod;
	float m_nThreshold;

	NIPLParam_Binarize() {
		Init();
	}
	virtual ~NIPLParam_Binarize() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nMethod = METHOD_NONE;
		m_nThreshold = 0.f;
	}

	bool SetMethod(wstring strMethod) {
		m_nMethod = METHOD_NONE;
		if (strMethod == L"METHOD_UPPER") m_nMethod = METHOD_UPPER;
		else if (strMethod == L"METHOD_LOWER") m_nMethod = METHOD_LOWER;
		else if (strMethod == L"METHOD_BOTH") m_nMethod = METHOD_BOTH;
		else {
			return false;
		}

		return true;
	}
};

struct AFX_EXT_CLASS NIPLParam_FitBackground  : public NIPLParam
{
	int m_nBlockCount;
	int m_nDegree;
	float m_nThreshold;
	bool m_bSubtrackFromImage;

	NIPLParam_FitBackground() {
		Init();
	}
	virtual ~NIPLParam_FitBackground() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nBlockCount = 0;
		m_nDegree = 0;
		m_nThreshold = 0.f;
		m_bSubtrackFromImage = false;
	}
};

struct AFX_EXT_CLASS NIPLParam_FindLine : public NIPLParam
{
	int m_nHoughThreshold;
	int m_nLineMinLength;
	int m_nLineMaxLength;
	int m_nLineMinGap;
	int m_nLineThickness;
	float m_nCannyLowerThreshold;
	float m_nCannyUpperThreshold;

	NIPLParam_FindLine() {
		Init();
	}
	virtual ~NIPLParam_FindLine() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nHoughThreshold = 0;
		m_nLineThickness = 0;
		m_nLineMinLength = 0;
		m_nLineMaxLength = 0;
		m_nLineMinGap = 0;
		m_nCannyLowerThreshold = 0.f;
		m_nCannyUpperThreshold = 0.f;
	}
};

struct AFX_EXT_CLASS NIPLParam_FindCircle: public NIPLParam
{
	float m_nMinCircleDistance;
	float m_nCannyUpperThreshold;
	float m_nCenterDetectionThreshold;
	int m_nMinRadius;
	int m_nMaxRadius;
	int m_nLineThickness;

	bool m_bCheckCenterPos;
	int m_nCenterPosX;
	int m_nCenterPosY;
	int m_nCenterPosRange;

	NIPLParam_FindCircle() {
		Init();
	}
	virtual ~NIPLParam_FindCircle() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nMinCircleDistance = 0.f;
		m_nCannyUpperThreshold = 0.f;
		m_nCenterDetectionThreshold = 0.f;
		m_nMinRadius = 0;
		m_nMaxRadius = 0;
		m_nLineThickness = 0;

		m_bCheckCenterPos = false;
		m_nCenterPosX = 0;
		m_nCenterPosX = 0;
		m_nCenterPosRange = 0;
	}
};

struct AFX_EXT_CLASS NIPLParam_SetCircleROI : public NIPLParam
{
	int m_nCenterPosX;
	int m_nCenterPosY;
	int m_nMinRadius;
	int m_nMaxRadius;

	NIPLParam_SetCircleROI() {
		Init();
	}
	virtual ~NIPLParam_SetCircleROI() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		int m_nCenterPosX = 0;
		int m_nCenterPosY = 0;
		int m_nMinRadius = 0;
		int m_nMaxRadius = 0;
	}
};

struct AFX_EXT_CLASS NIPLParam_Circle2Rect : public NIPLParam
{
	int m_nCenterPosX;
	int m_nCenterPosY;
	int m_nMinRadius;
	int m_nMaxRadius;
	float m_nAngleStep;

	NIPLParam_Circle2Rect() {
		Init();
	}
	virtual ~NIPLParam_Circle2Rect() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		int m_nCenterPosX = 0;
		int m_nCenterPosY = 0;
		int m_nMinRadius = 0;
		int m_nMaxRadius = 0;
		float m_nAngleStep = 0.f;
	}
};

struct AFX_EXT_CLASS NIPLParam_Rect2Circle : public NIPLParam
{
	int m_nOutputImgSizeX;
	int m_nOutputImgSizeY;
	int m_nCenterPosX;
	int m_nCenterPosY;
	int m_nMinRadius;
	float m_nAngleStep;

	NIPLParam_Rect2Circle() {
		Init();
	}
	virtual ~NIPLParam_Rect2Circle() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nOutputImgSizeX = 0;
		m_nOutputImgSizeY = 0;
		m_nCenterPosX = 0;
		m_nCenterPosY = 0;
		m_nMinRadius = 0;
		m_nAngleStep = 0.f;
	}
};

struct AFX_EXT_CLASS NIPLParam_EliminateNoise: public NIPLParam
{
	enum {
		METHOD_NONE = 0x00,
		METHOD_MORPHOLOGY_OPS = 0x01,
		METHOD_MEDIAN_FILTER = 0x02,
		METHOD_SIZE = 0x04,
	};

	int m_nMethod;
	int m_nMorphologyFilterSize;
	int m_nMedianFilterSize;
	int m_nMinSize;
	int m_nMaxSize;

	NIPLParam_EliminateNoise() {
		Init();
	}
	virtual ~NIPLParam_EliminateNoise() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nMethod = METHOD_NONE;
		m_nMorphologyFilterSize = 0;
		m_nMedianFilterSize = 0;
		m_nMinSize = 0;
		m_nMaxSize = 0;
	}

	bool SetMethod(wstring strMethod) {
		m_nMethod = METHOD_NONE;
		if (strMethod == L"METHOD_MORPHOLOGY_OPS") m_nMethod = METHOD_MORPHOLOGY_OPS;
		else if (strMethod == L"METHOD_MEDIAN_FILTER") m_nMethod = METHOD_MEDIAN_FILTER;
		else if (strMethod == L"METHOD_SIZE") m_nMethod = METHOD_SIZE;
		else {
			return false;
		}

		return true;
	}
};

struct AFX_EXT_CLASS NIPLParam_AnalyzeShape: public NIPLParam
{
	enum {
		METHOD_NONE = 0x00,
		METHOD_SCRATCH = 0x01
	};

	int m_nMethod;
	float m_nScratchLengthRatio;

	NIPLParam_AnalyzeShape() {
		Init();
	}
	virtual ~NIPLParam_AnalyzeShape() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nMethod = METHOD_NONE;
		m_nScratchLengthRatio = 0.f;
	}

	bool SetMethod(wstring strMethod) {
		m_nMethod = METHOD_NONE;
		if (strMethod == L"METHOD_SCRATCH") m_nMethod = METHOD_SCRATCH;
		else {
			return false;
		}

		return true;
	}
};

struct AFX_EXT_CLASS NIPLParam_ExcludeRegion: public NIPLParam {
	vector<Rect> m_listExcludeRegion;

	NIPLParam_ExcludeRegion() {
		Init();
	}
	virtual ~NIPLParam_ExcludeRegion() {
		Clear();
	}
	virtual void Clear() {
		m_listExcludeRegion.clear();
		Init();
	}

	void Init() {
	}

	void AddExcludeRegion(int nStartX, int nStartY, int nEndX, int nEndY) {
		m_listExcludeRegion.push_back(Rect(nStartX, nStartY, nEndX-nStartX, nEndY-nStartY));
	}
};

struct AFX_EXT_CLASS NIPLParam_CheckScrew : public NIPLParam
{
	vector<Rect> m_listTargetRegion;
	float m_nVarianceThreahold;

	NIPLParam_CheckScrew() {
		Init();
	}
	virtual ~NIPLParam_CheckScrew() {
		Clear();
	}
	virtual void Clear() {
		m_listTargetRegion.clear();
		Init();
	}

	void Init() {
		m_nVarianceThreahold = 0.f;
	}

	void AddTargetRegion(int nStartX, int nStartY, int nEndX, int nEndY) {
		m_listTargetRegion.push_back(Rect(nStartX, nStartY, nEndX-nStartX, nEndY-nStartY));
	}
};

struct AFX_EXT_CLASS NIPLParam_CheckLogo : public NIPLParam
{
	vector<Rect> m_listTargetRegion;
	float m_nMaxAngle;
	float m_nSimilarityThreshold;
	BOOL m_bFlip;

	NIPLParam_CheckLogo() {
		Init();
	}
	virtual ~NIPLParam_CheckLogo() {
		Clear();
	}
	virtual void Clear() {
		m_listTargetRegion.clear();
		Init();
	}

	void Init() {
		m_nMaxAngle = 0.f;
		m_nSimilarityThreshold = 0.f;
		m_bFlip = FALSE;
	}

	void AddTargetRegion(int nStartX, int nStartY, int nEndX, int nEndY) {
		m_listTargetRegion.push_back(Rect(nStartX, nStartY, nEndX-nStartX, nEndY-nStartY));
	}
};

/*
struct AFX_EXT_CLASS NIPLParam_DetectDefect : public NIPLParam
{
	NIPLParam_Smoothing m_dParam_Smoothing;
	NIPLParam_Binarize m_dParam_Binarize;
	NIPLParam_FindLine m_dParam_FindLine;
	NIPLParam_FitBackground m_dParam_FitBackground;
	NIPLParam_EliminateNoise m_dParam_EliminateNoise;
	NIPLParam_ExcludeRegion m_dParam_ExcludeRegion;
	NIPLParam_AnalyzeShape m_dParam_AnalyzeShape;

	NIPLParam_CheckScrew m_dParam_CheckScrew;
	NIPLParam_CheckLogo m_dParam_CheckLogo;

	NIPLParam_DetectDefect() {
		Init();
	}
	virtual ~NIPLParam_DetectDefect() {
		Clear();
	}
	virtual void Clear() {
		m_dParam_Smoothing.Clear();
		m_dParam_Binarize.Clear();
		m_dParam_FindLine.Clear();
		m_dParam_FitBackground.Clear();
		m_dParam_EliminateNoise.Clear();
		m_dParam_ExcludeRegion.Clear();
		m_dParam_AnalyzeShape.Clear();

		m_dParam_CheckScrew.Clear();
		m_dParam_CheckLogo.Clear();

		Init();
	}


	void Init() {
		m_dParam_Smoothing.Init();
		m_dParam_Binarize.Init();
		m_dParam_FindLine.Init();
		m_dParam_FitBackground.Init();
		m_dParam_EliminateNoise.Init();
		m_dParam_ExcludeRegion.Init();
		m_dParam_AnalyzeShape.Init();

		m_dParam_CheckScrew.Init();
		m_dParam_CheckLogo.Init();
	}
};
*/

struct AFX_EXT_CLASS NIPLParam_FindBlob : public NIPLParam
{
	float m_nMinSize;
	float m_nMaxSize;
	bool m_bFindInRange;

	NIPLParam_FindBlob() {
		Init();
	}
	virtual ~NIPLParam_FindBlob() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nMinSize = 0.f;
		m_nMaxSize = 0.f;
		m_bFindInRange = FALSE;
	}
};

struct AFX_EXT_CLASS NIPLParam_FitToCorrectCircle : public NIPLParam
{
	NIPLParam_FindCircle m_dParam_FindCircle;

	NIPLParam_FitToCorrectCircle() {
		Init();
	}
	virtual ~NIPLParam_FitToCorrectCircle() {
		Clear();
	}
	virtual void Clear() {
		m_dParam_FindCircle.Clear();

		Init();
	}

	void Init() {
		m_dParam_FindCircle.Init();
	}
};
