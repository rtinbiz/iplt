#pragma once

#include "NIPL.h"
#include "NIPLCVParam.h"
#include "NIPLCVResult.h"

#define NIPL_CV_METHOD_IMPL(FuncName) NIPL_ERR NIPLCV::FuncName(NIPLInput *pInput, NIPLOutput *pOutput)

class AFX_EXT_CLASS NIPLCV : public NIPL {
public :
	static NIPLCV *pThis;
	static NIPLCV *GetInstance(BOOL bNew = FALSE);
	static void ReleaseInstance(NIPLCV *pThat = 0x00);

	NIPLCV();
	~NIPLCV();

//	NIPL_METHOD_DECL(DetectDefect);
//	NIPL_METHOD_DECL(ApplyExcludingRegionToMask);
	NIPL_METHOD_DECL(DetectFixture);
	NIPL_METHOD_DECL(FindLine);
	NIPL_METHOD_DECL(FindCircle);
	NIPL_METHOD_DECL(SetCircleROI);
	NIPL_METHOD_DECL(Circle2Rect);
	NIPL_METHOD_DECL(Rect2Circle);
	NIPL_METHOD_DECL(FitBackground);
	NIPL_METHOD_DECL(Binarize);
	NIPL_METHOD_DECL(EliminateNoise);
	NIPL_METHOD_DECL(FindBlob);
	NIPL_METHOD_DECL(AnalyzeShape);
	NIPL_METHOD_DECL(FitToCorrectCircle);
//	NIPL_METHOD_DECL(CheckScrew);
//	NIPL_METHOD_DECL(CheckLogo);

private : 
	void _CheckLogoSimilarity(Mat dInputImg, Mat dTemplate, Mat dOutputImg, NIPLParam_CheckLogo &dParam);
};