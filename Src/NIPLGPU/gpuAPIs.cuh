#pragma once

#ifdef _DEBUG
#pragma comment (lib, "../Lib/CUDA64/cudart.lib")
#else
#pragma comment (lib, "../Lib/CUDA64/cudart_static.lib")
#endif

#include "opencv2/opencv.hpp"
using namespace cv;

#include "cuda_runtime.h"

#define GPU_BLOCK_SIZE 32

void gpuSmoothing(Mat dImg, Mat dOutputImg, int nFilterSize);
void gpuSmoothingByTexture(Mat dImg, Mat dOutputImg, int nFilterSize);
