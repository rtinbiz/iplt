#include "gpuAPIs.cuh"

texture<uchar, cudaTextureType2D> texImg;
surface<void, cudaSurfaceType2D> surfOutputImg;

__global__ 
void kernelSmoothing(uchar *pData, uchar *pOutputData, int nSizeX, int nSizeY, int nStep, int nFilterSize)
{
	const int nX = blockIdx.x*blockDim.x + threadIdx.x;
	const int nY = blockIdx.y*blockDim.y + threadIdx.y;
	if (nX >= nSizeX || nY >= nSizeY) {
		return;
	}

	int nX2, nY2;
	int nTotalCount = 0;
	int nValue;
	int nTotalValue = 0;
	int nHalfFilter = nFilterSize / 2;
	for (int i = -nHalfFilter; i <= nHalfFilter; i++) {
		nY2 = nY + i;
		if (nY2 < 0 || nY2 >= nSizeY) {
			continue;
		}

		for (int j = -nHalfFilter; j <= nHalfFilter; j++) {
			nX2 = nX + j;
			if (nX2 < 0 || nX2 >= nSizeX) {
				continue;
			}

			nValue = (int)(pData[nY2*nStep + nX2]);
			nTotalValue += nValue;
			nTotalCount++;
		}
	}

	if (nTotalCount == 0) {
		nValue = pData[nY*nStep + nX];
	}
	else {
		nValue = nTotalValue / nTotalCount;
	}

	pOutputData[nY*nStep + nX] = (uchar)nValue;
}

__global__
void kernelSmoothingByTexture(int nSizeX, int nSizeY, int nFilterSize)
{
	const int nX = blockIdx.x * blockDim.x + threadIdx.x;
	const int nY = blockIdx.y * blockDim.y + threadIdx.y;
	if (nX < 0 || nY <= 0 || nX >= nSizeX || nY >= nSizeY) {
		return;
	}

	int nX2, nY2;
	int nTotalCount = 0;
	double nTotalValue = 0;
	int nHalfFilter = nFilterSize / 2;
	for (int i = -nHalfFilter; i <= nHalfFilter; i++) {
		nY2 = nY + i;
		if (nY2 < 0 || nY2 >= nSizeY) {
			continue;
		}

		for (int j = -nHalfFilter; j <= nHalfFilter; j++) {
			nX2 = nX + j;
			if (nX2 < 0 || nX2 >= nSizeX) {
				continue;
			}

			nTotalValue += tex2D(texImg, nX2, nY2);
			nTotalCount++;
		}
	}

	uchar nValue;
	if (nTotalCount == 0) {
		nValue = tex2D(texImg, nX, nY);
	}
	else {
		nValue = (uchar)(nTotalValue / nTotalCount);
	}

	surf2Dwrite(nValue, surfOutputImg, nX, nY);
}

void gpuSmoothing(Mat dImg, Mat dOutputImg, int nFilterSize)
{
//	gpuSmoothingByTexture(dImg, dOutputImg, nFilterSize);
//	return;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;
	int nImgStep = dImg.step1();

	uchar *pData = dImg.data;
	uchar *pOutputData = dOutputImg.data;

	uchar *pDevData;
	uchar *pDevOutputData;
	cudaMalloc((void**)&pDevData, nImgSizeX*nImgSizeY * sizeof(uchar));
	cudaMalloc((void**)&pDevOutputData, nImgSizeX*nImgSizeY * sizeof(uchar));

	cudaMemcpy(pDevData, pData, nImgSizeX*nImgSizeY * sizeof(uchar), cudaMemcpyHostToDevice);

	dim3 dBlockDims(GPU_BLOCK_SIZE, GPU_BLOCK_SIZE);
	dim3 dGridDims(cvCeil((double)nImgSizeX / GPU_BLOCK_SIZE), cvCeil((double)nImgSizeY / GPU_BLOCK_SIZE));
	kernelSmoothing <<< dGridDims, dBlockDims >>> (pDevData, pDevOutputData, nImgSizeX, nImgSizeY, nImgStep, nFilterSize);

	cudaThreadSynchronize();

	cudaMemcpy(pOutputData, pDevOutputData, nImgSizeX*nImgSizeY * sizeof(uchar), cudaMemcpyDeviceToHost);

	cudaFree(pDevData);
	cudaFree(pDevOutputData);
}

void gpuSmoothingByTexture(Mat dImg, Mat dOutputImg, int nFilterSize)
{
	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;
	int nImgStep = dImg.step1();

	uchar *pData = dImg.data;
	uchar *pOutputData = dOutputImg.data;

	cudaArray *arrImg;
	cudaChannelFormatDesc dDesc = cudaCreateChannelDesc<uchar>();
	cudaMallocArray(&arrImg, &dDesc, nImgSizeX, nImgSizeY);
	cudaMemcpyToArray(arrImg, 0, 0, pData, nImgSizeX * nImgSizeY, cudaMemcpyHostToDevice);
	cudaBindTextureToArray(texImg, arrImg, dDesc);	// binding

	cudaArray *arrOutputImg;
	cudaMallocArray(&arrOutputImg, &dDesc, nImgSizeX, nImgSizeY, cudaArraySurfaceLoadStore);
	cudaBindSurfaceToArray(surfOutputImg, arrOutputImg);

	dim3 dBlockDims(GPU_BLOCK_SIZE, GPU_BLOCK_SIZE);
	dim3 dGridDims(cvCeil((double)nImgSizeX / GPU_BLOCK_SIZE), cvCeil((double)nImgSizeY / GPU_BLOCK_SIZE));
	kernelSmoothingByTexture <<< dGridDims, dBlockDims >>> (nImgSizeX, nImgSizeY, nFilterSize);

	cudaThreadSynchronize();

	cudaMemcpyFromArray(pOutputData, arrOutputImg, 0, 0, nImgSizeX * nImgSizeY, cudaMemcpyDeviceToHost);

	cudaUnbindTexture(texImg);			// unbinding
	cudaFreeArray(arrImg);
	cudaFreeArray(arrOutputImg);
}

