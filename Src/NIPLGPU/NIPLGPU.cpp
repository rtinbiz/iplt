// NIPLGPU.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "NIPLGPU.h"
#include "gpuAPIs.cuh"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

NIPLGPU *NIPLGPU::pThis = 0x00;

NIPLGPU *NIPLGPU::GetInstance(BOOL bNew)
{
	if(bNew) {
		return new NIPLGPU();
	}

	if(pThis == 0x00) {
		pThis = new NIPLGPU();
	}

	return pThis;
}

void NIPLGPU::ReleaseInstance(NIPLGPU *pThat)
{
	if(pThat != 0x00) {
		delete pThat;
	}
	else if(pThis != 0x00) {
		delete pThis;
		pThis = 0x00;
	}
}

NIPLGPU::NIPLGPU() : NIPL()
{
}

NIPLGPU::~NIPLGPU()
{
}

NIPL_ERR  NIPLGPU::Smoothing(NIPLInput *pInput, NIPLOutput *pOutput, NIPLParam *pParam)
{
	VERIFY_PARAMS();
	NIPLParam_Smoothing *pParam_Smoothing = (NIPLParam_Smoothing *)pParam;

	Mat dImg = pInput->m_dImg;
	int nMethod = pParam_Smoothing->m_nMethod;
	int nFilterSize = pParam_Smoothing->m_nFilterSize;

	int nImgSizeY = dImg.rows;
	int nImgSizeX = dImg.cols;

	Mat dOutputImg;
	if (nMethod & NIPLParam_Smoothing::METHOD_FILTER) {
		if (nMethod > 0) {
			dOutputImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
			gpuSmoothing(dImg, dOutputImg, nFilterSize);
			pOutput->m_dImg = dOutputImg;
		}
	}

	if (CHECK_EMPTY_IMAGE(pOutput->m_dImg)) {
		return NIPL_ERR_PASS;
	}

	return NIPL_ERR_SUCCESS;
}
